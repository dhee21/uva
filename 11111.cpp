#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <stack>
#include <utility>
using namespace std;
typedef pair<int,int> ii;

bool check(int *arr, int length) {
	stack<ii > st;
		for(int i = 0 ; i< length ;++i) {
				if(arr[i] < 0 ) {
					st.push(make_pair(arr[i], 0));
				} else {
					if(!st.empty() && st.top().first == -arr[i]) {
						st.pop();
						if(st.empty()) {
							return true;
						} else {
							st.top().second += arr[i];
							if(st.top().second >= -st.top().first) {
								return false;
							}
						}
					} else {
						return false;
					}
				}
		}
		return (length)?  false :  true;
		
}

int main() {
 char ip[100000], *ptr=NULL;
 int arr[100000], ans=1;
 	while(gets(ip)) {
 		int i=0;
 		ptr = strtok(ip, " ");
 		while(ptr) {
 			arr[i++] = atoi(ptr);
 			ptr = strtok(NULL, " ");
 		}
		ans = check(arr, i);
		if(ans) {
			printf(":-) Matrioshka!\n");
		}else {
			printf(":-( Try again.\n");
		}

 	}
}