# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
#define MAX_NODES 1000
#define inf 1 << 25
#define fr(i, st, end) for (int i = st; i < end; ++i)

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
map<pii, int> indexOf;
matrix adjMat(6);

// SSSP djakstra variables starts
int dist[MAX_NODES + 100], t[6], nodeIndex;
// pii nodes[MAX_NODES +100];
priority_queue<pii> pq;
vvi liftFloors(6);
pii sourceNode = pii(-1, -1);
// SSSP djakstra variables ends

void initialize () {
	memset(dist, inf, sizeof dist);
	while (!pq.empty()) pq.pop();
	fr(i, 0, 6) {
		adjMat[i].clear();
	}
	nodeIndex = 0;
	indexOf.clear();
	liftFloors.clear();
}

void SSSP (int source) {
	pq.push(pii(0, -source));
	dist[source] = 0;
	while (!pq.empty()) {
		pii minUpperbound = pq.top(); pq.pop();
		int node = -minUpperbound.second, minDist = -minUpperbound.first;
		if (dist[node] == minDist) {
			int neighbours = adjMat[node].size();
			// Relaxing dist of neighbours
			for (int i = 0 ; i < neighbours; ++i) {
				int edgeToNode = adjMat[node][i].first, edgeWeight = adjMat[node][i].second;
				int newDist = minDist + edgeWeight;
				if (newDist < dist[edgeToNode]) {
					dist[edgeToNode] = newDist;
					pq.push(pii(-newDist, -edgeToNode));
				}
			}
		}
	}
}

void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}

void makeNodeAndEdges (int liftNum, int index) {
	int floorNum = liftFloors[liftNum][index];
	pii node = pii(liftNum, floorNum);
	// nodes[nodeIndex] = node;
	indexOf[node] = nodeIndex++;
	if (index == 0) {
		int from = indexOf[sourceNode], to = indexOf[node], weight = 0;
		makeEdge(from, to, weight);
	} else {
		int prevFloor = liftFloors[liftNum][index - 1];
		int previousNode = pii(liftNumber, prevFloor);
		int from = indexOf[previousNode], to = indexOf[node], weight = (floorNum - prevFloor) * t[liftNum] ;
		makeEdge(from, to, weight);
	}
	for (int l = liftNum - 1; l >= 0; --l) {
		pii parallelNode = pii(l, floorNum);
		if (indexOf.count(parallelNode)) {
			int from = indexOf[parallelNode], to = indexOf[node], weight = 60;
			makeEdge(from, to, weight);
		}
	}
}

int main () {
	int n, k;
	while (scanf("%d %d", &n, &k)) {
		initialize();
		fr(i, 0, n) {
			scanf("%d", &t[i]);
		}
		scanf("\n");
		char inp[1000];
		fr(i, 0, n) {
			fgets(inp, 1000, stdin);
			char *token;
			token = strtok(inp, " ");
			while (token != NULL) {
				int flr = atoi(token);
				liftFloors[i].push_back(flr);
				token = strtok(NULL, " ");
			}
		}
		// nodes[nodeIndex] = sourceNode;
		indexOf[sourceNode] = nodeIndex++;
		fr(i, 0, n) {
			fr(j, 0, liftFloors[i].size()) {
				makeNodeAndEdges(i, j);
			}
		}
		SSSP(0);
		int ans = inf;
		fr(i, 0, n) {
			pii finalNode = pii(i, 99);
			if (indexOf.count(finalNode)) {
				ans = min (ans, dist[indexOf[finalNode]]);
			}
		}
		printf("%d sec\n", ans );
	}
	return 0;
}