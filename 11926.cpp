#include <stdio.h>
#include <iostream>
#include <bitset>
using namespace std;
#define maxSize 1000000
bitset<1000006> slots;
bool setTimeSlots (int st, int ed) {
		for (int i = st; (i < ed && i<=maxSize) ; ++i) {
			if(!slots[i]) {
				slots.set(i);
			} else {
				return true;
			}
		}
		return false;
}
bool setRepInterval(int st ,int ed, int interval) {
	int intStart=st, length = ed-st,conflict=0;
	for ( intStart ; intStart<=maxSize ; intStart+=interval) {
		conflict = setTimeSlots(intStart, intStart+length);
		if(conflict) {
			return true;
		}
	}
	return false;
}


int main () {
	int n,m, st, ed, conflict, repInterval;
	while (scanf("%d %d", &n,&m) && (n || m)) {
		conflict = 0;
		slots.reset();
		while(n--) {
			scanf("%d %d", &st, &ed);
			if(!conflict) {
				conflict = setTimeSlots(st, ed);
			}
		}
		while (m--) {
			scanf("%d %d %d", &st, &ed, &repInterval);
			if(!conflict) {
				conflict = setRepInterval(st, ed, repInterval) ;
			}
		}
		conflict? printf("CONFLICT\n") : printf("NO CONFLICT\n");
	}
}