# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<29)
# define EPS 1e-9
#define PI  3.141592653589793
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
#define earthRad 6371009
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

bool isCommon (pii &range1, pii range2) {
	if (range2.first >= range1.second || range2.second <= range1.first) return false;
	range1.first = max (range1.first, range2.first); range1.second = min (range2.second, range1.second);
	return true;
}

int main () {
	int n;
	while (ipInt(n) && n) {
		int x, y, z, len;
		scanf("%d %d %d %d", &x, &y, &z, &len);
		pii xRange = pii(x, x + len), yRange = pii(y, y + len), zRange = pii(z, z + len);
		bool xCommon, yCommon, zCommon; xCommon = yCommon = zCommon = true;
		fr (i, 1, n) {
			scanf("%d %d %d %d", &x, &y, &z, &len);
			if (xCommon && yCommon && zCommon) {
				xCommon = isCommon(xRange, pii(x, x + len));
				yCommon = isCommon(yRange, pii(y, y + len));
				zCommon = isCommon(zRange, pii(z, z + len));
			}
		}
		int l, b, h;
		l = xRange.second - xRange.first;
		b = yRange.second - yRange.first;
		h = zRange.second - zRange.first;
		if (!(xCommon && yCommon && zCommon)) printf("0\n");
		else printf("%d\n", l * b * h );
	}
	return 0;
}