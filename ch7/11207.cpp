# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions


int main () {
	int n;
	while (ipInt(n) && n) {
		double ans = 0 ; int instance = 0;
		fr (i, 0, n) {
			int w, h; double less, more;
			ipInt(w);ipInt(h);
			less = min (w, h); more = max(w, h);
			double maxSide = min(less, more / 4);
			maxSide = max (maxSide, less / 2);
			if (maxSide > ans) { ans = maxSide; instance = i + 1; }
		}
		printf("%d\n",instance);
	}
	return 0;
}