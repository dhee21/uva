# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

// double volFrustom (double r, double r, double h) {
// 	return ((PI * h) * (r * r + R * R + R * r)) / 3;
// }

int main () {
	int D, V;
	while (scanf("%d", &D) && D) {
		ipInt(V);
		double ans = cbrt((double)(D * D * D) - ((double)(6 * V) / (double)PI));
		printf("%0.3lf\n", ans);
	}
	return 0;
}