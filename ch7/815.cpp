# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

int main () {
	int m, n, height[1000], water, caseNo = 0;
	double ansHeight ;
	while (scanf("%d %d", &m, &n) && (m || n)) {
		caseNo++;
		int filledVolume = 0, submerged = 0;
		fr (i, 0, m * n) scanf("%d", &height[i]);
		sort(height, height + (m * n));
		scanf("%d", &water);
		ansHeight = height[0];
		fr (i, 1, m * n) {
			int heightThatCanIncrease = height[i] - height[i - 1];
			int volThatCanIncrease = (heightThatCanIncrease) * 100 * i ;
			if (volThatCanIncrease + filledVolume > water) {
				int realVolIncrease = water - filledVolume;
				if (water == filledVolume) break;
				filledVolume = water;
				ansHeight += (double)(realVolIncrease) / 100.0 / (double)i;
				submerged++;
				break; 
			} else {
				filledVolume += volThatCanIncrease;
				ansHeight = height[i];
				submerged++;
			}
		}
		if (fabs((double)filledVolume - water) >= EPS &&  filledVolume < water) {
			ansHeight += (water - filledVolume) / 100.0 / (m * n);
			submerged++;
		}
		printf("Region %d\n", caseNo);
		printf("Water level is %.2lf meters.\n", ansHeight );
		printf("%.2lf percent of the region is under water.\n\n",((double)submerged / (double)(m * n)) * 100.0);
	}
	return 0;
}