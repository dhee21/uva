# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) (double)PI / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

int main () {
	int n, caseNo = 0; double A;
	while (scanf("%d %lf", &n, &A) && n > 2) { caseNo++;
		double alpha = (2 * PI) / n;
		double sinAlpha = sin(alpha);
		double rSq = (2 * A) / (sinAlpha * n);
		double spec = PI * rSq - A;
		double rDashSq = rSq * ((1 + cos(alpha)) / 2);
		double officials = A - PI * rDashSq;
		printf("Case %d: %0.5lf %0.5lf\n", caseNo, spec, officials );
	}
	return 0;
}