# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

int main () {
	int t, xl1, yl1, xr1, yr1; ipInt(t);
	int xl2, yl2, xr2, yr2, caseNo = 0;
	while (t--) {
		if (caseNo++) printf("\n");
		scanf("%d %d %d %d", &xl1, &yl1, &xr1, &yr1);
		scanf("%d %d %d %d", &xl2, &yl2, &xr2, &yr2);
		if ((xl2 >= xr1 || xr2 <= xl1) || (yl2 >= yr1 || yr2 <= yl1)  ) {
			printf("No Overlap\n");
		} else {
			printf("%d %d %d %d\n", max(xl1, xl2), max(yl1, yl2), min(xr1, xr2), min(yr1, yr2) );
		}
	}
	return 0;
}