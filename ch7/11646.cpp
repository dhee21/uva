# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions
double sq(double x) { return x * x; }


int main () {
	int a, b, caseNo = 0; char c;
	double L, W;
	while (scanf("%d %c %d", &a, &c, &b) != EOF) {
		caseNo++;
		// double val = 2 + 2 * atan((double) b / a) * sqrt(1 + (sq(b) / sq(a)));
		// double L = 400.0 / val, W = ((double)b * L) / a;
		// printf("Case %d: %0.10lf %0.10lf\n",caseNo, L, W );
		double hi = 400, lo = 0;
		while (fabs(lo - hi) > EPS) {
			L = (lo + hi) / 2;
			W = ((double)b / a) * L;

			double CM = 0.5 * L, MX = 0.5 * W;
			double r = sqrt(CM * CM + MX * MX);
			double angle = 2 * (atan(MX / CM)) ;
			double arc = 2 * angle * r;
			if ((2 * L) + arc < 400.0 ) lo = L; else hi = L;
		}
		printf("Case %d: %.10lf %.10lf\n",caseNo, L, W );
	}
	return 0;
}