# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

int main () {
	int l, c, r1, r2;
	while (scanf("%d %d %d %d", &l, &c, &r1, &r2) && (l || c || r1 || r2)) {
		int minLen = min (l, c);
		if ((r1 << 1) > minLen || (r2 << 1) > minLen) {
			printf("N\n"); continue;
		}
		int x = l - r1 - r2, y = c - r1 - r2, r = r1 + r2;
		if (x * x + y * y >= r * r) printf("S\n"); else printf("N\n");
	}
	return 0;
}