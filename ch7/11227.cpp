# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI 3.14159
#define DEG_TO_RAD(val) (double)PI / 180.0
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions
map<pair<pii, pii >, int > isPresent;

// ================================ point definitions and methods ================================================
struct point_i { int x, y;
	point_i (int _x = 0.0, int _y = 0.0) { x = _x; y = _y; }
 };

struct point { double x, y;
	point (double _x = 0.0, double _y = 0.0) { x = _x; y = _y; }
	bool operator < (point other) {
		if (x < other.x) return true;
		return false;
	}
 };
bool areSame (point_i p1, point_i p2) { return p1.x == p2.x && p1.y == p2.y; }
bool areSame (point p1, point p2) { return fabs(p1.x - p2.x) < EPS && fabs(p1.y - p2.y) < EPS; }
double dist (point p1, point p2) { return hypot(p1.x - p2.x, p1.y - p2.y); }
point rotate (point p, double theta) {
	double rad = DEG_TO_RAD(theta);
	return point(p.x * cos(rad) - p.y * sin(rad), p.x * sin(rad) + p.y * cos(rad));
}
// ========================================================================================================

// ================================ line definitions and methods ================================================
struct line { double a, b, c; };
// ans stored is stored in third parameter
void pointsToLine ( point p1, point p2, line *l ) {
	if (p1.x == p2.x) {
		l->b = 0.0; l->a = 1; l -> c = - p1.x;
	} else {
		l->a = -(double) (p1.y - p2.y) / (p1.x - p2.x);
		l->b = 1.0;
		l->c = -(double) (l->a * p1.x) - (l->b * p1.y);
	}
}
bool areParallel (line l1, line l2) { return fabs (l1.a - l2.a) < EPS && fabs (l1.b - l2.b) < EPS; }
bool areSame (line l1, line l2) { return areParallel(l1, l2) && fabs (l1.c - l2.c) < EPS; }
bool areIntersect (line l1, line l2, point *p) {
	if (areSame(l1, l2) || areParallel(l1, l2)) return false;
	p->x = (l2.b * l1.c - l1.b * l2.c) / (l2.a * l1.b - l1.a * l2.b);
	if (fabs(l1.b) > EPS) p->y = - (l1.a * p->x + l1.c) / l1.b;
	else p->y = - (l2.a * p->x + l2.c) / l2.b;
	return true;
}
// if line is not parallel to x axis
double givenYTellX(line l, double y) {
	if (l.a == 0) return 1.0; // just to escape divByZero exception
	return (-l.c - l.b * y) / l.a;
}
bool lieOnLine (line l, point p) { return fabs (l.a * p.x + l.b * p.y + l.c) < EPS; }
// ========================================================================================================

// ================================ vector definitions and methods ================================================
struct vec { double x, y;
	vec (double _x = 0.0, double _y = 0.0) {x = _x; y = _y;}
};
// vector going from p1 -> p2 
vec toVector (point p1, point p2) { return vec (p2.x - p1.x, p2.y - p1.y); }
vec scaleVector (vec v, double s) { return vec(v.x * s, v.y * s); }
// displace p by displacement v
point translate (point p, vec v) { return point (p.x + v.x, p.y + v.y); }
// ========================================================================================================
// ================================ line and point AND line segment ans point  ==============================
// 
// returns distance from p to line defined by two points A ans B (A and B) must be different 
// the closest point is stored in 4th parameter (by reference)
double distToLine (point p, point A, point B, point *c) {
	double scale = (double) ( (p.x - A.x) * (B.x - A.x) + (p.y - A.y) * (B.y - A.y) ) /
							( (B.x - A.x) * (B.x - A.x) +  (B.y - A.y) * (B.y - A.y) );
	c->x = A.x + scale * (B.x - A.x);
	c->y = A.y + scale * (B.y - A.y);
	return dist (p, *c);
}
// returns distance from p to line segment defined by two points A ans B 
// OK if (A == B)
double distToLineSegment(point p, point A, point B, point *c) {
	if ((B.x - A.x) * (p.x - A.x) + (B.y - A.y) * (p.y - A.y) < EPS) {
		c->x = A.x; c->y = A.y; 
		return dist(p, A);
	}
	if ( (A.x - B.x) * (p.x - B.x) + (A.y - B.y) * (p.y - B.y) < EPS) {
		c->x = B.x; c->y = B.y;
		return dist(p, B);
	}
	return distToLine (p, A, B, c);
}
// ========================================================================================================
// ================================ cross product and counter clockwise test (point on left hand side)  =====
//  returns qp X qr
	double cross (point p, point q, point r) { return (r.x - q.x) * (p.y - q.y) - (r.y - q.y) * (p.x - q.x); }
	// return true if r lies on line pq
	bool collinear (point p, point q, point r) { return fabs(cross(p, q, r)) < EPS; }
	// return true id point r is on left side of line pq (CCW) (counter clockwise test)
	bool ccw(point p, point q, point r) { return cross(p, q, r) > 0; }
// ========================================================================================================

int main () {
	int t, caseNo = 0;
	point points[110];
	ipInt(t);
	while (t--) {
		isPresent.clear();
		caseNo++;
		int n, ans = 0, size = 0, a, b, c, d; ipInt(n);
		fr (i, 0, n) { scanf("%d.%d %d.%d", &a, &b, &c , &d);
				// printf("%d.%d %d.%d \n",a, b, c, d);
				if (isPresent.count(make_pair(pii(a, b), pii(c, d))) == 0) {
					isPresent[make_pair(pii(a, b), pii(c, d))] = 1;
					points[size].x = (double)a + (double)b / 100.0;
					points[size++].y = (double)c + (double)d / 100.0;
				}
		}
		// fr (i, 0, size) printf("%lf %lf \n",points[i].x, points[i].y);
		if (size == 1) {
			printf("Data set #%d contains a single gnu.\n",caseNo ); continue;
		}
		fr (i, 0, size) fr (j, i + 1, size) {
			line l ;
			pointsToLine(points[i], points[j], &l);
			int numPtsOnLine = 0;
			fr (k, 0, size) if (lieOnLine(l, points[k])) numPtsOnLine ++;
			ans = max(numPtsOnLine, ans);
		}
		printf("Data set #%d contains %d gnus, out of which a maximum of %d are aligned.\n",caseNo,size, ans );
	}
	return 0;
}
