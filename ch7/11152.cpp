# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

// ================================ point definitions and methods ================================================
struct point_i { int x, y;
	point_i (int _x = 0.0, int _y = 0.0) { x = _x; y = _y; }
 };

struct point { double x, y;
	point (double _x = 0.0, double _y = 0.0) { x = _x; y = _y; }
	bool operator < (point other) {
		if (x < other.x) return true;
		return false;
	}
 };
bool areSame (point_i p1, point_i p2) { return p1.x == p2.x && p1.y == p2.y; }
bool areSame (point p1, point p2) { return fabs(p1.x - p2.x) < EPS && fabs(p1.y - p2.y) < EPS; }
double dist (point p1, point p2) { return hypot(p1.x - p2.x, p1.y - p2.y); }
// theta in degrees
point rotate (point p, double theta) {
	double rad = DEG_TO_RAD(theta);
	return point(p.x * cos(rad) - p.y * sin(rad), p.x * sin(rad) + p.y * cos(rad));
}
point midPoint (point p1, point p2) { return point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2); }
// ========================================================================================================

// ================================ line definitions and methods ================================================
struct line { double a, b, c; };
// ans stored is stored in third parameter
void pointsToLine ( point p1, point p2, line *l ) {
	if (p1.x == p2.x) {
		l->b = 0.0; l->a = 1; l -> c = - p1.x;
	} else {
		l->a = -(double) (p1.y - p2.y) / (p1.x - p2.x);
		l->b = 1.0;
		l->c = -(double) (l->a * p1.x) - (l->b * p1.y);
	}
}
bool areParallel (line l1, line l2) { return fabs (l1.a - l2.a) < EPS && fabs (l1.b - l2.b) < EPS; }
bool areSame (line l1, line l2) { return areParallel(l1, l2) && fabs (l1.c - l2.c) < EPS; }
bool areIntersect (line l1, line l2, point *p) {
	if (areSame(l1, l2) || areParallel(l1, l2)) return false;
	p->x = (l2.b * l1.c - l1.b * l2.c) / (l2.a * l1.b - l1.a * l2.b);
	if (fabs(l1.b) > EPS) p->y = - (l1.a * p->x + l1.c) / l1.b;
	else p->y = - (l2.a * p->x + l2.c) / l2.b;
	return true;
}
// if line is not parallel to x axis
double givenYTellX(line l, double y) {
	if (l.a == 0) return 1.0; // just to escape divByZero exception
	return (-l.c - l.b * y) / l.a;
}
bool lieOnLine (point p, line l) { return fabs (l.a * p.x + l.b * p.y + l.c) < EPS; }
// ========================================================================================================

// ================================ vector definitions and methods ================================================
struct vec { double x, y;
	vec (double _x = 0.0, double _y = 0.0) {x = _x; y = _y;}
};
// vector going from p1 -> p2 
vec toVector (point p1, point p2) { return vec (p2.x - p1.x, p2.y - p1.y); }
vec scaleVector (vec v, double s) { return vec(v.x * s, v.y * s); }
// displace p by displacement v
point translate (point p, vec v) { return point (p.x + v.x, p.y + v.y); }
// rotate vec by angle theta in degree
vec rotateVector (vec v1, double theta) {
	point p1 = point(v1.x, v1.y); p1 = rotate(p1, theta); return vec (p1.x, p1.y);
}
// ========================================================================================================
// 
// ================================ line and point AND line segment ans point  ==============================
// 
// returns distance from p to line defined by two points A ans B (A and B) must be different 
// the closest point is stored in 4th parameter (by reference)
double distToLine (point p, point A, point B, point *c) {
	double scale = (double) ( (p.x - A.x) * (B.x - A.x) + (p.y - A.y) * (B.y - A.y) ) /
							( (B.x - A.x) * (B.x - A.x) +  (B.y - A.y) * (B.y - A.y) );
	c->x = A.x + scale * (B.x - A.x);
	c->y = A.y + scale * (B.y - A.y);
	return dist (p, *c);
}
// returns distance from p to line segment defined by two points A ans B 
// OK if (A == B)
double distToLineSegment(point p, point A, point B, point *c) {
	if ((B.x - A.x) * (p.x - A.x) + (B.y - A.y) * (p.y - A.y) < EPS) {
		c->x = A.x; c->y = A.y; 
		return dist(p, A);
	}
	if ( (A.x - B.x) * (p.x - B.x) + (A.y - B.y) * (p.y - B.y) < EPS) {
		c->x = B.x; c->y = B.y;
		return dist(p, B);
	}
	return distToLine (p, A, B, c);
}
// ========================================================================================================

// ================================ cross product and counter clockwise test (point on left hand side)  =====
//  returns qp X qr
	double cross (point p, point q, point r) { return (r.x - q.x) * (p.y - q.y) - (r.y - q.y) * (p.x - q.x); }
	// return true if r lies on line pq
	bool collinear (point p, point q, point r) { return fabs(cross(p, q, r)) < EPS; }
	// return true id point r is on left side of line pq (CCW) (counter clockwise test)
	bool ccw(point p, point q, point r) { return cross(p, q, r) > 0; }
	double dotProduct (vec v1, vec v2) { return v1.x * v2.x + v1.y * v2.y; }

// ========================================================================================================
 
// ================================ circle  ===============================================================
// tells if a point p is inside , on or outside circle of radius r and center c
struct circle {
	// center , radius
	point c; double r;
	circle (point _c = point(0, 0), double _r = 0) {
		c = _c; r = _r;
	}
};
// center c, radius r
int inCircle (point_i p, point_i c, int r) {
	int puttInEq = (p.x - c.x) * (p.x - c.x) + (p.y - c.y) * (p.y - c.y) - r * r;
	if (puttInEq <  0) return inside; else if (puttInEq == 0) return onCircle; else return outside;
}
// center c, radius r
int inCircle (point p, point c, double r) {
	double puttInEq = (p.x - c.x) * (p.x - c.x) + (p.y - c.y) * (p.y - c.y) - r * r;
	if (fabs(puttInEq) < EPS) return onCircle; else if (puttInEq <  0) return inside; else return outside;
}
// given 2 points ans radius , two circle can pass through those two points , this tells center of one of those circles
// TO KNOW THE SECOND CENTER JUST REVERSE THE INPUT TO THE FUNCTION p1 and p2
bool circle2PtsRad(point p1, point p2, double r, point *c) {
	double d2 = (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
	double det = r * r / d2 - 0.25;
	if (det < 0.0) return false; // as r is small , i.e r < |p1p2| / 2, r incapable of making a circle
	double h = sqrt(det);
	c -> x = (p1.x + p2.x) * 0.5 + (p1.y - p2.y) * h;
	c -> y = (p1.y + p2.y) * 0.5 + (p2.x - p1.x) * h;
	return true;
}
// ========================================================================================================

// ================================== triangle methods ====================================================
// given 3 DIFFERENT points and NON COLINEAR return circumcircle of triangle formed by 3 pts
circle circumCircle (point pt1, point pt2, point pt3) {
	point mid1 = midPoint(pt1, pt2), mid2 = midPoint(pt1, pt3);
	line line1 , line2; pointsToLine(pt1, pt2, &line1); pointsToLine(pt1, pt3, &line2);
	line bisec1, bisec2;
	bisec1.a = line1.b; bisec1.b = -line1.a; bisec1.c = -(bisec1.a * mid1.x + bisec1.b * mid1.y);
	bisec2.a = line2.b; bisec2.b = -line2.a; bisec2.c = -(bisec2.a * mid2.x + bisec2.b * mid2.y);
	point center;
	areIntersect(bisec1, bisec2, &center);
	double radius = dist(center, pt1);
	return circle(center, radius);
}
// s semiperimeter , Area of triangle A, s = (a + b + c) / 2 ... a, b, c arre length of sides of triangle
double herons (double a, double b, double c) { double s = (a + b + c) / 2; return sqrt(s * (s - a) * (s - b) * (s - c)); }
double radiusOfCircumCircle (double a, double b, double c, double A ) { return (a * b * c) / (4 * A); }
double radiusOfInCircle (double a, double b, double c, double A ) { double s = (a + b + c) / 2; return A / s ;}
// ========================================================================================================

int main () {
	int a, b, c;
	while (scanf("%d %d %d", &a, &b, &c) != EOF) {
		double A = herons(a, b, c);
		double rCircum = radiusOfCircumCircle(a, b, c, A), rIn = radiusOfInCircle(a, b, c, A);
		double areaCircum = PI * rCircum * rCircum, areaIn =  PI * rIn * rIn;
		printf("%0.4lf %0.4lf %0.4lf\n",areaCircum - A , A - areaIn, areaIn );
	} 
	return 0;
}
