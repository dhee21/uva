# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions


// ================================ point definitions and methods ================================================
struct point_i { int x, y;
	point_i (int _x = 0.0, int _y = 0.0) { x = _x; y = _y; }
 };

struct point { double x, y;
	point (double _x = 0.0, double _y = 0.0) { x = _x; y = _y; }
	bool operator < (point other) {
		if (x < other.x) return true;
		return false;
	}
 };
bool areSame (point_i p1, point_i p2) { return p1.x == p2.x && p1.y == p2.y; }
bool areSame (point p1, point p2) { return fabs(p1.x - p2.x) < EPS && fabs(p1.y - p2.y) < EPS; }
double dist (point p1, point p2) { return hypot(p1.x - p2.x, p1.y - p2.y); }
// theta in degrees
point rotate (point p, double theta) {
	double rad = DEG_TO_RAD(theta);
	return point(p.x * cos(rad) - p.y * sin(rad), p.x * sin(rad) + p.y * cos(rad));
}
point midPoint (point p1, point p2) { return point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2); }
void printPoint(point p) { printf("%lf %lf\n",p.x, p.y ); }
// ========================================================================================================

// ================================ cross product and counter clockwise test (point on left hand side)  =====
//  returns pq X pr
	double cross (point p, point q, point r) { return (q.x - p.x) * (r.y - p.y) - (q.y - p.y) * (r.x - p.x); }
	// return true if r lies on line pq
	bool collinear (point p, point q, point r) { return fabs(cross(p, q, r)) < EPS; }
	// return true if point r is on left side of line pq (CCW) (counter clockwise test)
	//  returns true if we need to move counter clockwise from direction pq to get to pr
	bool ccw(point p, point q, point r) { return cross(p, q, r) > 0; }
// ========================================================================================================

// ================================ polygon definitions and methods ================================================
// polygon represented as vector<point> where last entry = first entry
typedef vector <point> polygon;
double perimeter (polygon &P) {
	double result = 0.0;
	fr (i, 0, P.size() - 1) result += dist(P[i], P[i + 1]);
	return result;
}
double area (polygon &P) {
	double result = 0.0, x1, y1, x2, y2;
	fr (i, 0, P.size() - 1) {
		x1 = P[i].x; y1 = P[i].y; x2 = P[i + 1].x; y2 = P[i + 1].y;
		result += (x1 * y2 - x2 * y1);
	}
	return fabs(result / 2.0);
}
// returns true if all 3 consecutive vertices of P form the same turns
// returns false in cae of a line or a point
bool isConvex (polygon &P) {
	int sz = P.size(); if (sz < 3) return false;
	bool isLeft = ccw(P[0], P[1], P[2]);
	fr (i, 1, sz) if (ccw(P[i], P[(i + 1) % sz], P[(i + 2) % sz]) != isLeft) return false;
	return true;
}
// returns angle b/w line ab and ac
double angle (point a, point b, point c) {
	double abX = b.x - a.x, abY = b.y - a.y, acX = c.x - a.x, acY = c.y - a.y;
	return acos ((abX * acX + abY * acY) /
					sqrt((abX * abX + abY * abY) * (acX * acX + acY * acY)));
}
// returns if point p is inside a polygon p
bool inPolygon (point p, polygon &P) {
	if (P.size() == 0) return false;
	double sum = 0.0;
	fr (i, 0, P.size() - 1) {
		if (cross(p, P[i], P[i + 1]) < 0) sum -= angle(p, P[i], P[i + 1]);
		else sum += angle (p, P[i], P[i + 1]);
	}
	return (fabs(sum - 2*PI) < EPS || fabs(sum + 2*PI) < EPS);
}

// line segment p-q intersect line a-b
point lineIntersectSeg(point p, point q, point A, point B) {
	double a = B.y - A.y;
	double b = A.x - B.x;
	double c = B.x * A.y - A.x * B.y;
	double u = fabs(a * p.x + b * p.y + c);
	double v = fabs(a * q.x + b * q.y + c);
	return point ((p.x * v + q.x * u) / (u + v), (p.y * v + q.y * u) / (u + v));
}
// polygon Q cut along the line formed by pt a --> pt b
// gives a polygon P in counter clockwise direction cut by a --> b
// (note : the last point of P must be same as the first point)
void cutPolygon(polygon &Q, point a, point b, polygon &P) {
	P.clear();
	fr (i, 0, Q.size()) {
		double left1 = cross(a, b, Q[i]), left2 = 0.0;
		if (i != Q.size() - 1) left2 = cross(a, b, Q[i + 1]);
		if (left1 > -EPS) P.push_back(Q[i]);
		if (left1 * left2 < -EPS) {
			P.push_back(lineIntersectSeg(Q[i], Q[i + 1], a, b));
		}
	}
	if (P.size() == 0) return ;
	if (fabs(P.back().x - P.front().x) > EPS || fabs(P.back().y - P.front().y) > EPS) P.push_back(P.front());
	return;
}

point pivot;
bool angleCmp(point left, point right) {
	if (collinear(pivot, left, right)) return dist(pivot, left) < dist(pivot, right);
	double d1X = left.x - pivot.x, d1Y = left.y - pivot.y;
	double d2X = right.x - pivot.x, d2Y = right.y - pivot.y;
	return (atan2(d1Y , d1X) - atan2(d2Y , d2X)) < 0;
}

// gives the convex hull of a set of points given by vector <point> P and 
// gives convexHull (CH) in CH
void CH (vector<point> &P, vector<point> &convexHull) {
	convexHull.clear();
	int N = P.size();
	if (N <= 3) { fr (i, 0, N) convexHull.push_back(P[i]);  convexHull.push_back(P[0]); return; }
	// point with lowest Y coor value and if tie righest X coor value
	int PO = 0;
	fr (i, 1, N) if (P[i].y < P[PO].y || (P[i].y == P[PO].y && P[i].x > P[PO].x)) PO = i;
	// swap selected vertex with P[0]
	point temp = P[PO]; P[PO] = P[0]; P[0] = temp;
	// sort w.r.t pivot P[0]
	pivot = P[0];
	sort (++P.begin(), P.end(), angleCmp);
	// now keep on doing ccw test
	point prev, now;
	stack<point> S; S.push(P[N - 1]); S.push(P[0]);
	int i = 1;
	while (i < N) {
		now = S.top(); S.pop();
		prev = S.top(); S.push(now);
		// ccw for accepting collinear points
		if (ccw(prev, now, P[i]) || collinear(prev, now, P[i])) S.push(P[i++]); // left turn accepted
		else S.pop();// pop the top until we have left turn
	}
	while (!S.empty()) { convexHull.push_back(S.top()); S.pop(); }
	return ;
}
// ========================================================================================================
int main () {
	int N, W, H, x, y, caseNo = 0;
	while (scanf("%d %d %d %d %d", &N, &W, &H, &x, &y) != EOF) {
		caseNo++;
		polygon Q;
		point fountain = point(x, y);
		Q.push_back(point(0, 0)); Q.push_back(point(W, 0)); Q.push_back(point(W, H)); Q.push_back(point(0, H)); Q.push_back(Q[0]);
		// printf("%d %d %d\n", inPolygon(point(20, 30), Q), inPolygon(point(99, 99), Q), inPolygon(point(101, 1), Q) );
		while (N--) {
			int x1, y1, x2, y2;
			scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
			point a(x1, y1), b(x2, y2);
			polygon clockWise, antiClockWise; cutPolygon(Q, a, b,antiClockWise); cutPolygon(Q, b, a, clockWise);
			if (inPolygon(fountain, clockWise)) {
				Q.clear();
				Q = clockWise;
			} else {
				Q.clear();
				Q = antiClockWise;
			}
		}
		printf("Case #%d: %0.3lf\n",caseNo, area(Q) );
	}
	return 0;
}
