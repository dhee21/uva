# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

double sq(double x) { return x * x; }

int main () {
	int x[6000], y[6000], z[6000], index = 0, a, b, c;
	int histogram[10] = {0};
	while (scanf("%d %d %d", &a, &b, &c) && (a || b || c)) { x[index] = a; y[index] = b; z[index++] = c; }
	fr (i, 0, index) { 
		double minDist = inf;
		fr (j, 0, index) if (i != j) {
			double dist = sqrt(sq(x[i] - x[j]) + sq(y[i] - y[j]) + sq(z[i] - z[j]) );
			minDist = min (dist, minDist);
		}
		int minDistVal = (int)floor(minDist);
		if (minDistVal < 10) histogram[minDistVal]++;
	}
	fr (i, 0, 10) {
		int num = histogram[i];
		int spaces = num < 10 ? 3 : num < 100 ? 2 : num < 1000 ? 1 : 0;
		fr(j, 0, spaces) printf(" ");
		printf("%d",num );
	}
	printf("\n");
	return 0;
}