# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<29)
# define EPS 1e-9
#define PI  3.141592653589793
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
#define earthRad 6371009
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions


// gives great circle distance bw p and q pt on sphere
double angleAtCenter (double pLat, double pLong, double qLat, double qLong) {
	  pLong = DEG_TO_RAD(pLong); pLat = DEG_TO_RAD(pLat); qLat = DEG_TO_RAD(qLat); qLong = DEG_TO_RAD(qLong);
	  double dlon = pLong - qLong; 
	  double dlat = pLat - qLat; 
	  double a = pow((sin(dlat/2)),2) + cos(pLat) * cos(qLat) * pow(sin(dlon/2), 2); 
	  double c = 2 * atan2(sqrt(a), sqrt(1-a)); 
	  return c;
}
double sq(double x) { return x * x; }

int main () {
	int t;
	ipInt(t);
	while (t--) {
		double pLong, pLat, qLong, qLat;
		scanf("%lf %lf %lf %lf", &pLat, &pLong, &qLat, &qLong);
		double px = earthRad * cos(DEG_TO_RAD(pLat)) * cos(DEG_TO_RAD(pLong)), py = earthRad * sin(DEG_TO_RAD(pLat)), pz = earthRad * cos(DEG_TO_RAD(pLat)) * sin(DEG_TO_RAD(pLong));
		double qx = earthRad * cos(DEG_TO_RAD(qLat)) * cos(DEG_TO_RAD(qLong)), qy = earthRad * sin(DEG_TO_RAD(qLat)), qz = earthRad * cos(DEG_TO_RAD(qLat)) * sin(DEG_TO_RAD(qLong));
		double distance = sqrt (sq(px - qx) + sq (py - qy) + sq (pz- qz));
		double theta = angleAtCenter(pLat, pLong, qLat, qLong );
		// double distance = earthRad * sqrt (2 * (1 - cos(theta)));
		// cout << distance << endl;
		double gcDist = earthRad * theta;
		// cout << gcDist << endl;
		printf("%d\n", (int) round(gcDist - distance) );
	}
	return 0;
}