# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI  3.141592653589793
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

map <string , pdd > cities;

// gives great circle distance bw p and q pt on sphere
double gcDistance (double pLat, double pLong, double qLat, double qLong, double radius) {
	  pLong = DEG_TO_RAD(pLong); pLat = DEG_TO_RAD(pLat); qLat = DEG_TO_RAD(qLat); qLong = DEG_TO_RAD(qLong);
	  double dlon = pLong - qLong; 
	  double dlat = pLat - qLat; 
	  double a = pow((sin(dlat/2)),2) + cos(pLat) * cos(qLat) * pow(sin(dlon/2), 2); 
	  double c = 2 * atan2(sqrt(a), sqrt(1-a)); 
	  double d = radius * c; 
      return d;
}

int main () {
	char city[100];
	double lati, longi;
	while (true) {
		scanf("%s", city);
		if (city[0] == '#' && city[1] == '\0') break;
		scanf("%lf %lf", &lati, &longi);
		cities[city] = pdd(lati, longi);
	}
	while (true) {
		char from[100], to[100];
		scanf("%s %s", from, to);
		if (from[0] == '#' && to[0] == '#' && to[1] == '\0' && from[1] == '\0' ) break;
		printf("%s - %s\n",from, to );
		if (cities.count(from) && cities.count(to)) {
			double distVal = gcDistance(cities[from].first, cities[from].second, cities[to].first, cities[to].second, 6378);
			// cout << distVal << endl;
			printf("%d km\n", (int)round(distVal) );
		} else {
			printf("Unknown\n");
		}
	}
	return 0;
}
