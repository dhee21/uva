# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

int main () {
	double l, w, h, theta;
	while (scanf("%lf %lf %lf %lf",&l, &w, &h, &theta) != EOF) {
		double thetaRad = DEG_TO_RAD(theta);
		double tanTheta = tan(thetaRad);
		double volume = l * w * h;

		if (tanTheta * l < h) {
			double hght = l * tan(thetaRad), volEmpty = 0.5 * (l * w * hght);
			double milkVol = volume - volEmpty;
			printf("%0.3lf mL\n",milkVol );
		} else {
			double milkVol = (0.5 * h * h * w) / tanTheta;
			printf("%0.3lf mL\n",milkVol );
		}
		
	}
	return 0;
}