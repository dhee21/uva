#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
using namespace std;

struct rules {
	int person1, person2, distance;
};
bool passRules (int *arr, int size, rules* rulesArray, int numRules) {
int position[20] = {0};
for ( int i = 0 ; i< size ;++i) {
	position[arr[i]] = i;
}
for (int i = 0 ; i< numRules ;++i) {
	int p1 = rulesArray[i].person1;
	int p2 = rulesArray[i].person2;
	int distance = abs(position[p1] - position[p2]);
	if(rulesArray[i].distance > 0) {
		//atmost
		if(distance > rulesArray[i].distance) {
			return 0;
		}
	} else {
		//atleast
			if(distance < -rulesArray[i].distance) {
				return 0;
			}
	}
}
return 1;
}

int main () {
	int n , m , arr[10];
	rules rulesArray[1000];
	while (scanf("%d %d", &n , &m) && (n || m)) {
		for ( int i = 0 ; i < m ; ++i) {
			scanf("%d %d %d", &rulesArray[i].person1, &rulesArray[i].person2, &rulesArray[i].distance);
		}
		for ( int  i = 0 ; i < n ; ++i) {
			arr[i] = i ;
		}
		int ans = 0;
		do {
			if(passRules(arr, n, rulesArray, m)) {
				ans++;
			}
		} while(next_permutation(arr, arr+n));
		printf("%d\n", ans);
	}
	return 0;
}