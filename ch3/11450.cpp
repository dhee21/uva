#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int price[25][25], canReach[2][210], previous, nextRow;

void fillCanReach (int totalMoney, int c) {
	previous = 0; nextRow = 1;
	for (int i = 0; i <= totalMoney; ++i) {
		canReach[0][i] = 0;
		canReach[1][i] = 0;
	}
	canReach[0][totalMoney] = 1;

	for (int g = 0; g < c; ++g ) {
		for (int money = 0; money <= totalMoney; ++money) {
			if (canReach[previous][money] == 1) {
				for (int model = 1; model <= price[0][g]; ++model) {
					int moneyRem = money - price[model][g];
					if (moneyRem >= 0) {
						canReach[nextRow][moneyRem] = 1;
					}
				}	
			}
		}
		int a = nextRow;
		nextRow = previous;
		previous = a;
		for (int i = 0; i <= totalMoney; ++i) {
			canReach[nextRow][i] = 0;
		}
	}
	
}

int main () {
	int t, m, c, ans, modelNum;
	scanf("%d", &t);
	while (t--) {
		scanf("%d %d", &m, &c);
		for (int g = 0 ; g < c; ++g) {
			scanf("%d", &modelNum);
			price[0][g] = modelNum;
			for ( int mod = 1; mod <= modelNum; ++mod ) {
				scanf("%d", &price[mod][g]);
			}
		}
		// for (int i = 0 ;i< 20; ++i) {
		// 	for (int j = 0 ; j < 25; ++j ) {
		// 		printf("%d ", price[i][j] );
		// 	}
		// 	printf("\n");
		// }
		//memset(canReach, false, sizeof(canReach));
		// printf("%d\n", sizeof(canReach) );
		fillCanReach(m, c);
		bool noSol = 1;
		for (int col = 0; col <= m; ++col) {
			if (canReach[previous][col] == 1) {
				noSol = false;
				printf("%d\n", m - col );
				break;
			}
		}
		if (noSol) {
			printf("no solution\n");
		}
	}
		return 0;
	}