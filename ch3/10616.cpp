#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int num[210];
long long int dp[15][210][25];

int modulo (long long int num, long long int divisor) {
		if (num >= 0) {
			return num % divisor;
		} else {
			return (divisor + num) % divisor;
		}
}

long long int ways (int M, int D, int N) {
	for (int i = N; i >= 0; --i) {
		for (int m = 0; m <= M; ++m) {
			for (int x = 0; x < D; ++x) {
				if (!m) {
					dp[m][i][x] = (!x) ? 1 : 0;
				} else if (i == N) {
					dp[m][i][x] = 0; 
				} else {
					long long int a = dp[m][i + 1][x];
					int rem = modulo(x - modulo(num[i], D), D);
					long long int b = dp[m - 1][i + 1][rem];
					dp[m][i][x] = a + b;
				}
				if (m == M && i == 0 && x == 0) {
					return dp[m][i][x];
				}

			}
		}
	}

	return dp[M][0][0];
}

int main () {
	int n, q, set = 0;
	while (scanf("%d %d", &n, &q) && !(n==0 && q==0)) {
		set++;
		for (int i = 0; i < n; ++i) {
			scanf("%d", &num[i]);
		}
		int m, d;
		printf("SET %d:\n", set);
		for (int i = 0; i < q; ++i) {
			scanf("%d %d", &d, &m);
			memset(dp, -1, sizeof(dp));
			long long int ans = ways(m, d, n);
			printf("QUERY %d: %lld\n", i + 1, ans );
		}
}
		return 0;

}