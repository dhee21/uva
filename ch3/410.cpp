#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <vector>
#include <algorithm>

using namespace std ;
typedef pair<int, int> pr;
struct pairs {
	int first, second, index;
} pairsArray[30];

bool swapFunc ( pairs first, pairs second ) {
	return (first.index < second.index);
}

int main () {
	// freopen("test.inp", "r", stdin);
	// freopen("test.out", "w", stdout);
	int c, s,caseNo = 0;
	int sum;
	pr m[30];
	while (scanf ("%d %d", &c, &s) != EOF) {
		// if (caseNo) {
		//  printf("\n");
		// }
		caseNo++;
		sum = 0;
		for ( int i = 0; i < s; ++i ) {
			scanf("%d", &(m[i].first));
			m[i].second = i;
			sum += m[i].first; 
		}
		sum;
		for (int i = s; i < (c << 1) ; ++i) {
			m[i] = make_pair(0, i);
		}
		sort(m, m + (c << 1));
		double imbalance = 0;
		for (int i = 0; i < c; ++i) {
			int a = m[i].first, b = m[(c << 1) - i - 1].first;
			int aInd = m[i].second, bInd = m[(c << 1) - i - 1].second;
			pairsArray[i].first = (aInd < bInd) ? a : b;
			pairsArray[i].second = (aInd > bInd) ? a : b;
			pairsArray[i].index = (aInd < bInd) ? aInd : bInd;
			imbalance += abs(((a + b) *c ) - sum);
		}
		imbalance /= c;
		sort(pairsArray, pairsArray + c, swapFunc);
		printf("Set #%d\n", caseNo);
		for ( int i = 0; i < c; ++i) {
			int f = pairsArray[i].first ,s = pairsArray[i].second;
			printf(" %d:", i);
			if (f > 0 && s > 0) {
				printf(" %d %d\n",pairsArray[i].first, pairsArray[i].second );
			}
			else if ( f > 0 || s > 0 ) {
				printf(" %d\n", (f > 0) ? f : s );
			} else {
				printf("\n");
			}
		}
		printf("IMBALANCE = %0.5f\n\n", imbalance );

	}
}