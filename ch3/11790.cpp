#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

struct build
{
	int len, sum;
	/* data */
}lis[10000], lds[10000];

int main () {
	int t, n, h[10000], w[10000];
	scanf("%d", &t);
	for (int tc  = 0; tc < t; ++tc) {
		scanf("%d", &n);
		if (!n) {
			printf("Case %d. Increasing (0). Decreasing (0).\n", tc + 1);
			continue;
		}
		for (int i = 0; i < n; ++i) {
			scanf("%d", &h[i]);
		}

		for (int i = 0; i < n; ++i) {
			scanf("%d", &w[i]);
		}
		lds[0].sum = w[0];
		lds[0].len = 1;
		lis[0].sum = w[0];
		lis[0].len = 1;
		int longestLenLDS = 1, longestLenLIS = 1, maxSumLDS = w[0], maxSumLIS = w[0];
		for (int i = 1; i < n; ++i) {
			lds[i].sum = w[i]; lds[i].len = 1;
			lis[i].sum = w[i]; lis[i].len = 1;
			for (int j = 0; j < i; ++j) {
				if (h[j] < h[i]) {
					if (lis[j].sum + w[i] > lis[i].sum) {
						// lis[i].len = lis[j].len + 1;
						lis[i].sum = lis[j].sum + w[i];
					}
				} else if (h[j] > h[i]) {
					if (lds[j].sum + w[i] > lds[i].sum) {
						// lds[i].len = lds[j].len + 1;
						lds[i].sum = lds[j].sum + w[i];
					}
				}
			}
			maxSumLIS = max(maxSumLIS, lis[i].sum);
			maxSumLDS = max(maxSumLDS, lds[i].sum);
			// if (lis[i].len > longestLenLIS) {
			// 	longestLenLIS = lis[i].len;
			// 	maxSumLIS = lis[i].sum;
			// } else if (lis[i].len == longestLenLIS) {
			// 	maxSumLIS = max (maxSumLIS, lis[i].sum);
			// }
			// if (lds[i].len > longestLenLDS) {
			// 	longestLenLDS = lds[i].len;
			// 	maxSumLDS = lds[i].sum;
			// } else if (lds[i].len == longestLenLDS) {
			// 	maxSumLDS = max (maxSumLDS, lds[i].sum);
			// }
		}
		// for (int i = 0; i< n; ++i) {
		// 	printf("len = %d width = %d wJ =%d\n ", lis[i].len, lis[i].sum, w[i] );
		// }
		if (maxSumLIS >= maxSumLDS) {
			printf("Case %d. Increasing (%d). Decreasing (%d).\n", tc + 1, maxSumLIS, maxSumLDS );
		} else {
			printf("Case %d. Decreasing (%d). Increasing (%d).\n", tc + 1, maxSumLDS, maxSumLIS );
		}
	}

}