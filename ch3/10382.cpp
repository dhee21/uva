# include <iostream>
#include <stdio.h>
#include <algorithm>
#include <math.h>
using namespace std;

struct interval
{
	double st, ed;
}intervals[10010];

int indexToStart;

double findMaxEd (double ptToCover, int intervalNo) {
	double ed = 0;
	for (int i = indexToStart; i< intervalNo; ++i) {
		if (intervals[i].st > ptToCover) {
			indexToStart = i;
			break;
		} else {
			ed = fmax (ed, intervals[i].ed);
		}

	}
		return ed;
}

bool compare (interval first , interval second) {
	return first.st < second.st;
}
int main () {
	int n, l, w, index;
	while (scanf("%d %d %d", &n, &l, &w) != EOF) {
		indexToStart = 0;
		index = 0;
		int sprinklers = 0;
		for (int i = 0; i < n; ++i) {
			double x, r;
			scanf("%lf %lf", &x, &r);
			if ((r * 2) >= w) {
				double dX = sqrt(((4 * r * r) - (w * w)) / 4);
				intervals[index].st = x - dX;
				intervals[index++].ed = x + dX;
			}
		}
		sort (intervals, intervals + index, compare);
		double ptToCover = 0;

		while (ptToCover < l) {
			double ed = findMaxEd(ptToCover, index);
			if (ed <= ptToCover) {
				break;
			} else {
				ptToCover = ed;
				sprinklers ++; 
			}
		}
		if (ptToCover < l) {
			printf("-1\n");
		} else {
			printf("%d\n", sprinklers );
		}
	}
}