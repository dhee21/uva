# include <iostream>
#include <stdio.h>
#include <algorithm>
#include <math.h>
using namespace std;

int main () {
	int n, sum;
	while (scanf("%d", &n) && n) {
		int x;
		int sumFlag = 0;
		int flag = 0;
		for (int i = 0; i < n; ++i) {
			scanf("%d", &x);
			if (x) {
				if (flag) {
					printf(" ");
				}
				flag = 1;
				printf("%d", x );
				sumFlag = 1;
			}
		}
		if (!sumFlag) {
			printf("0\n");
		} else {
			printf("\n");
		}
	}
	return 0;
}