#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <string.h>
using namespace std;

int main () {
	int t,lds[2010], lis[2010], a[2010];
	scanf("%d", &t);
	while(t--) {
		int n;
		scanf("%d", &n);
		if (!n) {
			printf("0\n");
			continue;
		}
		for (int i = 0; i < n; ++i) {
			scanf("%d", &a[i]);
		}
		lis[n - 1] = 1;
		lds[n - 1] = 1;
		int  ans = 1;
		for (int i = n - 2; i >= 0; --i) {
			lis[i] = lds[i] = 1;
			for (int j = i + 1; j < n ; ++j) {
				if (a[j] > a[i]) {
					lis[i] = max (lis[i], lis[j] + 1);
				} else if (a[j] < a[i]) {
					lds[i] = max (lds[i], lds[j] + 1);
				}
			}
			ans = max(ans, lds[i] + lis[i] - 1);
		}
		printf("%d\n",ans );
	}
	

	
	return 0;
}