#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int mat[110][110], nInf = 1 << 31;

int matrixSum (int stRow, int stC, int edR, int edC  ) {
	int thisMatrixSum = 0;
	thisMatrixSum += mat[edR][edC];
	thisMatrixSum -= (stRow - 1 >= 0) ? mat[stRow -1][edC]: 0;
	thisMatrixSum -= (stC - 1 >= 0) ? mat[edR][stC - 1] : 0;
	thisMatrixSum += (stRow - 1 >= 0 && stC - 1 >= 0) ? mat[stRow - 1][stC - 1] : 0;
	return thisMatrixSum;
} 

int main () {
	int n, t;
	scanf("%d" , &t);
	for (int i = 0; i < t; ++i) {
		scanf("%d", &n);
		int ans = nInf;
		// printf("%d\n", ans );
		memset(mat, 0, sizeof(mat));
		for (int r = 0; r < n ; ++r) {
			for (int c = 0; c < n ; ++c) {
				scanf("%d", &mat[r][c]);
				mat[r][c] += (r-1 >= 0) ? mat[r-1][c] : 0 ;
				mat[r][c] += (c-1 >= 0) ? mat[r][c-1] : 0 ;
				mat[r][c] -= (r - 1 >= 0 && c - 1 >= 0) ? mat[r - 1][c - 1] : 0;
			}
		}
		int total = mat[n - 1][n - 1];
		for (int stRow = 0; stRow < n; ++stRow) for (int stC = 0; stC < n; ++stC) {
			for (int edC = stC; edC < n; ++edC) for (int edR = stRow ; edR < n; ++edR) {
				int thisMatrixSum = matrixSum(stRow, stC, edR, edC );
				// hrToSub is the the horizontal matrix from which the present matrix will be subtracted to get the right "wrapping" type matrix
				// vrToSub is the the vertical matrix from which the present matrix will be subtracted to get the "up wrapping" type matrix
				int hrToSub = matrixSum(stRow, 0, edR, n - 1), vrToSub = matrixSum(0, stC, n - 1, edC);
				int horizontal = nInf;
				if (!(stC == 0  && edC == n - 1)) {
					horizontal = hrToSub - thisMatrixSum;
				}
				int vertical = nInf;
				if (!(stRow == 0 && edR == n - 1)) {
					vertical = vrToSub - thisMatrixSum;
				}
				int complement = nInf;
				if (horizontal != nInf && vertical != nInf) {
					complement = total - hrToSub - vrToSub + thisMatrixSum;
				}
				int ans1 = max(thisMatrixSum, horizontal), ans2 = max(vertical, complement);
				int maxAns = max (ans1, ans2);
				
				ans = max (ans, maxAns);
				
			}
		}
		printf("%d\n", ans);
	}
}