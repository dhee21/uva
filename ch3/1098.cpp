#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
using namespace std;

int rows, cols, totalRoutes, visitedNum, canVisitNum, visited[10][10] ;
struct coordinates {
	int r, c ;
} one, two, three, final, nextCheckPoint;

bool isSame ( coordinates first, coordinates second) {
	return first.r == second.r && first.c == second.c;
}

void initializeGrid (int grid[][10]) {
	for (int i = 0; i < 10; ++i) {
		for ( int j = 0 ; j< 10; ++j) {
			grid[i][j] = 0;
		}
	}
}

bool canGoDfs (coordinates coor, int grid[][10]) {
	if ( coor.r < 0 || coor.r >= rows ) {
		return false;
	}
	if ( coor.c < 0 || coor.c >= cols ) {
		return false;
	}
	if ( grid[coor.r][coor.c] == -1 || visited[coor.r][coor.c] == -1 ) {
		return false;
	}
	return true;
}

void dfs(coordinates coor, int grid[][10]) {
	int toMove[4][2] = { {0, -1}, { 1, 0 }, { 0, 1 }, { -1, 0 } };
	canVisitNum ++;
	visited[coor.r][coor.c] = -1;
	for (int i=0; i<4; ++i ) {
		coordinates toGo;
		toGo.r = coor.r + toMove[i][0];
		toGo.c = coor.c + toMove[i][1];
		if ( canGoDfs(toGo, grid)) {
			dfs(toGo, grid);
		}
	}
}

bool canReachAllPoints (int grid[][10]) {

	int totalLength = rows * cols;
		canVisitNum = 0;
		initializeGrid(visited);
		dfs(final, grid);
		if ((visitedNum) + canVisitNum == totalLength) {
			return true;
		}
		return false;
}

bool canReachNextCheckpoint(coordinates coor, int lengthToGet) {
	int totalLength = rows * cols;
	int lenForCheckpoint = abs(coor.r - nextCheckPoint.r) + abs(coor.c  - nextCheckPoint.c)  + lengthToGet;
	if ( isSame(nextCheckPoint, one) && lenForCheckpoint > totalLength/4 ) {
		return false;
	}
	if ( isSame(nextCheckPoint, two) && lenForCheckpoint > totalLength/2 ) {
		return false;
	}
	if ( isSame(nextCheckPoint, three) && lenForCheckpoint > (3 * totalLength)/4 ) {
		return false;
	}
	if ( isSame(nextCheckPoint, final) && lenForCheckpoint > totalLength ) {
		return false;
	}
	return true;
	
}

bool canGo ( coordinates coor, int lengthToGet, int grid[][10] ) {
	if ( coor.r < 0 || coor.r >= rows ) {
		return false;
	}
	if ( coor.c < 0 || coor.c >= cols ) {
		return false;
	}
	if ( grid[coor.r][coor.c] == -1 ) {
		return false;
	}
	
	// 
	int totalLength = rows * cols;
	if ( isSame(one, coor) && lengthToGet != totalLength/4  ) {
		return false;
	}
	if ( isSame(two, coor) && lengthToGet != totalLength/2  ) {
		return false;
	}
	
	if ( isSame(three, coor) && lengthToGet != (3 * totalLength)/4 ) {
		return false;
	}
	if (isSame(final, coor) && lengthToGet != totalLength ) {
		return false;
	} 
	// 
	if ( lengthToGet == totalLength/4 && !isSame(one, coor) ) {
		return false;
	}
	if ( lengthToGet == totalLength/2 && !isSame(two, coor) ) {
		return false;
	}

	if ( lengthToGet == (3 * totalLength) / 4  && !isSame(three, coor)  ) {
		return false;
	}
	if ( lengthToGet == totalLength && !isSame(final, coor) ) {
		return false;
	}
	if (!canReachNextCheckpoint(coor, lengthToGet)) {
		return false;
	}
	// if ( !canReachAllPoints(coor, grid)) {
	// 	return false;
	// }
	
	if ( isSame(one, coor) && lengthToGet == totalLength/4  ) {
		nextCheckPoint = two;
	}
	if ( isSame(two, coor) && lengthToGet == totalLength/2  ) {
		nextCheckPoint = three;
	}
	
	if ( isSame(three, coor) && lengthToGet == (3 * totalLength)/4 ) {
		nextCheckPoint = final;
	}
	return true;
}

void MoveTheRobot (coordinates PC,int PL, int grid[][10]) {
	if ( PC.r == 0 && PC.c == 1) {
		if (PL != rows * cols) {
			return;
		}
		totalRoutes++;
		return;
	}
	
	grid[PC.r][PC.c] = -1;
	visitedNum++;
	if (canReachAllPoints(grid)) {
		int toMove[4][2] = { {0, -1}, { 1, 0 }, { 0, 1 }, { -1, 0 } };
		for (int i=0; i<4; ++i ) {
			coordinates toGo;
			toGo.r = PC.r + toMove[i][0];
			toGo.c = PC.c + toMove[i][1];
			if (canGo(toGo, PL + 1,  grid)) {
				MoveTheRobot(toGo, PL + 1, grid);
			}
		}
	}
	
		grid[PC.r][PC.c] = 0;
		visitedNum--;
}



int main () {
	final.r = 0; final.c = 1;
	int grid[10][10] , caseNo = 0;
	while (scanf("%d %d", &rows, &cols) && rows && cols ) {
		caseNo++;
		totalRoutes = 0;
		visitedNum = 0;
		scanf("%d", &(one.r));
		scanf("%d", &(one.c));
		scanf("%d", &(two.r));
		scanf("%d", &(two.c));
		scanf("%d", &(three.r));
		scanf("%d", &(three.c));

		initializeGrid(grid);
		nextCheckPoint = one;
		int presentLength = 1;
		totalRoutes = 0;
		coordinates start = { 0, 0 };
		if (canGo(start, presentLength, grid)) {
			MoveTheRobot(start, presentLength, grid);
		}
		printf ("Case %d: %d\n", caseNo,  totalRoutes);
	}
}