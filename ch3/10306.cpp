#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int conv[50], ift[50];
int dp[50][350][350], eMod;
#define inf (1<<30) - 1

int esq(int x, int y) {
	return x * x + y * y;
}

int minCoins (int i, int conVal, int infVal) {
	int val = esq(conVal, infVal);
	if (val > eMod) {
		return inf;
	}
	if (val == eMod) {
		return 0;
	}
	if (!i) {
		return inf;
	}
	if (dp[i][conVal][infVal] != -1) {
		return dp[i][conVal][infVal];
	}
	int a = minCoins(i, conVal + conv[i], infVal + ift[i]) + 1;
	if (a >= inf) {
		a = inf;
	}
	dp[i][conVal][infVal] = min(a, minCoins(i - 1, conVal, infVal));
	return dp[i][conVal][infVal];
}

int main () {
	int n, m, s ;
	scanf("%d", &n);
	while (n--) {
		scanf("%d %d", &m, &s);
		eMod = s * s;
		for (int i = 1; i <= m; ++i) {
			scanf ("%d %d", &conv[i], &ift[i]);
		}
		memset(dp, -1, sizeof dp);
		int ans = minCoins(m, 0, 0);
		if (ans == inf) {
			printf("not possible\n");
		} else {
			printf("%d\n", ans);
		}
	}
}