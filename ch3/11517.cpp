#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;
#define inf 1<<30

class transaction
{
	public:
	int coinsUsed, moneyPaid;
	
	transaction (int a = 0, int b = 0) {
		coinsUsed = a;
		moneyPaid = b;
	}
	/* data */
}dp[110][10010];

transaction findMin (transaction withCoin, transaction withoutCoin) {
	if (withCoin.moneyPaid < withoutCoin.moneyPaid) {
		withCoin.coinsUsed ++;
		return withCoin;
	} else if (withCoin.moneyPaid > withoutCoin.moneyPaid) {
		return withoutCoin;
	} else {
		// withCoin.coinsUsed + 1 using more than 32 bit maybe 64 bit
		//  but inf + 1 is -ve using 32 bit
		//  therefore in general case always see if var == inf then donrt var + 1 as 
		//  it will not be inf but -ve
		if (withoutCoin.coinsUsed <= withCoin.coinsUsed + 1) {
			return withoutCoin;
		} else {
			withCoin.coinsUsed ++;
			return withCoin;
		}
	}
}

int coin[10010];
int main () {
	int t, totalPrice, n;
	scanf("%d", &t);
	while (t--) {
		scanf ("%d", &totalPrice);
		scanf ("%d", &n);
		for (int i = 1; i <= n; ++i) {
			scanf ("%d", &coin[i]);
		}
		dp[0][0] = transaction(0, totalPrice);
		for (int p = 0; p <= totalPrice; ++p) {
			dp[0][p] = transaction(inf, inf);
		}

		for (int i = 1; i <= n; ++i) {
			for (int price = 0; price <= totalPrice; ++price) {
				int price1 = price - coin[i];
				transaction a = price1 <= 0 ? transaction(0, totalPrice - price1) : dp[i - 1][price1];
				transaction b = dp[i - 1][price];
				dp[i][price] = findMin(a, b); 
			}
		}
		transaction ans = dp[n][totalPrice];
		printf("%d %d\n",ans.moneyPaid , ans.coinsUsed);
	}
}