#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int dp[110][110];

void ways(int k, int n) {
	int ans = 0;
	for (int i = 0; i <= n; ++i) {
		ans += dp[k - 1][n - i];
		ans %= 1000000;
	}
	dp[k][n] = ans;
}

int main () {
	int k , n;
	while (scanf("%d %d", &n, &k) && (k && n) ) {
		for (int i = 0; i <= k; ++i) {
			dp[i][0] = 1;
		}
		for (int i = 0; i <=n; ++i ) {
			dp[1][i] = 1;
		}
		for (int row = 2; row <= k; ++row) {
			for (int col = 1; col <= n ; ++col) {
				ways(row, col);
			}
		}
		printf("%d\n",dp[k][n] );
	}
	return 0;
}