# include <iostream>
#include <stdio.h>
#include <algorithm>
#include <math.h>
#include <string.h>
using namespace std;

struct rock {
	char type;
	long long int dist, checked;
}rocks[110];
int maxHop = 0, ch = 0;

void goAhead(int rockNum, int length) {
	int presentMark = 0, index = -1;
	while (index < rockNum) {
		if (index + 1 == rockNum) {
			maxHop = max(maxHop, length  - presentMark);
			presentMark = length;
			index++;
		} else {
			int nextRockD = rocks[index + 1].dist;
			char nextRockT = rocks[index + 1].type;
			if (nextRockT == 'B') {
				maxHop = max(maxHop, nextRockD - presentMark);
				presentMark = nextRockD;
				rocks[index + 1].checked = 1;
				ch++;

				index++; 
			} else {
				int nextMark = (index + 2 < rockNum ) ? rocks[index + 2].dist: length;
				maxHop = max(maxHop, nextMark - presentMark);
				// printf("%d cc\n", nextMark - presentMark );
				if (index + 2 < rockNum ) {
					rocks[ index + 2 ].checked = 1;
				}
				ch++;
				presentMark = nextMark;
				index += 2;
			}
		}
	}

}

void goBack(int rockNum, int length) {
	int presentMark = length, index = rockNum;
	while (index > -1) {
		if (index - 1 == -1) {
			maxHop = max(maxHop, presentMark - 0);
			presentMark = 0;
			index--;
		} else {
			int nextRockD = rocks[index  - 1].dist;
			char nextRockT = rocks[index - 1].type;
			int nextRockC = rocks[index - 1].checked;

			int presentRockD = rocks[index].dist;
			char presentRockT = rocks[index].type;
			if (nextRockT == 'B') {
				maxHop = max(maxHop, presentMark - nextRockD);

				rocks[index - 1].checked = 1;
				ch++;

				presentMark = nextRockD;
				index--; 
			} else if ((presentRockT == 'B' || index == rockNum) && nextRockT =='S' && nextRockC == 0) {
				maxHop = max(maxHop, presentMark - nextRockD);
				rocks[index - 1].checked = 1;
				ch++;

				presentMark = nextRockD;
				index--;
			} else {
				int nextMark = (index - 2 > -1 ) ? rocks[index - 2].dist : 0;
				maxHop = max(maxHop, presentMark - nextMark);
				if (index - 2 > -1 ) {
					rocks[ index -2].checked = 1;
				ch++;

				}
				presentMark = nextMark;
				index -= 2;
			}
		}
	}
}

int main () {
	int t, n, length;
	char inputStr[600];
	scanf("%d", &t);
	for (int i = 0; i < t; ++i) {
		scanf("%d %d", &n, &length);
		getchar();
		char token[30];
		int index = 0;
		for (int j = 0; j< n; ++j) {
			scanf("%s", token);
			// printf("%s\n", token );
			rocks[index].type = token[0];
			rocks[index].dist = atoi(token + 2);
			rocks[index++].checked = 0;
		}
		maxHop = 0;
		goAhead(index, length);
		goBack(index, length);
		int check = 0;
		printf("Case %d: %d\n",i+1,  maxHop );
		}
		// printf("see\n\n");
	return 0;
		// 
		
	}
