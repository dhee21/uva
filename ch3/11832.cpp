#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;
int offset = 16400;
#define ind(i) i + offset
bool dp[150000][50], canReach[150000][50];
char ans[50];
int main () {
	int n, f, t[50]
	while (scanf("%d %d", &n, &f) && (n || f)) {
		for (int i = 0; i < n; ++i) {
			scanf("%d", &t[i]);
		}
		for (int i = 0 ; i < 50; ++i) {
			ans[i] = '*';
		}
		memset(canReach, 0, sizeof canReach);
		canReach[ind(f)][0] = 1;
		for (int i = 0; i < n; ++i) {
			for (int s = -offset; s <= offset; ++s) {
				if (canReach[ind(s)][i]) {
					int index1 = ind(s - t[i]), index2 = ind(s + t[i]);
					if (index1 >= 0) {
						canReach[index1][i + 1] = 1;
					} 
					if (index2 >= 0) {
						canReach[index2][i + 1] = 1;
					}
				}
			}
		}

		for (int i = n; i >= 0; --i) {
			for (int s = -offset; s <= offset; ++s) {
				if (i == n) {
					dp[ind(s)][i] = !s ? 1 : 0 ;
				} else {
					bool a = (ind(s - t[i]) >= 0) ? dp[ind(s - t[i])][i + 1] : 0, b = (ind(s + t[i]) >= 0) ? dp[ind(s + t[i])][i + 1] : 0;
					dp[ind(s)][i] = a || b;
					if (canReach[ind(s)][i]) {
						// printf("yo\n");
						if (ans[i] != '?' && (a || b)) {
							if (a && b) {
								ans[i] = '?';
							} else if (a) {
								ans[i] = ans[i] == '-' ? '?' : '+';
							} else {
								ans[i] = ans[i] == '+' ? '?' : '-';
							}
						}
					}
					
				}
			}
		}
		if (dp[ind(f)][0] == 0) {
			printf("*\n");
		} else {
			for (int i = 0 ; i < n; ++i) {
				printf("%c", ans[i]);
			}
			printf("\n");
		}
		
	}
	return 0;
}