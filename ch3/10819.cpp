#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int p[110], f[110], dp[10220][110];
int main () {
	int pockMon, pockMonNew, n;
	while( scanf("%d %d", &pockMon, &n) != EOF) {
			bool canBuyMore  = false;
			if (pockMon + 200 > 2000) {
				pockMonNew = pockMon + 200;
				canBuyMore = true;
			}
			if (n == 0) {
				printf("0\n");
				continue;
			}
			for (int i = 0; i < n; ++i) {
				scanf("%d %d", &p[i], &f[i]);
			}
			int pockToCheck = (canBuyMore) ? pockMonNew : pockMon;
			for (int i = n; i >= 0; --i) {
				for (int m = 0; m <= pockToCheck ; ++m) {
					if (!m) {
						dp[m][i] = 0;
					} else if (i == n) {
						dp[m][i] = -1;
					} else {
						int a = dp[m][i + 1];
						int b = (m - p[i] >= 0) ? dp[m - p[i]][i + 1] : -1;
						b = (b >= 0) ? b + f[i] : -1;
						dp[m][i] = max(a, b);
					}
				}
			}
			int ans = -1;
			for (int m = 0 ; m <= pockMon; ++m ) {
				ans = max(ans, dp[m][0]);
			}
			if (canBuyMore) {
				int start = max(2001, pockMon + 1);
				for (int m = start; m <= pockMonNew; ++m) {
					ans = max(ans, dp[m][0]);
				}
			}
			printf("%d\n", ans );
	}
}