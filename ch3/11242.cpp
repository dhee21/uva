#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
typedef vector<double> vi;
int main () {
	int front, rear, x;
	while (scanf("%d", &front) && front) {
		vi frontVec, rearVec, total;
		scanf("%d" , &rear);
		for (int i = 0 ; i < front ; ++i ) {
			scanf("%d", &x);
			frontVec.push_back(x);
		}
		for (int i = 0 ; i < rear; ++i ) {
			scanf("%d", &x);
			rearVec.push_back(x);
		}
		for ( int i = 0 ; i < rearVec.size() ; ++i) {
			for(int j = 0 ; j < frontVec.size() ; ++j) {
				total.push_back(rearVec[i]/frontVec[j]);
			}
		}
		sort(total.begin(), total.end());
		double spreadMax = 0;
		for(int k =  0 ; k<total.size() -1 ; ++k ) {
			double spread = total[k+1] / total[k];
			spreadMax = (spreadMax < spread) ? spread : spreadMax;
		}
		printf("%0.2lf\n", spreadMax );
	}
	
}