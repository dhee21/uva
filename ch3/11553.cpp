#include <stdio.h>
#include <iostream>
#include <utility>
#include <algorithm>
#include <limits>
using namespace std;

int infinity = numeric_limits<int>::max();
int board[10][10]={0};

int main () {
	int t,n;
	scanf("%d", &t);
	while(t--) {
		scanf("%d", &n);
		for(int i = 0 ; i < n ; ++i ) {
			for (int j = 0 ; j< n ; ++j) {
				scanf("%d", &board[i][j]);
			}
		}
		int Min = infinity;
		int choice[8] = {0,1,2,3,4,5,6,7};

		do{
			int sum = 0;
			for(int i = 0 ; i < n ;++i) {
				sum+=board[i][choice[i]];
			}
			Min = min (sum, Min);
		}while(next_permutation(choice, choice+n));
		printf("%d\n", Min);
	}
}