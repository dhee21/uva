#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <bits/stdc++.h>
#include <vector>
using namespace std;

#define leakRate 1

struct lineObject {
	int d, rate;
	string type;
};
vector <lineObject> inputs;


lineObject parse (string ip) {
	char cStr[100], cmd[100], a[100], b[100];
	for ( int i = 0; i< ip.size(); ++i) {
		cStr[i] = ip[i];
	}
	cStr[ip.size()] = '\0';
	int d, consRate = -1;
	// str = ip.c_str();
	sscanf (cStr, "%d %s", &d, cmd);
	int flag = 0;
	if (cmd[0] == 'F' || (cmd[0] == 'G' && cmd[1] == 'a') ) {
		flag  =1;
		sscanf (cStr, "%d %s %s %d", &d, a, b, &consRate);
	}
	lineObject toReturn;
	toReturn.d = d;
	if (flag) {
		string aNew = a, bNew = b;
		toReturn.type = aNew + " " + bNew;
		toReturn.rate = consRate;
		return toReturn;
	}
	string command = cmd;
	toReturn.type = command;
	toReturn.rate = -1;
	return toReturn;
}

bool isStart (lineObject line) {
	return line.type == "Fuel consumption" && line.d == 0;
}
bool isCons (lineObject line) {
	return line.type == "Fuel consumption";
}
bool isEnd (lineObject line) {
	return line.type == "Goal";
}
bool isLeak (lineObject line) {
	return line.type == "Leak";
}
bool isGas (lineObject line) {
	return line.type == "Gas station";
}
bool isMech (lineObject line) {
	return line.type == "Mechanic";
}
float findDmax (float distance, float leaks, float consRate) {
	return ((distance * consRate) / 100) + (distance * leaks * leakRate) ;
}

float consumptionInRide (float consRate, float leakCount, float distance) {
	return ( (consRate / 100) + (leakRate * leakCount) ) * distance;
}

float can () {
		float gasCapReq = 0;
		float gasUsed = 0;
		int leakCount = 0;
		float consRate; // per 100 km
		for (int i = 0 ; i < inputs.size() ; ++i) {
			if (i) {
				int distance = inputs[i].d - inputs[i-1].d;
				gasUsed += consumptionInRide(consRate, leakCount, distance);
				// if (fuel <= 0) {
				// 	return false;
				// }
			}
			lineObject ip = inputs[i];
			if (isCons(ip)) {
				consRate = ip.rate;
			} else if (isLeak(ip)) {
				leakCount++;
			} else if (isGas(ip)) {
				if (gasCapReq < gasUsed) {
					gasCapReq = gasUsed;
				}
				gasUsed = 0;
			} else if (isMech(ip)) {
				leakCount = 0;
			}
		}
		return max(gasUsed, gasCapReq);
}

float giveAnswer (float lo, float hi) {
	float mid , ans;
	float emptyTank = can();
	// printf("%f %f %f %f \n", hi, lo, mid, fabs(hi - lo) );

		while (fabs(hi - lo) > 0.0001) {
	// printf("%f %f %f \n", hi, lo, mid );
			mid = (hi + lo) / 2;
			if (mid - emptyTank > 0) {
				ans = mid;
				hi = mid;
			} else {
				lo = mid;
			}
		}
		// printf("%d\n", ans );
		return ans ;
}

int main () {
	string ipString, endString = "0 Fuel consumption 0";
	int leaks = 0, consRate = 0;
	while (1) {
		getline(cin, ipString);
		if (ipString == endString) {
			break;
		}
		lineObject line = parse(ipString);
		// printf("%d %s %d\n", line.d, line.type.data(), line.rate);
		if (isStart(line)) {
			leaks = 0;
			consRate = line.rate;
		}
		if (isCons(line)) {
			consRate = max(consRate, line.rate);
		}
		if (isLeak(line)) {
			leaks++;
		}
		inputs.push_back(line);
		if (isEnd(line)) {
			// for (int i = 0 ; i< inputs.size() ; ++i) {
		 // 		printf("%d %s %d\n", inputs[i].d, inputs[i].type.data(), inputs[i].rate);
			// 	// printf("%s\n", );
			// }
			// printf("\n\n\n");
			int distance = line.d;
			// printf("%d %d %d see\n", distance, leaks,  consRate);
			float dMin = 0;
			float dMax = findDmax(distance, leaks, consRate) + 1;
			// printf("%f %f seeeeeeee\n\n",dMin, dMax );
			float answer = can();
			printf ("%0.3f\n", answer);
			inputs.clear();
		}
	}
}