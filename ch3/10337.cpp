# include <iostream>
# include <stdio.h>
# include <string.h>
#include <string>
#include <math.h>
#include <utility>
using namespace std;
#define inf 1<<25

int wind[14][1020], dp[14][1020];

int main () {
	int t;
	scanf("%d", &t);
	while (t--) {
		int x, n;
		scanf("%d", &x);
		n = x / 100;
		for (int alt = 9; alt >= 0; --alt) {
			for (int mile = 0; mile < n; ++mile) {
				scanf("%d", &wind[alt][mile]);
			}
		}

		for (int alt = 0; alt < 10; ++alt) {
			dp[alt][n] =(!alt) ? 0 : inf;
		}

		for (int mile = n - 1; mile >= 0; --mile) {
			for (int alt = 0; alt < 10; ++alt) {
				int windSpeed = wind[alt][mile];
				int sink = inf, stay, climb = inf;
				if (alt > 0) {
					sink = 20 - windSpeed + dp[alt - 1][mile + 1];
				}
				stay = 30 - windSpeed + dp[alt][mile + 1];
				if (alt < 9) {
					// int windSpeedAbove = wind[alt + 1][mile];
					climb = 60 - windSpeed + dp[alt + 1][mile + 1];
				}
				dp[alt][mile] = min (stay, min (climb, sink));
			}
		}
		printf("%d\n\n", dp[0][0]);
 
	}
}