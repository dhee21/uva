#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int cuts[60], dp[60][60];

void findDP (int st, int ed) {
	if (st + 1 == ed) {
		dp[st][ed] = 0;
		return;
	}
	int ans = 1<<30;
	for ( int i = st + 1; i < ed; ++i) {
		int val = dp[st][i] + dp[i][ed] + cuts[ed] - cuts[st];
		ans = min(ans, val);
	}
	dp[st][ed] = ans;
}

int main () {
	int len, n;
	while(scanf("%d", &len) && len) {
		scanf("%d", &n);
		cuts[0] = 0; cuts[n + 1] = len ;
		for (int i = 1 ; i <= n; ++i) {
			scanf("%d", &cuts[i]);
		}
		if (n == 0) {
			printf("The minimum cutting is 0.\n");
			continue;
		}
		// length is length of cuts in bw st and ed
		for (int length = 0; length <= n; ++length) {
			for (int ed = 1 + length; ed <= n + 1; ++ed) {
				int st = ed - 1 - length; // st >=0
				findDP(st, ed);
			}
		}
		printf("The minimum cutting is %d.\n", dp[0][n+1] );
	}
	return 0;
}