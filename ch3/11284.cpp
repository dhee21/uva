# include <iostream>
# include <stdio.h>
# include <string.h>
#include <string>
#include <math.h>
#include <utility>
using namespace std;

#define inf 1 << 30

class tourObject
{
public:
	int tour, len;

	tourObject( int a = 0, int b = 0) {
		tour = a;
		len = b;
	}

}dp[(1 << 13) + 100];

int matrix[55][55], p, roadsNum;
float save[55], globalSaving;

bool visited(int node, int option) {
	option = option & (1 << node);
	return option != 0;
}
int removeStore (int i, int option) {
	return option & ~(1 << i);
}
float findSaving (int tour) {
	tour /= 10;
	int virtualSaving = 0;
	while (tour) {
		int node = tour % 10;
		virtualSaving += save[node];
	}
	return virtualSaving;
}

tourObject insertAt (int edgeToInsert, tourObject minTourWS, int storeToInsert) {
	string s = to_string(minTourWS.len);
	if (edgeToInsert >= s.size()) {
		return tourObject(-1, -1);
	}
	tourObject newTour;
	int nextNode = atoi(s[edgeToInsert + 1]);
	int presentNode = atoi (edgeToInsert == s.size() - 1 ? '0' : s[edgeToInsert]);
	newTour.len = minTourWS.len - matrix[presentNode][nextNode] + matrix[presentNode][storeToInsert] + matrix[storeToInsert][nextNode];
	newTour.tour = atoi((s.substr(0, edgeToInsert + 1) + to_string(storeToInsert) + s.substr(edgeToInsert + 1, s.size() - edgeToInsert)).data());
	return newTour;

}

tourObject minTour (int option) {
	if (option == 1) {
		return tourObject(0, inf);
	}
	if (!(option & 1)) {
		return tourObject(inf, inf);
	}
	int minTourLen = inf;
	float savings;
	tourObject  minTour;
	if (dp[option].tour != -1) {
		return dp[option];
	}
	for (int st = 1; st <= p; ++p) {
		if (visited(st, option)) {
			int optionWS = removeStore(st, option);
			tourObject minTourWS;
			minTourWS = minTour(optionWS);
			int tourLenWS = minTourWS.len, tour = minTourWS.tour;
			float virtualSaving = findSaving(tour);

			for ( int edge = 0; ; edge++) {
				tourObject tourNew = insertAt(edge, minTourWS, st);
				if (tourNew.len == -1) break;
				else if (tourNew.len < minTourLen) {
					minTourLen = tourNew.len;
					minTour = tourNew;
					savings = virtualSaving - minTour.len;
				}
			}
		} 
	}
	dp[option] = minTour;
	globalSaving = min(savings, globalSaving);
	return dp[option];
}

void floydWarshal () {
		for (int i = 0; i <= roadsNum; ++i) for (int j = 0; j <= roadsNum; ++j) for (int k = 0; k <= roadsNum; ++k) {
				matrix[j][k] = matrix[k][j] = min(matrix[j][k], matrix[j][i] + matrix[i][k]);
		}
}

int main () {
	int t, storeNum, operasNum;
	scanf("%d", &t);
	while (t--) {
		globalSaving = inf;
		memset(save, 0, sizeof save);
		// memset(dp, inst, sizeof dp);
		scanf("%d %d", &storeNum, &roadsNum);
		for (int i = 0; i <= roadsNum; ++i) for (int j = 0; j <= roadsNum; ++j) matrix[i][j] = inf;
		for (int r = 0; r < roadsNum; ++r) {
			int i, j;
			float cost;
			scanf("%d %d %f", &i, &j, &cost);
			matrix[i][j] = matrix[j][i] = cost;
		}
		floydWarshal();
		scanf("%d", &p);
		printf("%d rtbdfvdc\n",p );
		for (int st = 0; st < p; ++st) {
			int i;
			float x;
			scanf("%d %f", &i, &x);
			save[i] = x;
		}
	}
}