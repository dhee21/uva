# include <iostream>
# include <stdio.h>
# include <string.h>
#include <string>
#include <math.h>
#include <utility>
using namespace std;
#define inf 1<<30
int rows[15], col[15];
struct optimum
{
	long long int val;
	string ans;
	/* data */
}dp[15][15];

optimum matrix (int i, int j) {
	// printf("%d %d\n", i, j );
	if (i == j) {
		optimum var;
		var.val = 0;
		var.ans = "A" + to_string(i);
		return var;
	}
	if (dp[i][j].val != -1) {
		return dp[i][j];
	}
	int minMul = inf, index;
	string leftAns, rightAns;
	for ( int k = i; k < j; ++k) {
		optimum left;
		left = matrix(i, k);
		optimum right;
		right =  matrix(k + 1, j);
		long long int mul = left.val + right.val + rows[i] * col[k] * col[j];
		if (mul < minMul) {
			index = k;
			minMul = mul;
			leftAns = left.ans; rightAns = right.ans;
		}
	}
	optimum answer;
	answer.val = minMul;
	answer.ans = "(" + leftAns + " x " + rightAns + ")";
	dp[i][j] = answer;
	return dp[i][j];
}

int main () {
	int n, caseNo = 0;
	while (scanf ("%d", &n) && n) {
		caseNo ++;
		for (int i = 1; i <= n; ++i) {
			scanf("%d %d", &rows[i], &col[i]);
		}
		for (int i = 0 ; i <= n; ++i) for (int j = 0; j <= n; ++j) {
			dp[i][j].val = -1;
		}
		printf("Case %d: %s\n", caseNo,  matrix(1, n).ans.data() );
	}
}