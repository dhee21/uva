#include <stdio.h>
#include <iostream>
#include <algorithm>
using namespace std;
void ip(int n, int arr[]) {
 for(int i = 0; i< n; ++i) {
	scanf("%d", &arr[i]);
}
}

int main () {

	int n, d, r, mbr[110], ebr[110];
	while ( scanf("%d %d %d", &n, &d, &r)) {
			if (n || d || r) {
				ip(n, mbr);
				ip(n, ebr);
			sort(mbr, mbr + n);
			sort(ebr, ebr + n);
			int overtime = 0;
			for (int i = 0; i < n; ++i) {
				int val = ebr[i] + mbr[n - 1 - i] - d;
				overtime += (val > 0) ? val * r : 0;
			}
			printf("%d\n", overtime);
		} else {
			break ;
		}
	}
	return 0;
}