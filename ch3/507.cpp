#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

struct segment
{
	int st, ed, segNice;
	/* data */
}maxiSum[20010];

int niceArr[20010];
int main () {
	int b;
	scanf("%d", &b);
	for (int route = 1; route <= b; ++route) {
		int s, maxNice, stMax, edMax;
		scanf("%d", &s);
		for (int i = 1; i < s; ++i) {
			scanf("%d", &niceArr[i]);
			if (i == 1) {
				maxiSum[1].segNice = niceArr[1];
				maxiSum[1].st = 1;
				maxiSum[1].ed = 2;
				maxNice = niceArr[1];
				stMax = 1;
				edMax = 2;
			} else {
				// i != 1
				if (maxiSum[i - 1].segNice < 0) {
					maxiSum[i].segNice = niceArr[i];
					maxiSum[i].st = i;
					maxiSum[i].ed = i + 1;
				} else {
					maxiSum[i].segNice = niceArr[i] + maxiSum[i - 1].segNice;
					maxiSum[i].st = maxiSum[i - 1].st;
					maxiSum[i].ed = i + 1;
				}
				int segNiceI = maxiSum[i].segNice, stI = maxiSum[i].st, edI = maxiSum[i].ed;
				int segLen = edI - stI, segLenMax = edMax - stMax;
				bool chooseThisSeg = segNiceI > maxNice || (segNiceI == maxNice && (segLen > segLenMax || (segLen == segLenMax && stI < stMax) ) );
				if (chooseThisSeg) {
					maxNice = segNiceI;
					stMax = stI;
					edMax = edI;
				}
			}
		}
		if (maxNice <= 0) {
			printf("Route %d has no nice parts\n", route);
		} else {
			printf("The nicest part of route %d is between stops %d and %d\n",route, stMax, edMax);
		}
	}
}