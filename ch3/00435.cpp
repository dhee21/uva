#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std ;

int findSum( int num , int* array) {
	int i = 0, sum = 0;
	while (num) {
		if (num & 1) {
			sum += array[i];
		}
		num = num >> 1;
		++i;
	}
	return sum ;
}

int main () {
	int t, numParties, array[25],power[1009] = {0};
	scanf("%d", &t);
	while ( t-- ) {
		memset (power, 0, 4*1005);
		scanf("%d", &numParties);
			int total = 0;

		for ( int i = 0 ; i < numParties ; ++i ) {
			scanf("%d", &array[i]);
			total += array[i];
		}
		for ( int i = 0 ; i < (1<< numParties) ; ++i ) {
			int sum = findSum( i , array);
			int half = total >> 1;
			if(sum <= half ) {
				continue ;
			}
			for ( int j = 0 ; j < numParties ; ++j) {
				if( 1<<j & i) {
					if(sum - array[j] <= half) {
					power[j]++;
				}
				}
				
			}
		}
		for ( int i = 0 ; i< numParties; ++i ) {
			printf("party %d has power index %d\n", i+1 , power[i] );
		}
		
			printf("\n");
		
	} 
}