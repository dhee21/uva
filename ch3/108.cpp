#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

int mat[110][110];

int main () {
	int n;
	while (scanf("%d", &n) != EOF) {
		int ans = (1 << 31);
		// printf("%d\n", ans );
		memset(mat, 0, sizeof(mat));
		for (int r = 0; r < n ; ++r) {
			for (int c = 0; c < n ; ++c) {
				scanf("%d", &mat[r][c]);
				mat[r][c] += (r-1 >= 0) ? mat[r-1][c] : 0 ;
				mat[r][c] += (c-1 >= 0) ? mat[r][c-1] : 0 ;
				mat[r][c] -= (r - 1 >= 0 && c - 1 >= 0) ? mat[r - 1][c - 1] : 0;
			}
		}
		for (int stRow = 0; stRow < n; ++stRow) for (int stC = 0; stC < n; ++stC) {
			for (int edC = stC; edC < n; ++edC) for (int edR = stRow ; edR < n; ++edR) {
				int thisMatrixSum = 0;
				thisMatrixSum += mat[edR][edC];
				thisMatrixSum -= (stRow - 1 >= 0) ? mat[stRow -1][edC]: 0;
				thisMatrixSum -= (stC - 1 >= 0) ? mat[edR][stC - 1] : 0;
				thisMatrixSum += (stRow - 1 >= 0 && stC - 1 >= 0) ? mat[stRow - 1][stC - 1] : 0;
				ans = max (ans, thisMatrixSum);
			}
		}
		printf("%d\n", ans);
	}
}