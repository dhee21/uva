# include <iostream>
# include <stdio.h>
# include <string.h>
#include <string>
#include <math.h>
#include <utility>
using namespace std;
#define inf 1<<30
// #define lp(n) for (int i = 0; i < n; ++i) for (int j = 0; j < n; ++j)
// #define loop(n) for (int k = 0; k< n; ++k)
int f[255], dp[255][255], n;
int sum(int i, int j) {
	if (j < i) {
		return 0 ;
	}
	if (!i) {
		return f[j];
	}
	return f[j] - f[i - 1];
}

int matrix (int i, int j) {
	if (j <= i) {
		return 0;
	}
	if (dp[i][j] != -1) return dp[i][j];
	int ans = inf;
	for ( int k = i; k <= j; ++k) {
		ans = min (ans, matrix(i, k - 1) + matrix(k + 1, j) + sum(i, k - 1) + sum(k + 1, j));
	}
	dp[i][j] = ans;
	return dp[i][j];
}
int main () {
	// int n;
	while (scanf("%d", &n) != EOF) {
		memset(dp, -1, sizeof dp);
		for (int i = 0 ;i < n; ++i) {
			scanf("%d", &f[i]);
			if (i) { 
				f[i] += f[i - 1];
				// printf("%d\n",f[i] );
			}
		}
		// printf("%d\n", dp[0][45]);
		printf("%d\n", matrix(0, n - 1) );


	}
}