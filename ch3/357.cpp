#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;
long long int ways[30100];
#define lp(n) for (int i = 0; i < n; ++i) 
int main () {
	int num, den[5] = {1, 5, 10, 25, 50};
	memset(ways, 0, sizeof ways);
	ways[0] = 1;
	for ( int coin = 0; coin < 5 ; ++coin) {
		for (int sum = den[coin]; sum <= 30000; ++ sum) {
			ways[sum] += ways[sum - den[coin]];
		}
	}
	// lp(12){
	// 	printf("%lld\n", ways[i] );
	// }
	while (scanf("%d", &num) != EOF) {
		long long int ans = ways[num];
		if (ans == 1) {
			printf("There is only 1 way to produce %d cents change.\n", num );
		} else {
			printf("There are %lld ways to produce %d cents change.\n", ans, num );
		}
	}
	return 0;
}