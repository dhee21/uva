#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <string.h>
using namespace std;

// struct lis
// {
// 	/* data */
// 	int length, prev;
// }dp[100000];

int main () {
	int a[1000000], i = 0, m[1000000], mLen = 0, insertPos, prevSmall[1000000], index[1000000], lEnd;
	while (scanf("%d", &a[i]) == 1) {
		insertPos = lower_bound(m, m + mLen, a[i]) - m;
		m[insertPos] = a[i];
		if (insertPos  > 0) {
			prevSmall[i] = index[insertPos - 1];
		} else {
			prevSmall[i] = -1;
		}
		index[insertPos] = i;
		if (insertPos == mLen) {
			mLen++;
			// lEnd = i;
		}
		i++;
	}
	int ans[1000000], presentIndex = index[mLen - 1], indexAns = 0;;
	printf("%d\n-\n", mLen );
	while (presentIndex != -1) {
		// printf("%d\n", a[presentIndex] );
		ans[indexAns++] = a[presentIndex];
		presentIndex = prevSmall[presentIndex];
	}

	for(int i = indexAns - 1; i >= 0 ; --i) {
		printf("%d\n", ans[i] );
	}
	return 0;
}

// int main () {
// 	// printf("srf\n");
// 	int a[100000],ans[100000], ip, len = 0;
// 	while (scanf("%d", &ip) == 1) {
// 		a[len++] = ip;
// 		// printf("%d\n", ip );
// 	}
// 	// printf("%d\n", len );
// 	if (len) {
// 		dp[0].length = 1;
// 		dp[0].prev = -1;
// 		for (int i = 1; i < len; ++i) {
// 			dp[i].length = 1;
// 			dp[i].prev = -1;

// 			for (int j = 0; j < i; ++j) {
// 				if (a[j] < a[i]) {
// 					if (dp[j].length + 1 >= dp[i].length) {
// 						dp[i].length = dp[j].length + 1;
// 						dp[i].prev = j;
// 					}
// 				}
// 			}
// 		}
// 		int maxIndex, maxLen = 0;
// 		for ( int i = len -1; i >= 0; --i) {
// 			if (dp[i].length > maxLen) {
// 				maxIndex = i;
// 				maxLen = dp[i].length;
// 			}
// 		}
// 		int present = maxIndex, index = 0;
// 		while (present != -1) {
// 			// printf("%d->",present );
// 			ans[index++] = a[present];
// 			present = dp[present].prev;
// 		}
// 		// printf("\n");
// 		printf("%d\n-\n", maxLen );
// 		for (int i = index - 1; i >= 0; --i) {
// 			printf("%d\n", ans[i] );
// 		}
// 	}
// 	return 0;
// }