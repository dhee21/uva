#include <iostream>
// # include <bits/stdc++.h>
using namespace std;
int v[1010];
bool can (int containerVol, int n, int m) {
	int numVessels = 0, capLeft = containerVol;
	for (int i=0; i< n; ++i) {
		int nextVessel = v[i];
		// printf("%d %d %d\n", containerVol, capLeft,  nextVessel  );
		if (capLeft >= nextVessel) {
			capLeft -= nextVessel;
		} else {
			numVessels ++;
			capLeft = containerVol;
			i--;
			if (numVessels > m) {
				return false;
			}
		}
	}
	numVessels ++;
	if (numVessels > m) {
				return false;
	}
	// printf(" vessels = %d\n", numVessels );
	return true;
}

int main () {
	int n, m, maxV = 0, sigmaV = 0, ans;
	while (scanf("%d %d", &n, &m) != EOF ) {
		sigmaV = 0;
		maxV = 0;
		for ( int i = 0 ; i< n ; ++i ) {
			scanf("%d", &v[i]);
			maxV = max (maxV, v[i]);
			sigmaV += v[i];
		}
		int lo = maxV, hi = sigmaV;
		while (lo <= hi) {
			int mid  = (hi + lo) >> 1;
			// printf("mid = %d\n",mid );
			if (can(mid, n, m)) {
				ans = mid ;
				hi = mid - 1 ;
			} else {
				lo  = mid + 1;
			}
		}
		printf("%d\n", ans );
	}
}