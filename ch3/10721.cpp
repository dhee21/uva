# include <iostream>
# include <stdio.h>
# include <string.h>
#include <string>
#include <math.h>
#include <utility>
using namespace std;
#define inf 1<<30
int n, k, m;
long long int dp[55][55];

long long int barCodes(int i, int units) {
	// printf("%derg\n",m );
	if ( units < 0) {
		return 0;
	}
	if ((i == k) && units) {
		return 0;
	}
	if ((i == k) && units == 0) {
		return 1;
	}
	if (dp[i][units] != -1) {
		return dp[i][units];
	}
	int canAdd = m;
	long long int ways = 0;
	for (int bars = 1; bars <= canAdd; ++bars) {
		long long int x = barCodes(i + 1, units - bars);
		ways += x;
	}
	// printf("ways = %d\n", ways );
	dp[i][units] = ways;
	return dp[i][units];
}

int main () {
	while (scanf("%d %d %d", &n, &k, &m) != EOF) {
		memset(dp, -1, sizeof dp);
		printf("%lld\n", barCodes(0, n) );
	}
}