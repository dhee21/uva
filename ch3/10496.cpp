# include <iostream>
# include <stdio.h>
#include <math.h>
#include <utility>
using namespace std;
int x[14], y[14];
int dp[(1 << 11) + 100][14];
#define inf 1 << 30

int dist (int i, int j) {
	return abs(x[i] - x[j]) + abs(y[i] - y[j]) ;
}
int visitI(int node, int option) {
	option = option | (1 << node);
	return option;
}
bool visited(int option, int node) {
	option = option & (1 << node);
	return option != 0;
}
int main () {
	int n, t;
	scanf("%d", &t);
	while (t--) {
		int xSize, ySize, beepNum;
		scanf("%d %d", &xSize, &ySize);
		scanf("%d %d", &x[0], &y[0]);
		scanf("%d", &beepNum);
		for (int i = 1; i <= beepNum; ++i) {
			scanf("%d %d", &x[i], &y[i]);
		}
		n = beepNum + 1;
		int allSelectedOption = (1 << n) - 1;
		for (int node = 0; node < n; ++node) {
			dp[allSelectedOption][node] = dist(node, 0);
		}
		for (int option = allSelectedOption - 1; option >=1; option--) {
			for (int pN = 0; pN < n; ++pN) {
				if (!visited(option, pN)) {
					dp[option][pN] = inf;
				} else {
					int minCost = inf;
					for (int i = 0; i < n; ++i) {
						if (!visited(option, i)) {
							// option after visiting i
							int OAVI = visitI(i, option);
							int distance = dist(pN, i) + dp[OAVI][i];
							minCost = min(minCost, distance);
						}
					}
					dp[option][pN] = minCost;
				}
			}
		}
		int ans = dp[1][0];
		printf("The shortest path has length %d\n", ans);
	}
	return 0;
}