# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;

vvi charToFinger(150);
int fingerNotAvailable[20];
set<string> words;

bool canTypeChar (char c) {
	fr(i, 0, charToFinger[c].size()) {
		if (fingerNotAvailable[charToFinger[c][i]] == false) return true;
	}
	return false;
}

int main () {
	string lettersFingers[10] = {"qaz", "wsx", "edc", "rfvtgb", " ", " ", "yhnujm", "ik,", "ol.", "p;/"};
	fr (i, 0, 10) {
		fr (j, 0, lettersFingers[i].length()) {
			charToFinger[lettersFingers[i][j]].push_back(i + 1);
		}
	}
	int f, n;
	vs ans;
	char longWord[60];
	while (scanf("%d %d", &f, &n) != EOF) {
		ans.clear();
		words.clear();
		memset(fingerNotAvailable, 0, sizeof fingerNotAvailable);
		fr (i, 0, f) {
			int num;
			ipInt(num);
			fingerNotAvailable[num] = 1;
		}
		int bestLen = 0;
		fr (i, 0, n) {
			scanf("%s", longWord);
			if (words.count(longWord)) continue;
			words.insert(longWord);
			bool canBeTyped = true;
			int length = strlen(longWord);
			if (length < bestLen) continue;
			fr (i, 0, length) {
				canBeTyped = canBeTyped & canTypeChar(longWord[i]);
				if (!canBeTyped) break;
			}
			if (canBeTyped){
				if (length > bestLen) {
					ans.clear();
					bestLen = length;
				}
			 	ans.push_back(longWord);	
			}
		}
		sort(ans.begin(), ans.end());
		printf("%lu\n",ans.size());
		fr (i, 0, ans.size()) {
			printf("%s\n",ans[i].data() );
		}
	}
	return 0;
}