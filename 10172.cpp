#include <stdio.h>
#include <iostream>
#include <stack>
#include <queue>
#include <vector>
using namespace std ;

int loadTime=1, unloadTime = 1, moveTime = 2;

void unFillCargo(stack<int> &carrier, int &time, int &totalCargoes, int bCapacity, int onStation, vector<queue<int> > &stations) {
	// printf("fill %d\n", totalCargoes);
  		while(!carrier.empty()) {
  			if(carrier.top() == onStation) {
  			// printf(" yes %d %d %d\n", carrier.top() , onStation, totalCargoes);

  				carrier.pop();
  				totalCargoes--;
  				time += unloadTime;
  			}
  			else if (stations[onStation].size() < bCapacity) {
  				stations[onStation].push(carrier.top());
  				carrier.pop();
  				time += unloadTime;
  			}
  			else {
  				break;
  			}
  		}
}

void fillCargo(stack<int> &carrier, int &time, int &totalCargoes, int carrierCapacity, int onStation, vector<queue<int> > &stations) {
	// printf("unfill %d\n", totalCargoes);
	while(carrier.size() < carrierCapacity && !stations[onStation].empty()) {
		carrier.push(stations[onStation].front());
		stations[onStation].pop();
		time += loadTime;
	}
}

int main() {
	int set, numStations, carrierCapacity, bCapacity;
	scanf("%d", &set);
	while(set--) {
		int totalCargoes = 0;
		vector<queue<int> > stations(110);
		scanf("%d %d %d" , &numStations, &carrierCapacity, &bCapacity) ;
		// printf("%s\n", );
		for(int i=1 ; i<= numStations ; ++i) {
			int numCargo;
			scanf("%d", &numCargo);
			totalCargoes += numCargo;
			while(numCargo--) {
				int stId ;
				scanf("%d", &stId);
				// printf("%d", stId);
				stations[i].push(stId);
			}
		}
		int onStation = 1, time = 0;
		stack<int> carrier;
		while(totalCargoes) {
			unFillCargo(carrier, time, totalCargoes, bCapacity, onStation, stations);
			if(totalCargoes) {
				fillCargo(carrier, time, totalCargoes, carrierCapacity, onStation, stations);
				time += moveTime;
				onStation++;
				onStation = (onStation > numStations) ? 1 : onStation;
			}
		}
		printf("%d\n", time);
	}
	return 0;
}