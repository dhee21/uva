#include <algorithm>
#include <iostream>
#include <cstdio>
#include <vector>
#include <map>
#include <string.h>
#define  DFS_BLACK 1
#define  DFS_WHITE 0
#define  DFS_GRAY 2
# define MAX_NODES 110

using namespace std;
typedef vector<int> vi;

vector <vi> graph(MAX_NODES);
map<string, int> myMap;
string nameMap[MAX_NODES];
int visited[MAX_NODES], dfs_num[MAX_NODES], dfs_low[MAX_NODES], cutFromGraph[MAX_NODES], dfsNumberCounter, indexNameMap, nodes, edges ;
vi S;
void init () {
	// for (int i = 0; i < MAX_NODES; ++i) {
	// 	graph[i].clear();
	// }
	graph.clear();
	graph.resize(nodes);
	memset(visited, DFS_WHITE, sizeof visited);
	memset(dfs_num, 1 << 25, sizeof dfs_num);
	memset(dfs_low, 0, sizeof dfs_low);
	memset(cutFromGraph, 0, sizeof cutFromGraph);
	dfsNumberCounter = 0;
	myMap.clear();
	indexNameMap = 0;
	// S.clear();
}

void readGraph () {
	string str1 , str2;
	int node1, node2;
	for (int x = 0; x < edges; ++x) {
		cin>> str1>> str2;
		// cout << str1 << str2<<endl;
		if (myMap.count(str1) == 0) {
			nameMap[indexNameMap] = str1;
			myMap[str1] = indexNameMap++;
		}
		if (myMap.count(str2) == 0) {
			nameMap[indexNameMap] = str2;
			myMap[str2] = indexNameMap++;
		}
		node1 = myMap[str1];
		node2 = myMap[str2];

		graph[node1].push_back(node2);
	}
}

void tarjanSCC(int node) {
		dfs_num[node] = dfs_low[node] = dfsNumberCounter ++;
		visited[node] = DFS_GRAY;
		int leastArrivalTime = dfs_num[node];
		S.push_back(node);
		for (int x = 0; x < graph[node].size(); ++x) {
			if (visited[graph[node][x]] == DFS_WHITE) {
				tarjanSCC(graph[node][x]);
				leastArrivalTime = min (leastArrivalTime, dfs_low[graph[node][x]]);
			} else if (cutFromGraph[graph[node][x]] == 0) {
				leastArrivalTime = min( leastArrivalTime, dfs_num[ graph[node][x] ] );
			}
		}
		if (leastArrivalTime == dfs_num[node]) {
			while(1) {
				int v = S.back();
				S.pop_back();
            cutFromGraph[v] = 1;

				if (v == node) {
					printf("%s\n", nameMap[v].c_str());
					break;
				} else {
					printf("%s, ", nameMap[v].c_str() );
				}
			}
		}
		dfs_low[node] = leastArrivalTime;
		visited[node] = DFS_BLACK;
}

int main () {
	int casos = 0;
	while (scanf("%d %d", &nodes, &edges) && (nodes || edges)) {
		if (casos) {
			printf("\n");
		}
		casos++;
		init();
		readGraph();

        printf("Calling circles for data set %d:\n", casos);
		for (int i = 0; i < indexNameMap; ++i) {
			if (visited[i] == DFS_WHITE) {
				tarjanSCC(i);
			}
		}
	}
}
