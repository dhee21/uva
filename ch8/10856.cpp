# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 10000000
#define factorialTill 2703664

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// primefactorization and primeSieve variables
bitset<primesTill + 1> probablePrime; 
vi primeList, factors, exponents;
// primefactorization and primeSieve variables
// prob vars
int numPFs[factorialTill + 10];
// prob vars

void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	int goTill = (int)sqrt(primesTill);
	for (int i = 2; i <= goTill ; ++i) {
		if (probablePrime.test(i) == 1) {
			primeList.push_back(i);
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime.reset(j);
			}
		}
	}
	for (int i = goTill + 1; i <= primesTill; ++i) {
		if (probablePrime.test(i) == 1) primeList.push_back(i);
	}
}

void primeFactorization (lli N) {
	int PFIndex = 0;
	lli PF = primeList[0];
	while (PF * PF <= N && N != 1 && PFIndex < primeList.size()) {
		if (N % PF == 0) {
			numPFs[N] = 1 + numPFs[N / PF]; return;
		}
		PF = primeList[++PFIndex];
	}
	numPFs[N] = 1;
}

void calculateNumPFs () {
	numPFs[0] = 0; numPFs[1] = 0;
	for (lli i = 2; i <= factorialTill ; ++i) {
		primeFactorization(i);
	}
	fr (i, 2, factorialTill + 1) {
		numPFs[i] += numPFs[i - 1];
	}
}

int main () {
	primeSieve();
	calculateNumPFs();
	int n, caseNo = 0;
	while (ipInt(n) && n >= 0) {
		caseNo++;
		int x = lower_bound(numPFs, numPFs + factorialTill + 1, n) - numPFs;
		if (numPFs[x] != n) printf("Case %d: Not possible.\n", caseNo);
		else printf("Case %d: %d!\n",caseNo, x);
	}
	return 0;
}