# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define primesTill (1000000 - 1)

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition

int weight[50], n, sum[50];

int maxDiff (int a, int b, int c) { return max(abs(a - b), max(abs(b - c), abs(c - a))); }

int dp[35][700][700];
int maxWeight (int index, int w1, int w2) {
	if (index == n) {
		int sumVal = sum[index - 1];
		int w3 = sumVal - (w1 + w2);
		return maxDiff(w1, w2, w3);
	}
	if (dp[index][w1][w2] != -1) return dp[index][w1][w2];
	int minVal = maxWeight(index + 1, w1 + weight[index], w2);
	minVal = min (minVal, maxWeight(index + 1, w1, w2 + weight[index]));
	minVal = min(minVal, maxWeight(index + 1, w1, w2));
	return dp[index][w1][w2] = minVal;
}

int main () {
	int t, caseNo = 0; ipInt(t);
	while (t--) {
		ipInt(n);
		fr (i, 0, n) { scanf("%d", &weight[i]); sum[i] =  (i ? sum[i - 1] : 0) + weight[i]; }
		memset (dp, -1, sizeof dp);
		printf("Case %d: %d\n", ++caseNo, maxWeight(0, 0, 0) );
	}
	return 0;
}