# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define MAX_N 10001
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition

lli cubeVal (lli x) { return x * x * x; }

int main () {
	lli N;
	while (scanf("%lld", &N) && N) {
		bool exits = false;
		for (lli x = 2; ; ++x) {
			lli xCube = cubeVal(x), xMinusOneCube = cubeVal(x - 1);
			if (xCube - xMinusOneCube > N) { printf("No solution\n"); break;}
			fr (y, 1, x) if (xCube - cubeVal(y) == N) { printf("%lld %lld\n",x, y); exits = true; break; }
			if (exits) break;
		}
	}
	return 0;
}