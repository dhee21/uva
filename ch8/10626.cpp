# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define primesTill (1000000 - 1)

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition

int totalCokes, total, dp[160][110][55];

int minCoins (int cokesToBuy, int fives, int tens) {
	int ones = (total - (totalCokes - cokesToBuy) * 8) - (5 * fives + 10 * tens);
	if (cokesToBuy == 0) return 0;
	if (dp[cokesToBuy][fives][tens] != -1) return dp[cokesToBuy][fives][tens];
	int minVal  = inf;
	if (tens > 0) minVal = min (minVal, 1 + minCoins(cokesToBuy - 1, fives, tens - 1) );
	if (fives >= 2) minVal = min (minVal, 2 + minCoins(cokesToBuy - 1, fives - 2, tens));
	if (ones >= 8) minVal = min(minVal, 8 + minCoins(cokesToBuy - 1, fives, tens));
	if (fives >= 1 && ones >= 3) minVal = min(minVal, 4 + minCoins(cokesToBuy - 1, fives - 1, tens));
	if (ones >= 13) minVal = min (minVal, 13 + minCoins(cokesToBuy - 1, fives + 1, tens));
	if (tens > 0 && ones>= 3) minVal = min (minVal, 4 + minCoins(cokesToBuy - 1, fives + 1, tens - 1));
	return dp[cokesToBuy][fives][tens] = minVal;
}


int main () {
	int t; ipInt(t);
	while (t--) {
		int ones, fives, tens;
		memset(dp, -1, sizeof dp);
		scanf("%d %d %d %d", &totalCokes, &ones, &fives, &tens);
		total = ones + 5 * fives + 10 * tens;
		printf("%d\n",minCoins(totalCokes, fives, tens) );
	}
	return 0;
}