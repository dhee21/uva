# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define primesTill (1000000 - 1)

//typedefinitions

void bitRep(int x) {
	while (x) {
		if (x&1) printf("1"); else printf("0");
		x >>= 1;
	}
}

int main () {
	int s = (1 << 5) - 3;
	bitRep(s); 
	printf("\n");
    for (int s0 = (s - 1)&s; s0 > 0; s0 = (s0 - 1)&s) { 
        int s1 = (s^s0);  

    	bitRep(s0); printf("    "); bitRep(s1);
    	printf("\n");
    }

	return 0;
}