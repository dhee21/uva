# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition

int gcd (int a, int b) { return (b == 0) ? a : gcd (b, a % b); }
int LCM (int a, int b) { return a * (b / gcd(a, b)); }
int findLCM (int a, int b, int c, int d) { return LCM(LCM(LCM(a, b), c), d); }

int coins[60];

int main () {
	int n, t;
	while (scanf("%d %d", &n, &t) && (n || t)) {
		fr (i, 0, n) ipInt(coins[i]);
		fr (i, 0, t) {
			int length; ipInt(length);
			int less = -1, more = inf;
			fr (i, 0, n) fr (j, i + 1, n) fr (k, j + 1, n) fr (l, k + 1, n) {
				int lcm = findLCM(coins[i], coins[j], coins[k], coins[l]);
				if (length % lcm == 0) { less = length; more = length; }
				else {
					int quot = length / lcm;
					less = max(less, lcm * quot); more = min(more, lcm * (quot + 1));
				}
			}
			printf("%d %d\n", less, more);
		}
	}
	return 0;
}