# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_NODES 110

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pair<int, pii> > vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int m, n, source, dest, dist[MAX_NODES], parent[MAX_NODES], maxTemp;
matrix adjMat(MAX_NODES);

void makeEdge (int from, int to, pii weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}
void initialize () {
	fr (i, 0, MAX_NODES) adjMat[i].clear();
}

void djakstra (int tempBound) {
	priority_queue<pii> pq;
	memset(parent, -1, sizeof parent);
	fr (i, 0, MAX_NODES) dist[i] = inf;
	dist[source] = 0;
	pq.push(pii(0, -source));
	while (!pq.empty()) {
		pii minUpperbound = pq.top(); pq.pop();
		int node = -minUpperbound.second, distNode = -minUpperbound.first;
		if (dist[node] == distNode) {
			int neightbours = adjMat[node].size();
			fr (i, 0, neightbours) {
				int edgeToNode = adjMat[node][i].first, edgeLen = adjMat[node][i].second.first;
				if (adjMat[node][i].second.second > tempBound) continue;
				if (distNode + edgeLen < dist[edgeToNode]) {
					dist[edgeToNode] = distNode + edgeLen;
					parent[edgeToNode] = node;
					pq.push(pii(-dist[edgeToNode], -edgeToNode));
				}
			}
		}
	}
}

int main () {
	while (scanf("%d %d", &n, &m) != EOF) {
		initialize();
		ipInt(source); ipInt(dest);
		fr (i ,0, m) {
			int from, to, rA, rB, dA, dB;
			scanf("%d %d %d.%d %d.%d", &from, &to, &rA, &rB, &dA, &dB);
			// temp[from][to] = temp[to][from] = rA * 10 + rB;
			makeEdge(from, to, pii(dA * 10 + dB, rA * 10 + rB));
		}
		int lo = 0, hi = 500;
		int ansTemp = 501;
		while (hi >= lo) {
			int mid = (hi + lo) >> 1;
			djakstra(mid);
			if (dist[dest] != inf) {
				ansTemp = mid;
				hi = mid - 1;
			}
			else lo = mid + 1;
		}
		djakstra(ansTemp);
		int ansPathLen = dist[dest];
		int node = dest;
		vi ansNodes;
		while (node != -1) { ansNodes.push_back(node); node = parent[node]; }
		for (int i = ansNodes.size() - 1; i >= 0; --i) {
			i ? printf("%d ", ansNodes[i]) : printf("%d\n", ansNodes[i]);;
		}
		printf("%d.%d %d.%d\n", ansPathLen / 10, ansPathLen % 10, ansTemp / 10, ansTemp % 10 );
	}
	return 0;
}