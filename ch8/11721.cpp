# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define MAX_NODES 1100
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition

map <int, int> pointsThatCanReach, SCCsReachingNegativeCycle;;
int m, n, visited[MAX_NODES], cutFromGraph[MAX_NODES],arr[MAX_NODES], smallestArrivalTime[MAX_NODES], sccNum[MAX_NODES]
		,timeCounter, numSCC, dist[MAX_NODES], ansNodes[MAX_NODES];
int  adjacency[MAX_NODES][MAX_NODES], visitedSCC[MAX_NODES], nodesInSCC[MAX_NODES], dp[MAX_NODES];
vvi nodeValuesInSCC(MAX_NODES), incomingSCCs(MAX_NODES);
matrix adjMat(MAX_NODES), adjMatSCC(MAX_NODES);
vi travelledNodes;
vi topSortScc, components;

void initialize () {
	fr (i, 0, MAX_NODES) { adjMat[i].clear(); adjMatSCC[i].clear(); nodeValuesInSCC[i].clear(); incomingSCCs[i].clear(); }
	memset (visited, 0, sizeof visited);
	memset (visitedSCC, 0, sizeof visitedSCC);
	memset (cutFromGraph, 0, sizeof cutFromGraph);
	memset (adjacency, 0, sizeof adjacency);
	memset (dp, -1, sizeof dp);
	travelledNodes.clear();
	timeCounter = 0;
	numSCC = 0;
	topSortScc.clear();
	nodeValuesInSCC.clear();
	pointsThatCanReach.clear();
	SCCsReachingNegativeCycle.clear();
	components.clear();
}

void makeEdge (int from, int to, int weight) {
	adjMat[from].push_back(pii(to, weight));
}

void printSCC (int node) {
	int numNodes = 0;
	while (1) {
		int v = travelledNodes.back();
		travelledNodes.pop_back();
		sccNum[v] = numSCC;
		nodeValuesInSCC[numSCC].push_back(v);
		cutFromGraph[v] = 1;
		numNodes++;
		if (v == node) break;
	}
	nodesInSCC[numSCC] = numNodes;
	numSCC++;
}

void dfs(int node) {
	visited[node] = true;
	arr[node] = timeCounter++;
	travelledNodes.push_back(node);
	int minArrivalTime  = arr[node];
	int neighbours = adjMat[node].size();
	fr (i, 0, neighbours) {
		int edgeToNode = adjMat[node][i].first;
		if (visited[edgeToNode] && !cutFromGraph[edgeToNode]) {
			minArrivalTime = min (minArrivalTime, arr[edgeToNode]);
		} else if (visited[edgeToNode] == DFS_WHITE) {
			dfs(edgeToNode);
			minArrivalTime = min (minArrivalTime, smallestArrivalTime[edgeToNode]);
		}
	}
	if (minArrivalTime == arr[node]) printSCC(node);
	smallestArrivalTime[node] = minArrivalTime;
}

void makeEdgeSCCGraph (int from, int to) { adjMatSCC[from].push_back(pii(to, 0)); adjacency[from][to] = 1; }

void makeNewGraph () {
	fr (i, 0, n) fr (j, 0, adjMat[i].size()) {
		int edgeToNode = adjMat[i][j].first;
		int toNodeNum = sccNum[edgeToNode], fromNodeNum = sccNum[i];
		if (fromNodeNum != toNodeNum && adjacency[fromNodeNum][toNodeNum] == 0) makeEdgeSCCGraph(fromNodeNum, toNodeNum);
	}
}

void dfsSccGraph (int node) {
	visitedSCC[node] = true;
	int neighbours = adjMatSCC[node].size();
	fr (i, 0, neighbours) {
		int edgeToNode = adjMatSCC[node][i].first;
		if (!visitedSCC[edgeToNode]) dfsSccGraph(edgeToNode);
	}
	topSortScc.push_back(node);
}

void createDAG () {
	fr (i, 0, n) if (!visited[i]) { dfs(i); components.push_back(i); }
	makeNewGraph();
}
void getIncomingSCCs () {
	fr (i, 0, numSCC) if (!visitedSCC[i]) dfsSccGraph(i);
	for (int i = topSortScc.size() - 1; i >= 0 ; --i) {
		int SCCNode = topSortScc[i];
		int neighbours = adjMatSCC[SCCNode].size();
		fr (j, 0, neighbours) {
			int edgeToNode = adjMatSCC[SCCNode][j].first;
			incomingSCCs[edgeToNode].push_back(SCCNode);
			fr (k, 0, incomingSCCs[SCCNode].size()) incomingSCCs[edgeToNode].push_back(incomingSCCs[SCCNode][k]);
		}
	}
}

void storeNodesReachingSCC (int scc) {
	if (SCCsReachingNegativeCycle.count(scc) == 0) {
		SCCsReachingNegativeCycle[scc] = 1;
		fr (i, 0, incomingSCCs[scc].size()) {
			int sccReachingHere = incomingSCCs[scc][i];
			SCCsReachingNegativeCycle[sccReachingHere] = 1;
		}
	}
}
// store the sccs reaching a negative cycle in SCCsReachingNegativeCycle
void bellmenFord (int scc) {
	int size = nodeValuesInSCC[scc].size();
	fr (i, 0, size) dist[nodeValuesInSCC[scc][i]] = inf;	
	dist[nodeValuesInSCC[scc][0]] = 0;
	fr (times, 0, size - 1) fr (nodeIndex, 0, size) {
		int node = nodeValuesInSCC[scc][nodeIndex];
		int neighbours = adjMat[node].size();
		if (dist[node] != inf) fr (i, 0, neighbours) {
			int edgeToNode = adjMat[node][i].first, weight = adjMat[node][i].second;
			if (sccNum[edgeToNode] == scc && dist[node] + weight < dist[edgeToNode]) dist[edgeToNode] = dist[node] + weight;
		}
	}
	// fr (i, 0, n) printf("%d ",dist[i] );
	// printf("\n");
	fr (nodeIndex, 0, size) {
		int node = nodeValuesInSCC[scc][nodeIndex];
		int neighbours = adjMat[node].size();
		if (dist[node] != inf) fr (i, 0, neighbours) {
			int edgeToNode = adjMat[node][i].first, weight = adjMat[node][i].second;
			if ( sccNum[edgeToNode] == scc && dist[node] + weight < dist[edgeToNode]) {
				storeNodesReachingSCC(sccNum[node]);
				// dist[edgeToNode] = dist[node] + weight;
			}
		}
	}
}

int main () {
	int t, caseNo = 0; ipInt(t);
	while (t--) {
		caseNo++;
		ipInt(n); ipInt(m);
		initialize();
		fr (i, 0, m) {
			int from, to, weight;
			ipInt(from); ipInt(to); ipInt(weight);
			makeEdge(from, to, weight);
		}
		createDAG();
		getIncomingSCCs();
		fr (i, 0, numSCC) bellmenFord(i);
		int ansNodesNum = 0;
		map<int, int> :: iterator it = SCCsReachingNegativeCycle.begin(), itEnd = SCCsReachingNegativeCycle.end();
		for (; it != itEnd; ++it) {
			int scc = it -> first;
			fr (i, 0, nodeValuesInSCC[scc].size()) ansNodes[ansNodesNum++] = nodeValuesInSCC[scc][i];
		}
		sort(ansNodes, ansNodes + ansNodesNum);
		printf("Case %d:", caseNo);
		if (ansNodesNum) fr (i, 0, ansNodesNum) printf(" %d",ansNodes[i]); else printf(" impossible");
		printf("\n");
	}
	return 0;
}