# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<29) - 1
#define KNIGHT 'k'

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitio

char grid[10][10];
int dist[10][10];
int distVal[10][10], dp[1 << 10][10], players;
int dir[8][2] = { {-2, -1}, {-2, 1}, {2, -1} ,{2, 1}, {-1, -2}, {1, -2}, {-1, 2} ,{1, 2} };
bool isNode (char c) { return c >= '0' && c <= '9'; }
void init () {
	fr (i, 0, 10) fr (j, 0, 10) distVal[i][j] = inf;
}
bool visited (int option, int i) {
	return option & (1 << i);
}
int visitI (int option, int i) {
	return option | (1 << i);
}
int add (int a, int b) {
	if (a == inf || b == inf) return inf;
	return a + b;
}
bool canGo (int r, int c) { return r >= 0 && c >= 0 && r < 8 && c < 8 && (isNode(grid[r][c]) || grid[r][c] == '.') ;}
void bfs (int x, int y) {
	fr (i, 0, 10) fr (j, 0, 10) dist[i][j] = inf;
	queue<pii> q;
	q.push(pii(x, y)); dist[x][y] = 0;
	while (!q.empty()) {
		pii visitedNode = q.front(); q.pop();
		int row = visitedNode.first, col = visitedNode.second;
		fr (i, 0, 8) {
			int r = row + dir[i][0], c = col + dir[i][1];
			if (canGo(r, c) && dist[r][c] == inf) {
				dist[r][c] = dist[row][col] + 1;
				if (isNode(grid[r][c])) distVal[grid[x][y] - '0'][grid[r][c] - '0'] = dist[r][c];
				q.push(pii(r, c));
			}
		}
	}
}
int main () {
	int t; ipInt(t);
	while (t--) {
		init();
		int n; scanf("%d", &n);
		players = 1;
		fr (i, 0, 8){
			getchar();
			fr (j, 0, 8) {
				scanf("%c", &grid[i][j]);
				if (grid[i][j] == KNIGHT) grid[i][j] = '0';
				else if (grid[i][j] == 'P') grid[i][j] = '0' + players++;
			}
		}
		// fr (i, 0, 8) printf("%s\n",grid[i]);
		fr (i, 0, 8) fr (j, 0, 8) if (isNode(grid[i][j])) bfs(i, j);
		int allSelectedOption = (1 << players) - 1;
		fr (lastAt, 0, players) dp[allSelectedOption][lastAt] = 0;
		// fr (i, 0, players) fr (j, 0, players) printf("%lld - %lld = %d\n",i, j, distVal[i][j] );

		for (int option = allSelectedOption - 1; option >= 1; --option) fr (p, 0, players) {
				dp[option][p] = inf;
				if (visited(option , p)) fr (i, 0, players) if (!visited(option, i)) {
						int OAVI = visitI(option, i);
						dp[option][p] = min (dp[option][p], add(distVal[p][i] , dp[OAVI][i]));
					}
		}
		// printf("%d\n",dp[1][0] );
		if (dp[1][0] > n) printf("No\n"); else printf("Yes\n");
	}
	return 0;
}