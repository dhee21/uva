# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

pii intervals[10];
int minTime, maxTime, n;

bool schedulePossible (int minGap) {
	int prevFlight = intervals[0].first;
	fr (i, 1, n) {
		if (prevFlight + minGap <= intervals[i].second) prevFlight = max( prevFlight + minGap, intervals[i].first );
		else return false;
	}
	return true;
}

int main () {
	int caseNo = 0;
	while (ipInt(n) && n) {
		 minTime = inf; maxTime = -1;
		fr (i, 0, n) {
			scanf("%d %d", &intervals[i].first, &intervals[i].second);
			intervals[i].first *= 600; intervals[i].second *= 600;
			minTime = min(minTime, intervals[i].first);
			maxTime = max(maxTime, intervals[i].second);
		}
		int maxOfMinGap = -1;
		sort (intervals, intervals + n);
		do {
			int lo = 0, hi = maxTime - minTime, ans = 0;
			while (lo <= hi) {
				int mid = (lo + hi) >> 1;
				if (schedulePossible(mid)) { ans = mid; lo = mid + 1;
				} else hi = mid - 1;
			}
			maxOfMinGap = max (maxOfMinGap, ans);
		} while (next_permutation(intervals, intervals + n));
		int minutes = maxOfMinGap / 600, sec = ((maxOfMinGap % 600) + 5) / 10;  ;
		if (sec < 10) printf("Case %d: %d:0%d\n", ++caseNo, minutes, sec );
		else printf("Case %d: %d:%d\n", ++caseNo, minutes, sec);
	}
	return 0;
}