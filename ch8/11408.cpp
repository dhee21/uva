# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 5000000
#define sumTill 5000000

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// primefactorization and primeSieve variables
vi primeList;
// primefactorization and primeSieve variables
// prob vars
lli sumPFs[sumTill + 10], maxSum, dePrimes[sumTill + 10];
// prob vars

void primeSieve () {
	memset (sumPFs, 0, sizeof sumPFs);
	for (int i = 2; i <= primesTill ; ++i) {
		if (sumPFs[i] == 0) {
			sumPFs[i] = i;
			for (int j = i + i; j <= primesTill; j += i) {
				sumPFs[j] += i;
			}
		}
	}
}

bool isPrime (lli N) {
	return sumPFs[N] == N;
}

void calculatedePrimes () {
	dePrimes[0] = dePrimes[1] = 0;
	fr (i, 2, sumTill + 1) dePrimes[i] = isPrime(sumPFs[i]) ? dePrimes[i - 1] + 1 : dePrimes[i - 1];
}

int main () {
	primeSieve();
	calculatedePrimes();
	int a, b;
	while (ipInt(a) && a) {
		ipInt(b);
		printf("%lld\n", dePrimes[b] - dePrimes[a - 1] );
	}
	return 0;
}