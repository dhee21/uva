# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_NODES 110
#define EPS 1e-9

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int owner[MAX_NODES], indexNodes ;
bitset <MAX_NODES> visited;
vi red, blue;
pii nodes[MAX_NODES];

int makeNode (int x, int y) {
	nodes[indexNodes] = pii(x, y);
	return indexNodes++;
}

double dist(int left, int right) {
	pii leftP = nodes[left], rightP = nodes[right];
	return hypot(leftP.first - rightP.first, leftP.second - rightP.second);
}

bool augmentingPath (int left, int len) {
	if (visited[left]) return false;
	visited[left] = true;
	fr (i, 0, blue.size()) if ((double)len - dist(left, blue[i]) > -EPS) {
		int right = blue[i];
		if (owner[right] == -1 || augmentingPath(owner[right], len)) {
			owner[right] = left; return 1;
		}
	}
	return 0;
}

int MCBM (int len) {
	memset (owner, -1, sizeof owner);
	int pairsMade = 0;
	fr (i, 0, red.size()) {
		visited.reset();
		pairsMade += augmentingPath(red[i], len);
	}
	return pairsMade;
}

void initialize () {
	red.clear(); blue.clear();
	indexNodes = 0;
}

int main () {
	int t;
	ipInt(t);
	while (t--) {
		initialize();
		int P, k;
		scanf("%d %d", &P, &k);
		fr (i, 0, P) {
			int x, y;
			char color[10];
			scanf("%d %d %s", &x, &y, color);
			if (strcmp(color, "red") == 0) red.push_back(makeNode(x, y)); else blue.push_back(makeNode(x, y));
		}
		int hi = (int)hypot(2000, 2000) + 1, lo = 0;
		int ans = inf;
		while (hi >= lo) {
			int mid = (hi + lo) >> 1;
			int chaines = MCBM(mid);
			// printf("%d %d\n",mid, chaines );
			if (chaines >= k) {
				ans = mid;
				hi = mid - 1;
			} else lo = mid + 1;
		}
		if (ans == inf) printf("Impossible\n"); else printf("%d\n",ans);
	}
	return 0;
}