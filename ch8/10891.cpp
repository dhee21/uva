# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%lld", &x)
// #define inf ~(1 << 31)

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

lli numbers[110], dp[110][110], inf = numeric_limits<lli> :: max();


lli sum (int i, int j) {
	if (i <= j) return numbers[j] - (i ? numbers[i - 1] : 0);
	return 0;
}

void findState (int i, int j) {
	lli maxVal = -inf;
	fr (k, i, j + 1) maxVal = max(maxVal, sum(i, j) - (k + 1 <= j ? dp[k + 1][j] : 0) );
	for (int k = j; k >= i; --k) maxVal = max (maxVal, sum(i, j) - (i <= k - 1 ? dp[i][k - 1] : 0));
	dp[i][j] = maxVal;
}

int main () {
	lli n;
	while (ipInt(n) && n) {
		fr (i, 0, n) { ipInt(numbers[i]); if (i) numbers[i] += numbers[i - 1]; }
		fr (i, 0, n) dp[i][i] = numbers[i] - (i ? numbers[i - 1] : 0);
		fr (length, 2, n + 1) fr (st, 0, n - length + 1) {
			int ed = st + length - 1;
			findState(st, ed);
		}
		printf("%lld\n",2 * dp[0][n - 1] - numbers[n - 1]);
	}
	return 0;
}