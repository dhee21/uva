# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
# define FactorsTill 1010
# define primesTill 40
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition
vi primeList;
vector<vi> pFactors(FactorsTill + 1);
bitset<primesTill + 1> probablePrime;
bitset<FactorsTill + 1> visited;
int distVal[FactorsTill + 1];
void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	int goTill = (int)sqrt(primesTill) ;
	fr (i, 2, goTill + 1) if (probablePrime.test(i) == true) {
		primeList.push_back(i);
		for (int j = i + i; j <= primesTill; j += i) probablePrime.reset(j);
	}
	fr (i, goTill + 1, primesTill + 1) if (probablePrime.test(i) == true) primeList.push_back(i);
}

void primeFactorization (int N, vi &factors) {
	int PF = primeList[0], PFIndex = 0;
	while (PF * PF <= N && N != 1 && PFIndex < primeList.size()) {
		if (N % PF == 0) {
			while (N % PF == 0) N /= PF;
			factors.push_back(PF);
		}
		PF = primeList[++PFIndex];
	}
	if (N != 1) factors.push_back(N);
}

void calculatePFs () {
	fr (i, 2, FactorsTill + 1) primeFactorization(i, pFactors[i]);
}

int bfs (int from, int to) {
	visited.reset();
	memset(distVal, 0, sizeof distVal);
	if (to == from) return 0;
	if (from == 1 || to < from) return -1;
	queue <int> q;
	q.push(from); visited.set(from);
	distVal[from] = 0;
	while (!q.empty()) {
		int num = q.front(); q.pop();
		if (num > to) continue;
		if (num == to) return distVal[num];
		if (!(pFactors[num].size() == 1 && pFactors[num][0] == num)) fr (i, 0, pFactors[num].size()) {
			if (num + pFactors[num][i] <= to && !visited.test( num + pFactors[num][i] ) == true) {
				visited.set(num + pFactors[num][i]);
				q.push(num + pFactors[num][i]);
				distVal[num + pFactors[num][i]] = distVal[num] + 1;
			}
		}
	}
	return -1;
}

int main () {
	primeSieve();
	calculatePFs();
	int S, T, caseNo = 0;
	while (scanf("%d %d", &S, &T) && (S || T)) {
		printf ( "Case %d: %d\n", ++caseNo , bfs(S, T));
	}
	return 0;
}