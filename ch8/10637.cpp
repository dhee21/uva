# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition

int ans[110];

int gcd (int a, int b) { return b == 0 ? a : gcd(b, a % b); }

void breakNum (int num, int minVal, int coPrimeWith, int length, int index) {
	if (num < 0) return;
	if (!num && length) return;
	if (!length && num) return;
	if (!length && !num) fr (i, 0, index) { if (i == index - 1) printf("%d\n",ans[i]); else printf("%d ", ans[i]); }
	fr (n, minVal, num + 1) if (gcd(coPrimeWith, n) == 1) { ans[index] = n; breakNum(num - n, n, coPrimeWith * n, length - 1, index + 1); }
}

int main () {
	int tc, caseNo = 0; ipInt(tc);
	while (tc--) {
		int S, t;
		ipInt(S); ipInt(t);
		printf("Case %d:\n", ++caseNo );
		breakNum(S, 1, 1, t, 0);
	}
	return 0;
}