# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1

# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int p[510], m, k, books[510];
bool isPossible (lli maxPages) {
	lli bookIndex = 0;
	fr(scribers, 0, k) {
		lli sumPages = 0;
		for (; bookIndex < m && sumPages + p[bookIndex] <= maxPages; ++bookIndex) sumPages += p[bookIndex];
	}
	if (bookIndex != m) return false;
	return true;
}
void printAns (lli maxPages) {
	lli bookIndex = m - 1;
	for(int scribers = k - 1; scribers >= 0; --scribers) {
		lli sumPages = 0;
		for (; bookIndex >= 0  && sumPages + p[bookIndex] <= maxPages && bookIndex >= scribers ; --bookIndex) {
			books[bookIndex] = scribers;
			sumPages += p[bookIndex];
		}
	}
	bookIndex = 0;
	int scribers = 0;
	while (scribers < k) {
		bool first = true;
		for (; scribers == books[bookIndex] && bookIndex < m ; ++bookIndex) {
			if (first) { printf("%d",p[bookIndex]); first = false; } else printf(" %d",p[bookIndex]); 
		}
		++scribers;
		if (scribers < k) printf(" / ");
	}
	printf("\n");
}

int main () {
	int n;
	ipInt(n);
	while (n--) {
		ipInt(m); ipInt(k);
		int maxVal = 0;
		lli sumPages = 0;
		fr (i, 0, m) {
			ipInt(p[i]);
			maxVal = max (maxVal, p[i]);
			sumPages += p[i];
		}
		lli hi = sumPages, lo = maxVal, ans = 0;
		while (hi >= lo) {
			lli mid = (hi + lo) >> 1;
			if (isPossible(mid)) {
				ans = mid;
				hi = mid - 1;
			} else {
				lo = mid + 1;
			}
		}
		printAns(ans);
	}
	return 0;
}