# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define primesTill (1000000 - 1)
#define maxFs 50
#define valuesInRow 4

struct state {
	int board[16], emptyTile;
};

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition
string ansSteps, steps;
int iterativeFs, newFs;
state finalState;
int f (int gS, int hS) {
	return gS + hS;
}

bool solve (state&, int, char);

bool isSame (state &one , state &two) {
	fr (i, 0, 16) if (one.board[i] != two.board[i]) return false;
	return true;
}

pii numToRowCol(int num) {
	int row = num / valuesInRow;
	int col = num % valuesInRow;
	return pii (row, col);
}
int rowColToNum (pii rowCol) {
	int row = rowCol.first, col = rowCol.second;
	return (row * valuesInRow + col);
}
int dist(pii one, pii two) { return abs(one.first - two.first) + abs(one.second - two.second); }
int h (state &inputState) {
	int hSVal = 0;
	fr (i, 0, 16) {
		int tileVal = inputState.board[i];
		hSVal += tileVal ? dist(numToRowCol(i), numToRowCol(tileVal - 1)) : 0;
	}
	return hSVal;
}
bool canGo (char direction, state &presentState) {
	int emptyTilePosition = presentState.emptyTile;
	pii rowCol = numToRowCol(emptyTilePosition);
	int r = rowCol.first, c = rowCol.second;
	direction == 'L' ? --c : direction == 'R' ? ++c : direction == 'U' ? --r : ++r;
	return (r >= 0 && c >= 0 && r < 4 && c < 4);
}
void goLeft (state &presentState) {
	int emptyTilePosition = presentState.emptyTile;
	int leftVal = presentState.board[emptyTilePosition - 1];
	presentState.board[emptyTilePosition] = leftVal;
	presentState.board[emptyTilePosition - 1] = 0;
	presentState.emptyTile = emptyTilePosition - 1;
}
void goRight (state &presentState) {
	int emptyTilePosition = presentState.emptyTile;
	state newState = presentState;
	int rightVal = presentState.board[emptyTilePosition + 1];
	presentState.board[emptyTilePosition] = rightVal;
	presentState.board[emptyTilePosition + 1] = 0;
	presentState.emptyTile = emptyTilePosition + 1;
}
void goUp (state &presentState) {
	int emptyTilePosition = presentState.emptyTile;
	pii rowCol = numToRowCol(emptyTilePosition);
	rowCol.first--;
	int upPosition = rowColToNum(rowCol);
	int upVal = presentState.board[upPosition];
	presentState.board[upPosition] = 0; presentState.board[emptyTilePosition] = upVal;
	presentState.emptyTile = upPosition;
}
void goDown (state &presentState) {
	int emptyTilePosition = presentState.emptyTile;
	pii rowCol = numToRowCol(emptyTilePosition);
	rowCol.first++;
	int downPosition = rowColToNum(rowCol);
	int downVal = presentState.board[downPosition];
	presentState.board[downPosition] = 0; presentState.board[emptyTilePosition] = downVal;
	presentState.emptyTile = downPosition;
}

bool tryGoing (char direction, state &presentState, int gS) {
	if (canGo(direction, presentState)) {
		steps.push_back(direction);
		direction == 'L' ? goLeft(presentState): direction == 'R' ?
			goRight(presentState) : direction == 'U' ? goUp(presentState) : goDown(presentState);
		int solved = solve( presentState, gS + 1, direction);
		direction == 'L' ? goRight(presentState): direction == 'R' ?
			goLeft(presentState) : direction == 'U' ? goDown(presentState) : goUp(presentState);
		steps.pop_back();
		if (solved) return true;
	}
	return false;
}
// gS is g(s) i.e steps taken from root to reach here
bool solve (state &presentState, int gS, char fromDirection) {
	int hS = h(presentState);
	int fS = f(gS, hS);
	if (fS > iterativeFs) { newFs = min(newFs, fS); return false; }
	if (hS == 0) { ansSteps = steps; return true; }
	bool solved;
	solved = (fromDirection != 'R') ? tryGoing('L', presentState, gS) : false; if (solved) return true;
	solved = (fromDirection != 'L') ? tryGoing('R', presentState, gS) : false; if (solved) return true;
	solved = (fromDirection != 'D') ? tryGoing('U', presentState, gS) : false; if (solved) return true;
	solved = (fromDirection != 'U') ? tryGoing('D', presentState, gS) : false; if (solved) return true;
	return false;
}
int solvable (state &initState) {
	int inversions = 0, row;
	fr (i, 0, 16) if (initState.board[i]) fr (j, i + 1, 16) {
		if (initState.board[j] && initState.board[j] < initState.board[i]) ++inversions;
	} else {
		row = numToRowCol(i).first + 1;
	}
	return ((inversions + row) & 1) == 0;
}
int main () {
	// finalState 
	fr (i, 0, 15) finalState.board[i] = i + 1;
	finalState.board[15] = 0;
	finalState.emptyTile = 15;
	// finalState 

	int t; ipInt(t);
	while (t--) {
		state initState ;
		fr (i, 0, 16) { ipInt(initState.board[i]); if (initState.board[i] == 0) initState.emptyTile = i; }
		if (!solvable(initState)) { printf("This puzzle is not solvable.\n"); continue; }
		bool solved = false;
		for (iterativeFs = h(initState); iterativeFs <= maxFs;) {
			steps.clear();
			newFs = inf;
			solved = solve(initState, 0 ,'N') ;
			// printf("%d %d\n",solved, iterativeFs );
			if (solved) break;
			iterativeFs = newFs;
		}
		printf("%s\n", ansSteps.data());
	}
	return 0;
}