# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_NODES 400
#define MAX_EDGES 1100

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int n, m, d;
// MAxFlow variables
int cap[MAX_NODES][MAX_NODES], flow[MAX_NODES][MAX_NODES], cost[MAX_NODES][MAX_NODES], flowToIncrease, maxFlow;
vi bfsParent;
bitset<MAX_NODES> visited;
// MAxFlow variables
int costFlight[MAX_EDGES];
matrix adjMat(MAX_NODES);

void makeEdge (int from, int to, int capacity, int costVal) {
	cap[from][to] = capacity; cost[from][to] = costVal;
	if (cap[to][from] == 0) {
		adjMat[from].push_back(pii(to, 0));
		adjMat[to].push_back(pii(from, 0));
	}
}

int calculateNode (int city, int day) {
	int start = 1 + (d + 2) * (city - 1);
	return start + day + 1;
}

int resCapacity (int from, int to) {
	return cap[from][to] - flow[from][to];
}

void augmentFlow (int s, int t) {
	if (!visited[t]) return;
	int minResCap = inf;
	int node = t;
	while (bfsParent[node] != -1) {
		int parent = bfsParent[node];
		minResCap = min (minResCap, resCapacity(parent, node));
		node = bfsParent[node];
	}
	node = t ;
	while (bfsParent[node] != -1) {
		int parent = bfsParent[node];
		flow[parent][node] += minResCap;
		flow[node][parent] -= minResCap;
		node = bfsParent[node];
	}
	flowToIncrease = minResCap;
}

void bfs (int s, int t, int maxCost) {
	visited.reset();
	bfsParent.assign(MAX_EDGES, -1);
	queue<int> q;
	q.push(s);
	visited.set(s);
	while (!q.empty()) {
		int visitedNode = q.front(); q.pop();
		if (visitedNode == t) break;
		int neighbours = adjMat[visitedNode].size();
		fr (i, 0, neighbours) {
			int edgeToNode = adjMat[visitedNode][i].first;
			if (resCapacity(visitedNode, edgeToNode)  && visited.test(edgeToNode) == 0 && cost[visitedNode][edgeToNode] <= maxCost ) {
				visited.set(edgeToNode); 
				bfsParent[edgeToNode] = visitedNode;
				q.push(edgeToNode);
			}
		}
	}
}

void runMaxFlow (int s, int t, int maxCost) {
	maxFlow = 0;
	while (true) {
		flowToIncrease = 0;
		bfs(s, t, maxCost);
		augmentFlow(s, t);
		if (flowToIncrease == 0) break;
		maxFlow += flowToIncrease;
	}
}

void initialize () {
	fr (i, 0, MAX_NODES) adjMat[i].clear();
	memset (cap, 0, sizeof cap);
	memset (cost, 0, sizeof cost);
}

int main () {
	int cases, caseNo = 0; ipInt(cases);
	while (cases--) {
		caseNo++; initialize();
		scanf("%d %d %d", &n, &d, &m);
		int s = 0, t = calculateNode(n + 1, -1);
		fr (i, 0, m) {
			int u, v, c, p, e;
			scanf("%d %d %d %d %d", &u, &v, &c, &p, &e);
			costFlight[i] = p;
			int fromNode = calculateNode(u, e) , toNode = calculateNode(v, e + 1); 
			makeEdge(fromNode, toNode, c, p);
		}
		fr (city, 1, n + 1) fr (day, 0, d + 1) {
			if (day != d) makeEdge(calculateNode(city, day), calculateNode(city, day + 1), inf, 0);
			makeEdge(calculateNode(city, -1), calculateNode(city, day), inf, 0);
			if (city == n) makeEdge(calculateNode(city, day), t, inf, 0);
		}
		int numPeople = 0;
		fr (city, 1, n + 1) {
			int z; ipInt(z); numPeople += z;
			makeEdge(s, calculateNode(city, - 1), z, 0);
		}
		costFlight[m] = 0;
		sort (costFlight, costFlight + m + 1);
		int hi = m, lo = 0, ans = inf;
		while (hi >= lo) {
			int mid = (hi + lo) >> 1;
			int midVal = costFlight[mid];
			memset (flow, 0, sizeof flow);
			runMaxFlow(s, t, midVal);
			if (maxFlow == numPeople) {
				hi = mid - 1;
				ans = midVal;
			} else lo = mid + 1;
		}
		if (ans == inf) printf("Case #%d: Impossible\n", caseNo ); else printf("Case #%d: %d\n",caseNo, ans );
	}
	return 0;
}