# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition
int R, C, N, dp[(1 << 17)][20];

int bitcount (int num) { 
	int count  = 0;
	while (num) {
		if (num & 1) count ++;
		num >>= 1;
	}
	return count;
}

bool val(int board, int r, int c) {
	int index = r * C + c;
	return board & (1 << index) ;
}
bool canGo (int newR, int newC) { return newR >= 0 && newC >= 0 && newR < R && newC < C;  }

int dir[8][2] = { {-1, -1} , {-1, 0}, {-1 , 1}, {0, -1}, {0, 1} , {1, -1}, {1, 0}, {1, 1} };

int ways (int board , int jumps) {
	if (dp[board][jumps] != -1) return dp[board][jumps];
	if (bitcount(board) == 1) return 1;
	int waysCount = 0;
	fr (i, 0, R * C) if (board & (1 << i)) {
		int row = i / C, col = i % C;
		fr (d, 0, 8) {
			int newR = row + dir[d][0], newC = col + dir[d][1];
			int jumpR = newR + dir[d][0], jumpC = newC + dir[d][1];
			if (canGo(jumpR, jumpC) && val(board, jumpR, jumpC) == 0 && val(board, newR, newC) == 1) {
				int indexRemove = newR * C + newC, indexSet = jumpR * C + jumpC, indexRemovePresent = row * C + col;
				int newBoard = board ^ (1 << indexRemove) ^ (1 << indexRemovePresent) ^ (1 << indexSet) ;
				waysCount += ways(newBoard, jumps + 1);
			}
		}
	}
	return dp[board][jumps] = waysCount;
}

int main () {
	int t, caseNo = 0;
	ipInt(t);
	while (t--) {
		scanf("%d %d %d", &R, &C, &N);
		int board = 0;
		memset(dp, -1, sizeof dp);

		fr (i, 0, N) {
			int row, col;
			scanf("%d %d", &row, &col);
			--row; --col;
			int index = row * C + col;
			// printf("%d\n",index );
			board |= (1 << index);
		}
		printf("Case %d: %d\n",++caseNo, ways(board, 0));
	}
	return 0;
}