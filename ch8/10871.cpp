# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 10010

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// primefactorization and primeSieve variables
bitset<primesTill + 1> probablePrime; 
vi primeList;
// primefactorization and primeSieve variables


void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	int goTill = (int)sqrt(primesTill);
	for (int i = 2; i <= goTill ; ++i) {
		if (probablePrime.test(i) == 1) {
			primeList.push_back(i);
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime.reset(j);
			}
		}
	}
	for (int i = goTill + 1; i <= primesTill; ++i) {
		if (probablePrime.test(i) == 1) primeList.push_back(i);
	}
}

bool isPrime (lli N) {
	if (N <= primesTill) return probablePrime[N];
	int PFIndex = 0, factors = 0;
	lli PF = primeList[0];
	while (PF * PF <= N && N != 1 && PFIndex < primeList.size()) {
		if (N % PF == 0) return false;
		PF = primeList[++PFIndex];
	}
	return true;
}
int nums[10010];
int main () {
	int t, ansStart, ansEnd; ipInt(t);
	primeSieve();
	while (t--) {
		int n; ipInt(n);
		fr (i, 0, n) { ipInt(nums[i]); if (i) nums[i] += nums[i - 1]; }
		int found = 0;
		fr (length, 2, n + 1) { fr (st, 0, (n - length) + 1) {
				int ed = st + length - 1;
				int sum = nums[ed] - ((st) ? nums[st - 1] : 0);
				if (isPrime(sum)) { ansStart = st; ansEnd = ed; found = 1; break; }
			}
			if (found) break;
		}
		if (found) {
			int len = ansEnd - ansStart + 1;
			printf("Shortest primed subsequence is length %d:", len);
			fr (i, ansStart, ansEnd + 1) printf(" %d", nums[i] - (i ? nums[i - 1] : 0) );
			printf("\n");
		} else printf("This sequence is anti-primed.\n");
	}
	return 0;
}

