# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<bool> vb;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
struct state {
	int diskNum, timeLeft, songIndex;
	state (int d = 0, int t = 0, int s = 0) {
		diskNum = d; timeLeft = t; songIndex = s;
	}
	bool operator < (const state& right) const {
		return diskNum < right.diskNum;
	}
};
int dp[100][100][1000];
int n, t, m;
vi songs;

int fillSongs (int diskNum, int timeLeft, int songIndex) {
	if (diskNum == m) return 0;
	if (songIndex == n) return 0;
	if (timeLeft <= 0) { return fillSongs(diskNum + 1, t, songIndex); }
	if (dp[diskNum][timeLeft][songIndex] != -1) return dp[diskNum][timeLeft][songIndex];

	int maxVal = 0;
	if (timeLeft >= songs[songIndex]) {
		maxVal = max (maxVal, 1 + fillSongs(diskNum, timeLeft - songs[songIndex], songIndex + 1));
	} else {
		maxVal = max (maxVal, fillSongs(diskNum + 1, t, songIndex));
	}
	maxVal = (t >= songs[songIndex]) ? max (maxVal, fillSongs(diskNum + 1, t - songs[songIndex], songIndex + 1)) : maxVal;
	maxVal = max (maxVal, fillSongs(diskNum, timeLeft, songIndex + 1));
	return dp[diskNum][timeLeft][songIndex] = maxVal;
}

int main () {
	int tc, num, caseNo = 0;
	char c;
	ipInt(tc);
	while (tc--) {
		if (caseNo++) printf("\n");
		songs.clear(); memset(dp, -1, sizeof dp);
		scanf("%d %d %d", &n, &t, &m);
		// printf("%d %d %d\n",n,t,m );
		int x; ipInt(x);
		songs.push_back(x);
		fr (i, 1, n) { scanf("%c%c%d",&c, &c, &num ); songs.push_back(num); }
		state ini = state(0, t, 0);
		printf("%d\n", fillSongs(0, t, 0));
		
	}
	return 0;
}