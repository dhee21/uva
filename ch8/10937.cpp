# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<29) - 1

# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

char grid[55][55];
int visit[55][55], h, w;

bool isNode (int i, int j) { return (grid[i][j] >= '0' && grid[i][j] <= '9') || grid[i][j] == '@'; }
bool visited (int option, int i) { return option & (1 << i); }
int visitJ(int option, int node) { option = option | (1 << node); return option; }
int add (int a, int b) { return (a== inf || b == inf) ? inf : a + b; }
bool canGo (int r, int c) { return (r >= 0 && c >= 0 && r < h && c < w && !visit[r][c]); }
bool notFollowing (char c) { return ( c != '@' && c != '~' && c != '#' && c != '.' && c != '*' && c != '!'); }

int dir[4][2] = { {-1, 0}, {0, -1}, {0, 1}, {1, 0} };
int dirAngry[8][2] = { {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1} };

int distVal[11][11], numTreasures, dp[1 << 11][10];
void bfs (int i, int j) {
	memset(visit, 0, sizeof visit);
	queue<pair<pii, int> > q;
	q.push(make_pair(pii(i, j), 0));
	visit[i][j] = 1;
	while (!q.empty()) {
		pair<pii, int> box = q.front(); q.pop();
		int row = box.first.first, col = box.first.second;
		if (isNode(row, col)) {
			int nodeFrom = grid[i][j] == '@' ? numTreasures : grid[i][j] - '0';
			int nodeTo = grid[row][col] == '@' ? numTreasures : grid[row][col] - '0';
			distVal[nodeFrom][nodeTo] = box.second;
		}

		fr(k, 0, 4) {
			int r = box.first.first + dir[k][0], c = box.first.second + dir[k][1];
			if (canGo(r, c) && (grid[r][c] == '.' || isNode(r, c))) {
				visit[r][c] = 1;
				q.push(make_pair(pii(r, c), box.second + 1));
			}
		}
	}
}

int main () {
	while (scanf("%d %d", &h, &w) && (h || w)) {
		numTreasures = 0;
		bool present = false;
		fr (i, 0, h) {
				getchar();
				fr (j, 0, w) {
				scanf("%c", &grid[i][j]);
				if (grid[i][j] == '@') present = true;
				if (notFollowing(grid[i][j])) grid[i][j] = '#';
				grid[i][j] =  (grid[i][j] == '!') ? '0' + numTreasures++ : grid[i][j];
			}
		}
		if (!present) { printf("-1\n");  continue;}
		bool cantBeDone = false;
		fr (i, 0, h) { 
				fr (j, 0, w) {
					if (grid[i][j] == '*') fr(k, 0, 8) {
						int r = i + dirAngry[k][0], c = j + dirAngry[k][1];
						if (isNode(r, c)) { cantBeDone = true; break; }
						if (grid[r][c] == '.') grid[r][c] = 'X';
					}
					if (cantBeDone) break;
			}
			if (cantBeDone) break;
		}
		if (cantBeDone) { printf("-1\n"); continue;}
		fr (i, 0, numTreasures + 1) fr (j, 0, numTreasures + 1) distVal[i][j] = inf;
		fr (i, 0, h) fr (j, 0, w) if (isNode(i, j)) bfs(i, j);
		// TSP start
		int allSelectedOption = (1 << (numTreasures + 1)) - 1;
		fr (i, 0, numTreasures + 1) dp[allSelectedOption][i] = distVal[i][numTreasures];
		int answerOption = (1 << numTreasures);
		for (int option = allSelectedOption - 1; option >= answerOption; --option) fr(i, 0, numTreasures + 1) {
			if (!visited(option, i)) {
				dp[option][i] = inf;
			} else {
				int minDist = inf;
				fr (j, 0, numTreasures + 1) if (!visited(option, j)) {
					int OAVJ = visitJ(option, j);
					int dist = add(distVal[i][j], dp[OAVJ][j]);
					minDist = min (minDist, dist);
				}
				dp[option][i] = minDist;
			}
		}
		// TSP end
		if (dp[answerOption][numTreasures] >= inf) printf("-1\n"); else printf("%d\n",dp[answerOption][numTreasures]);
	}
	return 0;
}