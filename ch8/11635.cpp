# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<27) - 1
#define MAX_NODES 10010
#define MAX_HOTELS 110

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition
matrix adjMat(MAX_NODES);
// new graph made vars
int hotels[MAX_HOTELS];
int distVal[MAX_HOTELS][MAX_HOTELS];
int bfsDist[MAX_HOTELS];
bitset <MAX_HOTELS> visited;
// new graph made vars

// SSSP djakstra variables starts
int dist[MAX_NODES + 100], n, m;
priority_queue<pii> pq;
// SSSP djakstra variables ends

void makeEdge(int from, int to, int edgeWeight) {
	adjMat[from].push_back(pii(to, edgeWeight));
	adjMat[to].push_back(pii(from, edgeWeight));
}

void djakstra (int source) {
	priority_queue<pii> pq;
	fr (i, 0, MAX_NODES) dist[i] = inf;
	pq.push(pii(0, -source));
	dist[source] = 0;
	while (!pq.empty()) {
		pii minUpperbound = pq.top(); pq.pop();
		int node = -minUpperbound.second, minDist = -minUpperbound.first;
		if (dist[node] == minDist) {
			int neighbours = adjMat[node].size();
			// Relaxing dist of neighbours
			for (int i = 0 ; i < neighbours; ++i) {
				int edgeToNode = adjMat[node][i].first, edgeWeight = adjMat[node][i].second;
				int newDist = minDist + edgeWeight;
				if (newDist < dist[edgeToNode]) {
					dist[edgeToNode] = newDist;
					pq.push(pii(-newDist, -edgeToNode));
				}
			}
		}
	}
}

bool isConnected (int from, int to) { return distVal[from][to] <= 600 && from != to; }

void bfs (int source, int destination, int totalNodes) {
	visited.reset();
	fr (i, 0, MAX_HOTELS) bfsDist[i] = inf;
	queue <int> q;
	visited.set(source); bfsDist[source] = 0;
	q.push(source);
	while (!q.empty()) {
		int visitedNode = q.front(); q.pop();
		if (visitedNode == destination) {
			break;
		}
		int neighbours = totalNodes;
		fr (i, 0, neighbours) {
			if (!isConnected(visitedNode, i)) continue;
			int edgeToNode = i;
			if (visited.test(edgeToNode) == 0) {
				visited[edgeToNode] = 1;
				bfsDist[edgeToNode] = bfsDist[visitedNode] + 1;
				q.push(edgeToNode);
			}
		}
	}
}

void init () {
	fr (i, 0, MAX_NODES) adjMat[i].clear();
}

int main () {
	while (ipInt(n) && n) {
		init();
		int hotelsNum; ipInt(hotelsNum);
		fr (i, 0, hotelsNum){ ipInt(hotels[i]); if (hotels[i] == 1 || hotels[i] == n) { i--; hotelsNum--; } }
		ipInt(m);
		fr (i, 0, m) {
			int from, to, weight; scanf("%d %d %d", &from, &to, &weight);
			makeEdge (from, to, weight);
		}
		hotels[hotelsNum++] = n;
		djakstra(1);
		// as city one is the 0th node in new graph
		distVal[0][0] = 0;
		fr (j, 0, hotelsNum) distVal[0][j + 1] = dist[hotels[j]];	
		fr (i, 0, hotelsNum) {
			djakstra(hotels[i]);
			fr (j, 0, hotelsNum) distVal[i + 1][j + 1] = dist[hotels[j]];
		}
		bfs(0, hotelsNum, hotelsNum + 1);
		if (bfsDist[hotelsNum] == inf) printf("-1\n"); else printf("%d\n", bfsDist[hotelsNum] - 1);
	}
	return 0;
}
