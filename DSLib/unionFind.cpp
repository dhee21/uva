#include <stdio.h>
#include <iostream>
#include <vector>
using namespace std;

class unionFind {
	public :
	vector <int> pset,nodes;//pset is storing at which node a node is pointing , and nodes stores number o noes in a set if an node is the main node of as set
	int num_of_sets;

	void iniSet(int n) {
		pset.assign(n,0);
		nodes.assign(n,1);
		for(int i = 0 ; i < pset.size() ; ++i ) {
			pset[i] = i;
		}
		num_of_sets = n;
	}

	int findSet(int v) {
			if(v < 0 || v >= pset.size() ) {
				return -1 ;
			}
			int ptr = v;
			while ( pset[ptr] != ptr ) {
				ptr = pset[ptr];
			}
			compressThePath(v, ptr);
			return ptr;
	}
	void compressThePath (int vertex, int set ) {
		 int ptr = vertex;
		 while(pset[ptr] != ptr ) {
		 	int nextNode = pset[ptr];
		 	pset[ptr] = set ;
		 	ptr = nextNode;
		 }
	}

	bool isSameSet(int vertex1, int vertex2) {
			int set1 = findSet(vertex1), set2 = findSet(vertex2);
			if(set1 == -1 || set2 == -1) {
				return 0;
			}
			return set1 == set2;
	}

	int numDisjointSets() {
		return num_of_sets;
	}

	void unionSet(int vertex1, int vertex2 ) {
		int set1 = findSet(vertex1), set2 = findSet(vertex2);
		if(set1 == -1 || set2 == -1) {
			return ;
		}
		if(set1 == set2) {
			return ;
		}
		if(nodes[set1] > nodes[set2]) {
			pset[set2] = set1 ;
			nodes[set1] += nodes[set2] ;
 		} else {
 			pset[set1] = set2 ;
 			nodes[set2] += nodes[set1] ;
 		}
 		num_of_sets--;
	}

	int sizeOfSet(int vertex) {
		int set = findSet(vertex) ;
		if(set == -1) {
			return -1;
		}
		return nodes[set];
	}
};

int main () {
	unionFind ds ;
	ds.iniSet(5);
	printf("%d\n", ds.numDisjointSets());
	printf("%d %d\n", ds.findSet(1), ds.findSet(2) );
	ds.unionSet(1,2);
	printf("%d %d %d %d\n", ds.numDisjointSets() , ds.sizeOfSet(2), ds.sizeOfSet(1), ds.sizeOfSet(3));
	printf("%d %d %d %d %d %d\n", ds.findSet(0), ds.findSet(1), ds.findSet(2), ds.findSet(3), ds.findSet(4), ds.numDisjointSets());
	ds.unionSet(1,3);
	printf("%d %d %d %d %d %d\n", ds.findSet(0), ds.findSet(1), ds.findSet(2), ds.findSet(3), ds.findSet(4), ds.numDisjointSets());



}