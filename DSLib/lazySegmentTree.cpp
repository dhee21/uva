#include <stdio.h>
#include <iostream>
#include <cmath>
#include <limits>
#include <vector>
using namespace std;
typedef  vector<int> vi;
int infinity = numeric_limits<int>::max();
#define maxSize 100004

struct treeNode {
	int data,lazyVal,toUpdate;
}useless={infinity,0,0};

int treeSize = int (2*pow(2.0, floor(log(maxSize)/log(2.0) + 1 )));
vector<treeNode> tree(treeSize);

class lazySegmentTree{
	public :
	int size;

	void putVal(int treeId, int value) {
		tree[treeId].data = value;
		tree[treeId].lazyVal = infinity;
		tree[treeId].toUpdate = 0;
	}
	treeNode calculate(treeNode left , treeNode right) {
		treeNode node = {0,infinity,0};
		node.data = min(left.data , right.data);
		return node;
	}
	void propogate (int treeId, int val) {
		tree[treeId << 1].lazyVal = min(tree[treeId << 1].lazyVal, val);
		tree[treeId << 1].toUpdate = 1 ;
		tree[(treeId << 1) + 1].lazyVal = min(tree[(treeId << 1) + 1].lazyVal, val);
		tree[(treeId << 1) + 1].toUpdate = 1;
	}
	void updateNode (int treeId, int st, int ed, int val) {
		tree[treeId].data = min (tree[treeId].data, val);
		if(st != ed) {
			propogate(treeId, val);
		}
		tree[treeId].toUpdate = 0;
	}

	void createTree(int *array, double size) {
		this->size = size ;
		buildTree(array, 1,0,this->size - 1);
	}
	
	void buildTree( int *array, int id , int st, int ed) {
		if(st > ed) {
			return ;
		}
		if( st == ed ) {
			putVal(id, array[st]);
			return ;
		}
		int mid = (st + ed)>>1;
		buildTree(array,(id<<1) ,st, mid);
		buildTree(array, (id<<1) + 1 , mid+1, ed);
		tree[id] = calculate(tree[id<<1], tree[(id<<1) +1]);
		return ;
	}
	treeNode query(int id, int segSt, int segEd, int ourSt, int ourEd) {
			if(segSt > segEd || ourSt>ourEd || ourSt > segEd || ourEd <segSt) {
				return useless;
			}
			if(tree[id].toUpdate) {
				updateNode(id, segSt, segEd, tree[id].lazyVal);
			}
			if(segSt>=ourSt && segEd <=ourEd) {
				return tree[id];
			}
			int mid = (segSt + segEd ) >> 1;
			treeNode left = query((id <<1), segSt, mid, ourSt, ourEd);
			treeNode right = query( (id << 1) + 1 , mid+1, segEd, ourSt, ourEd );
			if(left.data == useless.data) {
				return right;
			}
			if(right.data == useless.data) {
				return left;
			}
			return calculate(left, right);
	}
	void update (int id, int segSt, int segEd , int ourSt, int ourEd, int changeVal) {
				if(tree[id].toUpdate) {
					updateNode(id, segSt, segEd, tree[id].lazyVal);
				}
				if(segSt > segEd || ourSt> ourEd || segSt > ourEd || segEd < ourSt) {
					return;
				}

				
				if(segSt >= ourSt && segEd <= ourEd) {
					updateNode(id, segSt, segEd, changeVal);
					return ;
				}
				int mid = (segSt + segEd) >> 1;
				int child = id << 1;
				update(child, segSt, mid , ourSt, ourEd, changeVal);
				update(child+1, mid +1, segEd, ourSt, ourEd, changeVal);
				tree[id] = calculate(tree[child], tree[child+1]);
				return ;
	}
	 void doUpdate(int* array , int stIndex, int endIndex , int changeVal) {
      update( 1 , 0 ,this->size -1 , stIndex, endIndex , changeVal);
    }

    int doQuery(int *array , int start , int end ) {
      return query( 1 , 0 ,this->size - 1 , start , end ).data ;
    }
};

int main () {
	lazySegmentTree tree;
	int array[] = {2,3,4,5,6,7,1,3,2,4,5} ;
	tree.createTree(array, 11);
	printf("%d\n", (tree.doQuery( array , 0 , 0 )) );
	printf("%d\n", (tree.doQuery( array , 1 , 1 )) );
	printf("%d\n", (tree.doQuery( array , 2 , 2 )) );
	printf("%d\n", (tree.doQuery( array , 3 , 3 )) );
	printf("%d\n", (tree.doQuery( array , 4 , 4 )) );
	printf("%d\n", (tree.doQuery( array , 5 , 5 )) );
	printf("%d\n", (tree.doQuery( array , 6 , 6 )) );
	printf("%d\n", (tree.doQuery( array , 7 , 7 )) );
	printf("%d\n", (tree.doQuery( array , 8 , 8 )) );
	printf("%d\n", (tree.doQuery( array , 9 , 9 )) );
	printf("%d\n", (tree.doQuery( array , 10 , 10 )) );
	printf("%d\n", tree.doQuery( array , 0 , 10 ) );
	printf("up\n");
	tree.doUpdate(array , 0,1,-1) ;
	printf("%d\n", tree.doQuery( array , 2 , 10 ) );
	tree.doUpdate(array , 0,1,2) ;
	tree.doUpdate(array , 7,10,-4) ;
	printf("%d\n", tree.doQuery( array , 0 , 6 ) );
	printf("%d\n", tree.doQuery( array , 7 , 10 ) );
	tree.doUpdate(array , 8,8,-11) ;
	printf("%d\n", tree.doQuery( array , 0 , 10 ) );
	printf("%d\n", tree.doQuery( array , 0 , 7 ) );
	printf("%d\n", tree.doQuery( array , 8 , 10 ) );
	printf("%d\n", tree.doQuery( array , 9 , 10 ) );
	printf("%d\n", tree.doQuery( array , 0 , 10 ) );




}
