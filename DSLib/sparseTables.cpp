# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define MAX_N 10000
#define MAX_LOG_N 13
// MAX_LOG_N is log2(MAX_N)

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// data structure of sparse table
int sparseTableDS[MAX_LOG_N][MAX_N];
// initialze sparseTableDS for intervals of length 1

class sparseTable {
	int size ;
public:
	int operation (int left, int right) { return min(left, right); }
	void preCompute (int arraySize) {
		size = arraySize;
		for (int k = 1; (1 << k) <= arraySize; ++k) {
			fr (i, 0, arraySize - (1 << k)) {
				sparseTableDS[k][i] = operation(sparseTableDS[k - 1][i], sparseTableDS[k - 1][i + (1 << (k - 1))]);
			}
		}
	}
	// l should be less than r
	int query(int l, int r) {
		if (l < 0 || r < 0 || l >= size || r > size) return -inf;
		int lenVal = r - l + 1, k = (int)log2(lenVal);
		return operation(sparseTableDS[k][l], sparseTableDS[k][r - (1 << k) + 1]);
	}
};
// data structure of sparse table


int main () {
	int N;
	ipInt(N);
	fr (i, 0, N) {
		ipInt(sparseTableDS[0][i]);
	}
	sparseTable sT;
	sT.preCompute(N);
	int q;
	ipInt(q);
	fr (i, 0, q) {
		int l, r; ipInt(l); ipInt(r);
		printf("%d\n",sT.query(l, r));
	}
	return 0;
}