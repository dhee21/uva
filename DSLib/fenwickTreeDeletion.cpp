# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define primesTill (1000000 - 1)

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition


class fenwickTree {
public:
	vi tree;
	int lsOne(int x) {
		return x & (-x);
	}
	void createTree(int *array, int size) {
		tree.assign( size + 1 , 0);
	}
	int rangeSumQuery(int index) {
		int sum = 0;
		if(index < 1  || index >= tree.size()) {
			return 0 ;
		}
		for( ; index ; index -= lsOne(index) ) {
			sum += tree[index];
		}
		return sum;
	}
	int rangeSumQuery(int start , int end) {
		if(end < start || end < 1 || start < 1 ) {
			return 0;
		}
		return rangeSumQuery(end) - ( (start == 1) ? 0 : rangeSumQuery(start-1) ) ; 
	}
	void buildTree(int *array, int size) {
			createTree(array, size);
			for (int i = 0 ; i < size ; ++i ) {
				updateTree(i+1 , array[i]);
			}
	}
	void updateTree(int nthElement, int value ) {
		int index = nthElement;
			for( ; index < tree.size() ; index += lsOne(index) ) {
				tree[index] += value ;
			}
	}
	void printTree() {
		for (int i =0 ;i< tree.size() ; ++i) {
			printf("%d\n", tree[i] );
		}
	}
	// tells the smallest index till where summations is not less than value
	// 1 BASED OUTPUT
	int lowerBound (int lo, int hi, int value) {
		int ans = 0;
		while (lo <= hi) {
			int mid = (hi + lo) >> 1;
			if (rangeSumQuery(mid) < value) lo = mid + 1;
			else { ans = mid; hi = mid - 1; }
		}
		return ans;
	}
};

// primefactorization and primeSieve variables
bitset<primesTill + 1> probablePrime; 
vi primeList;
// primefactorization and primeSieve variables

void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	int goTill = (int)sqrt(primesTill);
	for (int i = 2; i <= goTill ; ++i) {
		if (probablePrime.test(i) == 1) {
			primeList.push_back(i);
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime.reset(j);
			}
		}
	}
	for (int i = goTill + 1; i <= primesTill; ++i) {
		if (probablePrime.test(i) == 1) primeList.push_back(i);
	}
}

int primeFactorization (lli N) {
	int PFIndex = 0, numFactors = 0;
	lli PF = primeList[0];
	while (PF * PF <= N && N != 1 && PFIndex < primeList.size()) {
		if (N % PF == 0) {
			int expVal = 0;
			while (N % PF == 0) {
				N /= PF;  expVal ++; numFactors++;
			}
		}
		PF = primeList[++PFIndex];
	}
	if (N != 1) {
		numFactors++;
	}
	return numFactors;
}

int reverse (int x) {
	int newNum[10], i = 0;
	while (x) { newNum[i++] = x % 10; x /= 10; }
	while (i < 7) { newNum[i++] = 0; }
	int num = 0;
	fr (i, 0, 7) { num *= 10; num += newNum[i]; }
	return num;
}

int main () {
	primeSieve();
	int numPrimes = primeList.size();
	int revPrimes[numPrimes + 10], factorsNum[numPrimes + 10], indexes[numPrimes + 10];
	fr (i, 0, numPrimes) { revPrimes[i] = reverse(primeList[i]);}
	sort(revPrimes, revPrimes + numPrimes);
	fr (i, 0, numPrimes) { factorsNum[i] = primeFactorization(revPrimes[i]); indexes[i] = 1; }
	// indextree to help in deletion by making cumulative index decrease by one when an index is deleted
	// initially indexes array is 1 all over so that "fenwick index --> indexFW" of an (index in new set --> indexSET) (revPrimes here)
	// after deletion is the index(indexFW) where cumulative sum is equal to indexSET
	fenwickTree fw, indexTree;
	fw.buildTree(factorsNum, numPrimes);
	indexTree.buildTree(indexes, numPrimes);
	int num; char queryType;
	while (scanf("%c %d", &queryType, &num) != EOF) {
		getchar();
		if (queryType == 'q') {
			int index = indexTree.lowerBound(1, numPrimes, num + 1);
			printf("%d\n", fw.rangeSumQuery(1, index) );
		} else {
			int index = lower_bound(revPrimes, revPrimes + numPrimes, num) - revPrimes;
			fw.updateTree(index + 1, -factorsNum[index]);
			indexTree.updateTree(index + 1, -1);
			factorsNum[index] = 0;
		}
	}
	return 0;
}
