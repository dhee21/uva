#include <stdio.h>
#include <iostream>
#include <cmath>
#include <limits>
#include <vector>
using namespace std;
typedef vector<int> vi;
int infinity = numeric_limits<int>::max();
#define maxSize 100009
struct treeNode {
      int data;
    }defaultNode = {0},
    infiniteNode = {infinity};
int  treeSize = int (2*pow(2.0,floor(log(maxSize)/log(2.0) + 1)));
vector<treeNode> tree(treeSize);


class segmentTree {

  public :
    int size;

    void createTree(int* arr, double size) {
      // double size = arr.size();
      this->size = size;
      // int  treeSize = int (2*pow(2.0,floor(log(size)/log(2.0) + 1)));
      // tree.assign(treeSize, defaultNode);
      buildTree(arr, 1, 0, size - 1 );
    }

    treeNode operation (treeNode left , treeNode right) {
        treeNode node;
        node.data = min(left.data, right.data);
        return node;
    }
    void updateLeafNode ( int treeId , int value) {
      tree[treeId].data = value;
    }

    void buildTree(int* arr, int id , int st , int ed) {
        if(st > ed) {
          return ;
        }
        if(st == ed) {
          updateLeafNode(id, arr[st]);
          return; 
        }
        int mid = (st+ed)>>1;
        buildTree(arr, (id<<1) , st, mid );
        buildTree(arr, (id<<1) + 1 , mid+1, ed);
        tree[id] = operation( tree[id<<1] , tree[ (id<<1) + 1 ]);
    }

    treeNode query( int id , int segSt, int segEd , int ourSt, int ourEd ) {
        if(segSt > segEd || ourSt > ourEd || ourSt > segEd || ourEd < segSt) {
          return infiniteNode;
        }
        if(ourSt <= segSt && ourEd >= segEd) {
          return tree[id] ;
        }
        int mid = (segSt + segEd) >> 1 ;
        treeNode left = query(id<<1 , segSt, mid , ourSt , ourEd) ;
        treeNode right = query( (id<<1) + 1 , mid+1 , segEd , ourSt , ourEd );
        return operation( left , right);

    }
    void update( int id , int st, int ed, int index, int newValue ) {
      // printf("%d\n", id );
      // printf("start = %d end = %d index = %d id = %d\n", st, ed ,index, id );
        if( st > ed || index > ed || index < st) {
          return ;
        }
        if( st == ed ) {
          updateLeafNode(id, newValue);
          return ;
        }
        int mid = (st+ed) >> 1;
        update(id<<1 , st , mid , index, newValue);
        update( (id<<1) + 1 , mid+1, ed , index, newValue );
        tree[id] = operation(tree[ id<<1 ] , tree[ (id<<1) + 1 ]);
    }

    void doUpdate(int* array , int index , int newValue) {
      update( 1 , 0 ,this->size -1 , index , newValue);
    }

    int doQuery(int *array , int start , int end ) {
      return query( 1 , 0 ,this->size - 1 , start , end ).data ;
    }
};

int main() {
	segmentTree tree;
	int array[] = {2,3,4,5,6,7,1,3,2,4,5} ;
	// vi array(arr, arr+11);
	tree.createTree(array, 11);
	printf("%d\n", (tree.doQuery( array , 0 , 0 )) );
	printf("%d\n", (tree.doQuery( array , 1 , 1 )) );
	printf("%d\n", (tree.doQuery( array , 2 , 2 )) );
	printf("%d\n", (tree.doQuery( array , 3 , 3 )) );
	printf("%d\n", (tree.doQuery( array , 4 , 4 )) );
	printf("%d\n", (tree.doQuery( array , 5 , 5 )) );
	printf("%d\n", (tree.doQuery( array , 6 , 6 )) );
	printf("%d\n", (tree.doQuery( array , 7 , 7 )) );
	printf("%d\n", (tree.doQuery( array , 8 , 8 )) );
	printf("%d\n", (tree.doQuery( array , 9 , 9 )) );
	printf("%d\n", (tree.doQuery( array , 10 , 10 )) );
	printf("%d\n", tree.doQuery( array , 0 , 10 ) );
	tree.doUpdate(array , 5 , 0) ;
	printf("up\n");
	printf("%d\n", (tree.doQuery( array , 0 , 0 )) );
	printf("%d\n", (tree.doQuery( array , 1 , 1 )) );
	printf("%d\n", (tree.doQuery( array , 2 , 2 )) );
	printf("%d\n", (tree.doQuery( array , 3 , 3 )) );
	printf("%d\n", (tree.doQuery( array , 4 , 4 )) );
	printf("%d\n", (tree.doQuery( array , 5 , 5 )) );
	printf("%d\n", (tree.doQuery( array , 6 , 6 )) );
	printf("%d\n", (tree.doQuery( array , 7 , 7 )) );
	printf("%d\n", (tree.doQuery( array , 8 , 8 )) );
	printf("%d\n", (tree.doQuery( array , 9 , 9 )) );
	printf("%d\n", (tree.doQuery( array , 10 , 10 )) );
	printf("%d\n", tree.doQuery( array , 0 , 10 ) );
	printf("%d\n", (tree.doQuery(array , 3 , 4 )) );
    tree.doUpdate(array, 4 , -1) ;
	printf("%d\n", (tree.doQuery(array , 3 , 4 )) );
	



	return 0;
}