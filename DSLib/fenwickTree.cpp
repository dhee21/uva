#include <stdio.h>
#include <iostream>
#include <vector>
using namespace std;

typedef vector<int> vi ;
// indexing in fenwick tree is one based
class fenwickTree {
public:
	vi tree;
	int lsOne(int x) {
		return x & (-x);
	}
	void createTree(vi &array) {
		tree.assign( array.size()+1 , 0);
	}
	int rangeSumQuery(int index) {
		int sum = 0;
		if(index < 1  || index >= tree.size()) {
			return 0 ;
		}
		for( ; index ; index -= lsOne(index) ) {
			sum += tree[index];
		}
		return sum;
	}
	int rangeSumQuery(int start , int end) {
		if(end < start || end < 1 || start < 1 ) {
			return 0;
		}
		return rangeSumQuery(end) - ( (start == 1) ? 0 : rangeSumQuery(start-1) ) ; 
	}
	void buildTree(vi &array) {
			createTree(array);
			for (int i = 0 ; i< array.size() ; ++i ) {
				updateTree(i+1 , array[i]);
			}
	}
	void updateTree(int nthElement, int value ) {
		int index = nthElement;
			for( ; index <= tree.size() ; index += lsOne(index) ) {
				// printf("%d\n", index );
				tree[index] += value ;
			}
	}
	void printTree() {
		for (int i =0 ;i< tree.size() ; ++i) {
			printf("%d\n", tree[i] );
		}
	}
};
// indexing in fenwick tree is one based

int main() {
	int arr[] = {1,2,3,4,5,6};
	vi array(arr, arr+6);
	fenwickTree tree;
	tree.buildTree(array);
	// tree.printTree();
	printf("%d\n", tree.rangeSumQuery(1) );
	printf("%d\n", tree.rangeSumQuery(2) );
	printf("%d\n", tree.rangeSumQuery(3) );
	printf("%d\n", tree.rangeSumQuery(4) );
	printf("%d\n", tree.rangeSumQuery(5) );
	printf("%d\n", tree.rangeSumQuery(6) );
	printf("%d\n", tree.rangeSumQuery(7) );
	printf("%d\n", tree.rangeSumQuery(13) );
	printf("%d\n", tree.rangeSumQuery(-2) );
	printf("%d\n", tree.rangeSumQuery(1, 6) );
	printf("%d\n", tree.rangeSumQuery(2,3) );
	printf("%d\n", tree.rangeSumQuery(0,3) );
	printf("%d\n", tree.rangeSumQuery(-3,-1) );
	printf("%d\n", tree.rangeSumQuery(2,6) );
	return 0;
}