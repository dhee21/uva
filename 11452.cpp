# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;

char input[2100];

int main () {
	int t;
	ipInt(t);

	while (t--) {
		scanf("%s", input);
		int len = strlen(input);
		int index;
		int sameIndex;

		for (index = 0; 2*(index+1) <= len ; ++index) {
			bool notSame = false;
			fr (i, 0, index + 1) {
				if (input[i] != input[index + 1 + i]) {
					notSame = true; break;
				}
			}
			if (notSame == false) sameIndex = index;
		}
		int seqLength = sameIndex + 1;
		// cout << seqLength << "  see" << endl;
		int indexToStart = (2 * (seqLength)), patternIndex = 0;
		while (indexToStart < len) {indexToStart++; patternIndex++;}
		fr (i, 0, 8) {
			printf("%c",input[patternIndex % seqLength] );
			patternIndex++;
		}
		printf("...\n");
	}

	return 0;
}