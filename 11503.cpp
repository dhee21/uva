#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <utility>
#include <vector>
#include <map>
using namespace std;

typedef pair<int,int> pi;
typedef vector<pi > vpi;

class unionFind {
	public :
	vector <int> pset,nodes;//pset is storing at which node a node is pointing , and nodes stores number o noes in a set if an node is the main node of as set
	int num_of_sets;

	void iniSet(int n) {
		pset.assign(n,0);
		nodes.assign(n,1);
		for(int i = 0 ; i < pset.size() ; ++i ) {
			pset[i] = i;
		}
		num_of_sets = n;
	}

	int findSet(int v) {
			if(v < 0 || v >= pset.size() ) {
				return -1 ;
			}
			int ptr = v;
			while ( pset[ptr] != ptr ) {
				ptr = pset[ptr];
			}
			compressThePath(v, ptr);
			return ptr;
	}
	void compressThePath (int vertex, int set ) {
		 int ptr = vertex;
		 while(pset[ptr] != ptr ) {
		 	int nextNode = pset[ptr];
		 	pset[ptr] = set ;
		 	ptr = nextNode;
		 }
	}

	bool isSameSet(int vertex1, int vertex2) {
			int set1 = findSet(vertex1), set2 = findSet(vertex2);
			if(set1 == -1 || set2 == -1) {
				return 0;
			}
			return set1 == set2;
	}

	int numDisjointSets() {
		return num_of_sets;
	}

	void unionSet(int vertex1, int vertex2 ) {
		int set1 = findSet(vertex1), set2 = findSet(vertex2);
		if(set1 == -1 || set2 == -1) {
			return ;
		}
		if(set1 == set2) {
			return ;
		}
		if(nodes[set1] > nodes[set2]) {
			pset[set2] = set1 ;
			nodes[set1] += nodes[set2] ;
 		} else {
 			pset[set1] = set2 ;
 			nodes[set2] += nodes[set1] ;
 		}
 		num_of_sets--;
	}

	int sizeOfSet(int vertex) {
		int set = findSet(vertex) ;
		if(set == -1) {
			return -1;
		}
		return nodes[set];
	}
};


int main () {
	int testCases, fShips;
	char ip[200], f1[40], f2[40];
	string fr1, fr2;
	scanf("%d", &testCases);
	while(testCases --) {
		map<string, int> fMap;
		int index = 0;
		scanf("%d\n", &fShips);
		vpi inputs;
		while(fShips --) {
			gets(ip);
			sscanf(ip, "%s %s", f1, f2);
			fr1 = f1;
			fr2 = f2;
			if(fMap.find(fr1) == fMap.end()) {
				fMap[fr1] = index++;
			}
			if(fMap.find(fr2) == fMap.end()) {
				fMap[fr2] = index++;
			}
			inputs.push_back(make_pair(fMap[fr1],fMap[fr2]));
			// printf("%s %s\n", fr1.c_str(), fr2.c_str());
		}
		unionFind ds;
		ds.iniSet(index);
		for( int i=0 ; i< inputs.size() ; ++i ) {
			ds.unionSet(inputs[i].first, inputs[i].second);
			printf("%d\n",ds.sizeOfSet(inputs[i].first) );
		}
	//	printf("\n");
	}
	return 0 ;
}