# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
# include <limits.h>
using namespace std;

// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 110
#define inf LLONG_MAX

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<double> vdb;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions
matrix adjMat(MAX_NODES);
int n, m;

// MAxFlow variables
int flowToIncrease, maxFlow, flow[MAX_NODES][MAX_NODES], cap[MAX_NODES][MAX_NODES];
double minCost, cost[MAX_NODES][MAX_NODES], costToIncrease;
vi parentBellmen;
vdb dist;
// MAxFlow variables

// problem specific variables
lli flowToGet;
// problem specific variables

lli resCapacity (int from, int to) {
	return cap[from][to] - flow[from][to];
}

void makeEdge (int from, int to, int capVal, double costVal) {
	cap[from][to] = capVal;
	cost[from][to] = cost[to][from] = costVal;
	// this is the connection of residual graph in which both from -> to and to -> from are connected
	if (!cap[to][from]) {
		adjMat[from].push_back(pii(to, 0));
		adjMat[to].push_back(pii(from, 0));
	}
	
}
void augmentFlow (int source, int sink) {
	if (parentBellmen[sink] == -1) return;
	lli minResCap = inf;
	int  node = sink;
	while (parentBellmen[node] != -1) {
		int parent = parentBellmen[node];
		minResCap = min(minResCap, resCapacity(parent, node));
		node = parent;
	}
	node = sink;
	flowToIncrease = minResCap;
	while (parentBellmen[node] != -1) {
		int parent = parentBellmen[node];
		//doing it before augmenting the flow value is important
		double costVal = (flow[node][parent] > 0) ? -cost[parent][node] : cost[parent][node];
		costToIncrease += flowToIncrease * costVal;
		// printf("%lf %d %lf %d\n", costToIncrease, flowToIncrease, costVal, parent );
		//doing it before augmenting the flow value is important
		
		flow[parent][node] += minResCap;
		flow[node][parent] -= minResCap;
		node = parent;
	}
}

//djakstra 
void SPFA (int source) {
	//initialize dist
	queue<int> q;
	dist.assign(MAX_NODES, inf);
	vi visited(MAX_NODES, 0);
	parentBellmen.assign(MAX_NODES, -1);
	//initialization end
	
	//start
	dist[source] = 0;
	visited[source] = 1;
	q.push(source);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		visited[u] = 0;
		int neighbours = adjMat[u].size();
		fr (i, 0, neighbours) {
			int v = adjMat[u][i].first;
			double costVal = (flow[v][u] > 0) ? -cost[u][v] : cost[u][v];
			if (resCapacity(u, v) && dist[u] + costVal < dist[v]) {
				parentBellmen[v] = u;
				dist[v] = dist[u] + costVal;
				if (!visited[v]) {
					visited[v] = 1;
					q.push(v);
				}
			}
		}

	}
}

void bellmenFord (int source) {
	//initialize dist
	queue<int> q;
	dist.assign(MAX_NODES, inf);
	parentBellmen.assign(MAX_NODES, -1);
	//initialization ends

	//start
	dist[source] = 0;
	fr (turn, 0, n - 1){
		fr (v, 0, n) {
			if (dist[v] != inf) {
				int neighbours = adjMat[v].size();
				// going on every edge of v
				fr (i, 0, neighbours) {
					int edgeToNode = adjMat[v][i].first;
					lli edgeWeight = (flow[edgeToNode][v] > 0) ? -cost[v][edgeToNode] : cost[v][edgeToNode];
					if ((resCapacity(v, edgeToNode) != 0) && (dist[v] + edgeWeight < dist[edgeToNode])) {
						dist[edgeToNode] = dist[v] + edgeWeight;
						parentBellmen[edgeToNode] = v;
					}
				}
			}
		}
	}
	
}

// this now becomes minCost Max Flow
void runMaxFlow (int s, int t) {
	// while i can increase / augment flow i will increase the maxFlow value
	while (true) {
		flowToIncrease = 0;
		costToIncrease = 0;
		SPFA(s);
		augmentFlow(s, t);
		if (flowToIncrease == 0) {
			break;
		}
		// if (maxFlow + flowToIncrease >= flowToGet) {
		// 	lli num = costToAdd * (flowToGet - maxFlow);
		// 	minCost += num;
		// 	maxFlow = flowToGet;
		// 	break;
		// }
		maxFlow = maxFlow + flowToIncrease;
		minCost +=  costToIncrease;
	}
}
void initialize () {
	memset(cap, 0, sizeof cap);
	memset(flow, 0, sizeof flow);
	fr(i, 0, MAX_NODES) fr(j, 0, MAX_NODES) cost[i][j] = inf;
	fr(i, 0, adjMat.size()) {
		adjMat[i].clear();
	}
	maxFlow = 0;
	minCost = 0;
}

int main () {
	int s, t, c;
	while (scanf("%d %d", &n, &m) && (m || n)) {
		initialize();
		fr(bankNo, 1, n + 1) fr(cruiserNo, 1, m + 1) {
			double timeVal;
			scanf("%lf", &timeVal);
			makeEdge(cruiserNo, bankNo + m, 1, timeVal);
		}
		fr (cruiserNo, 1, m + 1) {
			makeEdge(0, cruiserNo, 1, 0);
		}
		fr (bankNo, 1, n + 1) {
			makeEdge(m + bankNo, n + m + 1, 1, 0);
		}
		s = 0; t = n + m + 1;
		runMaxFlow(s, t);
		minCost /= n;
		minCost = floor(minCost*100.0 + 0.5 + 1e-9)/100.0;
		printf("%0.2lf\n", minCost );
	}
	return 0;
}

// In this question capacity of each node was same i.e why to get the cost we just needed to do dist[sink] * flowToIncrease; because if i empty the flow 
// in a running stream in this question I only empty the stream because every edge has same capacity and when i emptied the flow, the flow also had
// the same capacity, so whenever i empty the flow i make  the flow empty..
// But if capacities are different then What ?
// in that case costToIncrease != dist[sink] * flowToIncrease....
// i have to increase the constToIncrease while augmenting like this
// costToIncrease += cost[parent[node]][node] * minResCap; 

// if reverting the flow then canceling the cost which was added earlier
		// lli costVal = (flow[node][parent] > 0) ? -cost[parent][node] : cost[parent][node];
		// costToIncrease += flowToIncrease * costVal;