# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define MAX_NODES 500
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef map <pii, int> mpi;
matrix adjMat(MAX_NODES);
vector<edge> EdgeList(MAX_EDGES);


// prob specififc vars
char grid[50][50];
vi leftSet;
pii nodes[MAX_NODES];
int nodeIndex, dir[4][2] = {{1, 0}, {-1, 0}, {0, -1}, {0, 1}}, visited[MAX_NODES], owner[MAX_NODES], rows, cols;
mpi indexOf;
// prob specififc vars


bool isNeighbour (int r, int c) {
	return (r >= 0 && r < rows && c >= 0 && c < cols && grid[r][c] == '*');
}
void makeEdge(pii fromCoor, pii toCoor, int weight) {
	int from, to;
	from = indexOf[fromCoor]; to = indexOf[toCoor];
	adjMat[from].push_back(make_pair(to, weight));
}
void connectNodes () {
	fr (i, 0, leftSet.size()) {
		pii node = nodes[leftSet[i]];
		int row = node.first, col = node.second;
		fr (d, 0, 4) {
			int r = row + dir[d][0], c = col + dir[d][1];
			if (isNeighbour(r, c)) {
				makeEdge(node, pii(r, c), 0);
			}
		}
	}
}
bool isEven(int x) {
	return !(x & 1);
}
void makeNode (int r, int c) {
	indexOf[pii(r, c)] = nodeIndex;
	nodes[nodeIndex++] = pii(r, c);
	if (isEven(r + c)) {
		leftSet.push_back(indexOf[pii(r, c)]);
	}
}
void init () {
	fr(i, 0, MAX_NODES) {
		adjMat[i].clear();
	}
	leftSet.clear();
	nodeIndex = 0;
	indexOf.clear();
}

// MCBM functions start
int canAugmentPath (int leftNode) {
	int neighbours = adjMat[leftNode].size();
	if (visited[leftNode]) return 0;
	visited[leftNode] = 1;
	fr (i, 0, neighbours) {
		int rightNode = adjMat[leftNode][i].first;
		if (owner[rightNode] == -1 || canAugmentPath(owner[rightNode])) {
			owner[rightNode] = leftNode;
			return 1;
		}
	}
	return 0;
}
// for this we neeed to get the adj list of left set only joining to nodes of right side
int MCBM() {
	int pairsMade = 0;
	fr(i, 0, MAX_NODES) owner[i] = -1;
	fr (i, 0, leftSet.size()) {
		memset(visited, 0, sizeof visited);
		pairsMade += canAugmentPath(leftSet[i]);
	}
	return pairsMade;
}
// MCBM functions ends

int main () {
	int t;
	ipInt(t);
	while (t--) {
		init();
		scanf("%d %d\n", &rows, &cols);
		fr(i, 0, rows) {
			fgets(grid[i], 50, stdin);
		}
		fr (i, 0 , rows) fr(j, 0, cols) {
			if (grid[i][j] == '*') {
				makeNode(i, j);
			}
		}
		connectNodes();
		int mcbm = MCBM();
		int ans = nodeIndex - mcbm;
		printf("%d\n",ans);
	}
}