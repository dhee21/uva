# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
using namespace std;
#define MAX_NODES 110
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;

matrix adjMat(MAX_NODES);
// arr => arrival times, dep => departure times, nodeForDepTime => index is time and valuew is node which departes on that time
// dfs parent => parent of each node while dfs, deppest backEdge stores smallest arrival times reachable from a node including the subtree
// not including its parent
int visited[MAX_NODES], arr[MAX_NODES], dep[MAX_NODES], nodeForDepTime[(2*MAX_NODES) + 50], dfsParent[MAX_NODES], deepestBackEdge[MAX_NODES];
int timeCounter, rootNode, n, m;
vi topSort, articulationPoints;
vii bridges;

void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}
void dfs(int node) {
	visited[node] = DFS_GRAY;
	arr[node] = timeCounter ++;
	int neighbours = adjMat[node].size();
	// deepest back edge arrival time of "node"
	int DBE = arr[node];
	// is This node an articulation point
	int isArticulation = false;
	// number of children of this node in spanning tree
	int numChildren = 0;
	for (int i = 0; i < neighbours; ++i) {
		pii edge = adjMat[node][i];
		// edgeToNode is the edge to which this edge go from "node"
		int edgeToNode = edge.first, edgeWeight = edge.second;
		if (visited[edgeToNode] == DFS_WHITE) {
			numChildren ++;
			dfsParent[edgeToNode] = node;
			dfs(edgeToNode);
			DBE = min(DBE, deepestBackEdge[edgeToNode]);
			isArticulation = isArticulation || (deepestBackEdge[edgeToNode] >= arr[node]);
		} else if (visited[edgeToNode] == DFS_GRAY && edgeToNode != dfsParent[node]) {
			DBE = min(DBE, arr[edgeToNode]);
		}
	}
	// ddeep back edge of this node
	deepestBackEdge[node] = DBE;
	//for bridges start
	if (node != rootNode && DBE == arr[node]) {
		int bridgeStart = min(dfsParent[node], node), bridgeEnd = max(dfsParent[node], node);
		bridges.push_back(make_pair(bridgeStart, bridgeEnd));
	}
	//for bridges end

	// for articulation points start
	if (node == rootNode) {
		if (numChildren > 1) {
			articulationPoints.push_back(node);
		}
	} else {
		if (isArticulation) {
			articulationPoints.push_back(node);
		}
	}
	// for articulation points end
	nodeForDepTime[timeCounter] = node;
	dep[node] = timeCounter ++;
	visited[node] = DFS_BLACK;
	topSort.push_back(node);
}

void initialize () {
	for( int i = 0; i < MAX_NODES; ++i) {
		adjMat[i].clear();
	}
	timeCounter = 0;
	memset(visited, DFS_WHITE, sizeof visited);
	memset(nodeForDepTime, -1, sizeof nodeForDepTime);
	memset(dfsParent, -1, sizeof dfsParent);
	memset(deepestBackEdge, MAX_NODES + 1, sizeof deepestBackEdge);
	topSort.clear();
	articulationPoints.clear();
	bridges.clear();
}

int main () {
	int from, to, numEdges;
	char waste;
	while (scanf("%d", &n) != EOF) {
		initialize();
		for (int i = 0; i < n; ++i) {
			scanf("%d%c%c%d%c%c", &from,&waste, &waste, &numEdges, &waste, &waste);
			for (int e = 0; e < numEdges; ++e) {
				scanf("%d", &to);
				makeEdge(from, to, 0);
			}
		}
		if (n == 0) {
				printf("0 critical links\n");
		} else {
			for (int i = 0; i < n; ++i) {
				if (!visited[i]) {
					rootNode = i;
					dfs(i);
				}
			}
			sort(bridges.begin(), bridges.end());
			printf("%d critical links\n", bridges.size());
			for (int i = 0; i < bridges.size(); ++i) {
				printf("%d - %d\n", bridges[i].first, bridges[i].second );
			}
		}
		printf("\n");
	}
	return 0;

}