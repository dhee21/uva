# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define MAX_NODES 210
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef map <int, int> mii;
matrix adjMat(MAX_NODES);


// prob specififc vars
int nodes[MAX_NODES];
int visited[MAX_NODES], owner[MAX_NODES], m, n;
// prob specififc vars


void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}
void connectNodes () {
	fr(i, 0, n) {
		int nodeVal = nodes[i];
		if (nodeVal) {
			fr(j, n, n + m) {
				if (nodes[j] % nodeVal == 0) makeEdge(i, j, 0);
			}
		} else {
			fr(j, n, n + m) {
				if (nodes[j] == 0) makeEdge(i, j, 0);
			}
		}
		
	}
}

void init () {
	fr(i, 0, MAX_NODES) {
		adjMat[i].clear();
	}
}

// MCBM functions start
int canAugmentPath (int leftNode) {
	int neighbours = adjMat[leftNode].size();
	if (visited[leftNode]) return 0;
	visited[leftNode] = 1;
	fr (i, 0, neighbours) {
		int rightNode = adjMat[leftNode][i].first;
		if (owner[rightNode] == -1 || canAugmentPath(owner[rightNode])) {
			owner[rightNode] = leftNode;
			return 1;
		}
	}
	return 0;
}

int MCBM() {
	int pairsMade = 0;
	fr(i, 0, MAX_NODES) owner[i] = -1;
	fr (i, 0, n) {
		memset(visited, 0, sizeof visited);
		pairsMade += canAugmentPath(i);
	}
	return pairsMade;
}
// MCBM functions ends

int main () {
	int t, caseNo = 0;
	ipInt(t);
	while (t--) {
		caseNo++;
		init();
		ipInt(n);
		fr (i, 0, n) {
			ipInt(nodes[i]);
		}
		ipInt(m);
		fr(i, n, n + m) {
			ipInt(nodes[i]);
		}
		connectNodes();
		int mcbm = MCBM();
		printf("Case %d: %d\n",caseNo, mcbm);
	}
}