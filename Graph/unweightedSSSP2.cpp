# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
bool visited[6][30][30][6];
char grid[30][30];
typedef pair <int ,int> pii;
#define inf 1 << 30
int rows, cols, ans;

class state
{
public:
	int dir, dist, row, col, color;
	state () {}
	state (int a, int b, int c,int d, int e) {
		dir = a; dist = b; row = c; col = d; color = e;
	}
};

struct coor {
	int r, c;
};
coor start, last;
state initialState, finalState;

void initialize () {
	memset (visited, 0, sizeof visited);
	initialState = state (0, 0, start.r, start.c, 0);
	ans = inf;
}

state giveStateFront (state presentState) {
		// state newState = presentState;
		int  dir = presentState.dir;
		switch(dir) {
			case 0:
				presentState.row--;
				break;
			case 1:
				presentState.col++;
				break;
			case 2:
				presentState.row++;
				break;
			case 3: 
				presentState.col--;
				break;
		}
		presentState.dist++;
		presentState.color = (presentState.color + 1 ) % 5;
		return presentState;

}
state giveStateRight (state presentState) {
	int dir = presentState.dir;
	dir = (dir + 1) % 4;
	presentState.dir = dir;
	presentState.dist ++;
	// presentState.color = (presentState.color + 1 ) % 5;

	return presentState;
}
state giveStateLeft ( state presentState) {
	int dir = presentState.dir;
	dir = dir == 0 ? 3 : dir - 1;
	presentState.dir = dir;
	presentState.dist ++;
	// presentState.color = (presentState.color + 1 ) % 5;
	return presentState;
}

bool canGo (int row, int col) {
	return row >= 0 && row < rows && col >= 0 && col < cols && grid[row][col] != '#';
}
 
void bfs () {
	queue <state> q;
	q.push(initialState);
	visited[initialState.dir][initialState.row][initialState.col][initialState.color] = 1;
	while (!q.empty()) {
		state presentState = q.front(); q.pop(); 
		if (presentState.row == last.r && presentState.col == last.c && presentState.color == 0) {
			ans = min (ans, presentState.dist );
		} else {
			state neighbours[] = { giveStateFront(presentState), giveStateRight(presentState) , giveStateLeft(presentState) };
			for (int i = 0 ; i < 3; ++i) {
				int dist = neighbours[i].dist, dir = neighbours[i].dir, row = neighbours[i].row
				, col = neighbours[i].col, color = neighbours[i].color;
				if (canGo(row, col) && !visited[dir][row][col][color]) {
					visited[dir][row][col][color] = 1;
					q.push(neighbours[i]);
				}
			}
		}
	}
}

int main () {
	int caseNo = 0;
	while (scanf("%d %d\n", &rows, &cols) && (rows || cols)) {
		if (caseNo) {
			printf("\n");
		}
		caseNo ++;
		//nput start	
		for (int i = 0; i < rows; ++i) {
			scanf("%s\n", grid[i]);
			for (int j = 0; j < strlen(grid[i]); ++j) {
				if (grid[i][j] == 'S') {
					start.r = i; start.c = j;
				} else if (grid[i][j] == 'T') {
					last.r = i; last.c = j;
				}
			}
		}
		//nput ends

			initialize();
			
			bfs();
			printf("Case #%d\n", caseNo );
			if (ans == inf) {
				printf("destination not reachable\n");
			} else {
				printf("minimum time = %d sec\n", ans );
			}
	}
	return 0;
}