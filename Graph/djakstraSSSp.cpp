# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
#define MAX_NODES 1000
#define inf 1 << 30
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
matrix adjMat(MAX_NODES);
// SSSP djakstra variables starts
int dist[MAX_NODES + 100];
priority_queue<pii> pq;
// SSSP djakstra variables ends
void initialize () {
	memset(dist, inf, sizeof dist);
	while (!pq.empty()) pq.pop();
}
void makeDiEdge(int from, int to, int edgeWeight) {
	adjMat[from].push_back(pii(to, edgeWeight));
}
void SSSP (int source) {
	initialize();
	pq.push(pii(0, -source));
	dist[source] = 0;
	while (!pq.empty()) {
		pii minUpperbound = pq.top(); pq.pop();
		int node = -minUpperbound.second, minDist = -minUpperbound.first;
		if (dist[node] == minDist) {
			int neighbours = adjMat[node].size();
			// Relaxing dist of neighbours
			for (int i = 0 ; i < neighbours; ++i) {
				int edgeToNode = adjMat[node][i].first, edgeWeight = adjMat[node][i].second;
				int newDist = minDist + edgeWeight;
				if (newDist < dist[edgeToNode]) {
					dist[edgeToNode] = newDist;
					pq.push(pii(-newDist, -edgeToNode));
				}
			}
		}
	}
}
