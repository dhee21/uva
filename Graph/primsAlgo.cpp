# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
using namespace std;
#define MAX_NODES 1100
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
matrix adjMat(MAX_NODES);

int mstCost, n, m;

// prims variables
priority_queue<pii> pq;
int taken[MAX_NODES];
//prims variable ends

void process (int node) {
	taken[node] = true;
	int neighbours = adjMat[node].size();
	for (int i = 0; i < neighbours; ++i) {
		int edgeToNode = adjMat[node][i].first, edgeWeight = adjMat[node][i].second;
		if (taken[edgeToNode] == false) {
			pq.push(make_pair(-edgeWeight, -edgeToNode));
		}
	}
}
void primsAlgo () {
	memset(taken, false, sizeof taken);
	mstCost = 0;
	process(0);
	while (!pq.empty()) {
		pii edge = pq.top(); pq.pop();
		int edgeWeight = -edge.first, edgeToNode = -edge.second;
		if (taken[edgeToNode] == false) {
			mstCost += edgeWeight;
			process(edgeToNode);
		}
	}
}


