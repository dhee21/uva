# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <map>
#include <list>
using namespace std;
#define MAX_NODES 55
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef list<int> li;
matrix adjMat(MAX_NODES);
vector<edge> EdgeList(MAX_EDGES);
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}
list<int> cycle;


void init () {
	fr(i, 0, MAX_NODES) {
		adjMat[i].clear();
	}
	cycle.clear();
}

void eulerPath(li :: iterator pos, int node) {
	int neighbours = adjMat[node].size();
	fr(i, 0, neighbours) {
		int edgeToNode = adjMat[node][i].first, edgeWeight = adjMat[node][i].second;
		if (edgeWeight == 1) {
			adjMat[node][i].second = 0;
			fr(j, 0, adjMat[edgeToNode].size()) {
				if (adjMat[edgeToNode][j].first == node && adjMat[edgeToNode][j].second == 1) {
					adjMat[edgeToNode][j].second = 0;
					break;
				}
			}
			eulerPath(cycle.insert(pos, node), edgeToNode);
		}
	}
}

int main () {
	int t;
	int from, to, caseNo = 0;

	ipInt(t);
	while (t--) {
		if (caseNo++) printf("\n");
		init();
		int n;
		ipInt(n);
		fr(i, 0, n) {
			ipInt(from);ipInt(to);
			makeEdge(from, to, 1);
		}
		int noPath = false;
		fr(i, 0, 50) {
			if (adjMat[i].size() & 1) {
				noPath = true;
				break;
			}
		}
		printf("Case #%d\n",caseNo );
		if (noPath) {
			printf("some beads may be lost\n");
		} else {
			eulerPath( cycle.begin() , from);
			for (li :: iterator it = cycle.begin(); it != cycle.end(); ++it) {
				li :: iterator itX = it;
				itX ++;
				if (itX != cycle.end()) {
					printf("%d %d\n",*it, *(itX));
				} else {
					printf("%d %d\n",*it, *cycle.begin() );
				}
			}
		}

	}
	return 0;
}