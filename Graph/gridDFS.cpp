# include <stdio.h>
# include <iostream>
# include <string.h>
using namespace std;
int m, n, visited[25][25], maxLandMass = 0, landMass, kingR, kingC;
char grid[25][25];
int dir[4][2] = {{1, 0}, {0, -1}, {0, 1}, {-1, 0}};
char land;

void dfs (int r, int c) {
	visited[r][c] = true;
	++landMass;
	for (int i = 0; i < 4; ++i) {
		int newR = r + dir[i][0], newC = (c + dir[i][1]) == -1 ? n - 1 :  (c + dir[i][1]) % n;
		if (newR >= 0 && newR < m && !visited[newR][newC] && grid[newR][newC] == land) {
			dfs(newR, newC);
		}
	}
}

int main () {
	while (scanf("%d %d\n", &m, &n) != EOF) {
		maxLandMass = 0;
		for (int r = 0; r < m; ++r){
			for (int c = 0; c < n; ++c) {
				scanf("%c", &grid[r][c]);
			}
			scanf("\n");
		} 
		scanf("%d %d", &kingR, &kingC);
		memset(visited, 0, sizeof visited);
		land = grid[kingR][kingC];
		dfs(kingR, kingC);
		for (int r = 0; r < m; ++r){
			for (int c = 0; c < n; ++c) {
				if (!visited[r][c] && grid[r][c] == land) {
					landMass = 0;
					dfs(r, c);
					maxLandMass = max (maxLandMass, landMass);
				}
			}
		}
		printf("%d\n", maxLandMass );
	}
	return 0;
}