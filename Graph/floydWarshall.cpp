# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 110
#define inf (1<<30) - 1
//macros

// floyd warshall variables
// adjacency matrix and parent matrix
int adjMat[MAX_NODES][MAX_NODES], p[MAX_NODES][MAX_NODES], n;
// floyd warshall variables


void floydWarshall () {
	initialize();
	fr (k, 0, n) fr (i, 0, n) fr (j, 0, n) {
		if ( adjMat[i][k] + adjMat[k][j] < adjMat[i][j]) {
			adjMat[i][j] = adjMat[i][k] + adjMat[k][j];
			p[i][j] = p[k][j];
		}
	}
}

void initialize () {
	fr (i, 0, MAX_NODES) fr(j, 0, MAX_NODES) {
		adjMat[i][j] = (i == j) ? 0 : inf;
		p[i][j] = i;
	}
}

// never forget to initialize test case
int main () {
	int p1, p2;
	while (scanf("%d %d"), &p1, &p2) && (p1 || p2) ) {
		initialize();
		n = 1;
		adjMat[p1 - 1][p2 - 1] = 1;
		while (scanf("%d %d"), &p1, &p2) && (p1 || p2) ) {
			adjMat[p1 - 1][p2 - 1] = 1;
			n++;
		}
		floydWarshall();
	}
}