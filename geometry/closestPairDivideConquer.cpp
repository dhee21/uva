# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (numeric_limits<double>::max())
# define EPS 1e-9
#define PI (2 * acos(0))
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

struct point { double x, y;
	point (double _x = 0.0, double _y = 0.0) { x = _x; y = _y; }
	bool operator < (point other) {
		if (x == other.x) return y < other.y;
		if (x < other.x) return true;
		return false;
	}
 };
 bool cmp(point left, point right) {
 	if (left.x == right.x) return left.y < right.y;
	if (left.x < right.x) return true;
	return false;
 }
double sq(double x) { return x * x; }
double dist (point p1, point p2) { return sq(p1.x - p2.x) + sq(p1.y - p2.y); }

point pointArr[10010];

double closestPair(int st, int ed) {
	if (st >= ed) return (double) inf;
	if (st == ed - 1) {
		return dist(pointArr[st], pointArr[ed]);
	}
	int mid = (st + ed) >> 1;
	double d1 = closestPair(st , mid), d2 = closestPair(mid + 1, ed);
	double delta = min(d1, d2);
	for (int i = mid; i >= 0 && sq(pointArr[i].x - pointArr[mid].x) < delta; --i) {
		for (int j = mid + 1; j <= ed && sq(pointArr[j].x - pointArr[mid].x) < delta; ++j) {
			delta = min (delta, dist(pointArr[i], pointArr[j]));
		}
	}
	return delta;
}

int main () {
	int n;
	while (ipInt(n) && n){
		fr (i, 0, n) scanf("%lf %lf", &pointArr[i].x, &pointArr[i].y);
		sort(pointArr, pointArr + n, cmp);
		double distanceVal = sqrt(closestPair(0, n - 1));
		if (distanceVal < 10000) printf("%0.4lf\n", distanceVal);
		else printf("INFINITY\n");
	}


	
	return 0;
}