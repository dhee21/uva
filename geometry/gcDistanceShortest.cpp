# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<29)
# define EPS 1e-9
#define PI  3.141592653589793
#define DEG_TO_RAD(val) ((double)PI * val) / 180.0
#define inside 0
#define onCircle  1
#define outside  2
#define MAX_NODES 102
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

map <string , pdd > cities;
mSI indexOf;
int size;
string nodes[110];
int adjMat[102][102];
// gives great circle distance bw p and q pt on sphere
double gcDistance (double pLat, double pLong, double qLat, double qLong, double radius) {
	  pLong = DEG_TO_RAD(pLong); pLat = DEG_TO_RAD(pLat); qLat = DEG_TO_RAD(qLat); qLong = DEG_TO_RAD(qLong);
	  double dlon = pLong - qLong; 
	  double dlat = pLat - qLat; 
	  double a = pow((sin(dlat/2)),2) + cos(pLat) * cos(qLat) * pow(sin(dlon/2), 2); 
	  double c = 2 * atan2(sqrt(a), sqrt(1-a)); 
	  double d = radius * c; 
      return d;
}

void makeNode (char *city) {
	indexOf[city] = size;
	nodes[size++] = city;
}
void makeEdge (char *from, char *to) {
	pdd fromCoor = cities[from], toCoor = cities[to];
	adjMat[indexOf[from]][indexOf[to]] = (int)round (gcDistance(fromCoor.first, fromCoor.second, toCoor.first, toCoor.second, 6378));
}


void floydWarshall (int n) {
	fr (k, 0, n ) fr(i, 0, n) fr (j, 0, n) {
		if (adjMat[i][k] + adjMat[k][j] < adjMat[i][j]) {
			adjMat[i][j] = adjMat[i][k] + adjMat[k][j];
		}
	} 
}

void init () {
	cities.clear();
	indexOf.clear();
	size = 0;
	fr (i, 0, MAX_NODES) fr (j, 0, MAX_NODES) {
		adjMat[i][j] = (i == j) ? 0 : (double)inf;
	}
}

int main () {
	char city[100];
	int n, m, q, caseNo = 0;
	double lati, longi;
	while (scanf("%d %d %d", &n, &m, &q) && (n || m || q)) {
		if (caseNo++) printf("\n");
		init();
		fr (i, 0, n) {
			scanf("%s %lf %lf", city, &lati, &longi);
			cities[city] = pdd(lati, longi);
			makeNode(city);
		}
		fr (i, 0, m) {
			char from[100], to[100];
			scanf("%s %s", from, to);
			makeEdge(from, to);
		}
		floydWarshall(n);
		printf("Case #%d\n",caseNo );
		fr (i, 0, q) {
			char from[100], to[100];
			scanf("%s %s", from, to);
			int distance = adjMat[indexOf[from]][indexOf[to]];
			if (distance != inf) {
				printf("%d km\n",distance );
			} else {
				printf("no route exists\n");
			}
		}

	}
	return 0;
}
