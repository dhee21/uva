#include <iostream>
#include <stdio.h>
#include <vector>
#include <list>
#include <utility>
using namespace std ;
typedef vector<vector<char> > vvc;
void connect(char area1, char area2, vvc &adjList) {
    // printf("%d%d%d", area1 - 'A', area2- 'A' , adjList.size());
	adjList[area1 - 'A'].push_back( area2 );
	adjList[area2 - 'A'].push_back( area1 ) ; 
}

int findNumYears ( list<char> &slept, vvc &adjList, int &awake, int &awakeNum, int numberSleptAreas) {
	int year = 0;
	while(slept.size() && (awakeNum < numberSleptAreas)) {
		// printf("%d\n", awake );
		list<char> :: iterator it, storeIt;
		vector<char > toAwake;
		for(it = slept.begin() ; it != slept.end() ;) {
			// printf("%c\n", *it );
			int willAwake = 0;
			for(int i = 0 ; i < adjList[(*it) - 'A'].size() ; i++) {
				if(awake & (1 << (adjList[(*it) - 'A'][i]) - 'A' )) {
					willAwake++;
				}
			}
			if(willAwake >= 3) {
				toAwake.push_back((*it));
				slept.erase(it++);
				// it= storeIt;

			} else {
				++it;
			}
		}
		if(!toAwake.size()) {
			return -1 ;
		}
		for(int i = 0 ; i< toAwake.size() ;++i) {
			awake = awake | (1 << (toAwake[i] - 'A'));
			awakeNum ++ ;
		}
		year++;
	}
	return year;
}

int main () {
	int numberSleptAreas, numConnections, awake=0, awakeNum=0;
	while(scanf("%d",&numberSleptAreas) != EOF) {
		awakeNum = 0;
		awake = 0;
		scanf("%d\n", &numConnections);
		// printf("%d\n%d\n",numberSleptAreas, numConnections );
		char awake1, awake2, awake3;
		scanf("%c%c%c\n", &awake1, &awake2 , &awake3);
		awake = awake | (1 << awake1-'A');
		awake = awake | (1 << awake2-'A' ) ;
		awake = awake | (1 << awake3-'A') ;
		awakeNum+=3;
		// printf("%c%c%c\n",awake1, awake2, awake3 );
		 vvc adjList(30);
		 while(numConnections--) {
		 	char area1, area2;
		 	scanf("%c%c\n", &area1, &area2);
		 	// printf("%c%c\n",area1, area2 );
		 	connect(area1, area2, adjList);
		 }
		list<char> slept;
		for(int i=0 ; i < 26 ; ++i) {
			if(!(awake & (1 << i))) {
			   slept.push_back('A' + i);
			}
		}
		int years = findNumYears(slept, adjList, awake, awakeNum, numberSleptAreas);
		(years != -1) ? printf("WAKE UP IN, %d, YEARS\n", years ) : printf("THIS BRAIN NEVER WAKES UP\n");
	}
	return 0 ;
}