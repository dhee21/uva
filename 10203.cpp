# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <list>
# include <bitset>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 210
#define inf (1<<30) - 1

//typedefinitions
typedef pair<int, int> pii;
typedef pair<int, float> pif;
typedef vector<int> vi;
typedef vector<pif> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef map<pii> mpii;
typedef list<int> li;
//typedefinitions
matrix adjMat(MAX_NODES);
pii nodes[MAX_NODES];
li cyc;
int nodeindex;
float eulerCost;
mpii indexOf;

void EulerTour(li :: iterator pos, int node) {
	int neighbours = adjMat[node].size();
	fr (i, 0, neighbours) {
		int edgeToNode = adjMat[node][i].first;
		float edgeWeight = adjMat[node][i].second;
		if (edgeWeight) {
			eulerCost += edgeWeight;
			adjMat[node][i].second = 0;
			fr(j, 0, adjMat[edgeToNode].size()) {
				if (adjMat[edgeToNode][j].first == node && adjMat[edgeToNode][j].second) {
					adjMat[edgeToNode][j].second = 0;
					break;
				}
			}
			EulerTour(cyc.insert(pos, node), edgeToNode);
		}
	}
}
void makeEdge(pii fromCoor, pii toCoor) {
	int from = indexOf[fromCoor], to = indexOf[toCoor];
	float weight = sqrt(pow((fromCoor.first - toCoor.first), 2) + pow((fromCoor.second - toCoor.second) , 2));
	adjMat[from].push_back(pii(to, weight));
	adjMat[to].push_back(pii(from, weight));
}
void makeNode (pii coor) {
	if (indexOf.count(coor) == 0) {
		indexOf[coor] = nodeindex++; 
	}
}

void initialize () {
	fr(i, 0, MAX_NODES) adjMat[i].clear();
	nodeindex = 0;
	indexOf.clear();
}

int main () {
	int t;
	char inp[50];
	ipInt(t); scanf("\n");
	while(t--) {
		fgets(inp, 50, stdin);
		pii start;
		sscanf(inp, "%d %d", &start.first, &start.second);
		makeNode(start);
		while(fgets(inp, 50, stdin) != NULL && strlen(inp) > 1) {
			pii from, to;
			sscanf(inp, "%d %d %d %d" , &from.fist, &from.second, &to.first, &to.second);
			makeNode(start);makeNode(end); makeEdge(from, to);
		}
			eulerCost = 0;
			EulerTour(cyc.begin(), 0);
			printf("%f\n", eulerCost);
			// string timeVal = calculateTime();
			// printf("%s\n", timeVal.data() );
	}
	return 0;
}

