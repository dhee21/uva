# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

char graph[150][150];
int maxRow, row, minRow;
void print( char type, int col) {
	// cout << type << "\n";
	if (type == 'R') {
		graph[row][col] = '/';
		maxRow = max(row, maxRow);
		minRow = min(row, minRow);
		row++;
	} else if (type == 'F') {
		row--;
		graph[row][col] = '\\';
		minRow = min(row, minRow);
		maxRow = max(maxRow, row);
	} else if (type == 'C') {
		graph[row][col] = '_';
		maxRow = max(maxRow, row);
		minRow = min(row, minRow);
	}
}

int main () {
	int t, caseNo = 0;
	ipInt(t);
	char input[70];
	while (t--) {
		caseNo++;
		printf("Case #%d:\n",caseNo);
		fr (i, 0, 150) fr (j, 0, 150) graph[i][j] = ' ';
		// input initialization
		scanf("%s", input);
		row = 70; maxRow = 70; minRow = 70;
		int len = strlen(input);

		fr (col, 0, len) print(input[col], col);
		for (int r = maxRow; r >= minRow; --r) {
			int colVal ;
			for (colVal = len - 1; colVal >=0; --colVal) {
				if (graph[r][colVal] != ' ') break;
			}
			if (colVal == -1) continue;
			printf("| ");
			fr (c, 0, colVal + 1) printf("%c",graph[r][c]);
			printf("\n");
		}
		printf("+-");
		fr(i, 0, len + 1) printf("-");
		printf("\n\n");
	}

	return 0;
}