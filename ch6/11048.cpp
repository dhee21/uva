# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;

char dict[10010][30];
int freq[30];
mSI dictionary;

pii check (char *word, int dicSize) {
	int lenWord = strlen(word);
	fr (i, 0, dicSize) {
		int lenDicWord = strlen(dict[i]);
		vi mismatchIndex;
		if (lenDicWord == lenWord) {
			int mismatches = 0;
			fr (j, 0, lenWord) {
				mismatches += (word[j] != dict[i][j]);
				if (word[j] != dict[i][j]) mismatchIndex.push_back(j);
			}
			if (mismatches == 1) return pii(1, i);
			else if (mismatches == 0) return pii(0, i);
			else if (mismatches > 2) continue;
			else {
				int first = mismatchIndex[0], sec = mismatchIndex[1];
				if (sec == first + 1 && word[first] == dict[i][sec] && word[sec] == dict[i][first]) return pii(1, i);
				else continue;
			}
		} else if (abs(lenDicWord - lenWord) > 1) {
			continue;
		} else {
			char *a = word, *b = dict[i];
			while (*a != '\0' && *b !='\0' && *a == *b) {++a; ++b;}
			if (*a == '\0' || *b == '\0') return pii(1, i);
			(lenWord > lenDicWord) ? a++ : b++;
			while (*a != '\0' && *b != '\0' && *a == *b) {++a; ++b;}
			if (*a == '\0' && *b =='\0') return pii(1, i);
			else continue;
		}
	}
	return pii(-1, 2);
}

int main () {
	int dicSize;
	ipInt(dicSize);
	fr (i, 0, dicSize) {
		scanf("%s", dict[i]);
		dictionary[dict[i]] = 1;
	}
	int numWords;
	ipInt(numWords);
	fr (i, 0, numWords) {
		char word[30] ;
		scanf("%s", word);
		// string wordStr = word;
		pii ans = check(word, dicSize);
		if (dictionary.count(word)) {
			printf("%s is correct\n", word);
		} else if (ans.first == 1) {
			printf("%s is a misspelling of %s\n",word, dict[ans.second] );
		} else {
			printf("%s is unknown\n",word);
		}
	}
	return 0;
}