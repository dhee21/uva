# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i <= ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

string input;

pii isExpression(int, int);

pii isFactor (int st, int ed) {
	// cout << st << "  fac  " << ed << endl;

	if (st > ed) return pii (0, 1);
	string subStr = input.substr(st, ed - st + 1); 
	int val ;
	int isNumber = true;
	fr (i, st, ed) if (isdigit(input[i]) == false) {isNumber = false; break;} 
	if (isNumber) {
		// cout << "yes" << subStr;
		val = atoi(subStr.c_str());
		// cout << " " << val << endl;
		return pii(1, val);
	} 
	if (input[st] == '(' && input[ed] == ')') {
		// cout << "oy " << st << " " << ed << endl;
		pii isExp =  isExpression(st + 1, ed - 1);
		return isExp;
	} 
	return pii (0, 1);
}

pii isComponent (int st, int ed) {
	// cout << st << " com " << ed << endl;

	if (st > ed) return pii(0, 1);
	pii isFact = isFactor(st, ed);
	if (isFact.first) return isFact;
	fr (i, st, ed) if (input[i] == '*') {
		pii isFactorLeft = isFactor(st, i - 1), isComponentRight = isComponent(i + 1, ed);
		if (isFactorLeft.first && isComponentRight.first) {
			// cout << " see val * " << isFactorLeft.second * isComponentRight.second << endl;
			return pii(1, isFactorLeft.second * isComponentRight.second);
		}
	}
	return pii(0, 1);
}

pii isExpression (int st, int ed) {
	// cout << st << " exp " << ed << endl;
	if (st > ed) return pii(0, 1);
	pii isComp = isComponent(st, ed);
	if (isComp.first) return isComp;
	// else cout << st << " "<< ed << " is not a comp";
	fr (i, st, ed) if (input[i] == '+') {
		pii isCompLeft = isComponent(st, i - 1), isExpressionRight = isExpression(i + 1, ed);
		if (isCompLeft.first && isExpressionRight.first) {
			// cout << " see val + " << isCompLeft.second + isExpressionRight.second << endl;
			return pii(1, isCompLeft.second + isExpressionRight.second);
		}
	}
	return pii (0, 1);
}

int main () {
	int t;
	ipInt(t);
	while (t--) {
		cin >> input;
		// cout << input << endl;
		pii ans = isExpression(0, input.size() - 1);
		if (ans.first == 1) {
			printf("%d\n",ans.second);
		} else {
			printf("ERROR\n");
		}
		input.clear();
	}
	return 0;
}