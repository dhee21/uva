# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

lli fib[65];
map <lli, int> indexOfFib;
int main () {
	int t;
	lli one = 1;
	lli last = (one << 31) - 1;
	// printf("%lld\n", last );
	fib[1] = 1; fib[2] = 2;
	indexOfFib[1] = 0; indexOfFib[2] = 1;
	fr(i, 3, 62) {
		fib[i] = fib[i - 1] + fib[i - 2];
		indexOfFib[fib[i]] = i - 1;
		if(fib[i] > last) break;
	}
	char input[110], ans[110];
	lli inputFib[70];

	ipInt(t);
	while (t--) {
		int n;
		ipInt(n);
		fr(i, 0, n) scanf("%lld", &inputFib[i]);
		fr(i, 0, 110) ans[i] = ' ';
		scanf("\n");
		fgets(input, 110, stdin);
		if(input[strlen(input) - 1] == '\n') input[strlen(input) - 1] = '\0';
		int index = 0, maxPosition = -1;

		fr (i, 0, strlen(input)) {
			if (input[i] >= 'A' && input[i] <= 'Z' && index < n) {
				int position = indexOfFib[inputFib[index]];
				index++;
				ans[position] = input[i];
				maxPosition = max (position, maxPosition);
			}
		}
		ans[maxPosition + 1] = '\0';
		// cout << "yo";
		printf("%s\n",ans);
	}
	return 0;
}