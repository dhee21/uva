# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;

char input[1100];
int dp[1010][1010];

int main () {
	int t;
	ipInt(t);
	getchar();
	while (t--) {
		fgets(input, 1100, stdin);
		if (input[strlen(input) - 1] == '\n') input[strlen(input) - 1] = '\0';
		int len = strlen (input);
		if (!len) { printf("0\n"); continue; }

		fr (st, 0, len) {
			dp[st][st] = 1;
			if (st + 1 < len) dp[st][st + 1] = ( input[st] == input[st + 1] ) ? 2 : 1;
		}
		fr (length, 3, len + 1) {
			int goTill = len - length;
			fr (left, 0, goTill + 1) {
				int right = left + length - 1;
				dp[left][right] = input[left] == input[right] ? 2 + dp[left + 1][right - 1] : max(dp[left + 1][right], dp[left][right - 1]);
			}
		}
		printf("%d\n",dp[0][len - 1]);
	}
	return 0;
}