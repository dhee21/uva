# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
mSI freq;
int main () {
	int len;
	char c;
	string input;
	while (ipInt(len) != EOF) {
		cin >> input;
		// cout << input << endl;
		int maxFreq = 0;
		string maxString;
		fr (i, 0, input.size() - len + 1) {
			string subStr = input.substr(i, len);
				freq[subStr]++;
			maxString = freq[subStr] > maxFreq ? subStr : maxString;
			maxFreq = max(maxFreq, freq[subStr]);
		}
		cout << maxString << endl;
		freq.clear();
		input.clear();
	}
	return 0;
}