# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

int main () {
	int n;
	ipInt(n);
	char inp[60];
	while (n--) {
		scanf("%s", inp);
		int len = strlen(inp);
		int index = 0, leftLen = 0;
		while (index < len && inp[index] != 'M' && inp[index] == '?') {index++; leftLen++;}
		if (index >= len || inp[index] != 'M' || leftLen == 0) {
			printf("no-theorem\n"); continue;
		}
		// printf("1\n");
		index++;
		int middleLen = 0;
		while (index < len && inp[index] != 'E' && inp[index] == '?') {index++; middleLen++;}
		if (index >= len || inp[index] != 'E' || middleLen == 0) {
			printf("no-theorem\n"); continue;
		}
		index++;
		// printf("2\n");
		int rightLen = 0;
		while (index < len && inp[index] == '?') {index++; rightLen++;}
		if (index < len || rightLen < 2) {
			printf("no-theorem\n"); continue;
		}
		int extraLen = rightLen - (leftLen + 1);
		// cout << extraLen << middleLen - 1 << endl;
		if (extraLen < 0 || (middleLen - 1) != extraLen) {
			printf("no-theorem\n"); continue;
		}
		printf("theorem\n");
	}
	return 0;
}