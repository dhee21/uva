# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1


//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;

char grid [20][20];
int visited [20][20];
mSI dict;
int maxLen;
int dir[8][2] = { {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1} };

void initialize () {
	dict.clear();
}

int addScore (int len) {
	if (len == 3 || len == 4) return 1;
	if (len == 5 ) return 2;
	if (len == 6 ) return 3;
	if (len == 7 ) return 5;
	if (len >= 8 ) return 11;
	return 0;
}

bool isOutside ( int r, int c ) {
	return r < 0 || c < 0 || r >= 4 || c >= 4;
}

bool doesMatchWord (char *word, int row, int col) {
	if (*word == '\0') return true;
	if (isOutside(row, col)) return false;
	if (*word != grid[row][col]) return false;
	visited[row][col] = 1;
	fr (i, 0, 8) {
		int r = row + dir[i][0], c = col + dir[i][1];
		if (!visited[r][c] &&  doesMatchWord(word + 1, r, c)) return true;
	}
	visited[row][col] = false;
	return false;
}

bool existInGrid (char *word) {
	fr (i, 0, 4) fr (j, 0, 4) {
		if(word[0] == grid[i][j]) {
			memset(visited, 0, sizeof visited);
			if (doesMatchWord(word, i, j)) return true;
		}
	}
	return false;
}

int main () {
	int t, caseNo = 0;
	ipInt(t);
	char word[100];
	while (t--) {
		initialize();
		int ans = 0;
		fr (i, 0, 4) scanf("%s", grid[i]);
		int dicSize;
		ipInt(dicSize); 
		while (dicSize--) {
			scanf("%s", word);
			int len = strlen(word);
			if (dict.count(word) == 0 && existInGrid(word)) {
				ans += addScore(len);
			}
			dict[word] = 1;
		}
		printf("Score for Boggle game #%d: %d\n", ++caseNo, ans );
	}
	return 0;
}