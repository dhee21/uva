# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;

vs codes;

bool isPrefix(string one, string two) {
	int lessIndex = min (one.size(), two.size());
	fr (i, 0, lessIndex) {
		if (one[i] != two[i]) return false;
	}
	return true;
}

void printAns(int caseNo) {
	fr (i, 0, codes.size()) fr(j, i + 1, codes.size()) {
		if (isPrefix(codes[i], codes[j])) {
			printf("Set %d is not immediately decodable\n",caseNo );
			return;
		}
	}
	printf("Set %d is immediately decodable\n",caseNo );
}

int main () {
	int nine, caseNo = 0;
	string inp;
	while (cin >> inp) {
		if (inp == "9") {
			caseNo++;
			printAns(caseNo);
			codes.clear();
		} else {
			codes.push_back(inp);
		}
	}
	return 0;
}