# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1


//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;
typedef map <int , int> mpii;

mpii indexOf;

int A[63000], B[63000], common[63000];
int list[63000], prevIndex[63000], corrIndex[63000];

int findLis (int len) {
	int sizeOfList = 0;
	fr (i, 0, len) {
		int num = common[i];
		int insertIndex = lower_bound(list, list + sizeOfList, num) - list;
		if (insertIndex == 0) {
			prevIndex[i] = -1;
		} else {
			int adjacentIndex = insertIndex - 1;
			prevIndex[i] = prevIndex[corrIndex[adjacentIndex]];
		}
		list[insertIndex] = num; corrIndex[insertIndex] = i;
		if (insertIndex == sizeOfList) sizeOfList++;
	}
	return sizeOfList;
}

void initialize () {
	indexOf.clear();
}

int main () {
	int t, n, p, q, caseNo = 0;;
	ipInt(t);
	while (t--) {
		initialize();
		scanf("%d %d %d", &n, &p, &q);
		fr (i, 0, p + 1) {
			ipInt(A[i]); indexOf[A[i]] = i;
		}
		int sizeCommon = 0;
		fr (i, 0, q + 1) {
			ipInt(B[i]);
			if (indexOf.count(B[i])) common[sizeCommon++] = indexOf[B[i]];
		}
		int ans = findLis(sizeCommon);
		printf("Case %d: %d\n", ++caseNo , ans);
	}
	return 0;
}