# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

int main () {
	char input[20];

	fgets(input, 20, stdin);
	while (fgets(input, 20, stdin)) {

		if (strcmp(input, "___________") == 0) break;
		int index = 9, powOf2 = 1, val = 0;
		fr (i, 0, 3) {
			if (input[index--] == 'o') val += powOf2;
			powOf2 <<= 1;
		}
		index--;
		fr (i, 0, 4) {
			if (input[index--] == 'o') val += powOf2;
			powOf2 <<= 1;
		}
		printf("%c", val);
	}
	return 0;
}