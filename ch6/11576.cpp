# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_N 3000000

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;


char T[MAX_N], P[MAX_N];
int b[MAX_N], n, m;
vi matchIndexes;

void kmpPreprocess () {
	b[0] = -1;
	int i = 0, j = -1;
	while (i < m) {
		while (j >= 0 && P[i] != P[j]) j = b[j];
		j++; i++;
		b[i] = j;
	}
}
// run kmpSearchbefore
int kmpSearch (int startSearchingFrom) {
	int i = startSearchingFrom, j = 0;
	while (i < n) {
		while (j >= 0 && T[i] != P[j]) j = b[j];
		j++; i++;
		if (j == m) {
			int matchedStartIndex = i - m;
			matchIndexes.push_back(matchedStartIndex);
			j = b[j];
		}
	}
	if (matchIndexes.size()) return m;
	return j;
}

void initialize () {
	matchIndexes.clear();
}

int main () {
	int t;
	ipInt(t);
	int k, w, ans;
	while (t--) {
		ans = 0;
		ipInt(k); ipInt(w);
		scanf("%s", T); 
		ans = n = m = k;
		w--;
		while (w--) {
			scanf("%s", P);
			initialize();
			kmpPreprocess();
			int matchedCharNum = kmpSearch(0);
			// cout << T << " " << P << " " << matchedCharNum << endl;
			ans += k - matchedCharNum;
			strcpy(T, P);
		}
		printf("%d\n",ans);

	}
	return 0;
}