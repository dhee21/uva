# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;

char input[25] , ans[25];
int totalLength , freq[30];
lli stringsMade, totalStrings, rankVal;

void initialize () {
	memset(freq, 0, sizeof freq);
	stringsMade = 0;
}
lli fact[21];
void factorial () {
	fact[0] = 1; fact[1] = 1;
	fr (i, 2, 21) fact[i] = fact[i - 1] * i;
}
char charAtPositionI (int index, int len) {
	fr(i, 0, 26) if (freq[i]) {
		lli canBeMade = ((totalStrings * fact[freq[i]]) / len ) /fact[freq[i] - 1];
		if (stringsMade + canBeMade >= rankVal) {
			totalStrings = canBeMade;
			freq[i]--; return ('a' + i);
		} else {
			stringsMade += canBeMade;
		}
	}
	return 'a'; // usless
}

int main () {
	factorial();
	int t;
	ipInt(t);
	while (t--) {
		initialize();
		scanf("%s", input);
		scanf("%lld", &rankVal); rankVal++;
		totalLength = strlen(input);
		fr (i, 0, totalLength) freq[input[i] - 'a']++;
		lli piFreq =1;
		fr (i, 0, 26) piFreq *= fact[freq[i]];
		totalStrings = fact[totalLength] / piFreq;
		int len = totalLength;
		fr (i, 0, totalLength) {
			ans[i] = charAtPositionI(i, len);
			len--;
		}
		ans[totalLength] = '\0';
		printf("%s\n",ans );
	}
	return 0;
}