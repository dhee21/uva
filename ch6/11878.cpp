# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i <= ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

int main () {
	char input[50];
	int correctAns = 0;
	while (scanf("%s", input) != EOF) {
		int a, b;
		char operation, equalSign, ans[10];
		sscanf(input, "%d%c%d%c%s", &a, &operation, &b, &equalSign, ans);
		if (isdigit(ans[0]) == '?') correctAns++;
		else {
			int number = atoi(ans);
			if (operation == '+') {
				correctAns += (a + b) == number;
			} else {
				correctAns += (a - b) == number;
			}
		}
		// cout << correctAns << endl;
	}
	printf("%d\n",correctAns);
	return 0;
}