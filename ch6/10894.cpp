# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

int indexOf[30];
vector<string> horizontal;

void printRow (int pixelSize, int row) {
	fr(col, 0, 61) {
		fr (i, 0, pixelSize) printf("%c", horizontal[row][col]);
	}
}

void printRowVert (string rowToPrint, int pixelSize) {
	fr (times, 0, pixelSize) {
		fr (i, 0, rowToPrint.size()) {
			fr (k, 0, pixelSize) printf("%c",rowToPrint[i]);
		}
		printf("\n");
	}
}

void print(char val, int pixelSize) {
	if (val == ' ') {
		fr (row, 0, 3) {
			printRowVert(".....", pixelSize);
		}
		return;
	}
	int indexStart = indexOf[val - 'A'];
	fr (row, 0, 5) {
		string rowToPrint = horizontal[row].substr(indexStart, 5);
		// cout << "See" << rowToPrint;
		printRowVert(rowToPrint, pixelSize);
	}
	if (val != 'Y' && val != 'E') {
		printRowVert(".....", pixelSize);
	}
}

int main () {

	horizontal.push_back("*****..***..*...*.*****...*...*.*****.*****.***...*****.*...*");
	horizontal.push_back("*.....*...*.*...*.*.......*...*.*...*...*...*..*..*...*..*.*.");
	horizontal.push_back("*****.*****.*...*.***.....*****.*****...*...*...*.*...*...*..");
	horizontal.push_back("....*.*...*..*.*..*.......*...*.*.*.....*...*..*..*...*...*..");
	horizontal.push_back("*****.*...*...*...*****...*...*.*..**.*****.***...*****...*..");
	memset (indexOf, 0, sizeof indexOf);
	// cout << "WEFwef";
	indexOf['S' - 'A'] = 0;
	indexOf['A' - 'A'] = 6;
	indexOf['V' - 'A'] = 12;
	indexOf['E' - 'A'] = 18;
	indexOf['H' - 'A'] = 26;
	indexOf['R' - 'A'] = 32;
	indexOf['I' - 'A'] = 38;
	indexOf['D' - 'A'] = 44;
	indexOf['O' - 'A'] = 50;
	indexOf['Y' - 'A'] = 56;

	int pixelSize, caseNo = 0;
	while (ipInt(pixelSize) && pixelSize) {
			// if (caseNo++) printf("\n\n");
			if (pixelSize > 0) {
				fr (row, 0, 5) {
					int times = pixelSize;
					while (times--) {
						printRow(pixelSize, row);
						printf("\n");
					}
				}
			} else {
				string toPrint = "SAVE HRIDOY";
				for (int i = 0; i < toPrint.size(); ++i) {
					print(toPrint[i], abs(pixelSize));
				}
			}
			printf("\n\n");
	}
	return 0;
}