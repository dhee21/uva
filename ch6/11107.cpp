# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
#include <limits>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_N 101010

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

mSI ans;

////////////////////// segtree class data structure  /////////////////////////////////////////////////////////
#define maxSize 101010
int infinity = numeric_limits<int>::max();

struct treeNode {
      int data;
    } defaultNode = {0},
     infiniteNode = {infinity};
int  treeSize = int (2 * pow(2.0, floor(log(maxSize) / log(2.0) + 1)));
vector<treeNode> tree(treeSize);

class segmentTree {

  public :
    int size;

    void createTree(int* arr, double size) {
      // double size = arr.size();
      this->size = size;
      // int  treeSize = int (2*pow(2.0,floor(log(size)/log(2.0) + 1)));
      // tree.assign(treeSize, defaultNode);
      buildTree(arr, 1, 0, size - 1 );
    }

    treeNode operation (treeNode left , treeNode right) {
        treeNode node;
        node.data = min(left.data, right.data);
        return node;
    }
    void updateLeafNode ( int treeId , int value) {
      tree[treeId].data = value;
    }

    void buildTree(int* arr, int id , int st , int ed) {
        if(st > ed) {
          return ;
        }
        if(st == ed) {
          updateLeafNode(id, arr[st]);
          return; 
        }
        int mid = (st+ed)>>1;
        buildTree(arr, (id<<1) , st, mid );
        buildTree(arr, (id<<1) + 1 , mid+1, ed);
        tree[id] = operation( tree[id<<1] , tree[ (id<<1) + 1 ]);
    }

    treeNode query( int id , int segSt, int segEd , int ourSt, int ourEd ) {
        if(segSt > segEd || ourSt > ourEd || ourSt > segEd || ourEd < segSt) {
          return infiniteNode;
        }
        if(ourSt <= segSt && ourEd >= segEd) {
          return tree[id] ;
        }
        int mid = (segSt + segEd) >> 1 ;
        treeNode left = query(id<<1 , segSt, mid , ourSt , ourEd) ;
        treeNode right = query( (id<<1) + 1 , mid+1 , segEd , ourSt , ourEd );
        return operation( left , right);

    }
    void update( int id , int st, int ed, int index, int newValue ) {
        if( st > ed || index > ed || index < st) {
          return ;
        }
        if( st == ed ) {
          updateLeafNode(id, newValue);
          return ;
        }
        int mid = (st+ed) >> 1;
        update(id<<1 , st , mid , index, newValue);
        update( (id<<1) + 1 , mid+1, ed , index, newValue );
        tree[id] = operation(tree[ id<<1 ] , tree[ (id<<1) + 1 ]);
    }

    void doUpdate(int* array , int index , int newValue) {
      update( 1 , 0 ,this->size -1 , index , newValue);
    }

    int doQuery(int *array , int start , int end ) {
      return query( 1 , 0 ,this->size - 1 , start , end ).data ;
    }
};
////////////////////// segtree class data structure  /////////////////////////////////////////////////////////

// suffixArray Vars
char T[MAX_N], str[MAX_N], ansStrings[MAX_N];
int n;
int SA[MAX_N], tempSA[MAX_N];
int RA[MAX_N], tempRA[MAX_N];
int freq[MAX_N];
// suffixArray Vars
// LCP vars
// previous suffix in Suffix array sorted order
 int Phi[MAX_N];
// permuted LCP
 int PLCP[MAX_N], LCP[MAX_N], colorOf[MAX_N], numSuffixOfColor[110];
// LCP vars, 

void countingSort (int k) {
	int maxRank = max(300, n), sum = 0;
	memset(freq, 0, sizeof freq);
	// for each suffix SA[i] calculate its SA[i] + k th suffix rank and increment its freq .. i.e put it in the bucket
	fr (i, 0, n) {
		int bucketNum = SA[i] + k < n ? RA[SA[i] + k] : 0;
		freq[bucketNum]++;
	}
	fr (i, 0, maxRank + 1) {
		int t = freq[i]; freq[i] = sum; sum += t; 
	}
	// freq[bucketNum] now stores thre index at which the current suff should be in sorted order
	fr (i, 0, n) {
		int bucketNum = SA[i] + k < n ? RA[SA[i] + k] : 0;
		tempSA[freq[bucketNum]++] = SA[i];
	}
	fr (i, 0, n) SA[i] = tempSA[i];

}
// sort the suffix array
void radixSort (int k) {
	countingSort(k); countingSort(0);
}
void constructSA ( ) {
	fr (i, 0, n) SA[i] = i; 
	fr (i, 0, n) RA[i] = T[i] - '.' + 1;
	
	for (int k = 1; k < n; k <<= 1) {
		radixSort(k);
		int r;
		// calculate tempRA using previous ranks i.e RA
		tempRA[SA[0]] = r = 1;
		fr (i, 1, n) {
			bool isRankPairFirstSame = RA[SA[i]] == RA[SA[i - 1]];
			bool isRankPairSecondSame = (SA[i] + k >= n && SA[i - 1] + k >= n)
					|| ( SA[i] + k < n && SA[i - 1] + k < n &&  RA[SA[i] + k] == RA[SA[i - 1] + k] );
			tempRA[SA[i]] = ( isRankPairFirstSame && isRankPairSecondSame ) ? r : ++r;
		}
		// copy tempRA into RA to get the rank of new SA
		fr (i, 0, n) RA[i] = tempRA[i];		
	}
}
// computes longest common prefix length values
void computeLCP () {
	// setting previous suffix of SA[i] in sorted suffic array i.e SA[i - 1] in Phi[SA[i]]
	Phi[SA[0]] = -1;
	fr (i, 1, n) Phi[SA[i]] = SA[i - 1];
	// numbr of characters matched of a suffix with its previous suffix in SA
	int L = 0;
	// iterate on ith suffix
	fr (i, 0, n) {
		if (Phi[i] == -1) { PLCP[i] = 0; continue; }
		while (T[Phi[i] + L] == T[i + L] && (T[i + L] != '.')) L++;
		PLCP[i] = L;
		L = max (L - 1, 0);
	}
	fr (i, 0, n) LCP[i] = PLCP[SA[i]];
}

void addSuffixToWindow (int suffixNum, int &numDiffColors) {
	numDiffColors =  numSuffixOfColor[colorOf[suffixNum]] == 0 ? numDiffColors + 1 : numDiffColors;
	numSuffixOfColor[colorOf[suffixNum]]++;
}

void removeSuffixFromWindow (int suffixNum, int &numDiffColors) {
	numSuffixOfColor[colorOf[suffixNum]]--;
	numDiffColors =  numSuffixOfColor[colorOf[suffixNum]] == 0 ? numDiffColors - 1 : numDiffColors;
}

void printLCS (vector<pii> &commonPreffix, int maxLCP) {
	if (commonPreffix.size() == 0) { printf("?\n"); return; }
	fr (i, 0, commonPreffix.size()) if (commonPreffix[i].second == maxLCP) {
		int stIndex =commonPreffix[i].first, lengthSuff = commonPreffix[i].second;
		int lenAns = 0;
		fr (j, stIndex, stIndex + lengthSuff ) ansStrings[lenAns++] = T[j];
		ansStrings[lenAns] = '\0';
		ans[ansStrings] = 1;
	}
	for (mSI :: iterator it = ans.begin(); it != ans.end(); ++it) {
		printf("%s\n",it->first.data());
	}
}

void mulStringsLCS (int numStrings) {
	segmentTree segTree;
	segTree.createTree(LCP, n);
	vector <pii> commonPreffix;
	int stWindow = 1, edWindow = 0, numDiffColors = 0;
	int maxLCP = 0;
	while (stWindow < n) {
		bool sizeWindow = edWindow - stWindow + 1;
		if (sizeWindow == 0) {
			for (; stWindow < n; ++stWindow) if (LCP[stWindow] != 0) break;
			if (stWindow >= n) break;
			// so now i hv got a starting pt
			addSuffixToWindow(SA[stWindow - 1], numDiffColors);
			edWindow = stWindow;
		}
		for (; edWindow < n && numDiffColors < numStrings && LCP[edWindow] ; ++edWindow) {
			addSuffixToWindow(SA[edWindow], numDiffColors);
		}
		if (numDiffColors == numStrings) {
			int minLcpOfWindow = segTree.doQuery(LCP, stWindow, edWindow - 1);
			if (minLcpOfWindow >= maxLCP) {
				maxLCP = minLcpOfWindow;
				commonPreffix.push_back(pii(SA[stWindow], minLcpOfWindow));
			}
			removeSuffixFromWindow(SA[stWindow - 1], numDiffColors);
			stWindow++;
		} else if (edWindow == n) break;
		else if (LCP[edWindow] == 0) {
			removeSuffixFromWindow(SA[stWindow - 1], numDiffColors);
			for (; stWindow < edWindow; ++stWindow) removeSuffixFromWindow(SA[stWindow], numDiffColors);
			edWindow--;
		}
	}
	printLCS(commonPreffix, maxLCP);
}

void printSuffixes () {
	fr (i, 0, n) {
		printf("%2d \t %s (%d)\n", SA[i], T + SA[i], LCP[i]);
	}
}
void initialize () {
	T[0] = '\0';
	memset(numSuffixOfColor, 0, sizeof numSuffixOfColor);
	ans.clear();
}
int main () {
	int numStrings, type = 0, lenT = 0, caseNo = 0;
	char str[1010];
	while (ipInt(numStrings) && numStrings) {
		if (caseNo++) printf("\n");
		// initialize
		type = 0;
		lenT = 0;
		initialize();
		//initialize
		fr (i, 0, numStrings) {
			scanf("%s", str);
			int len = strlen(str);
			str[len] = '.'; str[++len] = '\0';
			strcat(T, str);
			fr (i, lenT, lenT + len) colorOf[i] = type;
			lenT += len; ++type;
		}
		if (numStrings == 1) { T[lenT - 1] = '\0'; printf("%s\n",T ); continue; }
		n = lenT;
		constructSA();
		computeLCP();
		// printSuffixes();
		mulStringsLCS(numStrings / 2 + 1);
		
	}
	return 0;
}