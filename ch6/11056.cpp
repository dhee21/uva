# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
char pilot[500];
vector <pis> pilotArray;
bool func (pis left, pis right) {
	if (left.first != right.first) return left.first < right.first;
	transform(left.second.begin(), left.second.end(), left.second.begin(), ::tolower);
	transform(right.second.begin(), right.second.end(), right.second.begin(), ::tolower);
	return left.second < right.second;
}
int main () {
	int numPilots;
	while (ipInt(numPilots) != EOF) {
		scanf("\n");
		fr(i, 0, numPilots) {
			fgets(pilot, 500, stdin);
			pilot[strlen(pilot) - 1] = '\0';
			int min, sec, mill;
			char waste, name[100], wasteStr[100];
			sscanf(pilot, "%s %c %d %s %d %s %d %s",name, &waste, &min, wasteStr, &sec, wasteStr, &mill, wasteStr );
			// cal time and tring lower to sort
			int timeVal = (min * 100000) + (sec * 1000) + mill;
			pis thisPilot = pis(timeVal, name);
			pilotArray.push_back(thisPilot);
		}

		sort(pilotArray.begin(), pilotArray.end(), func);
		int rowNum = 1;
		fr(i, 0, pilotArray.size()) {
			if ((i & 1 )== 0) {
				printf("Row %d\n",rowNum++);
			}
			// cout << pilotArray[i].second;
			printf("%s\n",pilotArray[i].second.data());
		}
		cout << endl;
		pilotArray.clear();
	}
	return 0;
}