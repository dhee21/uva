# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

int main () {
	char a[1010] , b[1010];
	int freqA[30], freqB[30];
	char ans[1010];
	int index = 0;
	while (fgets(a, 1010, stdin) != NULL) {
		fgets(b, 1010, stdin);
		index = 0;
		memset(freqA, 0, sizeof freqA);
		memset(freqB, 0, sizeof freqB);
		fr(i, 0, strlen(a)) freqA[a[i] - 'a']++;
		fr(i, 0, strlen(b)) freqB[b[i] - 'a']++;
		fr(i, 0, 26) {
			int minFreq = min(freqB[i], freqA[i]);
			fr(j, 0, minFreq) ans[index++] = i + 'a';
		}
		ans[index] ='\0';
		sort(ans, ans + index);
		// cout << "printed" <<endl;
		cout << ans << endl;
	}
	return 0;
}