# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_N 100010


char input[200010];

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;

char T[MAX_N], P[MAX_N];
int b[MAX_N], n, m;
vi matchIndexes;

void kmpPreprocess () {
	b[0] = -1;
	int i = 0, j = -1;
	while (i < m) {
		while (j >= 0 && P[i] != P[j]) j = b[j];
		j++; i++;
		b[i] = j;
	}
}
// run kmpSearchbefore
int kmpSearch (int startSearchingFrom) {
	int i = startSearchingFrom, j = 0;
	while (i < n) {
		while (j >= 0 && T[i] != P[j]) j = b[j];
		j++; i++;
		if (j == m) {
			int matchedStartIndex = i - m;
			matchIndexes.push_back(matchedStartIndex);
			j = b[j];
		}
	}
	if (matchIndexes.size()) return 0;
	return i - j;
}

void reverse(int len) {
	fr (i, 0, len) {
		P[i] = T[len - 1 - i];
	}
}
void initialize () {
	matchIndexes.clear();
}
int main () {
	while (scanf("%s", T) != EOF) {
		initialize();
		int len = strlen(T);
		reverse(len);
		n = m = len;
		kmpPreprocess();
		int palStartIndex = kmpSearch(0);
		int backIndex = palStartIndex - 1, forwardIndex = len;
		for (; backIndex >= 0; --backIndex) {
			T[forwardIndex++] = T[backIndex];
		}
		T[forwardIndex] = '\0';
		printf("%s\n", T);
	}
	return 0;
}