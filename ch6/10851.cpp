# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

char cipher[90][90];

int main () {
	int n;
	ipInt(n);
	char message[90];
	while (n--) {
		scanf("\n");
		fr (i, 0, 10) scanf("%s", cipher[i]);
		int m = strlen(cipher[0]) - 2;
		fr (col, 1, m + 1) {
			int val = 0, powOf2 = 1;
			fr (row, 1, 10) {
				if (cipher[row][col] == 92) val  += powOf2;
				powOf2 <<=1;
			}
			message[col - 1] = val;
		}
		message[m] = '\0';
		printf("%s\n",message);
	}
	return 0;
}