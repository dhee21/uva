# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

int main () {
	int t, caseNo = 0;
	ipInt(t);
	while (t--) {
		if (caseNo++) printf("\n");
		int amp, freq;
		ipInt(amp); ipInt(freq);
		// wave start
		fr (num, 0, freq) {
			if (num) printf("\n");
			fr (row, 1, amp + 1){ 
				fr (j, 0, row) printf("%d",row);
				printf("\n");
			}
			for (int row = amp - 1; row >= 1; --row) {
				fr(j, 0, row) printf("%d",row);
				printf("\n");
			}
		}
		//wave end
	}
	return 0;
}