# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1


//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;


struct ansStruct {
	bool exists;
	pii coor;
};

char grid [110][110];
int l;

bool isOutside (int r, int c) {
	return r >= l || c >= l || r < 0 || c < 0 ;
}

ansStruct doesWordStartHere (int row, int col, char *word) {
	int len = strlen(word);
	ansStruct ans;

	bool existsRight = true;
	fr (i, 0, len) if (isOutside(row, col + i) ||  grid[row][col + i] != word[i]) { existsRight = false; break; }
	if (existsRight) { ans.exists = true; ans.coor = pii (row, col + len - 1 ); return ans; }

	bool existsLeft = true;
	fr (i, 0, len) if (isOutside(row, col - i) ||  grid[row][col - i] != word[i]) { existsLeft = false; break; }
	if (existsLeft) { ans.exists = true; ans.coor = pii (row, col - len + 1 ); return ans; }

	bool existsBelow = true;
	fr (i, 0, len) if (isOutside(row + i, col) ||  grid[row + i][col] != word[i]) { existsBelow = false; break; }
	if (existsBelow) { ans.exists = true; ans.coor = pii (row + len - 1, col); return ans; }

	bool existsDiagRightDown = true;
	fr (i, 0, len) if (isOutside(row + i, col + i) ||  grid[row + i][col + i] != word[i]) { existsDiagRightDown = false; break; }
	if (existsDiagRightDown) { ans.exists = true; ans.coor = pii (row + len - 1, col + len - 1); return ans; }

	bool existsDiagLeftDown = true;
	fr (i, 0, len) if (isOutside(row + i, col - i) ||  grid[row + i][col - i] != word[i]) { existsDiagLeftDown = false; break; }
	if (existsDiagLeftDown) { ans.exists = true; ans.coor = pii (row + len - 1, col - len + 1); return ans; }

	bool existsDiagRightUp = true;
	fr (i, 0, len) if (isOutside(row - i, col + i) ||  grid[row - i][col + i] != word[i]) { existsDiagRightUp = false; break; }
	if (existsDiagRightUp) { ans.exists = true; ans.coor = pii (row - len + 1, col + len - 1); return ans; }

	bool existsDiagLeftUp = true;
	fr (i, 0, len) if (isOutside(row - i, col - i) ||  grid[row - i][col - i] != word[i]) { existsDiagLeftUp = false; break; }
	if (existsDiagLeftUp) { ans.exists = true; ans.coor = pii (row - len + 1, col - len + 1); return ans; }

	ans.exists = false;
	return ans;
}

int main () {
	ipInt(l);
	fr (i, 0, l) {
		scanf("%s", grid[i]);
	}
	char word[110];
	while (scanf("%s", word)) {
		if (word[0] == '0' && word[1] == '\0') break;
		bool ansFound = false;
		fr (i, 0, l) {
			fr (j, 0, l) {
				ansStruct ans = doesWordStartHere(i, j, word);
				if (ans.exists) {
					printf("%d,%d %d,%d\n",i + 1, j + 1, ans.coor.first + 1, ans.coor.second + 1);
					ansFound = true; break;
				}
			}
			if (ansFound) break; 
		}
		if (!ansFound) printf("Not found\n");
	}
	return 0;
}