#include <stdio.h>
#include <iostream>
#include <cmath>
#include <limits>
#include <vector>
using namespace std;
typedef vector<int> vi;
int infinity = numeric_limits<int>::max();

struct treeNode {
      int leftFreq, leftData, rightFreq, rightData, maxFreq;
    }defaultNode = {0,0,0,0,0},
    infiniteNode = {0, -infinity, 0, infinity, 0};
int  treeSize = int (2*pow(2.0,floor(log(100009)/log(2.0) + 1)));
vector<treeNode> tree(treeSize);


class segmentTree {

  public :
    int size;

  	void createTree(int* arr, double size) {
  		// double size = arr.size();
      this->size = size;
  		// int  treeSize = int (2*pow(2.0,floor(log(size)/log(2.0) + 1)));
  		// tree.assign(treeSize, defaultNode);
  		buildTree(arr, 1, 0, size-1 );
  	}

  	treeNode operation (treeNode left , treeNode right) {
  			treeNode node;
  			int freq = 0;
  			if(left.rightData == right.leftData) {
  				freq = left.rightFreq + right.leftFreq;
  			}
  			node.leftData = left.leftData;
  			node.rightData = right.rightData;
  			node.leftFreq = (right.leftData == left.leftData) ? left.leftFreq + right.leftFreq : left.leftFreq;
  			node.rightFreq = (right.rightData == left.rightData) ? right.rightFreq + left.rightFreq : right.rightFreq;
  			node.maxFreq = max(max(left.maxFreq, right.maxFreq), freq);
  			return node;
  	}

  	void buildTree(int* arr, int id , int st , int ed) {
  			if(st > ed) {
  				return ;
  			}
  			if(st == ed) {
  				tree[id].leftData = arr[st];
  				tree[id].rightData = arr[st];
  				tree[id].leftFreq = 1;
  				tree[id].rightFreq = 1;
  				tree[id].maxFreq = 1;
  				return; 
  			}
  			int mid = (st+ed)>>1;
  			buildTree(arr, (id<<1) , st, mid );
  			buildTree(arr, (id<<1) + 1 , mid+1, ed);
  			tree[id] = operation( tree[id<<1] , tree[ (id<<1) + 1 ]);
  	}

  	treeNode query( int id , int segSt, int segEd , int ourSt, int ourEd ) {
  			if(segSt > segEd || ourSt > ourEd || ourSt > segEd || ourEd < segSt) {
  				return infiniteNode;
  			}
  			if(ourSt <= segSt && ourEd >= segEd) {
  				return tree[id] ;
  			}
  			int mid = (segSt + segEd) >> 1 ;
  			treeNode left = query(id<<1 , segSt, mid , ourSt , ourEd) ;
  			treeNode right = query( (id<<1) + 1 , mid+1 , segEd , ourSt , ourEd );
  			return operation( left , right);

  	}
  	void update( int id , int st, int ed, int index, int newValue ) {
  		// printf("%d\n", id );
  		// printf("start = %d end = %d index = %d id = %d\n", st, ed ,index, id );
  			if( st > ed || index > ed || index < st) {
  				return ;
  			}
  			if( st == ed ) {
  				tree[id].leftData = newValue;
  				tree[id].rightData = newValue;
  				tree[id].leftFreq = 1;
  				tree[id].rightFreq = 1;
  				tree[id].maxFreq = 1;
  				return ;
  			}
  			int mid = (st+ed) >> 1;
  			update(id<<1 , st , mid , index, newValue);
  			update( (id<<1) + 1 , mid+1, ed , index, newValue );
  			tree[id] = operation(tree[ id<<1 ] , tree[ (id<<1) + 1 ]);
  	}

  	void doUpdate(int* array , int index , int newValue) {
  		update( 1 , 0 ,this->size -1 , index , newValue);
  	}

  	int doQuery(int *array , int start , int end ) {
  		return query( 1 , 0 ,this->size - 1 , start , end ).maxFreq ;
  	}
};

int main () {
	int n,q;
  int  array[100008];

	while(scanf("%d", &n) && n) {
		// printf("erdas\n");
		scanf("%d", &q);
		for(int i =0 ; i < n ; ++i ) {
			scanf("%d", &array[i]);
		}
		segmentTree segTree;

		segTree.createTree(array, n);

		int st , ed ;
		while(q--) {
			scanf("%d %d" ,&st, &ed);
			printf("%d\n", segTree.doQuery(array, st-1, ed-1) );
		}
	}
	return 0 ;
}