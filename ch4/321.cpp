# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
#define MAX_ROOMS 10
#define initialState 1
# define inf 1<<30
# define MAX_STATES 1 << 14

bool doorPresent[MAX_ROOMS][MAX_ROOMS], visited[MAX_STATES];
int switches[MAX_ROOMS], finalState, numRooms, numDoors, numSwitches, parent[MAX_STATES];

int giveRoom (int state) {
	return state >> MAX_ROOMS;
}
int giveLights (int state) {
	int num = (1 << MAX_ROOMS) - 1;
	return state & num;
}
bool isLightOn (int lights, int room) {
	return lights & (1 << room);
}
int giveNextState (int room, int lights) {
	return (room << MAX_ROOMS) | lights;
}
bool canControlRoom (int room , int fromRoom) {
	int switchesFromRoom = switches[fromRoom];
	return switchesFromRoom & (1 << room);
}
int toggleSwitch (int room, int lights) {
	lights ^= (1 << room);
	return lights;
}
int bfs () {
	queue <int> q;
	q.push(initialState);
	visited[initialState] = true;
	while (!q.empty()) {
		int state = q.front(); q.pop();
		if (state == finalState) {
			return true;
		}
		int room = giveRoom(state), lights = giveLights(state);
		for (int roomNum = 0 ; roomNum < numRooms; ++roomNum) {
			if (doorPresent[room][roomNum] && isLightOn(lights, roomNum)) {
				int nextState = giveNextState(roomNum, lights);
				if (!visited[nextState]) {
					visited[nextState] = true;
					parent[nextState] = state;
					q.push(nextState);
				}
			}
		}
		for (int l = 0; l < numRooms; ++l) {
			if (canControlRoom(l, room) && l != room) {
				int newStateLights = toggleSwitch(l, lights);
				int nextState = giveNextState(room, newStateLights);
				if (!visited[nextState]) {
					visited[nextState] = true;
					parent[nextState] = state;
					q.push(nextState);
				}
				
			}
		}
	}
	return false;
}

int giveBitOn(int num) {
	int bit = 0;
	while (num) {
		bit++;
		num = num >> 1;
	}
	return bit;
}
void printTheAnswer () {
	vector <int> ans;
	int state = finalState;
	while (true) {
		ans.push_back(state);
		if (state == initialState) {
			break;
		} else {
			state = parent[state];
		}
	}
	reverse(ans.begin(), ans.end());
	printf("The problem can be solved in %d steps:\n",ans.size() - 1 );
	for (int i = 1; i < ans.size(); ++i) {
		int prevState = ans[i - 1], presentState = ans[i];
		int prevStateRoom = giveRoom(ans[i - 1]), presentStateRoom = giveRoom(ans[i]);
		int prevStateLights = giveLights(ans[i - 1]), presentStateLights = giveLights(ans[i]);
		if (presentStateRoom == prevStateRoom) {
			int diff = presentState ^ prevState;
			int roomOffed = giveBitOn(diff);
			if (prevState & diff) {
				printf("- Switch off light in room %d.\n", roomOffed );
			} else {
				printf("- Switch on light in room %d.\n", roomOffed);
			}
		} else {
			printf("- Move to room %d.\n", presentStateRoom + 1);
		}
	}

}

void makeSwitch (int room1, int room2) {
	switches[room1] |= 1 << room2;
}

void initialize () {
	memset(doorPresent, 0, sizeof doorPresent);
	memset(visited, 0, sizeof visited);
	memset(switches, 0, sizeof switches);
	int finalStateRoom = numRooms - 1, finalStateSwitches = (1 << (numRooms - 1));
	finalState = (finalStateRoom << MAX_ROOMS ) | (finalStateSwitches);
}

int main () {
	int caseNo = 0;
	while (scanf("%d %d %d", &numRooms, &numDoors, &numSwitches) && (numSwitches || numDoors || numRooms)) {
		// if (caseNo) {
		// 	printf("\n");
		// }
		caseNo ++;
		initialize();
		for (int line = 0 ; line < numDoors; ++line) {
			int r1, r2;
			scanf("%d %d", &r1, &r2);
			doorPresent[r1 - 1][r2 - 1] = doorPresent[r2 - 1][r1 - 1] = 1;
		}
		for (int line = 0 ; line < numSwitches; ++line) {
			int r1, r2;
			scanf("%d %d", &r1, &r2);
			makeSwitch(r1 - 1, r2 - 1);
		}
		// printf("yo\n");
		bool possible = bfs();
		printf("Villa #%d\n", caseNo);
		if (!possible) {
			printf("The problem cannot be solved.\n");
		} else {
			printTheAnswer();
		}
		printf("\n");

	}
	return 0;

}