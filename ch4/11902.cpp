# include <stdio.h>
# include <iostream>
# include <string.h>
using namespace std;

int adjMat[110][110], normalV[110], absentV[110], ans[110][110], n;

void dfs(int node, int absent) {
	if ( node == absent) {
		return;
	}
	absentV[node] = true;
	for (int i = 0; i < n; ++i) {
		int edge = adjMat[node][i];
		if (edge && !absentV[i]) {
			dfs(i, absent);
		}
	}
}

int main () {
	int tc;
	scanf("%d", &tc);
	for (int t = 1; t <= tc; ++t) {
		scanf("%d", &n);
		for (int r = 0; r < n; ++r) for (int c = 0; c < n; ++c) {
			scanf("%d", &adjMat[r][c]);
		}
		memset(absentV, 0, sizeof absentV);
		dfs(0, -1);
		for (int i = 0; i < n; ++i) normalV[i] = absentV[i];
		memset (ans, 0, sizeof ans);
		for (int i = 0; i < n; ++i) {
			memset(absentV, 0, sizeof absentV);
			dfs(0, i);
			for (int ind = 0; ind < n; ++ind) {
				ans[i][ind] = !absentV[ind] && normalV[ind];
			}
		}
		printf("Case %d:\n", t );
		for (int r = 0; r < n; ++r) {
			printf("+");
			int x = 2*n - 1;
			while (x--) printf("-");
			printf("+\n");

			for(int c = 0; c < n; ++c) {
				printf("|%c",ans[r][c] ? 'Y' : 'N' );
			}
			printf("|\n");
		}
		printf("+");
		int x = (n<<1) - 1;
		while (x--) printf("-");
		printf("+\n");
	}
	return 0;
}