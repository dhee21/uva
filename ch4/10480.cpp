# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 210
#define inf (1<<30) - 1

//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions
matrix adjMat(MAX_NODES);
int n, m;

// MAxFlow variables
int cap[MAX_NODES][MAX_NODES], flow[MAX_NODES][MAX_NODES], flowToIncrease, maxFlow;
vi bfsParent;
bitset<MAX_NODES> visited;
// MAxFlow variables
// problem specific variables
int totalApp;
// problem specific variables

int resCapacity (int from, int to) {
	return cap[from][to] - flow[from][to];
}

void makeEdge (int from, int to, int weight) {
	cap[from][to] = weight;
	cap[to][from] = weight;
	adjMat[from].push_back(pii(to, 0));
	adjMat[to].push_back(pii(from, 0));
}
void augmentFlow (int source, int sink) {
	if (!visited[sink]) return;
	int minResCap = inf, node = sink;
	while (bfsParent[node] != -1) {
		int parent = bfsParent[node];
		minResCap = min(minResCap, resCapacity(parent, node));
		node = parent;
	}
	node = sink;
	while (bfsParent[node] != -1) {
		int parent = bfsParent[node];
		flow[parent][node] += minResCap;
		flow[node][parent] -= minResCap;
		node = parent;
	}
	flowToIncrease = minResCap;
}

void bfs (int source, int sink) {
	visited.reset();
	bfsParent.assign(MAX_NODES, -1);
	queue <int> q;
	visited.set(source);
	q.push(source);
	while (!q.empty()) {
		int visitedNode = q.front(); q.pop();
		if (visitedNode == sink) {
			break;
		}
		int neighbours = adjMat[visitedNode].size();
		fr (i, 0, neighbours) {
			int edgeToNode = adjMat[visitedNode][i].first;
			int resCap = resCapacity(visitedNode, edgeToNode);
			if (resCap && visited.test(edgeToNode) == 0) {
				visited[edgeToNode] = 1;
				bfsParent[edgeToNode] = visitedNode;
				q.push(edgeToNode);
			}
		}
	}
}

void bfs2 (int source, int sink) {
	vii edges;
	edges.clear();
	visited.reset();
	bfsParent.assign(MAX_NODES, -1);
	queue <int> q;
	visited.set(source);
	q.push(source);
	while (!q.empty()) {
		int visitedNode = q.front(); q.pop();
		if (visitedNode == sink) {
			break;
		}
		int neighbours = adjMat[visitedNode].size();
		fr (i, 0, neighbours) {
			int edgeToNode = adjMat[visitedNode][i].first;
			int resCap = resCapacity(visitedNode, edgeToNode);
			if (resCap && visited.test(edgeToNode) == 0) {
				visited[edgeToNode] = 1;
				bfsParent[edgeToNode] = visitedNode;
				q.push(edgeToNode);
			} else if (resCap == 0 && visited.test(edgeToNode) == 0) {
				edges.push_back(pii(visitedNode, edgeToNode));
			}
		}
	}
	if (visited[sink] == 0) {
		fr (i, 0, edges.size()) {
			if(visited.test(edges[i].second) == 0) {
				printf("%d %d\n",edges[i].first + 1, edges[i].second + 1);
			}
		}
	}
}

void runMaxFlow (int s, int t) {
	// while i can increase / augment flow i will increase the maxFlow value
	while (true) {
		flowToIncrease = 0;
		bfs(s, t);
		augmentFlow(s, t);
		if (flowToIncrease == 0) {
			break;
		}
		maxFlow += flowToIncrease;
	}
}
void initialize () {
	memset(cap, 0, sizeof cap);
	memset(flow, 0, sizeof flow);
	fr(i, 0, adjMat.size()) {
		adjMat[i].clear();
	}
	maxFlow = 0;
}

int main () {
	int s, t, c;
	while (scanf("%d %d", &n, &m) && (n || m)) {
		initialize();
		while(m--) {
			int from, to, cap;
			ipInt(from);ipInt(to);ipInt(cap);
			makeEdge(from - 1, to - 1, cap);
		}
		s = 0; t = 1;
		runMaxFlow(s, t);
		bfs2(s, t);
		printf("\n");
	}
}