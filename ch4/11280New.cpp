# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 150
#define inf 1<<30
//macros

//definitions
typedef pair<int, int> pii;
typedef pair<string, string> pss;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef vector<vs> vvs;
matrix adjMat(MAX_NODES);
matrix inadjMat(MAX_NODES);
//definitions

// bellman Ford variables
bool hasNegativeCycle;
int dist[MAX_NODES][MAX_NODES];
// bellman Ford variables
int m, n;
// problem specific variables
mSI indexOf;
// problem specific variables

void makeNode (string city, int nodeNum) {
	indexOf[city] = nodeNum;
}

void makeEdge(string fromStr, string toStr, int edgeWeight) {
	int from = indexOf[fromStr], to = indexOf[toStr];
	adjMat[from].push_back(pii(to, edgeWeight));
	inadjMat[to].push_back(pii(from, edgeWeight));
}

void bellmanFord (int source) {
	dist[0][source] = 0;
	fr (i, 0, n - 1){
			fr(node, 0, n) {
			int neighbours = inadjMat[node].size();
			dist[i + 1][node] = dist[i][node];

			fr(v, 0, neighbours) {
				// printf("%d\n",v );
					int edgeToNode = inadjMat[node][v].first, edgeWeight = inadjMat[node][v].second;
					if (dist[i][edgeToNode] != inf) {
						dist[i + 1][node] = min (dist[i + 1][node], (dist[i][edgeToNode] + edgeWeight));
					}
				}
		}
	}
}

void initialize () {

	fr(i, 0, MAX_NODES) {
		adjMat[i].clear();
		inadjMat[i].clear();
		fr(j, 0, MAX_NODES) {
			dist[i][j] = inf;
		}
	}
	indexOf.clear();
}

int main () {
	int t, q, caseNo = 0;
	ipInt(t);
	fr(tc, 0, t) {
		if(caseNo) printf("\n");
		initialize();
		caseNo++;

		ipInt(n);		
		fr(i, 0, n) {
			string s;
			cin >> s;
			makeNode(s, i);
		}
		ipInt(m);
		fr(i, 0, m) {
			string from, to;
			int cost;
			cin >> from >> to >> cost;
			makeEdge(from, to, cost);
		}
		ipInt(q);
		bellmanFord(0);
		printf("Scenario #%d\n",caseNo);
		// fr(i, 0, n) {
		// 	printf("pl = %d val = %d\n",i, dist[i][n - 1]);
		// }
		fr(i, 0, q) {
			int stops;
			ipInt(stops);
			stops = min (stops, n - 2);
			if (stops + 1 >= 0 && n - 1 >= 0 ) {
				if (dist[stops + 1][n - 1] != inf) {
				printf("Total cost of flight(s) is $%d\n", dist[stops + 1][n - 1]);
				} else {
				printf("No satisfactory flights\n");
				}
			}
		}
	}
	return 0;
}