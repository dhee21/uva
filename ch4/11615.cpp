# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define MAX_NODES 5100

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

int findLevel(int x) {
	int level;
	for(level = 0; x!=0; ++level) {
		x = x >> 1;
	}
	return level;
}

int main () {
	int t;
	ipInt(t);
	while (t--) {
		int n, a, b;
		ipInt(n);ipInt(a);ipInt(b);
		int bigNode = max(a,b);
		int levelBigNode = findLevel(bigNode);
		int ans = (1 << n) + 1 - (1 << (n - levelBigNode + 1));
		printf("%d\n",ans );
	}
}