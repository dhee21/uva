# include <iostream>
# include <vector>
# include <algorithm>
# include <string>
# include <queue>
# include <map>
# include <string.h>
using namespace std;

# define MAX_NODES 110
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef priority_queue<int> pq;

matrix adjMat(MAX_NODES); // to initialize
mSI indexOf; // map from string to int if nodes are of string type
string nodes[MAX_NODES];
int indegree[MAX_NODES];// to initialize
int n, m, graphLen;
pq q;

void initializeHeap () {
	for (int i = 0; i < graphLen; ++i) {
		if (indegree[i] == 0) {
			q.push(-i);
		}
	}
}

void printNode(int node) {
	printf(" %s",nodes[node].data() );
}

// general function with integer nodes in queue
void topSortBFS() {
	initializeHeap(); // if no priority then we can use queue also
	while (!q.empty()) {
		int node = -q.top(); q.pop();
		printNode(node);
		for (int i = 0; i < adjMat[node].size(); ++i) {
			pii edge = adjMat[node][i];
			int edgeToNode = edge.first, edgeWeight = edge.second;
			if (!(--indegree[edgeToNode])) {
				q.push(-edgeToNode);
			}
		}
		adjMat[node].clear();
	}
}

void makeEdge (string from, string to, int weight) {
	int fromInd = indexOf[from], toInd = indexOf[to];
	adjMat[fromInd].push_back(make_pair(toInd, weight));
	indegree[toInd]++;
}
void initialize() {
	memset(indegree, 0, sizeof indegree);
	for( int i = 0; i < MAX_NODES; ++i) {
		adjMat[i].clear();
	}
	indexOf.clear();
}
int main () {
	char str[200], fromStr[200], toStr[200], caseNo = 0;
	while(scanf("%d\n", &n) != EOF) {
		caseNo++;
		initialize();
		for (graphLen = 0; graphLen < n; ++graphLen) {
			string alcohol;
			scanf("%s\n", str);
			alcohol = str;
			nodes[graphLen] = alcohol;
			indexOf[nodes[graphLen]] = graphLen;
		}
		
		scanf("%d\n", &m);	
		string from, to;
		for (int i = 0; i < m; ++i) {
			scanf("%s %s\n", fromStr, toStr);
			from = fromStr; to = toStr;
			makeEdge(from, to, 0);
		}
		printf("Case #%d: Dilbert should drink beverages in this order:",caseNo );
		topSortBFS();
		printf(".\n\n");
	}
	return 0;
}