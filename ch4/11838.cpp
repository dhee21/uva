# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <map>
using namespace std;
#define MAX_NODES 2100
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;

matrix adjMat(MAX_NODES);
// arr => arrival times, dep => departure times, nodeForDepTime => index is time and valuew is node which departes on that time
// dfs parent => parent of each node while dfs, deppest backEdge stores smallest arrival times reachable from a node including the subtree
// not including its parent
// smallest arrival time stores the smallest arrival time a subtree can reach to not including nodes that are cut from tree
// cutFromGraph stores bool variable whether a node has been cut from a graph or not as a result of taking out a strongly connected component
int visited[MAX_NODES], arr[MAX_NODES], dep[MAX_NODES], smallestArrTime[MAX_NODES], cutFromGraph[MAX_NODES];
int timeCounter, n, m, numSCC;
vi travelledNodes;

void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}

void makeDoubleEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}

void printSCC (int node) {
	numSCC ++;
	while(1) {
		int v = travelledNodes.back();
		travelledNodes.pop_back();
  	  	cutFromGraph[v] = 1;
		if (v == node) {
			// printf("%s\n", nodes[v].c_str());
			break;
		} else {
			// printf("%s, ", nodes[v].c_str() );
		}
	}
}

void dfs(int node) {
	visited[node] = DFS_GRAY;
	travelledNodes.push_back(node);
	arr[node] = timeCounter ++;
	int neighbours = adjMat[node].size();
	int leaseArrivalTime = arr[node];
	for (int i = 0; i < neighbours; ++i) {
		pii edge = adjMat[node][i];
		// edgeToNode is the edge to which this edge go from "node"
		int edgeToNode = edge.first, edgeWeight = edge.second;
		if (visited[edgeToNode] == DFS_WHITE) {
			dfs(edgeToNode);
			leaseArrivalTime = min (leaseArrivalTime, smallestArrTime[edgeToNode] );
		} else if (cutFromGraph[edgeToNode] == false) {
			leaseArrivalTime = min (leaseArrivalTime, arr[edgeToNode]);
		}
	}
	if (leaseArrivalTime == arr[node]) {
		printSCC(node);
	}
	smallestArrTime[node] = leaseArrivalTime;
	dep[node] = timeCounter ++;
	visited[node] = DFS_BLACK;
}

void readGraph () {
	int from, to, p;
	for (int i = 0 ; i < m; ++i) {
		scanf("%d%d%d", &from, &to, &p);
		if (p == 1) {
			makeEdge(from - 1, to - 1, 0);
		} else {
			makeDoubleEdge(from - 1, to - 1, 0);
		}
	}
}

void initialize () {
	for( int i = 0; i < MAX_NODES; ++i) {
		adjMat[i].clear();
	}
	timeCounter = 0;
	memset(visited, DFS_WHITE, sizeof visited);
	memset(smallestArrTime, 1 << 25, sizeof smallestArrTime );
	memset(cutFromGraph, 0, sizeof cutFromGraph );
	numSCC = 0;
}

int main () {
	while (scanf("%d %d", &n, &m) && (n || m)) {
		initialize();
		readGraph();
		for (int i = 0; i < n; ++i) {
			if (visited[i] == DFS_WHITE) {
				dfs(i);
				if (numSCC > 1) {
					break;
				}
			}
		}
		if (numSCC == 1) {
			printf("1\n");
		} else {
			printf("0\n");
		}
	}
	return 0;
}