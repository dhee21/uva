# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 210
#define inf (1<<30) - 1

//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions
matrix adjMat(MAX_NODES);
// MAxFlow variables
int cap[MAX_NODES][MAX_NODES], flow[MAX_NODES][MAX_NODES], flowToIncrease, maxFlow;
vi bfsParent;
bitset<MAX_NODES> visited;
// MAxFlow variables
// problem specific variables
int totalApp;
// problem specific variables

int resCapacity (int from, int to) {
	return cap[from][to] - flow[from][to];
}

void makeEdge (int from, int to, int weight) {
	cap[from][to] = weight;
	if (cap[to][from] == 0) {
		adjMat[from].push_back(pii(to, 0));
		adjMat[to].push_back(pii(from, 0));
	}
}
void augmentFlow (int source, int sink) {
	if (!visited[sink]) return;
	int minResCap = inf, node = sink;
	while (bfsParent[node] != -1) {
		int parent = bfsParent[node];
		minResCap = min(minResCap, resCapacity(parent, node));
		node = parent;
	}
	node = sink;
	while (bfsParent[node] != -1) {
		int parent = bfsParent[node];
		flow[parent][node] += minResCap;
		flow[node][parent] -= minResCap;
		node = parent;
	}
	flowToIncrease = minResCap;
}

void bfs (int source, int sink) {
	visited.reset();
	bfsParent.assign(MAX_NODES, -1);
	queue <int> q;
	visited.set(source);
	q.push(source);
	while (!q.empty()) {
		int visitedNode = q.front(); q.pop();
		if (visitedNode == sink) {
			break;
		}
		int neighbours = adjMat[visitedNode].size();
		fr (i, 0, neighbours) {
			int edgeToNode = adjMat[visitedNode][i].first;
			int resCap = resCapacity(visitedNode, edgeToNode);
			if (resCap && visited.test(edgeToNode) == 0) {
				visited[edgeToNode] = 1;
				bfsParent[edgeToNode] = visitedNode;
				q.push(edgeToNode);
			}
		}
	}
}

void runMaxFlow (int s, int t) {
	maxFlow = 0;
	// while i can increase / augment flow i will increase the maxFlow value
	while (true) {
		flowToIncrease = 0;
		bfs(s, t);
		augmentFlow(s, t);
		if (flowToIncrease == 0) {
			break;
		}
		maxFlow += flowToIncrease;
	}
}
void initialize () {
	memset(cap, 0, sizeof cap);
	memset(flow, 0, sizeof flow);
	fr(i, 0, adjMat.size()) {
		adjMat.clear();
	}
}

void findAnswer () {
	fr (i, 27, 37) {
		makeEdge(i, 37, 1);
	}
	runMaxFlow(0, 37);
	// printf("%d %d\n", totalApp, maxFlow );
	if (totalApp == maxFlow ) {
		fr (comp, 27, 37) {
			bool flag = 0; 
			fr(app, 1, 27) {
				if (flow[app][comp]) {
					printf("%c",app + 'A' - 1);
					flag = 1;
					break;
				}
			}
			if (!flag) {
				printf("_");
			}
		}
		printf("\n");
	} else {
		printf("!\n");
	}
	totalApp = 0;
}

int main () {
	char inp[100], compAccess[100], appType;
	int appNum;
	totalApp = 0;
	initialize();
	// while (fgets(input, 100, stdin))
	while (fgets(inp, 100, stdin) != NULL){
		if (strlen(inp) == 1) {
				findAnswer();
				initialize();
		} else {
			sscanf (inp, "%c %d %s", &appType, &appNum, compAccess);
			// printf("%c %d %s\n", appType, appNum, compAccess );
			totalApp += appNum;
			int appNode = appType - 'A' + 1;
			makeEdge(0, appNode, appNum);
			int len = strlen(compAccess);
			fr (i, 0, len - 1) {
				int comp = compAccess[i] - '0';
				makeEdge(appNode, 27 + comp, 1);
			}
		}
	}
	findAnswer();
	return 0 ;
}