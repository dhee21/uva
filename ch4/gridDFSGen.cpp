#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <map>
#include <sstream>
#include <cmath>
#include <bitset>
#include <utility>
#include <set>
#include <numeric>
 
using namespace std;
int dir[4][2] = {{1, 0}, {0, -1}, {0, 1}, {-1, 0}}, length = 0;
int n, grid[110][110], visited[110][110];

void dfs(int r, int c) {
	visited[r][c] = true;
	length ++;
	for (int i = 0; i < 4; ++i) {
		int newR = r + dir[i][0], newC = c + dir[i][1];
		if (newR >= 0 && newR < n && newC >= 0 && newC < n && !visited[newR][newC] && grid[newR][newC] == grid[r][c]) {
			dfs(newR, newC);
		}
	}
}
int main () {
	int r, c;
	char ch;
	while(scanf("%d\n", &n) && n) {
		memset(grid, 0, sizeof grid);
		memset(visited, 0, sizeof visited);
		int components = 0;
		char input[500];
		int rowVal;
		for (int i = 1; i <= n - 1; ++i) {
			while(scanf("%d%d%c",&r,&c,&ch)){
				grid[r - 1][c - 1] = i;
				if (ch =='\n' || ch == '\0') break;
			}
		}
		bool goOut = false;
		for (int r = 0; r < n; ++r){
			for (int c = 0; c < n; ++c) {
				if (!visited[r][c]) {
					length = 0;
					dfs(r, c);
					if (length > n || length < n) {
						goOut = true;
						break;
					}
					components ++;
				}
			}
			if (goOut) break;
		}
		if (goOut) {
			printf("wrong\n");
		}
		else if (components == n) {

			printf("good\n");
		} else {

			printf("wrong\n");
		}
	}
	return 0;
}