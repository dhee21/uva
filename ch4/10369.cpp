# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
#define MAX_NODES 1000
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1010
# define inf 1<<30
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<double , pii> edge;
// matrix adjMat(MAX_NODES);
vector<edge> EdgeList;

int mstCost, n, m;
// problem specific variable starts
int componentsToMake;
pii coordinates[MAX_NODES]; 
double maxD;
// problem specific variable ends

class unionFind {
	public :
	vector <int> pset,nodes;//pset is storing at which node a node is pointing , and nodes stores number o noes in a set if an node is the main node of as set
	int num_of_sets;

	void iniSet(int n) {
		pset.assign(n,0);
		nodes.assign(n,1);
		for(int i = 0 ; i < pset.size() ; ++i ) {
			pset[i] = i;
		}
		num_of_sets = n;
	}
	int findSet(int v) {
			if(v < 0 || v >= pset.size() ) {
				return -1 ;
			}
			int ptr = v;
			while ( pset[ptr] != ptr ) {
				ptr = pset[ptr];
			}
			compressThePath(v, ptr);
			return ptr;
	}
	void compressThePath (int vertex, int set ) {
		 int ptr = vertex;
		 while(pset[ptr] != ptr ) {
		 	int nextNode = pset[ptr];
		 	pset[ptr] = set ;
		 	ptr = nextNode;
		 }
	}
	bool isSameSet(int vertex1, int vertex2) {
			int set1 = findSet(vertex1), set2 = findSet(vertex2);
			if(set1 == -1 || set2 == -1) {
				return 0;
			}
			return set1 == set2;
	}
	int numDisjointSets() {
		return num_of_sets;
	}
	void unionSet(int vertex1, int vertex2 ) {
		int set1 = findSet(vertex1), set2 = findSet(vertex2);
		if(set1 == -1 || set2 == -1) {
			return ;
		}
		if(set1 == set2) {
			return ;
		}
		if(nodes[set1] > nodes[set2]) {
			pset[set2] = set1 ;
			nodes[set1] += nodes[set2] ;
 		} else {
 			pset[set1] = set2 ;
 			nodes[set2] += nodes[set1] ;
 		}
 		num_of_sets--;
	}
	int sizeOfSet(int vertex) {
		int set = findSet(vertex) ;
		if(set == -1) {
			return -1;
		}
		return nodes[set];
	}
};

void kruskalsAlgo () {
	mstCost = 0;
	unionFind unionWorker;
	unionWorker.iniSet(n);
	if (componentsToMake == unionWorker.numDisjointSets()) {
		return;
	}
	sort(EdgeList.begin(), EdgeList.end());
	for (int i = 0; i < EdgeList.size(); ++i) {
		double edgeWeight = EdgeList[i].first;
		int node1 = EdgeList[i].second.first, node2 = EdgeList[i].second.second;
		if (unionWorker.isSameSet(node1, node2) == false) {
			unionWorker.unionSet(node1, node2);
			maxD = max (maxD, edgeWeight);
			if (componentsToMake == unionWorker.numDisjointSets()) {
				return;
			}
			mstCost += edgeWeight;
		}
	}
}
double dist(pii coor1, pii coor2) {
	double x1 = coor1.first, x2 = coor2.first, y1 = coor1.second, y2 = coor2.second;
	double diff1 = x1 - x2, diff2 = y1 - y2;
	return sqrt(diff1 * diff1 + diff2 * diff2);
}
void pushEdge (int index) {
	for (int i = 0 ; i < index; ++i) {
		double x = dist(coordinates[index], coordinates[i]);
		EdgeList.push_back(make_pair(x, pii(index, i)));
	}
}
void initialize () {
	EdgeList.clear();
	maxD = 0;
}
int main () {
	int t, satChannels;
	scanf("%d", &t);
	while (t--) {
		initialize();
		scanf("%d%d", &satChannels, &n);
		componentsToMake = satChannels;
		for (int i = 0 ; i < n; ++i) {
			int x, y;
			scanf("%d %d", &x, &y);
			coordinates[i].first = x; coordinates[i].second = y;
			pushEdge(i);
		}
		kruskalsAlgo();
		printf("%0.2lf\n", maxD);
	}
	return 0;
}
