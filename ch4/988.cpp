# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <map>
using namespace std;
#define MAX_NODES 1100
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
matrix adjMat(MAX_NODES);
vector<edge> EdgeList(MAX_EDGES);
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}
vi topSort;

int choices[MAX_NODES], ways[MAX_NODES], visited[MAX_NODES];

void dfs(int node) {
	visited[node] = true;
	int neighbours = adjMat[node].size();
	fr (i, 0, neighbours) {
		int edgeToNode = adjMat[node][i].first;
		if (!visited[edgeToNode]) {
			dfs(edgeToNode);
		}
	}
	topSort.push_back(node);
}

void initialize () {
	memset(ways, 0, sizeof ways);
	memset(choices, 0, sizeof choices);
	memset(visited, 0, sizeof visited);
	topSort.clear();
	fr (i, 0, MAX_NODES) {
		adjMat[i].clear();
	}
}

int main () {
	int eventsNum, caseNo = 0;
	while (ipInt(eventsNum) != EOF) {
		if (caseNo) printf("\n");
		caseNo++;
		initialize();
		fr (i, 0, eventsNum) {
			int choicesNum;
			ipInt(choicesNum);
			choices[i] = choicesNum;
			fr (c, 0, choicesNum) {
				int choice;
				ipInt(choice);
				makeEdge(i, choice, 0);
			}
		}
		fr (i, 0, eventsNum) {
			if (!visited[i]) {
				dfs(i);
			}
		}
		int ans = 0;
		ways[0] = 1;
		reverse(topSort.begin(), topSort.end());
		fr (i, 0, topSort.size()) {
			int node = topSort[i];
			int neighbours = adjMat[node].size();
			fr (v, 0, neighbours) {
				int edgeToNode = adjMat[node][v].first;
				ways[edgeToNode] += ways[node];
			}
			if (choices[node] == 0) {
				ans += ways[node];
			}
		}
		printf("%d\n", ans);
	}
	return 0;
}