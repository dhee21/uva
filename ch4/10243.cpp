# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <map>
using namespace std;
#define MAX_NODES 1100
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
matrix adjMat(MAX_NODES);
vector<edge> EdgeList(MAX_EDGES);
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}
int MVC[MAX_NODES][2], visited[MAX_NODES], isLeaf[MAX_NODES];

int minVertexCover (int node, int isTaken, int parent) {
	if (isLeaf[node]) return isTaken;
	if (MVC[node][isTaken] != -1) return MVC[node][isTaken];
	int value = isTaken ? 1 : 0;
	int neighbours = adjMat[node].size();
	fr (i, 0, neighbours) {
		int edgeToNode = adjMat[node][i].first;
		if (edgeToNode == parent) continue;
		int taken = minVertexCover(edgeToNode, true, node), notTaken = isTaken ?  minVertexCover(edgeToNode, false, node) : inf;
		value += min (taken, notTaken);
	}
	MVC[node][isTaken] = value;
	return value;
}

void dfs (int node) {
	visited[node] = true;
	int neighbours = adjMat[node].size() ;
	bool isLeafFlag = true;
	fr (i, 0, neighbours) {
		int edgeToNode = adjMat[node][i].first;
		if (!visited[edgeToNode]) {
			isLeafFlag = false;
			dfs(edgeToNode);
		}
	}
	isLeaf[node] = isLeafFlag;
}

void initialize () {
	memset (MVC, -1, sizeof MVC);
	memset (visited, 0, sizeof visited);
	memset (isLeaf, 0, sizeof isLeaf);
	fr (i, 0, MAX_NODES) {
		adjMat[i].clear();
	}
}

int main () {
	int n;
	while (ipInt(n) && n) {
		initialize();
		fr (gall, 0, n) {
			int neighbours; ipInt(neighbours);
			fr(v, 0, neighbours) {
				int adjGall; ipInt(adjGall);
				makeEdge(gall, adjGall - 1, 0);
			}
		}
		if (n == 1){
			printf("1\n");
		continue;

		}
		dfs(0);
		int ans = min(minVertexCover(0, true, -1), minVertexCover(0, false, -1));
		printf("%d\n", ans);
	}
	return 0;
}