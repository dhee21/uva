# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define MAX_NODES 5100

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
matrix adjMat(MAX_NODES);
// vector<edge> EdgeList(MAX_EDGES);
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}
int bfsParent[MAX_NODES], level[MAX_NODES];

void bfs(int root) {
	queue<int> q;
	bfsParent[root] = -1;
	level[root] = 0;
	q.push(root);
	while(!q.empty()) {
		int visitedNode = q.front(); q.pop();
		int neighbours = adjMat[visitedNode].size();
		fr (i, 0, neighbours) {
			int edgeToNode = adjMat[visitedNode][i].first;
			// printf("%d \n",edgeToNode );
			if (edgeToNode != bfsParent[visitedNode]) {
				bfsParent[edgeToNode] = visitedNode;
				level[edgeToNode] = level[visitedNode] + 1;
				q.push(edgeToNode);
			}
		}
	}
}

void LCA(int node1, int node2) {
	int LcaVal;
		if (node1 == node2) {
			LcaVal = node1;
			printf("The fleas meet at %d.\n",LcaVal + 1 );
			return ;
		}
		int MoreLevelNode = (level[node2] > level[node1] ) ? node2 : node1;
		int lessLevelNode = (node1 == MoreLevelNode) ? node2 : node1;
		vi pathMoreLevel, pathLessLevel;
		for (int node = MoreLevelNode; node != -1; node = bfsParent[node]) {
			pathMoreLevel.push_back(node);
		}
		for (int node = lessLevelNode; node != -1; node = bfsParent[node]) {
			pathLessLevel.push_back(node);
		}
		int MoreLevelIndex = pathMoreLevel.size() - 1, lessLevelIndex = pathLessLevel.size() - 1;
		for (; lessLevelIndex >= 0 && pathLessLevel[lessLevelIndex] == pathMoreLevel[MoreLevelIndex]; lessLevelIndex--) { MoreLevelIndex--;}

		LcaVal = pathLessLevel[lessLevelIndex + 1];
		int distNode1 = MoreLevelIndex + 1, distNode2 = lessLevelIndex + 1;
		int distFromMoreLevel = (distNode1 + distNode2) >> 1;
		int meetingNode1 = pathMoreLevel[distFromMoreLevel] ;
		if ((distNode1 + distNode2) & 1) {
			int meetingNode2 = pathMoreLevel[distFromMoreLevel + 1];
			printf("The fleas jump forever between %d and %d.\n", min(meetingNode1, meetingNode2) + 1, max(meetingNode1, meetingNode2) + 1 );
		} else {
			printf("The fleas meet at %d.\n",meetingNode1 + 1 );
		}

}

void initialize () {
	fr (i, 0, MAX_NODES) bfsParent[i] = -2;
	fr(i, 0, adjMat.size()) adjMat[i].clear();
}

int main () {
	int n;
	while (ipInt(n) && n) {
		initialize();
		fr (i, 0, n - 1) {
			int from, to;
			ipInt(from), ipInt(to);
			makeEdge(from - 1, to - 1, 0);
		}
		bfs(0);
		int l;
		ipInt(l);
		fr (i, 0, l) {
			int a, b;
			ipInt(a);ipInt(b);
			LCA(a - 1, b - 1);
		}

	}
	return 0;
}