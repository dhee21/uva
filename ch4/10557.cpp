# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 11000
#define inf 1 << 28
//macros

//definitions
typedef pair<int, int> pii;
typedef pair<string, string> pss;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef vector<vs> vvs;
matrix adjMat(MAX_NODES);
//definitions

//problem specififc variables
int m, n, energy[MAX_NODES], dist[MAX_NODES], visited[MAX_NODES];
bool hasNegativeCycle;
//problem specififc variables

void initialize () {
	fr(i, 0, MAX_NODES) {
		adjMat[i].clear();
	}
	fr(i, 0, MAX_NODES) {
		dist[i] = inf;
	}
}

void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}

bool canReachEndBFS(int node) {
	memset(visited, 0, sizeof visited);
	queue<int> q;
	q.push(node);
	visited[node] = true;
	while(!q.empty()) {
		int visitedNode = q.front(); q.pop();
		if (visitedNode == n - 1) {
			return true;
		}
		int neighbours = adjMat[visitedNode].size();
		fr(i, 0, neighbours) {
			int edgeToNode = adjMat[visitedNode][i].first;
			if (!visited[edgeToNode]) {
				visited[edgeToNode] = true;
				q.push(edgeToNode);
			}
		} 
	}
	return false;
}

void checkCycle () {
	hasNegativeCycle = false;
	fr(node, 0, n) {
		int neighbours = adjMat[node].size();
		fr(v, 0, neighbours) {
			if (dist[node] != inf) {
				int edgeToNode = adjMat[node][v].first, edgeWeight = -energy[edgeToNode];
				if ((dist[node] + edgeWeight < 100) && (dist[node] + edgeWeight < dist[edgeToNode])) {
					if (canReachEndBFS(node)) {
						hasNegativeCycle = true;
						break;
					}
					dist[edgeToNode] = dist[node] + edgeWeight;
				}
			}
		}
		if (hasNegativeCycle) break;
	}
}

void bellmanFord (int source) {
	dist[source] = 0;
	fr (i, 0, n - 1) fr(node, 0, n) {
		int neighbours = adjMat[node].size();
			if (dist[node] != inf) {
			fr(v, 0, neighbours) {
					int edgeToNode = adjMat[node][v].first, edgeWeight = -energy[edgeToNode];
					if ( (dist[node] + edgeWeight < 100) && dist[node] + edgeWeight < dist[edgeToNode]) {
						dist[edgeToNode] = dist[node] + edgeWeight;
					}
				}
			}
		}
}

int main () {
	while (scanf("%d", &n) && n != -1) {
		initialize();
		fr(i, 0, n) {
			int doors;
			ipInt(energy[i]);
			ipInt(doors);
			fr(d, 0, doors) {
				int dr;
				ipInt(dr);
				makeEdge(i, dr - 1, 0);
			}
		}
		bellmanFord(0);
		checkCycle();
		// printf("see %d %d \n", hasNegativeCycle, dist[n - 1] );
		if ((dist[n - 1] != inf && dist[n - 1] < 100) || hasNegativeCycle) {
			printf("winnable\n");				
		} else {
			printf("hopeless\n");
		}

	}
}