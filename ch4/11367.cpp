# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
#define MAX_CITIES 1100
#define MAX_CAP 110
#define inf 1 << 30
# define fr(i, st, ed) for (int i = st ; i < ed; ++i)

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
matrix adjMat(MAX_CITIES);

//problem specific variables
int prices[MAX_CITIES], dist[1 << 19], c;
priority_queue<pii> pq;
//problem specific variables

void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}

int indexOf(int cap, int city) {
	return city << 8 | cap;
}
int giveCity (int node) {
	return node >> 8;
}
int giveCap (int node) {
	return node & ((1 << 8) - 1);
}
void SSSP (int source) {
	pq.push(pii(0, -source));
	dist[source] = 0;
	while (!pq.empty()) {
		pii minUpperbound = pq.top(); pq.pop();
		int node = -minUpperbound.second, minDist = -minUpperbound.first;
		if (dist[node] == minDist) {
			int nodeCap = giveCap(node), nodeCity = giveCity(node);
			if (nodeCap + 1 <= c) {
				int edgeToNode = indexOf(nodeCap + 1, nodeCity);
				if (minDist + prices[nodeCity] < dist[edgeToNode]) {
					int newDist = dist[edgeToNode] = minDist + prices[nodeCity];
					pq.push(pii(-newDist, -edgeToNode));
				}
			}
			int neighbours = adjMat[nodeCity].size();
			// Relaxing dist of neighbours
			for (int i = 0 ; i < neighbours; ++i) {
				int adjCity = adjMat[nodeCity][i].first, distance = adjMat[nodeCity][i].second;
				if (nodeCap >= distance) {
					int edgeToNode = indexOf(nodeCap - distance, adjCity);
					int newDist = minDist;
					if (newDist < dist[edgeToNode]) {
						dist[edgeToNode] = newDist;
						pq.push(pii(-newDist, -edgeToNode));
					}
				}
				
			}
		}
	}
}

void initialize () {
	for (int i = 0 ; i < MAX_CITIES; ++i) {
		adjMat[i].clear();
	}
}
int main () {
	int n, m;
	while (scanf("%d %d", &n, &m) != EOF) {
		fr (i, 0, n) {
			scanf("%d", &prices[i]);
			// printf("%d ",prices[i] );
		}
		fr(i, 0, m) {
			int u, v, d;
			scanf("%d %d %d", &u, &v, &d);
			// printf("%d %d %d\n", u, v, d );
			makeEdge(u, v, d);
		}
		int q;
		scanf("%d", &q);
		while (q--) {
			fr (i, 0, 1 << 19) {
				dist[i] = inf;
			}
			int s, e;
			scanf("%d %d %d", &c, &s, &e);
			// printf("%d %d %d\n", c, s, e );
			SSSP(indexOf(0, s));
			int ans = inf;
			fr(cp, 0, c + 1) {
				ans = min (ans, dist[indexOf(cp, e)]);
			}
			if (ans == inf) {
				printf("impossible\n");
			} else {
				printf("%d\n", ans);
			}
		}
	}
	return 0;
}