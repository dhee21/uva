# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 110
#define inf (1<<30) - 1
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions
matrix adjMat(MAX_NODES);
vi topSort;
int visited[MAX_NODES];
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}
// prob vars
int grid[MAX_NODES][MAX_NODES], walkVal[MAX_NODES][MAX_NODES];
vector<edge> nodes;
int dir[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0} };

void initialize () {
	memset (grid, 0, sizeof grid);
	fr(i, 0, MAX_NODES) fr(j, 0, MAX_NODES) {
		walkVal[i][j] = 1;
	}
	nodes.clear();
}

int main () {
	int t;
	ipInt(t);
	while (t--) {
		initialize();
		string name;
		int rows, cols, ans = 0;
		cin >> name >> rows >> cols;
		fr (i, 0, rows) fr (j, 0, cols) {
			int k; ipInt(k); grid[i][j] = k; nodes.push_back(make_pair(k, pii(i, j)));
		}
		sort(nodes.begin(), nodes.end());
		reverse(nodes.begin(), nodes.end());
		fr (i, 0, nodes.size()) {
			edge node = nodes[i];
			int r = node.second.first, c = node.second.second, val = node.first;
			ans = max (ans, walkVal[r][c]);
			fr (index, 0, 4) {
				int row = r + dir[index][0], col = c + dir[index][1];
				if (row >= 0 && row < rows && col >= 0 && col < cols && grid[row][col] < val) {
					walkVal[row][col] = max(walkVal[row][col], walkVal[r][c] + 1);
					ans = max (ans, walkVal[row][col]); 
				}
			}
		}
		printf("%s: %d\n", name.c_str() , ans );
	}
}