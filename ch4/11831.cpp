# include <stdio.h>
# include <iostream>
using namespace std;
#define NORTH 'N'
#define SOUTH 'S'
#define EAST 'L'
#define WEST 'O'
#define STICKER '*'
#define VISITED 'V'
#define PILLAR '#'
#define TURN_RIGHT 'D'
#define TURN_LEFT 'E'
typedef pair<int, int> pii;
char grid[110][110];

struct coor
{
	int r, c;
	/* data */
};

int direction[4][2] = {{-1, 0}, {0, -1},{1, 0},{0, 1}},n, m;

coor canGo (int dir, coor presentCoor) {
		coor newCoor;
		newCoor.r = presentCoor.r + direction[dir][0];
		newCoor.c = presentCoor.c + direction[dir][1];
		if (newCoor.r >= 0 && newCoor.r < n && newCoor.c >= 0 && newCoor.c < m && grid[newCoor.r][newCoor.c] != PILLAR) {
			return newCoor;
		} else {
			newCoor.r = -1; newCoor.c = -1;
			return newCoor;
		}
}

int main () {
	int s, dir;
	coor presentCoor;
	char inst;
	while (scanf("%d %d %d\n", &n, &m, &s) && (n || m || s)) {
		int sticker = 0;
		for (int r = 0; r < n; ++r){
			for (int c = 0; c < m; ++c) {
				scanf("%c", &grid[r][c]);
				if (grid[r][c] == NORTH || grid[r][c] == SOUTH || grid[r][c] == EAST || grid[r][c] == WEST) {
					char val = grid[r][c];
					presentCoor.r = r; presentCoor.c = c;
					dir = val == NORTH ? 0 : val == WEST ? 1 : val == SOUTH ? 2 : 3;
				}
			}
			scanf("\n");
		}
		for (int i = 0 ; i < s; ++i) {
			scanf("%c", &inst);
			if (inst == TURN_RIGHT) {
				dir = dir == 0 ? 3 : dir - 1;
			} else if (inst == TURN_LEFT) {
				dir++;
				dir %= 4;
			} else {
				coor newCoor = canGo(dir, presentCoor);
				if (newCoor.r != -1) {
					presentCoor = newCoor;
					if (grid[presentCoor.r][presentCoor.c] == STICKER) sticker++;
					grid[presentCoor.r][presentCoor.c] = VISITED;
				}
			}
			
		}
		printf("%d\n", sticker );
	}
	return 0;
}