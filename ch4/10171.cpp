# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 110
#define inf (1<<30) - 1
#define YOUNG 'Y'
#define BI_DIRECTION 'B'

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<char> vc;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef map<char, int> mCI;

int adjMat[MAX_NODES][MAX_NODES], p[MAX_NODES][MAX_NODES], n, nodeIndex;
int adjMatProf[MAX_NODES][MAX_NODES], pProf[MAX_NODES][MAX_NODES];
char nodes[MAX_NODES];
vc ans;
mCI indexOf;


void floydWarshallMe () {
	fr (k, 0, n) fr (i, 0, n) fr (j, 0, n) {
		if ( adjMat[i][k] + adjMat[k][j] < adjMat[i][j]) {
			adjMat[i][j] = adjMat[i][k] + adjMat[k][j];
			p[i][j] = p[j][k];
		}
	}
}

void floydWarshallProf () {
	fr (k, 0, n) fr (i, 0, n) fr (j, 0, n) {
		if ( adjMatProf[i][k] + adjMatProf[k][j] < adjMatProf[i][j]) {
			adjMatProf[i][j] = adjMatProf[i][k] + adjMatProf[k][j];
			pProf[i][j] = pProf[j][k];
		}
	}
}

void makeNode (char fromChar, char toChar, int weight, char reserved, char direction) {
		
		if (indexOf.count(fromChar) == 0) {
			indexOf[fromChar] = nodeIndex;
			nodes[nodeIndex++] = fromChar;
		}
		if (indexOf.count(toChar) == 0) {
			indexOf[toChar] = nodeIndex;
			nodes[nodeIndex++] = toChar;
		}
		int from = indexOf[fromChar], to = indexOf[toChar];

		if (reserved == YOUNG) {
			adjMat[from][to] = (to != from) ? weight : 0;
			if (direction == BI_DIRECTION) {
				adjMat[to][from] = (to != from) ? weight : 0;
			}
		} else {
			adjMatProf[from][to] = (to != from) ? weight : 0;
			if (direction == BI_DIRECTION) {
				adjMatProf[to][from] = (to != from) ? weight : 0;
			}
		}
}
void initialize () {
	nodeIndex = 0;
	fr(i, 0, MAX_NODES) fr(j, 0, MAX_NODES) {
		adjMatProf[i][j] = adjMat[i][j] = (i == j) ? 0 : inf;
		p[i][j] = pProf[i][j] = i;
	}
	ans.clear();
	indexOf.clear();
}

int findPoint (int me, int prof) {
	int minDist = inf;
	fr(v, 0, n) {
		if (adjMat[me][v] != inf && adjMatProf[prof][v] != inf ) {
			if (adjMat[me][v] + adjMatProf[prof][v] < minDist) {
				// printf("see %c \n",nodes[v] );
				minDist = adjMat[me][v] + adjMatProf[prof][v];
				ans.clear();
				ans.push_back(nodes[v]);
			} else if (adjMat[me][v] + adjMatProf[prof][v] == minDist) {
				ans.push_back(nodes[v]);
			}
		}
	}
	sort(ans.begin(), ans.end());
	return minDist;
}

int main () {
	int streets, weight;
	char reserved, direction, from, to, meChar, profChar;
	while (ipInt(streets) && streets) {
		scanf("\n");
		initialize();
		fr(i, 0, streets) {
			scanf("%c %c %c %c %d\n", &reserved, &direction, &from, &to, &weight );
			// printf("%c %c %c %c %d\n", reserved, direction, from, to, weight );
			makeNode(from, to, weight, reserved, direction);
		}
		scanf("%c %c", &meChar, &profChar);
		// printf("%c %c\n", meChar, profChar);
		if (indexOf.count(meChar) == 0) {
			indexOf[meChar] = nodeIndex;
			nodes[nodeIndex++] = meChar;
		}
		if (indexOf.count(profChar) == 0) {
			indexOf[profChar] = nodeIndex++;
			nodes[nodeIndex++] = profChar;
		}
		n = nodeIndex;
		int me = indexOf[meChar], prof = indexOf[profChar];
		floydWarshallMe();
		floydWarshallProf();
		// printf("n = %d\n", n );
		int distanceMin = findPoint(me, prof);
		if (distanceMin == inf) {
			printf("You will never meet.\n");
		} else {
			printf("%d",distanceMin);
			fr (i, 0, ans.size()) {
				printf(" %c", ans[i]);
			}
			printf("\n");
		}

	}
}