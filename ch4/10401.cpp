# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <map>
using namespace std;
#define MAX_NODES 20
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
matrix adjMat(MAX_NODES);
vector<edge> EdgeList(MAX_EDGES);
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}

lli grid[MAX_NODES][MAX_NODES];
int len;

void initialize () {
	memset(grid, 0, sizeof grid);
}

int getHexaVal (char c) {
	if (c >= '1' && c <= '9') {
		return c - '1';
	}
	return c - 'A' + 9;
}

lli findWays (int r, int c) {
	if (c == len - 1) return 1;
	lli value = 0;
	fr(row, 0, len) {
		value += ((row != r - 1 && row != r && row != r + 1)) ? grid[row][c + 1] : 0;
	}
	return value;
}

int main () {
	lli ans;
	char inp[20];
	while (scanf("%s\n", inp) != EOF) {
		len = strlen(inp);
		// if (inp[len - 1] == '\n') len--;
		// printf("%d\n", len );
		ans = 0;
		initialize();
		for (int col = len - 1; col >= 0; --col) {
			fr (row, 0, len) {
				grid[row][col] = ( inp[col] != '?' && row != getHexaVal(inp[col]) ) ? 0 : findWays(row, col);
				ans += (!col) ? grid[row][col] : 0;
			}
		}
		printf("%lld\n", ans );

	}
}