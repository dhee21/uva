# include <iostream>
# include <stdio.h>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <map>
# include <bitset>
# include <queue>
using namespace std;
#define MAX_NODES 20
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared
#define modVal  20437

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1


//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions

char grid[MAX_NODES][MAX_NODES];
int pathNum[MAX_NODES], distances[MAX_NODES], level[MAX_NODES][MAX_NODES], visited[MAX_NODES][MAX_NODES], valueTo;
pii coor[40];
int paths[MAX_NODES][MAX_NODES];


bool canGo(int r, int c, char last, int n) {
	return r >= 0 && r < n && c >= 0 && c < n && grid[r][c] != '#' && (grid[r][c] == '.' || grid[r][c] <= last) ;
}
int dir[4][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

void bfs (char st, char ed, int n) {
	// init start
	memset(visited, 0, sizeof visited);
	memset(level, -1, sizeof level);
	memset(paths, 0, sizeof paths);
	queue<pii> q;
	// init end
	
	pii root = coor[st - 'A']; 
	pii end = coor[ed - 'A'];
	level[root.first][root.second] = 0;
	q.push(root);
	visited[root.first][root.second] = 1;
	paths[root.first][root.second] = 1;
	
	while (!q.empty()) {
		pii node = q.front(); q.pop();
		if (node.first == end.first && node.second == end.second){
			break;
		}
		fr(i, 0, 4) {
			int r = node.first + dir[i][0], c = node.second + dir[i][1];
			if (canGo(r, c, ed, n) && !visited[r][c]) {
				visited[r][c] = 1;
				level[r][c] = level[node.first][node.second] + 1;
				q.push(pii(r,c));
			} 
			if (canGo(r, c, ed, n) && visited[r][c] && (level[node.first][node.second] + 1 == level[r][c])) {
				paths[r][c] = ((paths[r][c] % modVal) +  (paths[node.first][node.second] % modVal)) % modVal;
			}
		}
	}
	
	pathNum[ed - 'A'] = level[end.first][end.second] == -1 ? 0 : paths[end.first][end.second];
	distances[ed - 'A'] = level[end.first][end.second] == -1 ? 0 : level[end.first][end.second];
}

void init () {
	memset(pathNum, 0, sizeof pathNum);
	memset(distances, 0, sizeof distances);
}

int main () {
	int caseNo = 0;
	int n;
	while (ipInt(n) > 0 && n) {
		fr (i, 0, n) {
			scanf("%s", grid[i]);
		}
		init();
		caseNo ++;
		char maxChar = 'A';
		
		fr (i, 0, n) {
			fr(j, 0, n) {
				if (grid[i][j] >= 'A' && grid[i][j] <='Z') {
					maxChar = max (maxChar, grid[i][j]);
					coor[grid[i][j] - 'A'] = pii(i, j);
				}
			}
		}
				
		// printf("%d %d \n",coor['Y' - 'A'].first, coor['Y'-'A'].second );
		bool isimpossible = false;
		for (char st = 'A' ; st < maxChar; st ++) {
			bfs(st, st + 1, n);
			if (pathNum[st - 'A' + 1] == 0) {
				isimpossible = true;
				break;
			}
		}

		int numPaths = 1, sp = 0;
		if (isimpossible) {
			printf("Case %d: Impossible\n", caseNo);
		} else {
			for (char st = 'A' + 1; st <= maxChar; ++st) {
				// printf("dist of %c %d \n", st, distances[st - 'A'] );
				sp += distances[st - 'A'];
				numPaths = ((numPaths % modVal) * (pathNum[st - 'A'] % modVal)) % modVal;
			}
			printf("Case %d: %d %d\n",caseNo, sp, numPaths );
		}
	}
	return 0;
}