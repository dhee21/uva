# include <stdio.h>
# include <iostream>
# include <string.h>
using namespace std;
#define SHIP 'x'
#define SHOT '@'
int n, visited[110][110];
char grid[110][110];
int dir[8][2] = {{1, 0}, {0, -1}, {0, 1}, {-1, 0}};

void dfs (int r, int c) {
	visited[r][c] = true;
	for (int i = 0; i < 4; ++i) {
		int newR = r + dir[i][0], newC = c + dir[i][1];
		if (newR >= 0 && newR < n && newC >= 0 && newC < n && !visited[newR][newC] && (grid[newR][newC] == SHIP || grid[newR][newC] == SHOT)){
			dfs(newR, newC);
		}
	}
}

int main () {
	int t;
	scanf ("%d", &t);
	for (int tc = 1; tc <= t; ++tc) {
		scanf("%d\n", &n);
		memset(visited, 0, sizeof visited);
		for (int r = 0; r < n; ++r){
			for (int c = 0; c < n; ++c) {
				scanf("%c", &grid[r][c]);
			}
			scanf("\n");
		}
		int numShips = 0;
		for (int r = 0; r < n; ++r){
			for (int c = 0; c < n; ++c) {
				if (!visited[r][c] && grid[r][c] == SHIP) {
					numShips ++;
					dfs(r, c);
				}
			}
		}
		printf("Case %d: %d\n", tc, numShips);

	}
}