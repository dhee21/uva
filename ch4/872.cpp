# include <stdio.h>
# include <iostream>
# include <string.h>
# include <vector>
# include <algorithm>
using namespace std;
typedef vector<int> vi;
typedef vector<vi> vvi;
int visited[40], nodeLen;
vvi adjMat(40);
char ans[25], nodes[25];

bool canBeVisited (int index) {
	for (int i = 0; i < adjMat[index].size(); ++i) {
		int nodeInd  = adjMat[index][i];
		if (visited[nodeInd]) return false;
	}
	return true;
}

bool backTrack (int placeTOFill) {
	if (placeTOFill == nodeLen) {
		for (int i = 0; i < nodeLen ; ++i) {
			if (i == nodeLen - 1) {
				printf("%c\n", ans[i] );
			} else printf("%c ", ans[i] );
		}
		return true;
	}
	bool possible = false;
	for (int i = 0; i < nodeLen; ++i) {
		int index = nodes[i] - 'A';
		if (!visited[index] && canBeVisited(index)) {
			ans[placeTOFill] = nodes[i];
			visited[index] = true;
			possible = backTrack(placeTOFill + 1) || possible;
			visited[index] = false;
		}
	}
	return possible;
}
int main () {
	int tc;
	scanf("%d\n", &tc);
	while(tc --) {
		nodeLen = 0;
		// printf("%s\n", );
		for (int nd = 0; nd < 25; ++nd) adjMat[nd].clear();
		memset (visited, 0, sizeof visited);
		scanf("\n");

		char node, after, from, to, waste;
		while(scanf("%c%c", &node, &after)) {
			// printf("%c%c\n", node, after );
			nodes[nodeLen++] = node;
			if (after == '\n') break;
		}
		sort(nodes, nodes + nodeLen);
		while(scanf("%c%c%c%c", &from, &waste, &to, &after)) {
			int fromInd = from - 'A', toInd = to - 'A';
			// printf("%c %c %d %d\n", from, to, fromInd, toInd );
			adjMat[fromInd].push_back(toInd);
			if (after == '\n') break;
		}
		bool possible = backTrack(0);
		if (!possible) {
			printf("NO\n");
		}
		if (tc) printf("\n");

	}
	return 0;
}