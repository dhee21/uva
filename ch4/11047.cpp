# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 110
#define inf (1<<30) - 1
//macros

//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions

mSI indexOf;
string nodes[MAX_NODES];

// floyd warshall variables
// adjacency matrix and parent matrix
int adjMat[MAX_NODES][MAX_NODES], p[MAX_NODES][MAX_NODES], n;
// floyd warshall variables


void floydWarshall () {
	fr (k, 0, n) fr (i, 0, n) fr (j, 0, n) {
		if ( adjMat[i][k] + adjMat[k][j] < adjMat[i][j]) {
			adjMat[i][j] = adjMat[i][k] + adjMat[k][j];
			p[i][j] = p[k][j];
		}
	}
}

void initialize () {
	fr (i, 0, MAX_NODES) fr(j, 0, MAX_NODES) {
		// adjMat[i][j] = (i == j) ? 0 : inf;
		p[i][j] = i;
	}
	indexOf.clear();
}
void printPath(int start, int end) {
	vi ans;
	ans.push_back(end);
	end = p[start][end];
	while (true) {
		ans.push_back(end);
		if (start == end) {
			break;
		}
		end = p[start][end];
	}
	while (ans.size()) {
		printf("%s", nodes[ans.back()].data());
		ans.pop_back();
		if (!ans.size()) {
			printf("\n");
		} else {
			printf(" ");
		}
	}
}

// never forget to initialize test case
int main () {
	int t, routes;
	ipInt(t);
	while (t--) {
		initialize();
		ipInt(n);
		string str;
		fr (i, 0, n) {
			cin >> str;
			nodes[i] = str;
			indexOf[str] = i;
		}
		// printf("yo\n");
		fr(i, 0, n) fr(j, 0, n) {
			int wgt;
			ipInt(wgt);
			// printf("%d ", wgt);
			adjMat[i][j] = wgt;
			adjMat[i][j] = (adjMat[i][j] == -1) ? inf : adjMat[i][j];
		}
		floydWarshall();
		// printf("fe\n");
		ipInt(routes);
		while(routes --) {
			string emp, start, end;
			cin >> emp >> start >> end;
			int startNode = indexOf[start], endNode = indexOf[end];
			// cout << startNode << endNode << adjMat[startNode][endNode] << endl;
			if (adjMat[startNode][endNode] != inf) {
				// printf("wgwegfewfwefewfwegvwef\n");
				printf("Mr %s to go from %s to %s, you will receive %d euros\n", emp.data(), start.data(), end.data(), adjMat[startNode][endNode] );
				printf("Path:");
				printPath(startNode, endNode);
			} else {
				printf("Sorry Mr %s you can not go from %s to %s\n", emp.data(), start.c_str(), end.data());
			}
		}
		
	}
}