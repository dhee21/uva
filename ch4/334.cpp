# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 210
#define inf (1<<30) - 1

//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions

mSI indexOf;
vector<pii> pairs;
bool adjMat[MAX_NODES][MAX_NODES];
int  nodeIndex, n;
string nodes[MAX_NODES];

void transitiveClosure () {
		fr (k, 0, n) fr (i, 0, n) fr (j, 0, n) {
			adjMat[i][j] = adjMat[i][j] | (adjMat[i][k] & adjMat[k][j]);
		}
}
void makeEdge (int from, int to) {
	adjMat[from][to] = 1;
}

void inputEvents (int events) {
	string str;
	int prevNode;
	fr (i, 0, events) {
		cin >> str;
		// cout << str;
		if (indexOf.count(str) == 0) {
			nodes[nodeIndex] = str;
			indexOf[str] = nodeIndex ++;
		}
		if (i) {
				int presentNode = indexOf[str];
				makeEdge(prevNode, presentNode);
		}
		prevNode = indexOf[str];
	}
	// cout << endl;
}
void inputMessages (int messages) {
	string str1, str2;
	fr (i, 0, messages) {
		cin >> str1 >> str2;
		// cout << str1 << str2 <<endl;
		makeEdge(indexOf[str1], indexOf[str2]);
	}
}
void initialize () {
	memset(adjMat, 0, sizeof adjMat);
	nodeIndex = 0;
	indexOf.clear();
	pairs.clear();
}

int main () {
	int computations, events, messages, casNo = 0;
	while (ipInt(computations) && computations) {
		casNo++;
		initialize();
		fr (i, 0, computations) {
			ipInt(events);
			inputEvents(events);
		}
		n = nodeIndex;
		fr(i, 0, n) {
			adjMat[i][i] = 1;
		}
		ipInt(messages);
		inputMessages(messages);
		transitiveClosure();
		int pairsNum = 0;
		fr (i, 0, n) fr(j , i, n) {
			if (adjMat[i][j] == 0 &&  adjMat[j][i] == 0) {
				if (pairsNum < 2) {
					pairs.push_back( pii (i, j) );
				}
				pairsNum ++;
			}
		}
		printf("Case %d, ",casNo );
		if (pairsNum == 0) {
			printf("no concurrent events.\n");
		} else {
			printf("%d concurrent events:\n", pairsNum);
			fr (i, 0, pairs.size()) {
				printf("(%s,%s) ", nodes[pairs[i].first].data(), nodes[pairs[i].second].data());
				// i == pairs.size() - 1 ? printf("\n") : printf(" ");
			}
			printf("\n");
		}
	}
}