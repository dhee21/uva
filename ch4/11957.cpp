# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <map>
using namespace std;
#define MAX_NODES 110
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
# define moduloVal 1000007

typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
matrix adjMat(MAX_NODES);
vector<edge> EdgeList(MAX_EDGES);
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}

char grid[MAX_NODES][MAX_NODES];
int ansGrid[MAX_NODES][MAX_NODES];

int n;
bool canGo (int r, int c) {
	return r >= 0 && r < n && c >= 0 && c < n;
}
bool isEnemy (int r, int c) {
	return grid[r][c] == 'B';
}

int findWays (int row, int col) {
	int ans = 0;
	if (grid[row][col] == 'B') return 0;
	if (canGo(row - 1, col + 1)) {
		if (isEnemy(row - 1, col + 1)) {
			if (canGo(row - 2, col + 2) && !isEnemy(row - 2, col + 2)) {
				ans += ansGrid[row - 2][col + 2];
			}
		} else {
			ans += ansGrid[row - 1][col + 1];
		}
	}
	ans %= moduloVal;
	if (canGo(row - 1, col - 1)) {
		if (isEnemy(row - 1, col - 1)) {
			if (canGo(row - 2, col - 2) && !isEnemy(row - 2, col - 2)) {
				ans += ansGrid[row - 2][col - 2];
			}
		} else {
			ans += ansGrid[row - 1][col - 1];
		}
	}
	ans %= moduloVal;
	return ans;
}

int main () {
	int t, stR, stC;
	ipInt(t);
	fr (tc, 0, t) {
		ipInt(n);
		scanf("\n");
		// printf("%d\n",n );
		fr(i, 0, n) {
			fr (j, 0, n) {
				scanf("%c", &grid[i][j]);
				if (grid[i][j] == 'W') {
					stR = i; stC = j;
				}
			}
			scanf("\n");
		}
		// fr(i, 0, n) {
		// 	fr (j, 0, n) {
		// 		printf("%c", grid[i][j]);
		// 	}
		// 	printf("\n");
		// }
		fr(col, 0, n) {
			if (grid[0][col] == 'B') ansGrid[0][col] = 0;
			else ansGrid[0][col] = 1;
		}
		fr (row, 1, stR) {
			fr(col, 0, n) {
				ansGrid[row][col] = findWays(row, col);
			}
		}
		int ans = findWays(stR, stC);
		printf("Case %d: %d\n",tc + 1, ans );
	}
}