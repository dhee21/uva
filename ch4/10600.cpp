# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
#define MAX_NODES 1000
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1010
# define inf 1<<30
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// matrix adjMat(MAX_NODES);
vector<edge> EdgeList;

int n, m;
long long int mstCost;
// problem specific variable starts
  vi MStEdges;
  int flaggedEdge, ComponentsLeft ;
// problem specific variable ends

class unionFind {
	public :
	vector <int> pset,nodes;//pset is storing at which node a node is pointing , and nodes stores number o noes in a set if an node is the main node of as set
	int num_of_sets;

	void iniSet(int n) {
		pset.assign(n,0);
		nodes.assign(n,1);
		for(int i = 0 ; i < pset.size() ; ++i ) {
			pset[i] = i;
		}
		num_of_sets = n;
	}
	int findSet(int v) {
			if(v < 0 || v >= pset.size() ) {
				return -1 ;
			}
			int ptr = v;
			while ( pset[ptr] != ptr ) {
				ptr = pset[ptr];
			}
			compressThePath(v, ptr);
			return ptr;
	}
	void compressThePath (int vertex, int set ) {
		 int ptr = vertex;
		 while(pset[ptr] != ptr ) {
		 	int nextNode = pset[ptr];
		 	pset[ptr] = set ;
		 	ptr = nextNode;
		 }
	}
	bool isSameSet(int vertex1, int vertex2) {
			int set1 = findSet(vertex1), set2 = findSet(vertex2);
			if(set1 == -1 || set2 == -1) {
				return 0;
			}
			return set1 == set2;
	}
	int numDisjointSets() {
		return num_of_sets;
	}
	void unionSet(int vertex1, int vertex2 ) {
		int set1 = findSet(vertex1), set2 = findSet(vertex2);
		if(set1 == -1 || set2 == -1) {
			return ;
		}
		if(set1 == set2) {
			return ;
		}
		if(nodes[set1] > nodes[set2]) {
			pset[set2] = set1 ;
			nodes[set1] += nodes[set2] ;
 		} else {
 			pset[set1] = set2 ;
 			nodes[set2] += nodes[set1] ;
 		}
 		num_of_sets--;
	}
	int sizeOfSet(int vertex) {
		int set = findSet(vertex) ;
		if(set == -1) {
			return -1;
		}
		return nodes[set];
	}
};

void kruskalsAlgo (bool storeEdges) {
	mstCost = 0;
	unionFind unionWorker;
	unionWorker.iniSet(n);
	if (storeEdges) {
		sort(EdgeList.begin(), EdgeList.end());
	}
	for (int i = 0; i < EdgeList.size(); ++i) {
		if (flaggedEdge == i && storeEdges == false) continue;
		int  edgeWeight = EdgeList[i].first;
		int node1 = EdgeList[i].second.first, node2 = EdgeList[i].second.second;
		if (unionWorker.isSameSet(node1, node2) == false) {
			unionWorker.unionSet(node1, node2);
			mstCost += edgeWeight;
			if (storeEdges) {
				MStEdges.push_back(i);
			}
		}
	}
	ComponentsLeft = unionWorker.numDisjointSets();
}
void makeEdge(int from, int to, int weight) {
	EdgeList.push_back(make_pair(weight, pii(from, to)));
}
void initialize () {
	EdgeList.clear();
	MStEdges.clear();
	flaggedEdge = -1;
}
int main () {
	int t, satChannels;
	long long int firstMin, secondMin;
	scanf("%d", &t);
	while (t--) {
		initialize();
		secondMin = inf;
		scanf("%d%d", &n, &m);
		for (int i = 0 ; i < m; ++i) {
			int from, to, cost;
			scanf("%d %d %d", &from, &to, &cost);
			makeEdge(from - 1, to - 1, cost);
		}
		kruskalsAlgo(1);
		firstMin = mstCost; 
		for (int i = 0 ; i < MStEdges.size(); ++i) {
			flaggedEdge = MStEdges[i];
			kruskalsAlgo(0);
			if (ComponentsLeft == 1) {
				secondMin = min(secondMin, mstCost);
				if (secondMin == firstMin) {
					break;
				}
			}
		}
		printf("%lld %lld\n", firstMin, secondMin);
	}
	return 0;
}
