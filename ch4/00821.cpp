# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 110
#define inf (1<<30) - 1
map<int, int> indexOf;
//macros

// floyd warshall variables
// adjacency matrix and parent matrix
int adjMat[MAX_NODES][MAX_NODES], p[MAX_NODES][MAX_NODES], n, nodeIndex;
// floyd warshall variables


void floydWarshall () {
	fr (k, 0, n) fr (i, 0, n) fr (j, 0, n) {
		if ( adjMat[i][k] + adjMat[k][j] < adjMat[i][j]) {
			adjMat[i][j] = adjMat[i][k] + adjMat[k][j];
			p[i][j] = p[j][k];
		}
	}
}

void initialize () {
	fr (i, 0, MAX_NODES) fr(j, 0, MAX_NODES) {
		adjMat[i][j] = (i == j) ? 0 : inf;
		p[i][j] = i;
	}
	n = -1;
	indexOf.clear();
	nodeIndex = 0;
}

float calculateAverage () {
	float  sum = 0, pairs = 0;
	fr (i, 0, n) fr (j, 0, n) {
		if (i != j && adjMat[i][j] != inf) {
			sum += adjMat[i][j];
		}
	}
	pairs = n * (n - 1);
	return sum / pairs;
}

void makeNode (int p1, int p2) {
	if (indexOf.count(p1) == 0) {
		indexOf[p1] = nodeIndex ++;
	}
	if (indexOf.count(p2) == 0) {
		indexOf[p2] = nodeIndex++;
	}
	adjMat[indexOf[p1]][indexOf[p2]] = 1;
}

int main () {
	int p1, p2, caseNo = 0;
	while(scanf("%d %d", &p1, &p2) && (p1 || p2) ) {
		caseNo ++;
		initialize();
		makeNode(p1, p2);
		while (scanf("%d %d  ", &p1, &p2) && (p1 || p2) ) {
			makeNode(p1, p2);
		}
		n = nodeIndex;
		floydWarshall();
		float ans = calculateAverage();
		printf("Case %d: average length between pages = %0.3f clicks\n", caseNo, ans );
	}
	return 0;
}