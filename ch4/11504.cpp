# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
using namespace std;
#define MAX_NODES 100100
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;

matrix adjMat(MAX_NODES);
int visited[MAX_NODES], cutFromGraph[MAX_NODES];
int timeCounter;
vi topSort;

void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}
void dfs(int node) {
	visited[node] = DFS_GRAY;
	int neighbours = adjMat[node].size();
	for (int i = 0; i < neighbours; ++i) {
		pii edge = adjMat[node][i];
		int edgeToNode = edge.first, edgeWeight = edge.second;
		if (visited[edgeToNode] == DFS_WHITE) {
			dfs(edgeToNode);
		}
	}
	visited[node] = DFS_BLACK;
	topSort.push_back(node);
}

void initialize () {
	for( int i = 0; i < MAX_NODES; ++i) {
		adjMat[i].clear();
	}
	memset(visited, DFS_WHITE, sizeof visited);
	// memset(nodeForDepTime, -1, sizeof nodeForDepTime);
	// memset(dfsParent, -1, sizeof dfsParent);
	memset(cutFromGraph, 0, sizeof cutFromGraph);
	topSort.clear();
}

void dfs2(int node) {
	cutFromGraph[node] = 1;
	int neighbours = adjMat[node].size();
	for (int i = 0; i < neighbours; ++i) {
		pii edge = adjMat[node][i];
		int edgeToNode = edge.first, edgeWeight = edge.second;
		if (cutFromGraph[edgeToNode] == 0) {
			dfs2(edgeToNode);
		}
	}
}
int main () {
	int n, m, t, from, to;
	scanf("%d", &t);
	for (int tc = 0; tc < t; ++tc) {
		initialize();
		scanf("%d %d", &n, &m);
		// printf("%d %d\n",n, m);
		for (int edge = 0; edge < m; ++edge) {
			scanf("%d %d", &from, &to);
			// printf("%d %d\n",from, to );
			makeEdge(from - 1, to - 1, 0);
		}
		// break;
		for (int i = 0; i < n; ++i) {
			if (visited[i] == DFS_WHITE) {
				dfs(i);
			}
		}
		int numPushes = 0;
		for (int i = topSort.size() - 1; i >= 0; --i) {
			int oneNode = topSort[i];
			if (cutFromGraph[oneNode] == 0) {
				numPushes++;
				dfs2(oneNode);
			}
		}
		printf("%d\n", numPushes );
	}
	return 0;

}