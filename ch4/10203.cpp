# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <list>
# include <bitset>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 210
#define inf (1<<30) - 1

//typedefinitions
typedef pair<int, int> pii;
typedef pair<int, double> pif;
typedef vector<int> vi;
typedef vector<pif> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef map<pii, int> mpii;
typedef list<int> li;
//typedefinitions
matrix adjMat(MAX_NODES);

double dist(pii fromCoor, pii toCoor) {
	return sqrt(pow((fromCoor.first - toCoor.first), 2) + pow((fromCoor.second - toCoor.second) , 2));
}


int main () {
	int t;
	char inp[50], caseNo = 0;
	ipInt(t); scanf("\n");
	double sum;
	while(t--) {
		if(caseNo++){printf("\n");}
		fgets(inp, 50, stdin);
		pii start;
		sscanf(inp, "%d %d", &start.first, &start.second);
		sum = 0;
		while(fgets(inp, 50, stdin) != NULL && strlen(inp) > 1) {
			pii from, to;
			sscanf(inp, "%d %d %d %d" , &from.first, &from.second, &to.first, &to.second);
			sum += dist(from, to);
		}
		sum = sum * 2;
		double timeVal = (sum * 3)/1000;
		int hrs = int(timeVal) / 60;
		double minutes = timeVal - (hrs * 60);
		int minutesVal = floor(minutes + 0.5);
		if (minutesVal == 60) {
			minutesVal = 0;
			hrs++;
		}
		if (minutesVal < 10) {
			printf("%d:0%d\n",hrs, minutesVal );
		} else {
			printf("%d:%d\n",hrs, minutesVal );
		}

	}
	return 0;
}

