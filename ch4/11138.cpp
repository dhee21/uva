# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define MAX_NODES 500
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
int adjMat[MAX_NODES][MAX_NODES], rows, cols, visited[MAX_NODES], owner[MAX_NODES];

int canAugmentPath (int left) {
	if (visited[left]) return 0;
	visited[left] = 1;
	fr(i, 0, cols) {
		if (adjMat[left][i]) {
			if (owner[i] == -1 || canAugmentPath(owner[i])) {
				owner[i] = left;
				return 1;
			}
		}
	}
	return 0;
}

int MCBM () {
	int pairsMade = 0;
	fr (i, 0, MAX_NODES) owner[i] = -1;
	fr(i, 0, rows) {
		memset(visited, 0, sizeof visited);
		pairsMade += canAugmentPath(i); 
	}
	return pairsMade;
}

int main () {
	int t, caseNo = 0;
	ipInt(t);
	while (t--) {
		caseNo ++;
		ipInt(rows); ipInt(cols);
		fr (i, 0, rows) fr (j, 0, cols) scanf("%d", &adjMat[i][j]);
		int mcbm = MCBM();
		printf("Case %d: a maximum of %d nuts and bolts can be fitted together\n",caseNo, mcbm );
	}
	return 0;
}