# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 1100
#define inf 1 << 28
//macros

//definitions
typedef pair<int, int> pii;
typedef pair<string, string> pss;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef vector<vs> vvs;
matrix adjMat(MAX_NODES);
//definitions

// bellman Ford variables
bool hasNegativeCycle;
int dist[MAX_NODES];
// bellman Ford variables
int m, n;

void makeEdge(int from, int to, int edgeWeight) {
	adjMat[from].push_back(pii(to, edgeWeight));
}

void checkCycle () {
	hasNegativeCycle = false;
	fr(node, 0, n) {
		int neighbours = adjMat[node].size();
		fr(v, 0, neighbours) {
			if (dist[node] != inf) {
				int edgeToNode = adjMat[node][v].first, edgeWeight = adjMat[node][v].second;
				if (dist[node] + edgeWeight < dist[edgeToNode]) {
					hasNegativeCycle = true;
					break;
					dist[edgeToNode] = dist[node] + edgeWeight;
				}
			}
		}
		if (hasNegativeCycle) break;
	}
}

void bellmanFord (int source) {
	dist[source] = 0;
	fr (i, 0, n - 1) fr(node, 0, n) {
		int neighbours = adjMat[node].size();
			if (dist[node] != inf) {
			fr(v, 0, neighbours) {
					int edgeToNode = adjMat[node][v].first, edgeWeight = adjMat[node][v].second;
					dist[edgeToNode] = min (dist[edgeToNode], dist[node] + edgeWeight);
				}
			}
		}
}

void initialize () {

	fr(i, 0, MAX_NODES) {
		adjMat[i].clear();
		dist[i] = inf;
	}
}
int main () {
	int t;
	scanf("%d", &t);
	while (t --) {
		initialize();
		scanf("%d %d", &n, &m);
		fr(i, 0, m) {
			int x, y, t;
			ipInt(x);ipInt(y);ipInt(t);
			makeEdge(x, y, t);
		}
		bellmanFord(0);
		checkCycle();
		if (hasNegativeCycle) {
			printf("possible\n");
		} else {
			printf("not possible\n");
		}
	}

}