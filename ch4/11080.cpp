# include <iostream>
# include <vector>
# include <algorithm>
# include <string>
# include <queue>
# include <map>
# include <string.h>
# define MAX_NODES 210

using namespace std;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
matrix adjMat(MAX_NODES);
typedef queue<int> qu;

int n, m, visited[MAX_NODES], level[MAX_NODES], minGuards, oddLevelNodes, evenLevelNodes,caseNo = 0;

bool isBipartite (int node) {
	oddLevelNodes = evenLevelNodes = 0;
	qu q;
	visited[node] = true;
	level[node] = 0;
	q.push(node);
	while (!q.empty()) {
		int visitedNode = q.front(); q.pop();
		((level[visitedNode] % 2) != 0) ? oddLevelNodes++ : evenLevelNodes++ ;
		for (int i = 0; i < adjMat[visitedNode].size(); ++i) {
			pii edge = adjMat[visitedNode][i];
			int edgeToNode = edge.first, edgeWeight = edge.second;
			if (visited[edgeToNode] && level[edgeToNode] == level[visitedNode]) {
				return false;
			} else if (!visited[edgeToNode]) {
				visited[edgeToNode] = true;
				level[edgeToNode] = level[visitedNode] + 1;
				q.push(edgeToNode);
			}
		}
	}
	if (evenLevelNodes == 1 && oddLevelNodes == 0) {
		minGuards += 1;
	} else {
		minGuards += min(oddLevelNodes, evenLevelNodes);
	}
	return true;
}

void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}
void initialize () {
	for( int i = 0; i < MAX_NODES; ++i) {
		adjMat[i].clear();
	}
	memset(visited, 0, sizeof visited);
	memset(level, 0, sizeof level);
	minGuards = 0;
}
int main () {
	int t;
	scanf("%d", &t) ;
	while (t--) {
		caseNo ++;
		initialize();
		scanf("%d %d", &n, &m);
		while (m--) {
			int from, to;

			scanf("%d %d", &from, &to);
			makeEdge(from, to, 0);
		}
		int flagBipartite = true;
		for ( int i = 0; i < n; ++i) {
				if (!visited[i]) {
					if (!isBipartite(i)) {
						flagBipartite = false;
						break;
					}
			}
		} 
		if (!flagBipartite) {
			printf("-1\n");
		} else {
			printf("%d\n", minGuards);
		}
	}
		return 0;

}