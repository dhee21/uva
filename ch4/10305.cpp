# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
using namespace std;
#define MAX_NODES 110
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;

matrix adjMat(MAX_NODES);
int visited[MAX_NODES], arr[MAX_NODES], dep[MAX_NODES], nodeForDepTime[(2*MAX_NODES) + 50], dfsParent[MAX_NODES];
int timeCounter;
vi topSort;

void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}
void dfs(int node) {
	visited[node] = DFS_GRAY;
	arr[node] = timeCounter ++;
	int neighbours = adjMat[node].size();
	for (int i = 0; i < neighbours; ++i) {
		pii edge = adjMat[node][i];
		int edgeToNode = edge.first, edgeWeight = edge.second;
		if (visited[edgeToNode] == DFS_WHITE) {
			dfsParent[edgeToNode] = node;
			dfs(edgeToNode);
		}
	}
	nodeForDepTime[timeCounter] = node;
	dep[node] = timeCounter ++;
	visited[node] = DFS_BLACK;
	topSort.push_back(node);
}

void initialize () {
	for( int i = 0; i < MAX_NODES; ++i) {
		adjMat[i].clear();
	}
	timeCounter = 0;
	memset(visited, DFS_WHITE, sizeof visited);
	memset(nodeForDepTime, -1, sizeof nodeForDepTime);
	memset(dfsParent, -1, sizeof dfsParent);
	topSort.clear();
}

int main () {
	int n, m;
	while (scanf("%d %d", &n, &m) && (n || m)) {
		initialize();
		int from, to;
		for (int edge = 0; edge < m; ++edge) {
			scanf("%d %d", &from, &to);
			makeEdge(from, to, 0);
		}
		for (int i = 1; i <= n; ++i) {
			if (visited[i] == DFS_WHITE) {
				dfs(i);
			}
		}
		reverse(topSort.begin(), topSort.end());
		for (int i = 0 ; i < topSort.size() ; ++i) {
			printf("%d", topSort[i]);
			i == topSort.size() - 1 ? printf("\n") : printf(" ");
		}
	}
	return 0;

}