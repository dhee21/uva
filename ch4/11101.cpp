# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
#define length 10000
typedef pair<int, int> pii;
int city[length][length] ,ans;
typedef pair<int, pii> ipii;
queue<ipii> q;
bool canGo(int row, int col) {
	return row >= 0 && col >= 0;
}
int dir[4][2] = { {0, 1}, {0, -1}, {-1, 0}, {1, 0} };
int bfs () {
	while(!q.empty()) {
		ipii node = q.front(); q.pop();
		for (int i = 0 ; i < 4; ++i) {
			int newRow = node.second.first + dir[i][0], newCol = node.second.second + dir[i][1], newDist = node.first + 1;
			ipii neighbour = make_pair(newDist, pii(newRow, newCol));
			if (canGo(newRow, newCol) && city[newRow][newCol] != 1) {
				if (city[newRow][newCol] == 2) {
					return newDist;
				}
				q.push(neighbour);
				city[newRow][newCol] = 1;
			}
		}
	}
	return 0;
}

void initialize () {
	ans = 0;
	memset(city, 0, sizeof city);
	while (!q.empty()) q.pop();
}
int main () {
	int p, st, av;
	while (scanf("%d", &p) && p) {
		// printf("tk\n");
		initialize();
		while (p--) {
			scanf("%d %d", &st, &av);
			city[st][av] = 1;
			q.push(make_pair(0, pii(st, av)));
		}
		// printf("tk\n");
		scanf("%d", &p);
		while (p--) {
			scanf("%d %d", &st, &av);
			city[st][av] = 2;
		}
		ans = bfs();
		printf("%d\n", ans );
	}
	return 0;
}