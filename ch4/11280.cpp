# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 105
#define inf 1 << 28
//macros

//definitions
typedef pair<int, int> pii;
typedef pair<string, string> pss;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef vector<vs> vvs;
matrix adjMat(MAX_NODES);
//definitions

// bellman Ford variables
bool hasNegativeCycle;
int dist[MAX_NODES][MAX_NODES];
// bellman Ford variables
int m, n;
// problem specific variables
mSI indexOf;
vi stopOvers;
// problem specific variables

void makeNode (string city, int nodeNum) {
	indexOf[city] = nodeNum;
}

void makeEdge(string fromStr, string toStr, int edgeWeight) {
	int from = indexOf[fromStr], to = indexOf[toStr];
	adjMat[from].push_back(pii(to, edgeWeight));
}

void bellmanFord (int source) {
	dist[0][source] = 0;
	fr (i, 0, n - 1){
		dist[i + 1][0] = 0;
		fr(node, 0, n) {
		int neighbours = adjMat[node].size();
			if (dist[i][node] != inf) {
			fr(v, 0, neighbours) {
					int edgeToNode = adjMat[node][v].first, edgeWeight = adjMat[node][v].second;
					dist[i + 1][edgeToNode] = min (dist[i + 1][edgeToNode], dist[i][node] + edgeWeight);
				}
			}
		}
	}
}

void initialize () {

	fr(i, 0, MAX_NODES) {
		adjMat[i].clear();
		fr(j, 0, MAX_NODES) {
			dist[i][j] = inf;
		}
	}
	stopOvers.clear();
	indexOf.clear();
}

int main () {
	int t, q, caseNo = 0;
	ipInt(t);
	fr(tc, 0, t) {
		if(caseNo) printf("\n");
		initialize();
		caseNo++;
		ipInt(n);getchar();
		fr(i, 0, n) {
			string s;
			cin >> s;
			makeNode(s, i);
		}
		ipInt(m);getchar();
		fr(i, 0, m) {
			string from, to;
			int cost;
			cin >> from >> to >> cost;
			makeEdge(from, to, cost);
		}
		ipInt(q);
		bellmanFord(0);
		printf("Scenario #%d\n",caseNo);
		fr(i, 0, q) {
			int stops;
			ipInt(stops);
			stops = min(stops, n - 2);
			if (dist[stops + 1][n - 1] != inf) {
				printf("Total cost of flight(s) is $%d\n", dist[stops + 1][n - 1]);
			} else {
				printf("No satisfactory flights\n");
			}
		}
	}
	return 0;
}