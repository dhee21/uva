# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 22
#define inf (1<<30) - 1

typedef vector <int> vi;

// floyd warshall variables
// adj matrix and parent matrix
int  p[MAX_NODES][MAX_NODES][MAX_NODES], n, nodeIndex;
float adjMat[MAX_NODES][MAX_NODES][MAX_NODES];
// floyd warshall variables

int m, found, ansLen, vertex;

void floydWarshall () {
	fr (l, 2, n + 1){
			fr (i, 0, n) fr(j, 0, n){
				adjMat[i][j][l] = adjMat[i][j][l - 1];
				p[i][j][l] = p[i][j][l - 1];
				fr (v, 0, n) {
					if (adjMat[i][v][l - 1] * adjMat[v][j][1] > adjMat[i][j][l]) {
						adjMat[i][j][l] = adjMat[i][v][l - 1] * adjMat[v][j][1];
						p[i][j][l] = v;
					}
				}
			}
			fr (k, 0, n) {
				if (adjMat[k][k][l] > 1.01) {
					found = true;
					ansLen = l;
					vertex = k;
					break;
				}
			}
			if (found) break;
	}
}
void initialize () {
	found = false;
}
void printPath () {
	vi ans;
	int i = vertex;
	for (int l = ansLen; l >= 0; --l) {
		if (ans.size() && ans.back() != i) {
			ans.push_back(i);
		} else {
			ans.push_back(i);
		}
		i = p[vertex][i][l];
	}
	for(int i = ans.size() - 1; i >= 0; --i) {
		if (i == 0) {
			printf("%d\n",ans[i] + 1 );
		} else {
			printf("%d ", ans[i] + 1);
		}
	}
}
int main () {
	while(ipInt(n) != EOF) {
		initialize();
		fr(i, 0, n) fr(j, 0, n) {
			if (i != j) {
				scanf("%f", &adjMat[i][j][1]);
				p[i][j][1] = i;
			} else {
				adjMat[i][j][1] = 1;
				p[i][j][1] = i;
			}
		}
		floydWarshall();
		if (found) {
			printPath();
		} else {
			puts("no arbitrage sequence exists");
		}
	}
}