# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <map>
using namespace std;
#define MAX_NODES 80
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define CAME_FROM_TOP 0
#define CAME_FROM_RIGHT 1
#define CAME_FROM_LEFT 2

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 10
#define infNotVisited (1<<30) - 1

int grid [MAX_NODES][MAX_NODES], longestWalk[MAX_NODES][MAX_NODES][6][3], n;

void initialize () {
	fr(i, 0, MAX_NODES) fr(j, 0, MAX_NODES) fr (k, 0, 6) fr(dr, 0, 3) {
		longestWalk[i][j][k][dr] = -infNotVisited;
	}
}

int canGo( int r, int c) {
	return r >= 0 && r < n && c >= 0 && c < n;
}

int findLongestWalk (int r, int c, int k, int cameFrom) {
		int value = k;
		if (grid[r][c] < 0) value-- ;
		if (value < 0) return -inf;
		if (r == n - 1 && c == n - 1) {
			return grid[r][c];
		}
		if (longestWalk[r][c][k][cameFrom] != -infNotVisited) return longestWalk[r][c][k][cameFrom];
		int longestVal = -inf, ans = -inf;
		if (canGo(r, c + 1) && cameFrom != CAME_FROM_RIGHT) {
			longestVal = max (longestVal, findLongestWalk(r, c + 1, value, CAME_FROM_LEFT));
		}
		if (canGo(r, c - 1) && cameFrom != CAME_FROM_LEFT) {
			longestVal = max (longestVal, findLongestWalk(r, c - 1, value, CAME_FROM_RIGHT));
		}
		if (canGo(r + 1, c)) {
			longestVal = max (longestVal, findLongestWalk(r + 1, c,value, CAME_FROM_TOP));
		}
		if (longestVal != -inf) {
			ans = longestVal + grid[r][c];
		}
		return longestWalk[r][c][k][cameFrom] = ans;
}

int main () {
	int k, caseNo = 0;
	while (scanf("%d %d", &n, &k) && (n || k)) {
		caseNo ++;
		fr (i, 0, n) fr (j, 0, n) scanf("%d", &grid[i][j]);
		initialize();
		int ans = -inf;
		if (n) {
			ans = findLongestWalk(0, 0 , k, CAME_FROM_TOP);
		}
		if (ans == -inf) {
			printf("Case %d: impossible\n", caseNo);
		} else {
			printf("Case %d: %d\n",caseNo, ans );
		}
	}
	return 0;
}