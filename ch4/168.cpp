# include <iostream>
# include <stdio.h>
# include <string.h>
#include <string>
#include <math.h>
#include <utility>
// #include <bits/stdc++.h>
using namespace std;
#define CANDLE 1

int adj[30][30], visited[30];
int K;

void dfs(int presentNode, long long int stepBefore, int beforeVertex) {
	// if (!k) {
	// 	printf("ASDASDAD\n");
	// }
	long long int newStep = (stepBefore + 1) % K;
	if (!newStep) {
		visited[presentNode] = CANDLE;
	}
	bool noWay= true;
	for (int i = 1; i <= adj[presentNode][0]; ++i) {
		int ver = adj[presentNode][i];
		if(visited[ver] != CANDLE && ver != beforeVertex) {
			noWay = false;
			if (!newStep) {
				printf("%c ",'A' + presentNode);
			}
			dfs(ver, newStep, presentNode);
			break;
		}
	}
	if (noWay) {
		printf("/%c\n",'A' + presentNode );
	}
}

int main () {
	char line[10000], mi[100000], th[100000];
	int intMi, intTh;
	while (fgets(line, 300, stdin) && line[0] != '#') {
		memset (visited, 0, sizeof visited);
		memset (adj, 0, sizeof adj);
		int lenInp = strlen(line);
		int s, t, i, ei = 0, e[2];
		for (i = 0; i < lenInp; i++) {
            if (line[i] == '.') break;
            if (line[i] == ':' || line[i] == ';') ei = line[i] == ':' ? 1 : 0;
            else {
                e[ei] = line[i];
                if (ei == 1) {
               	int index = ++adj[e[0] - 'A'][0];
                	adj[e[0] - 'A'][index] = e[1] - 'A';
                } 
            }
        }
        {
            i++;
            while (line[i] == ' ') i++;  s = line[i++];
            while (line[i] == ' ') i++;  t = line[i++];
            while (line[i] == ' ') i++;  K = 0;
            sscanf(line  + i, "%d", &K);
        }
		intMi = s - 'A'; intTh = t - 'A';
		dfs(intMi, 0, intTh);

	}
	return 0;
}