# include <stdio.h>
# include <iostream>
# include <string.h>
# include <utility>
# include <vector>
# include <algorithm>
# include <map>
#include <list>
using namespace std;
#define MAX_NODES 220
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
#define MAX_EDGES 1000100 // nsquared

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef list<int> li;
matrix adjMat(MAX_NODES);
vector<edge> EdgeList(MAX_EDGES);
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}

int degree[MAX_NODES], visited[MAX_NODES];

void init () {
	memset(degree, 0, sizeof degree);
	memset(visited, 0, sizeof visited);
	fr(i, 0, MAX_NODES) {
		adjMat[i].clear();
	}
}

void dfs(int node) {
	visited[node] = 1;
	int neighbours = adjMat[node].size();
	fr (i, 0, neighbours) {
		int edgeToNode = adjMat[node][i].first;
		 if (!visited[edgeToNode]) {
		 	dfs(edgeToNode);
		 }
	}
}

int main () {
	int from, to, caseNo = 0;
	int n, m;
	while (scanf("%d %d", &n, &m) != EOF) {
		init();
		if (!m) {
			printf("Not Possible\n");
			continue;
		}
		fr(i, 0, m) {
			ipInt(from);ipInt(to);
			makeEdge(from, to, 0);
			degree[from]++;
			degree[to]++;
		}
		int comp = 0;
		int noPath = false;
		int dfsDone = 0;
		fr (i, 0, n) {
			if (degree[i] && dfsDone == 0) {
				dfs(i);
				dfsDone = 1;
			} else if (degree[i] && !visited[i] && dfsDone == 1) {
				noPath = true;
				break;
			}
		}
		if (noPath == false) {
			fr(i, 0, n) {
				if (degree[i] & 1) {
					noPath = true;
					break;
				}
			}
		}
		if (noPath) {
			printf("Not Possible\n");
		} else {
			printf("Possible\n");
		}
	}
	return 0;
}