# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 110
#define inf (1<<30) - 1

// floyd warshall variables
// adj matrix and parent matrix
int adjMat[MAX_NODES][MAX_NODES], p[MAX_NODES][MAX_NODES], n, nodeIndex;
// floyd warshall variables

int m;

void floydWarshall () {
	fr (k, 0, n) fr (i, 0, n) fr (j, 0, n) {
		if ( adjMat[i][k] + adjMat[k][j] < adjMat[i][j]) {
			adjMat[i][j] = adjMat[i][k] + adjMat[k][j];
			p[i][j] = p[k][j];
		}
	}
}

void initialize () {
	fr (i, 0, MAX_NODES) fr(j, 0, MAX_NODES) {
		adjMat[i][j] = (i == j) ? 0 : inf;
		p[i][j] = i;
	}
}

int main () {
	int t, x, y, s, d;
	ipInt(t);
	fr(tc, 0, t) {
		initialize();
		ipInt(n);
		ipInt(m);
		while (m --) {
			ipInt(x); ipInt(y);
			adjMat[x][y] = adjMat[y][x] = 1;
		}
		ipInt(s);ipInt(d);
		floydWarshall();
		int ans = -inf;
		fr(i, 0, n) {
			if (adjMat[s][i] != inf && adjMat[i][d] != inf) {
				ans = max (ans , (adjMat[s][i] + adjMat[i][d]));
			}
		}
		printf("Case %d: %d\n",tc + 1, ans );

	}
}