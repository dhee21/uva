# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <math.h>
using namespace std;
#define MAX_NODES 4100
#define inf 1 << 25
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
typedef pair<int, int> pii;
typedef pair<string, string> pss;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef vector<vs> vvs;
matrix adjMat(MAX_NODES);
priority_queue<pii> pq;

// problem specific variables
int languageIndex, nodeIndex, startNode;
vvs languageWords(MAX_NODES);
mSI indexOflanguage;
map<pss, int > indexOf;
int dist[MAX_NODES + 100];
// proble mspecific variables ends

void makeEdge(int from, int to, int edgeWeight) {
	adjMat[from].push_back(pii(to, edgeWeight));
	adjMat[to].push_back(pii(from, edgeWeight));
}

void makeNeighbour(string lang, string word) {
	int node = indexOf[pss(word, lang)];
	int indexLang = indexOflanguage[lang]; 
	fr(i, 0, languageWords[indexLang].size()) {
		string wordNeighbour = languageWords[indexLang][i];
		int neighbourNode = indexOf[pss(wordNeighbour, lang)];
		
			if (wordNeighbour[0] != word[0]) {
				makeEdge(neighbourNode, node, wordNeighbour.size() + word.size());
			}
		
	}
	languageWords[indexLang].push_back(word);
}

int makeNodeReturnIndex (string word, string lang) {
	int toReturnIndex = nodeIndex;
	indexOf[pss(word, lang)] = nodeIndex++;
	if (indexOflanguage.count(lang) == 0) {
		indexOflanguage[lang] = languageIndex++;
	}
	makeNeighbour(lang, word);
	return toReturnIndex;
}

void makeNodeAndEdges (string word, string lang1, string lang2) {
	int node1 = makeNodeReturnIndex(word, lang1), node2 = makeNodeReturnIndex(word, lang2);
	makeEdge(node1, node2, 0);
}

void SSSP (int source) {
	pq.push(pii(0, -source));
	dist[source] = 0;
	while (!pq.empty()) {
		pii minUpperbound = pq.top(); pq.pop();
		int node = -minUpperbound.second, minDist = -minUpperbound.first;
		// printf("%d %d %d\n", node, minDist, dist[node]  );

		if (dist[node] == minDist) {
			int neighbours = adjMat[node].size();
			// Relaxing dist of neighbours
			for (int i = 0 ; i < neighbours; ++i) {
				int edgeToNode = adjMat[node][i].first, edgeWeight = adjMat[node][i].second;
				int newDist = minDist + edgeWeight;
				if (newDist < dist[edgeToNode]) {
					dist[edgeToNode] = newDist;
					pq.push(pii(-newDist, -edgeToNode));
				}
			}
		}
	}
}

void initialize () {
	nodeIndex = 0;
	for (int i = 0; i < MAX_NODES; ++i ) {
		adjMat[i].clear();
		languageWords[i].clear();
	}
	while(!pq.empty()) pq.pop(); 
	languageIndex = 0;
	indexOf.clear();
	indexOflanguage.clear();
}

int main () {
	int m;
	while (scanf("%d\n", &m) && m) {
		initialize();
		fr(i, 0, MAX_NODES) {
			dist[i] = inf;
		}
		string l1, l2;
		cin >> l1 >> l2;
		cin >> l1 >> l2;
		startNode = makeNodeReturnIndex("", l1);
	
		fr(i, 0, m) {
			string l1, l2, word;
			cin >> l1 >> l2 >> word;
			makeNodeAndEdges(word, l1, l2);
		}
		SSSP(startNode);
		int ansFinal = inf;
		fr (i, 0, languageWords[indexOflanguage[l2]].size()) {
			string word = languageWords[indexOflanguage[l2]][i];
			pss node = pss(word, l2);
			int ans = dist[indexOf[node]];
			if (ans == inf) {continue;}
			ans += word.size();
			ans/=2;
			ansFinal = (indexOf[node] == startNode) ? ansFinal : min(ans, ansFinal);
		}
		if (ansFinal == inf) {
			printf("impossivel\n");
		} else {
			printf("%d\n",ansFinal );
		}
	}
	return 0;
}