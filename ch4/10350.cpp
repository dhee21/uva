# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;

// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 210
#define inf (1<<30) - 1

int timeTaken [125][20][20], finalTimeTaken[125][20];

void initialize () {
	fr (i, 0, 125) fr (j, 0, 20) {
		finalTimeTaken[i][j] = inf;
	}
}

int main () {
	char name[200];
	while (scanf("%s", name) != EOF) {
		initialize();
		int m, n;
		ipInt(n);ipInt(m);
		fr (flr, 0, n - 1) fr (holeBelow, 0, m) fr (holeAbove, 0, m) {
			int k;
			ipInt(k);
			// printf("%d\n", k );
			timeTaken[flr][holeBelow][holeAbove] = k + 2;
		}
		fr (i, 0, m) finalTimeTaken[0][i] = 0;

		fr(roofFloor, 0, n - 1) fr (i, 0, m) fr(j, 0, m) {
			finalTimeTaken[roofFloor + 1][j] = min (finalTimeTaken[roofFloor + 1][j], finalTimeTaken[roofFloor][i] + timeTaken[roofFloor][i][j]);
		}
		int ans = inf;
		fr (i, 0, m) {
			ans = min (ans, finalTimeTaken[n - 1][i]);
		}
		printf("%s\n%d\n", name, ans);
	}
}