# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define MAX_NODES 3000

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
matrix adjMat(MAX_NODES);
// vector<edge> EdgeList(MAX_EDGES);
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}
int bfsParent[MAX_NODES], level[MAX_NODES];

bool isConnected(int node1, int node2, pii notConnectedNodes) {
	int notConnectedNode1 = notConnectedNodes.first, notConnectedNode2 = notConnectedNodes.second;
	return !((node1 == notConnectedNode1 && node2 == notConnectedNode2) || (node1 == notConnectedNode2 && node2 == notConnectedNode1));
}

int bfs(int root, pii notConnectedNodes) {
	memset(level, MAX_NODES, 0);
	fr (i, 0, MAX_NODES) {
		level[i] = -1;
		bfsParent[i] = -1;
	}
	// memset(bfsParent, MAX_NODES, 0);
	int maxLevelNode = root, maxLevel = 0;
	queue<int> q;
	bfsParent[root] = -1;
	level[root] = 0;
	q.push(root);
	while(!q.empty()) {
		int visitedNode = q.front(); q.pop();
		int neighbours = adjMat[visitedNode].size();
		fr (i, 0, neighbours) {
			int edgeToNode = adjMat[visitedNode][i].first;
			// printf("%d \n",edgeToNode );
			if (edgeToNode != bfsParent[visitedNode] && isConnected(edgeToNode, visitedNode, notConnectedNodes)) {
				bfsParent[edgeToNode] = visitedNode;
				level[edgeToNode] = level[visitedNode] + 1;
				maxLevelNode = (level[edgeToNode] > maxLevel) ? edgeToNode: maxLevelNode;
				q.push(edgeToNode);
			}
		}
	}
	return maxLevelNode;
}

void findLongestPath (int root, pii notConnectedNodes, vi &longestPathContainer) {
	int maxLevelNode = bfs(root, notConnectedNodes);
	int endPointOfDiameter = bfs(maxLevelNode, notConnectedNodes);
	for (int node = endPointOfDiameter; node != -1; node = bfsParent[node]) {
		longestPathContainer.push_back(node);
		// printf("%d \n", node);
	}
}

void removeEdge(int from, int to) {
	adjMat[from].pop_back();
	adjMat[to].pop_back();
}
void initialize () {
	fr (i, 0, adjMat.size()) adjMat[i].clear();
}

int main () {
	int tc;
	ipInt(tc);
	while (tc--) {
		initialize();

		int n;
		ipInt(n);
		fr(i, 0, n - 1) {
			int from, to;
			ipInt(from); ipInt(to);
			makeEdge(from - 1, to - 1, 0);
		}
		vi longestPath;
		findLongestPath(0, pii(-1, -1), longestPath);
		int minLongestPath = inf;
		pii nodesToRemove, nodesToAdd; 
		fr(i, 0, longestPath.size() - 1) {
			int notConnectedNode1 = longestPath[i], notConnectedNode2 = longestPath[i + 1];
			pii notConnectedNodes = pii(notConnectedNode1, notConnectedNode2);
			vi longestPath1, longestPath2;
			// longest path of tree with root notConnectedNode1
			findLongestPath(notConnectedNode1, notConnectedNodes, longestPath1);
			// longest path of tree with root notConnectedNode2
			findLongestPath(notConnectedNode2, notConnectedNodes, longestPath2);
			int mid1 = longestPath1.size() >> 1, mid2 = longestPath2.size() >> 1;
			makeEdge(longestPath1[mid1], longestPath2[mid2], 0);

			vi longestPathAfterCutt;
			findLongestPath(longestPath1[mid1], notConnectedNodes, longestPathAfterCutt);
			// printf("%lu\n",longestPathAfterCutt.size() - 1 );
			if (longestPathAfterCutt.size() - 1 < minLongestPath) {
				minLongestPath = longestPathAfterCutt.size() - 1;
				nodesToRemove = notConnectedNodes;
				nodesToAdd = pii(longestPath1[mid1], longestPath2[mid2]);
			}
			removeEdge(longestPath1[mid1], longestPath2[mid2]);
			longestPathAfterCutt.clear(); longestPath1.clear(); longestPath2.clear();
		}
		printf("%d\n",minLongestPath);
		printf("%d %d\n",nodesToRemove.first + 1, nodesToRemove.second + 1);
		printf("%d %d\n",nodesToAdd.first + 1, nodesToAdd.second + 1);
	}

	return 0;
}