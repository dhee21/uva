# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
// macros
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define MAX_NODES 210
#define inf (1<<30) - 1
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2
//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions
matrix adjMat(MAX_NODES);
vi topSort;
int visited[MAX_NODES];
void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
}
// problem specific varibles
int timeTaken[MAX_NODES], finalTimeTaken[MAX_NODES];
// problem specific varibles
void dfs (int node) {
	visited[node] = true;
	int neighbours = adjMat[node].size();
	fr(i, 0, neighbours) {
		int edgeToNode = adjMat[node][i].first;
		if (!visited[edgeToNode]) {
			dfs(edgeToNode);
		}
	}
	topSort.push_back(node);
}
void initialize () {
	fr (i, 0, MAX_NODES) {
		adjMat[i].clear();
	}
	memset(timeTaken, 0, sizeof timeTaken);
	memset(finalTimeTaken, 0, sizeof finalTimeTaken);
	memset(visited, 0, sizeof visited);
	topSort.clear();
}

int calculateTime () {
	int ans = 0;
	reverse(topSort.begin(), topSort.end());
	fr (i, 0, topSort.size()) {
		int node = topSort[i];
		int neighbours = adjMat[node].size();
		ans = max(ans, finalTimeTaken[node]);
		fr (v, 0, neighbours) {
			int edgeToNode = adjMat[node][v].first;
			finalTimeTaken[edgeToNode] = max(finalTimeTaken[edgeToNode], finalTimeTaken[node] + timeTaken[edgeToNode]);
			ans = max(ans, finalTimeTaken[edgeToNode]);
		}
	}
	return ans;
}

int main () {
	int t, timeToComplete;
	char inp[200], dependency[200], task;
	ipInt(t);
	scanf("\n");
	scanf("\n");
	while (t--) {
		initialize();
		while(fgets(inp, 200, stdin) != NULL) {
			if (inp[0] == '\n') break;
			dependency[0] = '\0';
			sscanf(inp, "%c %d %s", &task, &timeToComplete, dependency);
			// printf("%s %lu %s %lu\n", inp, strlen(inp), dependency, strlen(dependency) );
			// printf("%c %d %s %lu\n",task, timeToComplete, dependency, strlen(dependency) );
			timeTaken[task - 'A'] = finalTimeTaken[task - 'A'] = timeToComplete;
			fr (i, 0, strlen(dependency)) {
				makeEdge(dependency[i] - 'A', task - 'A', 0);
			}
		}
		fr (i, 0, 26) {
			if (!visited[i]) dfs(i);
		}
		int ans = calculateTime();
		printf("%d\n", ans );
		if (t) printf("\n");
	}
}