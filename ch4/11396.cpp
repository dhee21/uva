# include <iostream>
# include <vector>
# include <algorithm>
# include <string>
# include <queue>
# include <map>
# include <string.h>
# define MAX_NODES 310

using namespace std;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
matrix adjMat(MAX_NODES);
typedef queue<int> qu;

int n, m, visited[MAX_NODES], level[MAX_NODES];

bool isBipartite (int node) {
	qu q;
	visited[node] = true;
	level[node] = 0;
	q.push(node);
	while (!q.empty()) {
		int visitedNode = q.front(); q.pop();
		for (int i = 0; i < adjMat[visitedNode].size(); ++i) {
			pii edge = adjMat[visitedNode][i];
			int edgeToNode = edge.first, edgeWeight = edge.second;
			if (visited[edgeToNode] && level[edgeToNode] == level[visitedNode]) {
				return false;
			} else if (!visited[edgeToNode]) {
				visited[edgeToNode] = true;
				level[edgeToNode] = level[visitedNode] + 1;
				q.push(edgeToNode);
			}
		}
	}
	return true;
}

void makeEdge(int from, int to, int weight) {
	adjMat[from].push_back(make_pair(to, weight));
	adjMat[to].push_back(make_pair(from, weight));
}
void initialize () {
	for( int i = 0; i < MAX_NODES; ++i) {
		adjMat[i].clear();
	}
	memset(visited, 0, sizeof visited);
	memset(level, 0, sizeof level);
}
int main () {
	while (scanf("%d", &n) && n) {
		initialize();
		int from, to;
		while (scanf("%d %d", &from, &to) && (from || to)) {
			makeEdge(from - 1, to - 1, 0);
		}
		if (isBipartite(0)) {
			printf("YES\n");
		} else {
			printf("NO\n");
		}
	}
}