#include <stdio.h>
#include <iostream>
#include <math.h>
#include <algorithm>
using namespace std;

#define clear(size) for(int i = 0 ; i< size ; ++i){array[i] = 0;}

int array[10010] = {0}, sum=0;
int sigma( int k , int numOfVertices) {
	sum += array[k-1];
	return sum;
}
int minSumCal(int k , int numOfVertices) {
	int sumToReturn = 0;
	for ( int i = k+1 ; i<= numOfVertices ; ++i) {
			sumToReturn += min( k , array[i-1]) ;
	}
	return sumToReturn ;
} 

bool tellIfPossible(int numOfVertices, int sumVertices) {
	if(sumVertices & 1) {
		return 0 ;
	}
	// printf("%d\n", sumVertices );
	for ( int k = 1 ; k <= numOfVertices ; ++k ) {
		int sigmaD = sigma(k, numOfVertices);
		int minSum = minSumCal(k, numOfVertices);
		// printf("k = %d , sigmak = %d , minSum = %d\n", k, sigmaD, minSum );
		if(sigmaD > ((k*(k-1)) + minSum)) {
			return 0;
		}
	}
	return 1 ;
}
bool maxFunc(int left , int right) {
		return left > right ;
}
int main () {
	int numOfVertices, sumVertices;
	while(scanf("%d", &numOfVertices) && numOfVertices ) {
		sumVertices = 0 ;
		for(int i =0 ; i < numOfVertices ; ++i ) {
			scanf("%d" , &array[i]);
			sumVertices += array[i];
		}
		sort(array, array+numOfVertices, maxFunc);
		// for(int i =0 ; i< numOfVertices ;++i) {
		// 	printf("%d ", array[i] );
		// }
		// printf("\n");
		sum = 0;
		int possible = tellIfPossible(numOfVertices, sumVertices);
		possible ? printf("Possible\n") : printf("Not possible\n");
		clear(numOfVertices)
	}
	return 0;
}