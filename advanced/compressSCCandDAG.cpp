# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define MAX_NODES 1100
#define DFS_WHITE 0
#define DFS_BLACK 1
#define DFS_GRAY 2

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int m, n, visited[MAX_NODES], cutFromGraph[MAX_NODES],arr[MAX_NODES], smallestArrivalTime[MAX_NODES], sccNum[MAX_NODES]
		,timeCounter, numSCC;
int  adjacency[MAX_NODES][MAX_NODES], visitedSCC[MAX_NODES], nodesInSCC[MAX_NODES], dp[MAX_NODES];
matrix adjMat(MAX_NODES), adjMatSCC(MAX_NODES);
vi travelledNodes;
vi topSortScc;

void initialize () {
	fr (i, 0, MAX_NODES) { adjMat[i].clear(); adjMatSCC[i].clear(); }
	memset (visited, 0, sizeof visited);
	memset (visitedSCC, 0, sizeof visitedSCC);
	memset (cutFromGraph, 0, sizeof cutFromGraph);
	memset (adjacency, 0, sizeof adjacency);
	memset (dp, -1, sizeof dp);
	travelledNodes.clear();
	timeCounter = 0;
	numSCC = 0;
	topSortScc.clear();
}

void makeEdge (int from, int to) {
	adjMat[from].push_back(pii(to, 0));
}

void printSCC (int node) {
	int numNodes = 0;
	while (1) {
		int v = travelledNodes.back();
		travelledNodes.pop_back();
		sccNum[v] = numSCC;
		cutFromGraph[v] = 1;
		numNodes++;
		if (v == node) break;
	}
	nodesInSCC[numSCC] = numNodes;
	numSCC++;
}

void dfs(int node) {
	visited[node] = true;
	arr[node] = timeCounter++;
	travelledNodes.push_back(node);
	int minArrivalTime  = arr[node];
	int neighbours = adjMat[node].size();
	fr (i, 0, neighbours) {
		int edgeToNode = adjMat[node][i].first;
		if (visited[edgeToNode] && !cutFromGraph[edgeToNode]) {
			minArrivalTime = min (minArrivalTime, arr[edgeToNode]);
		} else if (visited[edgeToNode] == DFS_WHITE) {
			dfs(edgeToNode);
			minArrivalTime = min (minArrivalTime, smallestArrivalTime[edgeToNode]);
		}
	}
	if (minArrivalTime == arr[node]) printSCC(node);
	smallestArrivalTime[node] = minArrivalTime;
}

void makeEdgeSCCGraph (int from, int to) { adjMatSCC[from].push_back(pii(to, 0)); adjacency[from][to] = 1; }

void makeNewGraph () {
	fr (i, 0, n) fr (j, 0, adjMat[i].size()) {
		int edgeToNode = adjMat[i][j].first;
		int toNodeNum = sccNum[edgeToNode], fromNodeNum = sccNum[i];
		if (fromNodeNum != toNodeNum && adjacency[fromNodeNum][toNodeNum] == 0) makeEdgeSCCGraph(fromNodeNum, toNodeNum);
	}
}

void dfsSccGraph (int node) {
	visitedSCC[node] = true;
	int neighbours = adjMatSCC[node].size();
	fr (i, 0, neighbours) {
		int edgeToNode = adjMatSCC[node][i].first;
		if (!visitedSCC[edgeToNode]) dfsSccGraph(edgeToNode);
	}
	topSortScc.push_back(node);
}

int main () {
	int t;
	ipInt(t);
	while (t--) {
		initialize();
		ipInt(n); ipInt(m);
		fr (i, 0, m) {
			int from, to;
			ipInt(from); ipInt(to);
			makeEdge(from - 1, to - 1);
		}
		fr (i, 0, n) if (!visited[i]) dfs(i);
		makeNewGraph();
		// to get the topSortSCC
		fr (i, 0, numSCC) if (!visitedSCC[i]) dfsSccGraph(i);
		int maxCliqueSize = 0;
		for (int i = topSortScc.size() - 1; i >= 0 ; --i) {
			int node = topSortScc[i];
			if (dp[node] == -1) dp[node] = nodesInSCC[node];
			int neighbours = adjMatSCC[node].size();
			maxCliqueSize = max (maxCliqueSize, dp[node]);
			fr (j, 0, neighbours) {
				int edgeToNode = adjMatSCC[node][j].first;
				dp[edgeToNode] = max (dp[edgeToNode], dp[node] + nodesInSCC[edgeToNode]);
				maxCliqueSize = max (maxCliqueSize, dp[edgeToNode]);
			}
		}
		printf("%d\n",maxCliqueSize );
	}
	return 0;
}