# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define primesTill (1000000 - 1)
#define maxFs 50
#define valuesInRow 4
#define SIZE 4

// rev[i] is reverse direction of dir[i]
int board[16], emptyTile, dir[4][2] = {{-1, 0}, {0, -1}, {0, 1}, {1, 0}}, rev[4] = {3, 2, 1, 0};
char dirVal[4] = { 'U', 'L', 'R', 'D' };

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition
string steps;
int iterativeFs, newFs;
int f (int gS, int hS) { return gS + hS; }

int h () {
	int hSVal = 0;
	fr (i, 0, 16) {
		int tileVal = board[i], pos = tileVal - 1;
		hSVal += tileVal ? abs(i/SIZE - pos/SIZE) + abs(i%SIZE - pos%SIZE) : 0;
	}
	return hSVal;
}

// gS is g(s) i.e steps taken from root to reach here
bool solve (int gS, int fromDirection) {
	int hS = h();
	int fS = f(gS, hS);
	if (fS > iterativeFs) { newFs = min(newFs, fS); return false; }
	if (hS == 0) { printf("%s\n",steps.data()); return true; }
	bool solved = false;
	int r = emptyTile / SIZE, c = emptyTile % SIZE, oldPos = emptyTile;
	fr (i, 0, 4) {
		if (fromDirection < SIZE && fromDirection == rev[i]) continue;
		int row = r + dir[i][0], col = c + dir[i][1];
		int pos = row * SIZE + col;
		if (row >= 0 && col >= 0 && row < SIZE && col < SIZE) {
			board[oldPos] = board[pos]; board[pos] = 0; emptyTile = pos; 
			steps.push_back(dirVal[i]);
			solved = solve(gS + 1, i);
			steps.pop_back();
			board[pos] = board[oldPos]; board[oldPos] = 0; emptyTile = oldPos;
			if (solved) return true;
		}
	}
	return false;
}
bool solvable () {
	int inversions = 0, row;
	fr (i, 0, 16) if (board[i]) fr (j, i + 1, 16) {
		if ( board[j] && board[j] < board[i]) ++inversions;
	} else {
		row = i / SIZE + 1;
	}
	return ((inversions + row) & 1) == 0;
}
void IDAStar () {
	bool solved = false;
	// IDA*
	for (iterativeFs = h(); iterativeFs <= maxFs;) {
		steps.clear();
		newFs = inf;
		solved = solve(0 ,10) ;
		if (solved) break;
		iterativeFs = newFs;
	}
	//IDA*
}
int main () {
	int t; ipInt(t);
	while (t--) {
		fr (i, 0, 16) { ipInt(board[i]); if (board[i] == 0) emptyTile = i; }
		if (!solvable()) { printf("This puzzle is not solvable.\n"); continue; }
		IDAStar();
	}
	return 0;
}