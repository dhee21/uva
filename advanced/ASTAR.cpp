# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1
#define primesTill (1000000 - 1)

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition
int maxd;
map<vi, int> dist;

struct State {
	vi seq;  
    int d, h;
    bool operator < (const State& s) const {  
        return d + h > s.d + s.h;       //A* algorithm  
    }  
};
typedef pair<int, State> stateScore;


vi finalState;
int n, ans;

int h(vi state) {
	int count = 0;
	fr (i, 0, n - 1) if (state[i] + 1 != state[i + 1]) count++;
	return count;
}

vi addBefore (int addBeforeIndex, int st, int ed, vi &state) {
	vi stateToReturn;
	fr (i, 0, addBeforeIndex) stateToReturn.push_back(state[i]);
	fr (i, st, ed + 1) stateToReturn.push_back(state[i]);
	fr (i, addBeforeIndex, st) stateToReturn.push_back(state[i]);
	fr (i, ed + 1, state.size()) stateToReturn.push_back(state[i]);
	return stateToReturn;
}
vi addAfter (int addAfterIndex, int st, int ed, vi &state) {
	vi stateToReturn;
	fr (i, 0, st) stateToReturn.push_back(state[i]);
	fr (i, ed + 1, addAfterIndex + 1) stateToReturn.push_back(state[i]);
	fr (i, st, ed + 1) stateToReturn.push_back(state[i]);
	fr (i, addAfterIndex + 1, state.size()) stateToReturn.push_back(state[i]);
	return stateToReturn;
}

// it is like djakstra of states
void ASTAR (vi initState) {
	priority_queue<State> pq;
	State start;
	start.seq = initState;
	start.d = 0;
	start.h = h(initState);
	pq.push(start);
	while (!pq.empty()) {
		State u = pq.top(); pq.pop();
		if (dist.count(u.seq)) {
			if (u.d < dist[u.seq]) dist[u.seq] = u.d; else continue;
		} else dist[u.seq] = u.d;
		int hVal = h(u.seq);
		if (hVal == 0) { ans = u.d; return ;}
		 if(u.d > maxd) continue;        //Iterative Deepening  
        if(3*u.d + h(u.seq) > 3*maxd) continue; //Pruning  

		// edges
		fr (i, 0, n) fr (j, i , n) {
			for (int k = i - 1 ; k >= 0; --k) {
				vi beforeAddedState = addBefore(k, i, j, u.seq);
				State newS;  
                newS.seq = beforeAddedState;  
                newS.d = u.d + 1;  
                newS.h = h(newS.seq); 
				pq.push( newS );
			}
			fr (k, j + 1, n) {
				vi afterAddedState = addAfter(k, i, j, u.seq);
				State newS;  
                newS.seq = afterAddedState;  
                newS.d = u.d + 1;  
                newS.h = h(newS.seq); 
				pq.push(  newS  );
			}
		}
		//edges
	}
}

int main () {
	int caseNo = 0;
	while (ipInt(n) && n) {
		dist.clear(); finalState.clear();
		fr (i, 0, n) finalState.push_back(i + 1);
		vi initState;
		fr (i, 0, n){ int x; ipInt(x); initState.push_back(x); }
		// printf("%d\n",h(initState) );
        for(maxd = 1; maxd <= 9; maxd ++) {         //Iterative Deepening  
        	// printf("%d\n",maxd );
            dist.clear();  
            ans = -1;
            ASTAR(initState);
            if(ans != -1) break;  
        }  
		
		printf("Case %d: %d\n",++caseNo, ans );
	}
	return 0;
}