# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<28) - 1
#define primesTill (1000000 - 1)
#define KING 1
#define SIZE 4

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition
//IDAStar vars
int iterativeFs, newFs;
//IDAStar vars
int N, state[50], kingIndex, minJumps, indexToGo[4][4]= { { -3, -1, 4, -4 }, {1, 3, 4, -4}, { 1, -1, 4, -4},  {1, -1, 4, -4} };
int f(int gS, int hS) { return gS + hS; }

int adjMat[45][45];

void floydWarshall() {
	fr (i, 0, 40) fr (j, 0, 40) adjMat[i][j] = (i != j) ? inf : 0;
	fr (i, 0, 40) {
		int pos = i;
		fr (d, 0, 4) {
			int neighbour = pos + indexToGo[(pos + 1)%SIZE][d];
			if (neighbour < 0 || neighbour >= 40 ) continue;
			adjMat[i][neighbour] = 1;
		}
	}
	fr (k, 0, 40) fr (i, 0, 40) fr (j, 0, 40) if (adjMat[i][k] + adjMat[k][j] < adjMat[i][j]) adjMat[i][j] = adjMat[i][k] + adjMat[k][j];
}

int h () {
	int jumps = 0;
	fr (i, 0, N) {
		if (state[i] == KING) continue;
		int pos = i, jaquar = state[i] - 1;
		jumps += adjMat[pos][jaquar];
	}
	return jumps;
}

bool solve(int gS, int prevIndexOfKing) {
	int hS = h();
	int fS = f(gS , hS);
	if (fS > iterativeFs) { newFs = min(newFs, fS); return false ; }
	if (hS == 0) { minJumps = min(minJumps, gS); return true; }
	int oldPos = kingIndex;
	bool solved = false;
	fr (i, 0, 4) {
		int newPos = oldPos + indexToGo[(kingIndex + 1)%4][i];
		if (newPos == prevIndexOfKing || newPos < 0 || newPos >= N) continue;
		// new State
		state[oldPos] = state[newPos]; state[newPos] = KING;
		kingIndex = newPos;
		// newState
		solved |= solve(gS + 1, oldPos);
		// oldState
		state[newPos] = state[oldPos]; state[oldPos] = KING;
		kingIndex = oldPos;
		// oldSTate
	}
	return solved;
}

void IDAStar (int caseNo) {
	for (iterativeFs = h(); ;iterativeFs = newFs) {
			printf("%d\n",iterativeFs );
			newFs = inf;
			minJumps = inf;
			bool solved = solve(0, -1); if (solved) break;
		}
	printf("Set %d:\n",caseNo );
	printf("%d\n",minJumps);
}

int main () {
	int caseNo = 0;
	floydWarshall();
	while (ipInt(N) && N) {
		caseNo++;
		fr (i, 0, N) { ipInt(state[i]); if (state[i] == 1) kingIndex = i;}
		IDAStar(caseNo);
	}
	return 0;
}