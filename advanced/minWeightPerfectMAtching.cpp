# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <stack>
# include <bitset>
# include <limits>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
// #define inf (numeric_limits<double>::max())
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<vi> vvi;
//typedefinition

double dp[1<<17];
int finalState, n;
pii coor[20];

double dist(int i, int j) {
	return hypot(coor[i].first - coor[j].first, coor[i].second - coor[j].second);
}

double minDist(int state) {
	if (dp[state] != -1) return dp[state];
	if (state == finalState) return 0;
	int index ;
	fr (i, 0, 2*n) if ((state & (1<<i)) == 0) { index = i; break; }
	double val = (double)inf;
	fr (j, index + 1, 2*n) if ((state & (1 << j)) == 0) {
		int newState = state | (1 << index);
		newState |= 1 << j;
		val = min(val, dist(index, j) + minDist(newState));
	}
	return dp[state] = val;
}


int main () {
	int caseNo = 0;
	char name[100];
	while (ipInt(n) && n) {
		fr (i, 0, n << 1) scanf("%s %d %d",name, &coor[i].first, &coor[i].second);
		finalState = (1 << (2*n)) - 1;
		fr (i, 0, finalState + 1) dp[i] =  - 1;
		dp[finalState] = 0;
		double ans = minDist(0);
		printf("Case %d: %0.2lf\n", ++caseNo, ans);
	}
	return 0;
}