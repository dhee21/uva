# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_NODES 100010
#define MAX_TSP_NODES 12

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition
matrix adjMat(MAX_NODES);
int distVal[MAX_TSP_NODES][MAX_TSP_NODES];
// SSSP djakstra variables starts
int dist[MAX_NODES + 100], n, m;
priority_queue<pii> pq;
// SSSP djakstra variables ends
// tsp vars
int dp[1 << MAX_TSP_NODES][MAX_TSP_NODES];
// tsp vars
void makeEdge(int from, int to, int edgeWeight) {
	adjMat[from].push_back(pii(to, edgeWeight));
	adjMat[to].push_back(pii(from, edgeWeight));
}

void djakstra (int source) {
	priority_queue<pii> pq;
	fr (i, 0, MAX_NODES) dist[i] = inf;

	pq.push(pii(0, -source));
	dist[source] = 0;
	while (!pq.empty()) {
		pii minUpperbound = pq.top(); pq.pop();
		int node = -minUpperbound.second, minDist = -minUpperbound.first;
		if (dist[node] == minDist) {
			int neighbours = adjMat[node].size();
			// Relaxing dist of neighbours
			for (int i = 0 ; i < neighbours; ++i) {
				int edgeToNode = adjMat[node][i].first, edgeWeight = adjMat[node][i].second;
				int newDist = minDist + edgeWeight;
				if (newDist < dist[edgeToNode]) {
					dist[edgeToNode] = newDist;
					pq.push(pii(-newDist, -edgeToNode));
				}
			}
		}
	}
}

// tsp start
bool visited (int option, int i) {
	return option & (1 << i);
}
int visitI (int option, int i) {
	return option | (1 << i);
}
int add (int a, int b) {
	if (a == inf || b == inf) return inf;
	return a + b;
}

void tsp (int startNode, int totalNodes) {
	int startOption = 1 << startNode;
	int allSelectedOption = (1 << totalNodes) - 1;
	fr (lastAt, 0, totalNodes) dp[allSelectedOption][lastAt] = distVal[lastAt][startNode];

	for (int option = allSelectedOption - 1; option >= startOption; --option) fr (p, 0, totalNodes) {
		dp[option][p] = inf;
		if (visited(option , p)) fr (i, 0, totalNodes) if (!visited(option, i)) {
				int OAVI = visitI(option, i);
				dp[option][p] = min (dp[option][p], add(distVal[p][i] , dp[OAVI][i]));
			}
	}
}
//tsp end

void init () {
	fr (i, 0, MAX_NODES) adjMat[i].clear();
	memset (distVal, 0, sizeof distVal);
}
int main () {
	int t; ipInt(t);
	while (t--) {
		init();
		scanf("%d %d", &n, &m);
		fr (i, 0, m) {
			int from, to, len;
			scanf("%d %d %d", &from, &to, &len);
			makeEdge(from, to, len);
		}
		int shopNum, shops[12];
		ipInt(shopNum);
		fr (i, 0, shopNum) ipInt(shops[i]);
		djakstra(0);
		fr (j, 0, shopNum) distVal[0][j + 1] = dist[shops[j]];
		fr (i, 0, shopNum) { 
			djakstra(shops[i]);
			fr (j, 0, shopNum) if (i != j) distVal[i + 1][j + 1] = dist[shops[j]];
			distVal[i + 1][0] = dist[0];
		}
		tsp(0, shopNum + 1);
		printf("%d\n",dp[1][0]);
	}
	return 0;
}