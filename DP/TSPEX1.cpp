# include <iostream>
# include <stdio.h>
#include <math.h>
#include <utility>
using namespace std;
typedef pair<int, int> pii;
int x[10], y[10];
double dp[(1 << 8) + 100][10];
pair<int, int> prevState[(1 << 8) + 100][10];
#define inf 1 << 30

double dist (int i, int j) {
	return sqrt(pow(abs(x[i] - x[j]), 2) + pow(abs(y[i] - y[j]), 2));
}
int visitI(int node, int option) {
	option = option | (1 << node);
	return option;
}
bool visited(int option, int node) {
	option = option & (1 << node);
	return option != 0;
}
int main () {
	int n, Network = 0;;
	while (scanf("%d", &n) && n) {
		Network++;
		for (int i = 0; i < n; ++i) {
			scanf("%d %d", &x[i], &y[i]);
		}
		int allSelectedOption = (1 << n) - 1;
		for (int node = 0; node < n; ++node) {
			dp[allSelectedOption][node] = 0;
			prevState[allSelectedOption][node] = make_pair(-1, -1);
		}
		for (int option = allSelectedOption - 1; option >= 1; option--) {
			for (int pN = 0; pN < n; ++pN) {
				if (!visited(option, pN)) {
					dp[option][pN] = inf;
				} else {
					double minWire = inf;
					pii prev = make_pair(inf, inf);
					for (int i = 0; i < n; ++i) {
						if (!visited(option, i)) {
							// option after visiting i
							int OAVI = visitI(i, option);
							double distance = 16 + dist(pN, i) + dp[OAVI][i];
							// printf("%lf\n", distance );
							prev = (distance < minWire) ? make_pair(OAVI, i) : prev;
							minWire = min(minWire, distance);
						}
					}
					dp[option][pN] = minWire;
					prevState[option][pN] = prev;
				}
			}
		}
		int option = 1, pN = 0;
		pii minState = make_pair(inf, inf);
		double minWireDist = inf;
		while(option != (1 << n)) {
			minState = dp[option][pN] < minWireDist ? make_pair(option, pN) : minState;
			minWireDist = min(minWireDist, dp[option][pN]);
			// printf(" see %lf\n", minWireDist );
			++pN;
			option = option << 1;
		}
		pii prState = minState;
		printf("**********************************************************\n");
		printf("Network #%d\n",Network );
		while ( prevState[minState.first][minState.second].second != -1 ) {
			int nextCable = prevState[minState.first][minState.second].second;
			int presentCable = minState.second;
				printf("Cable requirement to connect (%d,%d) to (%d,%d) is %0.2f feet.\n",
				x[presentCable], y[presentCable], x[nextCable], y[nextCable], 16 + dist(presentCable, nextCable));
				// printf("op = %d i = %d\n", minState.first, minState.second);
				minState = prevState[minState.first][minState.second];
		}
		printf("Number of feet of cable required is %0.2lf.\n", minWireDist );
		// printf("**********************************************************\n");

	}
}