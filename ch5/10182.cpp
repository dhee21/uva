# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define PI 3.14159265
#define lastVal 100000
#define west 0
#define north 1
#define northEast 2
#define east 3
#define south 4
#define southWest 5

typedef pair<int, int> pii;
typedef vector<int> vi;
int lastVals[10000], lastIndex;

int findHexNum (int entry) {
	int start = 0, end = lastIndex, ans;
	while (start <= end) {
		int mid = (start + end) >> 1;
		if (lastVals[mid] >= entry) {
			ans = mid;
			end = mid - 1;
		} else {
			start = mid + 1;
		}
	}
	return ans;
}

pii moveNow (int direction ,pii from, int stepToMove) {
	if (direction == west) {
		return pii(from.first - stepToMove, from.second);
	} else if (direction == north) {
		return pii (from.first, from.second - stepToMove);
	} else if (direction == northEast) {
		return pii (from.first + stepToMove, from.second - stepToMove );
	} else if (direction == east) {
		return pii (from.first + stepToMove, from.second);
	} else if (direction == south) {
		return pii (from.first, from.second +  stepToMove);
	} else if (direction == southWest) {
		return pii (from.first - stepToMove, from.second + stepToMove);
	}
	return from;
}

pii takeSteps( pii from, int noOfSteps, int hexNum) {
	int southSteps = 1;
	from = moveNow(south, from, southSteps);
	noOfSteps -= southSteps;
	if (!noOfSteps) return from;

	int southWestSteps = hexNum - 1;
	southWestSteps = min(southWestSteps, noOfSteps);
	from = moveNow(southWest, from, southWestSteps);
	noOfSteps -= southWestSteps;
	if (!noOfSteps) return from;

	fr (i, 0, 5) {
		int stepToMove = hexNum;
		stepToMove = min(stepToMove, noOfSteps);
		from = moveNow(i, from, stepToMove);
		noOfSteps -= stepToMove;
		if (!noOfSteps) return from;

	}

	return from;
}

int main () {
		lastVals[0] = 1;
		lastVals[1] = 7;
		for (int i = 2; i < 10000; ++i) {
			int diff = lastVals[i - 1] - lastVals[i - 2];
			lastVals[i] = lastVals[i - 1] + (6 * i);
			if (lastVals[i] > lastVal) {
				lastIndex = i;
				break;
			}
		}
		int number;
		while(ipInt(number) != EOF) {
			int hexNum = findHexNum(number);
			if (hexNum == 0) {
				printf("0 0\n");
				continue;
			} else if (number == lastVals[hexNum]) {
				printf("%d %d\n",hexNum, 0 ); continue;
			}
			int offset = number - lastVals[hexNum - 1];
			pii ans = takeSteps(pii(hexNum - 1, 0), offset, hexNum);
			printf("%d %d\n",ans.first, ans.second );
		}
		return 0;
}