# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

int main () {
	int t, caseNo = 0;
	ipInt(t);
	while (t--) {
		caseNo++;
		double d, v, u;
		scanf("%lf %lf %lf", &d, &v, &u);
		if (v >= u || u == 0 || v == 0) {
			printf("Case %d: can't determine\n", caseNo);
		} else {
			double val = sqrt(pow(u, 2.0) - pow(v, 2.0));
			double ans = ( d * (u - val) ) / (u * val);
			ans = floor(((ans * 1000) + 0.5 + 1e-9)) / 1000; 
			printf("Case %d: %0.3lf\n", caseNo , ans );
		}
	}
	return 0;
}