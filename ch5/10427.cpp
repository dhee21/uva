# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define PI 3.14159265
typedef pair<int, int> pii;
typedef long long int lli;
int l[15];
int ten[] = {1,10,100,1000,10000,100000,1000000,10000000};

int digit(int number, int index) {
	fr (i, 0, index) {
		number /= 10;
	}
	return number % 10;
}
int main () {
	l[1] = 9;
	fr (i, 2, 9) {
		l[i] = ten[i - 1] * 9 * i + l[i - 1];
	}
	int n;
	while (ipInt(n) != EOF) {
		if (n <= 9) {
			printf("%d\n", n);
		} else {
			int len;
			fr (i, 1, 9) {
				if (n <= l[i]) {
					len = i; break;
				}
			}
			int offset = n - l[len - 1];
			int number = (offset / len) + (ten[len - 1] - 1);
			if ((offset % len) == 0) {
				printf("%d\n", digit(number, 0));
			} else {
				printf("%d\n", digit(number + 1, len - (offset % len)) );
			}
		}
	}	
	return 0;
}