# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int main () {
	int cows, cars, shows;
	while (scanf("%d %d %d", &cows, &cars, &shows) != EOF) {
		double cowToCar, carToCar;
		cowToCar = (double) (cows * cars) / (double)( (cows + cars) * (cars + cows - 1 - shows) );
		carToCar = (double) (cars * (cars - 1)) / (double) ( (cows + cars) * (cars - 1 + cows - shows) );
		double ans = cowToCar + carToCar;
		printf("%.5lf\n", ans);
	}
	return 0;
}