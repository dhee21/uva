# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
# include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
typedef long long int lli;
typedef pair<lli, int> pii;
typedef pair<char, char> pcc;
int main () {
	char inp[40];
	while (scanf("%s",inp) && !(inp[0] == '0' && inp[1] == '\0')) {
		int len = strlen(inp);
		int power = 2, ans = 0;
		for (int i = len - 1; i >= 0; --i) {
			ans += (power - 1) * (inp[i] - '0');
			power <<= 1;
		}
		printf("%d\n",ans);
	}
	return 0;
 }