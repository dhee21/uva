# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i <= ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef unsigned long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

lli dp[26][155];

void calculateWays() {
	memset(dp, 0, sizeof dp);
	fr (val, 1, 6) dp[1][val] = 6 - (val - 1);
	dp[1][0] = dp[1][1];
	fr (i, 2, 24){
		fr (val, i, 6 * i) fr (diceVal, 1, 6) {
			dp[i][val] += (val >= diceVal) ? dp[i - 1][val - diceVal] : dp[i - 1][0];
		}
		fr (val, 0, i - 1) {
			dp[i][val] = dp[i][i];
		}
	}
}

lli gcd (lli a, lli b) { return (b == 0) ? a : gcd(b, a % b);}

int main () {
	lli n, x;
	calculateWays();
	// fr (i, 0, 6) printf("%lld " , dp[1][i] ); printf("\n");
	while (scanf("%lld %lld", &n, &x) && (n || x)) {
		lli val1 = dp[n][x], val2 = (lli)pow(6, n);
		lli gcdVal = gcd(val1, val2);
		val1 /= gcdVal; val2 /= gcdVal;
		if (val1 == 0) {
			printf("0\n"); continue;
		} else if (val1 == val2) {
			printf("%lld\n",val1 ); continue;
		}
		printf("%lld/%lld\n", val1 , val2  );
	}
	return 0;
}