# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i <= ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define forAllEmptyPositions fr(i, 0, 3) fr(j, 0, 3) if (board[i][j] == empty)
#define empty '.'

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;

int main () {
	string s = "ABCDFE";
	transform(s.begin(), s.end(), s.begin(), ::tolower);
	cout << s;
	return 0;
}