# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
# include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
typedef long long int lli;
typedef pair<lli, int> pii;
typedef pair<char, char> pcc;
map<int, char> charValue;
map<char, int> value;

int stringToNum (string number) {
    int num = 0;
    for (int i = 0; i < number.size() ; ++i) {
      num *= 4;
      num += value[number[i]];
    }
    return num;
}

int operate (int num2, char operation, int num1) {
  if (operation == 'N') return num2;
  if (operation == 'L') return num2 * 4;
  if (operation == 'R') return num2 / 4;
  return num1 + num2;
}

int main () {
  int t;
  charValue[0] = 'V'; value['V'] = 0;
  charValue[1] = 'U'; value['U'] = 1;
  charValue[2] = 'C'; value['C'] = 2;
  charValue[3] = 'D'; value['D'] = 3 ;
  printf("COWCULATIONS OUTPUT\n");
  ipInt(t);
  while (t--) {
    string num1, num2, result;
    char op1, op2, op3;
    cin >> num1; cin >> num2; cin >> op1; cin >> op2; cin >> op3; cin >> result;
    int num1Val = stringToNum(num1);
    int num2Val = stringToNum(num2);

    num2Val = operate(num2Val, op1, num1Val);
    num2Val = operate(num2Val, op2, num1Val);
    num2Val = operate(num2Val, op3, num1Val);
    (num2Val == stringToNum(result)) ? printf("YES\n") : printf("NO\n");;
  }
  printf("END OF OUTPUT\n");
  return 0;
}