# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
# include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
typedef long long int lli;
typedef pair<lli, int> pii;
lli constants[10010];

int divide(int orderOfDivisor, int orderOfDivident) {
	while (orderOfDivisor <= orderOfDivident) {
		lli highestPowerConst = constants[orderOfDivident];
		pii polToMultiply = pii(highestPowerConst, orderOfDivident - orderOfDivisor);
		constants[polToMultiply.second] -= polToMultiply.first;
		constants[orderOfDivident] = 0;
		orderOfDivident--;
	}
	return orderOfDivident;
}

int main () {
	int n, k;
	while(scanf("%d %d", &n, &k) && !(n == -1 && k == -1)) {
		fr (i, 0, n + 1) scanf("%lld", &constants[i]);
		int orderOfRemainder = divide(k, n);
		bool isRemainderZero = true;
		fr (i, 0, orderOfRemainder + 1) if (constants[i]) isRemainderZero = false;
		if (isRemainderZero) {
			printf("0\n"); continue;
		}
		fr (i, 0, orderOfRemainder + 1) {
			if (!i) printf("%lld", constants[i]);
			else printf(" %lld",constants[i] );
		}
		printf("\n");
	}
	return 0;
}