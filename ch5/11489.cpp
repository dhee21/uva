# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition


void printResult(int caseNo, bool isWin) {
	if (isWin) {
		printf("Case %d: S\n",caseNo);
	} else {
		printf("Case %d: T\n",caseNo);
	}
}

int main () {
	int t, caseNo = 0;
	int freq[10];
	char number[1010];
	ipInt(t); getchar();
	while (t--) {
		memset(freq, 0, sizeof freq);
		caseNo++;

		scanf("%s", number);
		int sum = 0;
		fr(i, 0, strlen(number)) {
			freq[number[i] - '0']++;
			sum += number[i] - '0';
		}
		int sumOf3And9Freq = freq[3] + freq[9] + freq[6];
		// cout << sum % 3<< endl;
		switch (sum % 3) {
			case 0:
				{
					(sumOf3And9Freq & 1) ? printResult(caseNo, true) : printResult(caseNo, false);
				}
				break;
			case 1:
				{
					// printf("%d %d %d %d\n",freq[1], freq[4], freq[7], sumOf3And9Freq);
					((freq[1] || freq[4] || freq[7]) && ( (sumOf3And9Freq & 1) == 0 ) ) ? printResult(caseNo, true) : printResult(caseNo, false);
				}
				break;
			default:
			{
				// printf("%d %d %d %d\n",freq[2], freq[5], freq[8], sumOf3And9Freq);
				((freq[2] || freq[5] || freq[8]) && ( (sumOf3And9Freq & 1) == 0 ) ) ? printResult(caseNo, true) : printResult(caseNo, false);
			}
		}
	}
	return 0;
}