# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define PI 3.14159265
#define lastVal 10000

int sumFirstTerms(int n) {
	return (n * (n + 1)) >> 1;
}

int findDiag (int n) {
	int right = lastVal , left = 1, ans;
	while (right >= left) {
		int mid = (left + right) >> 1;
		int sum = sumFirstTerms(mid);
		if (sum <= n) {
			ans = mid;
			left = mid + 1;
		} else {
			right = mid - 1;
		}
	}
	if (n == sumFirstTerms(ans)) return ans;
	return ans + 1;
}

int main () {
	int entry;
	while(ipInt(entry) != EOF) {
		int diagonalNumber = findDiag(entry);
		int prevEntry = sumFirstTerms(diagonalNumber - 1);
		int offset = entry - prevEntry;
		if (diagonalNumber & 1) {
			printf("TERM %d IS %d/%d\n",entry, diagonalNumber + 1 - offset, offset );
		} else {
			printf("TERM %d IS %d/%d\n",entry, offset, diagonalNumber + 1 - offset );
		}
	}
	return 0;
}