# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

int main () {
	int r, n, caseNo = 0;
	while(scanf("%d %d", &r, &n) && (n || r)) {
		caseNo++;
		bool ansFound = false;
		int ans = -1;
		fr (choices, 1, 28) {
			if (n * choices >= r) {
				ans = choices - 1; ansFound = 1; break;
			}
		}
		if (!ansFound) {
			printf("Case %d: impossible\n", caseNo);
		} else {
			printf("Case %d: %d\n",caseNo, ans);
		}
	}
	return 0;
}