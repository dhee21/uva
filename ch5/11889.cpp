# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 4000

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// primefactorization and primeSieve variables
bitset<(primesTill) + 1> probablePrime; 
vi primeList;
// primefactorization and primeSieve variables


void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	int goTill = (int)sqrt(primesTill);
	for (int i = 2; i <= goTill ; ++i) {
		if (probablePrime.test(i) == 1) {
			primeList.push_back(i);
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime.reset(j);
			}
		}
	}
	for (int i = goTill + 1; i <= primesTill; ++i) {
		if (probablePrime.test(i) == 1) primeList.push_back(i);
	}
}

void primeFactorization (lli N, vi &factors, vi &exponents) {
	int PFIndex = 0;
	lli PF = primeList[0];
	factors.clear(); exponents.clear();
	while (PF * PF <= N && N != 1 && PFIndex < primeList.size()) {
		if (N % PF == 0) {
			int expVal = 0;
			while (N % PF == 0) {
				N /= PF;  expVal ++;
			}
			factors.push_back(PF);
			exponents.push_back(expVal);
		}
		PF = primeList[++PFIndex];
	}
	if (N != 1) {
		factors.push_back(N); exponents.push_back(1);
	}
}

int main () {
	primeSieve();
	int t;
	ipInt(t);
	while (t--) {
		int a, c;
		scanf("%d %d", &a, &c);
		if (a == 1) { printf("%d\n",c); continue; }
		if (c % a != 0)	{ printf("NO SOLUTION\n"); continue; }
		int b = 1;
		vi factorsA, exponentsA, factorsC, exponentsC;
		primeFactorization(c, factorsC, exponentsC);
		primeFactorization(a, factorsA, exponentsA);
		int ptrA = 0, ptrC = 0, ans = 1;
		while (ptrA < factorsA.size()) {
			if (factorsA[ptrA] == factorsC[ptrC]) {
				if (exponentsA[ptrA] < exponentsC[ptrC]) {
					ans *= (int) pow(factorsC[ptrC], exponentsC[ptrC]);	
				}
				ptrA ++; ptrC++;
			} else if (factorsA[ptrA] > factorsC[ptrC]) {
				ans *= pow(factorsC[ptrC], exponentsC[ptrC]);
				ptrC++;
			}
		}
		for (;ptrC < factorsC.size(); ++ptrC) {
			ans *= pow(factorsC[ptrC], exponentsC[ptrC]);
		}
		printf("%d\n",ans);
	}
	return 0;
}