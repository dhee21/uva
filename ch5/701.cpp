# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
#include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_VAL 2147483648

typedef long long int lli;

int findLen(lli n) {
	int len = 0;
	while (n) {
		n /= 10; len++;
	}
	return len;
}

int main () {
	lli n;
	while(scanf("%lld", &n) != EOF) {
		int len = findLen(n);
		int ans;
		int prevLen = len;
		len ++;
		for (; ; len++) {
			int len1 = log2(n) + len * log2(10);
			int len2 = log2(n + 1) + len * log2(10);
			if (len2 != len1) {
				ans = len1 + 1;
				printf("%d\n",ans );
				break;
			}
		}
	}
	return 0;
}