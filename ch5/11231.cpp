# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define PI 3.14159265
typedef pair<int, int> pii;
typedef long long int lli;

int main () {
	lli m, n, c;
	while (scanf("%lld %lld %lld", &n, &m, &c) && (n || m ||c)) {
		if (c == 0) {
			lli ans1 = ((m - 7) / 2) * ((n - 6) / 2);
			lli ans2 = ((m - 6) / 2) * ((n - 7) / 2);
			lli ans = ans1 + ans2;
			printf("%lld\n", ans );
		} else {
			lli ans1 = ((m - 6) / 2) * ((n - 6) / 2);
			lli ans2 = ((n - 7) / 2) * ((m - 7) / 2);
			lli ans = ans1 + ans2;
			printf("%lld\n", ans );
		}
	}
	return 0;
}