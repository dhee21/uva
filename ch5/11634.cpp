# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition


int f(int x) {
	x *= x;
	string num = to_string(x);
	while (num.size() != 8) {
		num = "0" + num;
	}
	num = num.substr(2, 4);
	return stoi(num);
}

pii floydCycleFinding (int X0) {
	// find Klambda
	int tort = f(X0), hare = f(f(X0));
	while (tort != hare) {tort = f(tort); hare = f(f(hare)); }
	// find mu
	int mu = 0; hare = X0;
	while (tort != hare) { tort = f(tort); hare = f(hare); mu ++; }
	// findlambda
	int lambda = 1; tort = hare; hare = f(hare);
	while (tort != hare) { hare = f(hare); lambda++; }

	return pii(mu, lambda);
}

int main () {
	int caseNo = 0;
	int X0;
	while (scanf("%d", &X0) && X0) {
		pii ans = floydCycleFinding(X0);
		printf("%d\n", ans.first + ans.second);
	}
	return 0;
}