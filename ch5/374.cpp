# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

lli findPow (lli base, lli power, lli m) {
	if (power == 0) return 1 % m;
	if (power == 1) return base % m;
	lli halfPower = findPow(base, power / 2, m);
	lli value = (halfPower * halfPower) % m;
	if (power & 1) {
		value = (value * (base % m)) % m;
	}
	if (value < 0) return m - value;
	return value;
}

int main () {
	lli b, p, m;
	while (scanf("%lld %lld %lld", &b, &p, &m) != EOF) {
		printf("%lld\n",findPow(b, p, m));
	}
	return 0;
}