# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

bool isPossible(int n, int c) {
	fr(i, 0, n) {
		if (c % n != 1) return 0;
		c -= (c / n) + 1;
	}
	return c % n == 0;
}

int main () {
	int c;
	while (ipInt(c) && c != -1) {
		int n = int(sqrt(c) + 1);
		int ans = -1;
		fr (i, 2, n + 1) {
			bool possible = isPossible(i, c);
			ans = possible ? i : ans;
		}
		if (ans != -1) {
			printf("%d coconuts, %d people and 1 monkey\n",c, ans );
		} else {
			printf("%d coconuts, no solution\n",c);
		}
	}
	return 0;
}