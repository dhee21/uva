# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define maxVal 33000

typedef pair <int, int> pii;
map <double, pii> fairy;
vector<pii> seq;

int gcd (int divisor, int divident) {
	while (divident % divisor) {
		int rem = divident % divisor;
		divident = divisor;
		divisor = rem;
	}
	return divisor;
}

int main () {
	int n, k;
	while (scanf("%d %d", &n, &k) != EOF) {
		fr(den , 2, n + 1) fr(num, 1, den) {
			if (gcd(num, den) == 1) {
				fairy[(double)num / (double)den] = pii(num, den);
			}
		}
		map <double, pii> :: iterator it;
		fairy[1] = pii(1, 1);
		pii ansPii;
		int index = 0;
		for (it = fairy.begin(); it != fairy.end(); ++it) {
			index++;
			if (index == k) {
				ansPii = it->second;
			}
		}
		printf("%d/%d\n", ansPii.first, ansPii.second);
		fairy.clear();
	}
	return 0;
}