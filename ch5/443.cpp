# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define lastVal 2000000002

typedef vector<int> vi;
typedef long long int lli;
vi humbleNumbers;

string giveString(int x) {
	int num = x;
	int a = num % 10; num /= 10; 
	int b = num % 10; a = b * 10 + a;
	// printf("sees  %d\n",a );
	if (a > 10 && a <= 20) return "th";
	int lastDigit = x % 10;
	// printf("see %d\n",lastDigit );
	if (lastDigit == 1) return "st";
	if (lastDigit == 2) return "nd";
	if (lastDigit == 3) return "rd";
	return "th"; 
}

int main () {
	bool flag = 0;
	for (lli a = 1; a < lastVal; a *= 7) for (lli b = 1; b * a < lastVal; b *= 5) for(lli c = 1; c * b * a < lastVal; c *= 3)
		for (lli d = 1; d * c * b * a < lastVal; d *= 2) {
			humbleNumbers.push_back(a * b * c * d);
		}
	// printf("a\n");
	// for (lli a = 0; a < 4; ++a) {}
	sort(humbleNumbers.begin(), humbleNumbers.end());
	int x;
	while (ipInt(x) && x) {
		int ans = humbleNumbers[x - 1];
		string toPut = giveString(x);
		printf("The %d%s humble number is %d.\n",x, toPut.c_str(), ans);
	}
	return 0;
}