# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 1000

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int main () {
	int t;
	ipInt(t);
	while (t--) {
		int u, l;
		ipInt(l); ipInt(u);
		int divNumAns = 0, number;
		fr (i, l, u + 1) {
			int numDivLessThanRootN = 0;
			for (int j = 1; j <= sqrt(i); ++j) {
				numDivLessThanRootN += (i % j) == 0;
			}
			int numDiv;
			int temp = sqrt(i);
			if (temp == sqrt(i)) {
				numDiv = (2 * numDivLessThanRootN) - 1;
			} else {
				numDiv = (2 * numDivLessThanRootN);
			}
			if(numDiv > divNumAns) {
				divNumAns = numDiv;
				number = i;
			}
		}
		printf("Between %d and %d, %d has a maximum of %d divisors.\n",l, u, number, divNumAns);
	}
	return 0;
}