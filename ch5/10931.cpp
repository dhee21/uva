# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
# include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
typedef long long int lli;
typedef pair<lli, int> pii;
typedef pair<char, char> pcc;
int parity;

string printBinary(int input) {
	string ans;
	while (input) {
		parity += input & 1 ;
		ans.push_back('0' + (input & 1));
		input >>= 1;
	}
	return ans;
}

int main () {
	int input;
	while (ipInt(input) && input) {
		printf("The parity of ");
		parity = 0;
		string number = printBinary(input);
		reverse(number.begin(), number.end());
		printf("%s",number.data());
		printf(" is %d (mod 2).\n", parity );
	}
	return 0;
}