# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 1000000

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions

bitset<primesTill + 1> probablePrime; 
vi primeList;
vlli almostPrimes;

void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	lli lastVal = 1000000000000;	
	int goTill = (int)sqrt(primesTill);
	for (int i = 2; i <= goTill ; ++i) {
		if (probablePrime.test(i) == 1) {
			primeList.push_back(i);
			for (lli k = (lli)i * (lli)i; k < lastVal; k *= (lli) i) {
				almostPrimes.push_back(k);
			}
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime.reset(j);
			}
		}
	}
	for (int i = goTill + 1; i <= primesTill; ++i) {
		if (probablePrime.test(i) == 1){
			primeList.push_back(i);
			for (lli k = (lli)i * (lli)i; k < lastVal; k *= (lli) i) {
				almostPrimes.push_back(k);
			}
		} 
	}
	sort(almostPrimes.begin(), almostPrimes.end());
}

int main () {
	int t;
	primeSieve();
	ipInt(t);
	while (t--) {
		lli a, b;
		scanf("%lld %lld", &a, &b);
		int aIndex = lower_bound(almostPrimes.begin(), almostPrimes.end(), a) - almostPrimes.begin();
		int bIndex = upper_bound(almostPrimes.begin(), almostPrimes.end(), b) - almostPrimes.begin();
		printf("%d\n", bIndex - aIndex);
	}
	return 0;
}