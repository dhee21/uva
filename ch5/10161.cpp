# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define PI 3.14159265
typedef pair<int, int> pii;

int main () {
	int x;
	while (ipInt(x) && x) {
		pii coor;
		int val = floor(sqrt(x));
		if (val * val == x) {
			if (x & 1) coor = pii(val, 1);
			else coor = pii(1, val);
		} else {
			int prevBoxLastVal = val * val;
			if (prevBoxLastVal & 1) {
				int toChechVal = prevBoxLastVal + (val + 1);
				if (x <= toChechVal) coor = pii(val + 1, x - prevBoxLastVal);
				else coor = pii((val + 1) - (x - toChechVal), val + 1);
			} else {
				int toChechVal = prevBoxLastVal + (val + 1);
				if (x <= toChechVal) coor = pii(x - prevBoxLastVal, val + 1);
				else coor = pii(val + 1, (val + 1) - (x - toChechVal));
			}
		}
		printf("%d %d\n",coor.second, coor.first );
	}
	return 0;
}