# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef unsigned long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

bitset<13> hats;

lli ways[13], fact[13], numWays;

void findWays (lli person, lli totalPersons) {
	// cout << person << " "  << totalPersons<< endl;
	if (person == totalPersons) {
		numWays ++;
		return;
	}
	for (int i = 0; i < totalPersons ; ++i) {
		if (hats.test(i) == 0 && i != person) {
			hats.set(i);
			findWays(person + 1, totalPersons);
			hats.reset(i);
		}
	}
}


int main () {
	fact[1] = 1;
	ways[2] = 1;
	ways[3] = 2;
	fact[1] = 1;
	fr (i, 2, 13) fact[i] = fact[i - 1] * i;
	for (lli i = 4; i <= 12 ; ++i) {
		ways[i] = (i - 1) * (ways[i - 1] + ways[i - 2]);
	}
	int t;
	ipInt(t);
	while (t--) {
		int n;
		ipInt(n);
		printf("%lld/%lld\n",ways[n] , fact[n] );
	}
	return 0;
}