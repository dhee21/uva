# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
#include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
typedef long long int lli;
int constants[100000];

lli findPolVal(int x, int order) {
	lli ans = 0, powerOfX = 1;
	for (int power = 0; power <= order; ++power) {
		int constantIndex = order - power;
		lli constant = constants[constantIndex];
		ans += powerOfX * constant;
		powerOfX *= x;
	}
	return ans;
}

int main () {
	char inp[200];
	while (fgets(inp, 200, stdin) != NULL) {
		char *ptr = strtok(inp, " ");
		int order = 0;
		while (ptr != NULL) {
			int c = atoi(ptr);
			constants[order ++] = c;
			ptr = strtok(NULL, " ");
		}
		order--;
		fgets(inp, 200, stdin);
		ptr = strtok(inp, " ");
		int num = 0;
		while (ptr != NULL) {
			if (num ++) printf(" ");
			int x = atoi(ptr);
			printf("%lld", findPolVal(x, order));
			ptr = strtok(NULL, " ");
		}
		printf("\n");
	}
	return 0;
}