# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i <= ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int main () {
	int t;
	ipInt(t);
	while (t--) {
		int m, n, r, c;
		scanf("%d %d %d %d", &m, &n, &r, &c);
		int leftGameStones = c, upGameStones = r, belowGameStones = m - 1 - r, rightGameStones = n - 1 - c;
		// cout<< "right " << rightGameStones << endl;
		int value = leftGameStones ^ upGameStones ^ belowGameStones ^ rightGameStones;
		// printf(" %d Xor %d Xor %d Xor %d = %d\n", leftGameStones, upGameStones, belowGameStones, rightGameStones, value ); 
		if (leftGameStones ^ upGameStones ^ belowGameStones ^ rightGameStones) {
			printf("Gretel\n");
		} else {
			printf("Hansel\n");
		}
	}
	return 0;
}