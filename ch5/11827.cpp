# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int gcd (int a, int b) { return b == 0 ? a : gcd(b, a % b); }

int main () {
	int t;
	int a;
	char waste;
	ipInt(t);
	getchar();
	while (t--) {
		vi input;
		string s;
		getline (cin, s);
		stringstream ss(s);
		int num;
		while (ss >> num) {
			input.push_back(num);
		}
		int ans = -1;
		fr (i, 0, input.size()) fr(j, i + 1, input.size()) {
			ans = max (ans , gcd(input[i], input[j]));
		}
		printf("%d\n",ans );
	}
	return 0;
}