# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

int subsets[80];

int main () {
	subsets[1] = 1; subsets[2] = 2; subsets[3] = 2;
	fr(i, 4, 77) {
		subsets[i] = subsets[i - 2] + subsets[i - 3];
	}
	int n;
	while(ipInt(n) != EOF) {
		printf("%d\n", subsets[n] );
	}
	return 0;
}