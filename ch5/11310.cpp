# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

long int ways[50];

int main () {
	ways[0] = 1; ways[1] = 1; ways[2] = 5;
	fr(i, 3, 41) {
		ways[i] = ways[i - 1] + (4 * ways[i - 2]) + (2 * ways[i - 3]);
	}
	int t;
	ipInt(t);
	while (t--) {
		int n;
		ipInt(n);
		printf("%ld\n", ways[n]);
	}
	return 0;
}