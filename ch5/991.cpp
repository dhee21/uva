# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

int cat[12];

void calculateCatalan () {
	cat[0] = 1;
	fr (n, 0, 10) {
		cat[n + 1] =  ( 2 * ((2*n) + 1) * cat[n] ) / (n + 2);
	}
}

int main () {
	int n, caseNo = 0;
	calculateCatalan();
	while (ipInt(n) != EOF) {
		if (caseNo++) printf("\n");
		printf("%d\n", cat[n]);
	}
	return 0;
}