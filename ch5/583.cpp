# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 1 << 16

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// primefactorization and primeSieve variables
bitset<(primesTill) + 1> probablePrime; 
vi primeList, factors, exponents;
// primefactorization and primeSieve variables


void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	int goTill = (int)sqrt(primesTill);
	for (int i = 2; i <= goTill ; ++i) {
		if (probablePrime.test(i) == 1) {
			primeList.push_back(i);
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime.reset(j);
			}
		}
	}
	for (int i = goTill + 1; i <= primesTill; ++i) {
		if (probablePrime.test(i) == 1) primeList.push_back(i);
	}
}

void primeFactorization (lli N) {
	bool firstPF = true;
	int PFIndex = 0;
	lli PF = (lli) primeList[0];
	factors.clear(); exponents.clear();
	while (PF * PF <= N && N != 1 && PFIndex < primeList.size()) {
		if (N % PF == 0) {
			int expVal = 0;
			while (N % PF == 0) {
				N /= PF;  expVal ++;
				if (firstPF) {
					printf("%lld",PF); firstPF = false;
				} else {
					printf(" x %lld",PF);
				}
			}
			factors.push_back(PF);
			exponents.push_back(expVal);
		}
		PF = primeList[++PFIndex];
	}
	if (N != 1) {
		if (firstPF) {
			printf("%lld",N);
		} else {
			printf(" x %lld",N);
		}
		factors.push_back(N); exponents.push_back(1);
	}
}

int main () {
	lli num;
	primeSieve();
	while (scanf("%lld", &num) && num) {
		printf("%lld = ", num );
		if (num < 0) printf("-1 x ");
		primeFactorization(abs(num));
		printf("\n");
	}
	return 0;
}