# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef long long int lli;
lli values[1000010];
int main () {
	lli incrementIncreasedBy = 0, inc = 0;
	values[3] = 0;
	fr (n, 4, 1000001) {
		if (n & 1) {
			inc += incrementIncreasedBy;
			values[n] = values[n-1] + inc;
		} else {
			incrementIncreasedBy ++;
			inc += incrementIncreasedBy;
			values[n] = values[n-1] + inc;
		}
	}
	int n;
	while(ipInt(n) && n >= 3) {
		printf("%lld\n", values[n]);
	}
	return 0;
}