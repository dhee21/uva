# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// linear Diophantine variables
lli x, y, d, X0, Y0, XAns, YAns;
bool solutionExists;
// linear Diophantine variables

void extendedEuclid (lli a, lli b) {
	if (b == 0) {x = 1; y = 0; d = a; return;}
	extendedEuclid(b, a % b);
	lli x1, y1;
	x1 = y;
	y1 = x - (a / b) * y;
	x = x1;
	y = y1;
}

void calculateSpecificSolution (lli c) {
	X0 = x * (c / d);
	Y0 = y * (c / d);
}

lli cost(lli nVal, lli cX, lli cY, lli a, lli b) {
	lli XVal = X0 + (b / d) * nVal, YVal = Y0 - (a / d) * nVal;
	return (cX * XVal) + (cY * YVal);
}

lli slope (lli cX, lli cY, lli a, lli b) {
	return cX * (b / d) - cY * (a / d);
}
// x = x0 + (b / d) * n
// y = y0 - (a / d) * n
// changes according to question
void calculateGeneralSolution (lli a, lli b, lli cX, lli cY) {
	lli bByD = b / d, aByD = a / d;
	lli nMin = ceil(-(double)X0 / bByD), nMax =floor((double)Y0 / aByD);
	if (nMin > nMax) {
		solutionExists = false;
		return ;
	}
	lli nValueForAns;
	if (nMin == nMax) {
		nValueForAns = nMin;
	} else {
		nValueForAns = cost(nMin, cX, cY, a, b) < cost(nMax, cX, cY, a, b) ? nMin : nMax; 
	}
	XAns = X0 + (bByD * nValueForAns);
	YAns = Y0 - (aByD * nValueForAns);
}



void doesSolutionExists (lli c) {
	solutionExists = (c % d) == 0;
}

void initialize () {
	solutionExists = false;
}

// n1X + n2Y = N OR n1Y + n2X = N
int main () {
	lli N, n1, n2, c1, c2;
	while (scanf("%lld", &N) && N) {
		scanf("%lld %lld %lld %lld", &c1, &n1, &c2, &n2);
		initialize();
		lli a = n1, b = n2, c = N;
		extendedEuclid(a, b);
		doesSolutionExists(c);
		if (solutionExists == false) {
			printf("failed\n"); continue;
		}
		calculateSpecificSolution(c);
		calculateGeneralSolution(a, b, c1, c2);
		if (solutionExists == false) {
			printf("failed\n"); continue;
		}
		lli m1 = XAns, m2 = YAns;
		printf("%lld %lld\n",m1, m2 );
		
	}
	return 0;
}