# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_N 15
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int d, n, modVal;
int coeff[20], baseVal[20];
struct Matrix { lli mat[MAX_N][MAX_N]; };

Matrix matMul (Matrix a,Matrix b, int aRow, int aColbRow, int bCol ) {
	int k;
	Matrix ans;
	fr(i, 0, aRow) fr(j, 0, bCol) for(ans.mat[i][j] = k = 0; k < aColbRow; ++k) {
		ans.mat[i][j] = ( ans.mat[i][j] +  (a.mat[i][k] % modVal) * (b.mat[k][j] % modVal) ) % modVal;
	}
	return ans;
}

Matrix matPow (Matrix base, int p, int size) {
	Matrix ans;
	fr (i, 0, size) fr(j, 0, size) ans.mat[i][j] = (i == j);
	while (p) {
		if (p & 1) ans = matMul(ans, base, size, size, size);
		base = matMul(base, base, size, size, size);
		p >>= 1;
	}
	return ans;
}

int main () {
	Matrix baseMat;
	while (scanf("%d %d %d", &d, &n, &modVal) && (n || modVal || d)) {
		fr(i, 0, d) scanf("%d", &coeff[i]);
		// filling only column in baseMAtrix
		fr(i, 0, d) scanf("%d", &baseVal[i]);
		fr(i, 0, d) baseMat.mat[i][0] = baseVal[d - i - 1];
		if (n <= d) {
			 printf("%lld\n", baseMat.mat[d - n][0] % modVal);
			 continue;	
		}

		Matrix powerMat ;
		fr(i, 0, d) powerMat.mat[0][i] = coeff[i];
		fr (i, 1, d) fr (j, 0, d) powerMat.mat[i][j] = ((i - 1) == j );
		powerMat = matPow(powerMat, n - d, d);
		Matrix ans = matMul(powerMat, baseMat, d, d, 1);
		printf("%lld\n", ans.mat[0][0] );
	}
	return 0;
}