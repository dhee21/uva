# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_N 2
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

struct Matrix { lli mat[MAX_N][MAX_N]; };
lli modVal ;

Matrix matMul (Matrix a,Matrix b) {
	int k;
	Matrix ans;
	fr(i, 0, MAX_N) fr(j, 0, MAX_N) for(ans.mat[i][j] = k = 0; k < MAX_N; ++k) {
		ans.mat[i][j] = ( ans.mat[i][j] +  (a.mat[i][k] % modVal) * (b.mat[k][j] % modVal) ) % modVal;
	}
	return ans;
}

Matrix matPow (Matrix base, int p) {
	Matrix ans;
	fr (i, 0, MAX_N) fr(j, 0, MAX_N) ans.mat[i][j] = (i == j);
	while (p) {
		if (p & 1) ans = matMul(ans, base);
		base = matMul(base, base);
		p >>= 1;
	}
	return ans;
}

int main () {
	int n, m, fibMat[2][2] = { {1, 1}, {1, 0} };
	Matrix fib;
	fr (i, 0, 2) fr (j, 0, 2) fib.mat[i][j] = fibMat[i][j];
	while (scanf("%d %d", &n, &m) != EOF) {
		modVal = 1 << m;
		Matrix ans = matPow(fib, n);
		printf("%lld\n", ans.mat[0][1]);
	}
	return 0;
}