# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

int main () {
	int n, k;
	while (scanf("%d %d", &n, &k) != EOF) {
		int used = 0, ans = 0;
		while (n >= k) {
			ans += (n / k) * k;
			n = (n / k) + (n % k);
		}
		printf("%d\n",ans + n );
	}
	return 0;
}