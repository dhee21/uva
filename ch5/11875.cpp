# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
int main () {
	int t;
	ipInt(t);
	fr (tc, 1, t + 1) {
		int num, ans;
		ipInt(num);
		fr (i, 0, num) {
			int val;
			ipInt(val);
			if (i == (num - 1) / 2) ans = val;
		}
		printf("Case %d: %d\n",tc, ans );
	}
	return 0;
}