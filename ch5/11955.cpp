# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;

#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

typedef long long int lli;
lli C[55][55];

void binomialPrint (char *a, char *b, int power) {
	power == 1 ? printf("%s+",a ) : printf("%s^%d+",a, power);
	fr(r, 1, power) {
		int aPower = power - r, bPower = r;
		lli coeff = C[power][r];
		coeff == 1 ? printf("") : printf("%lld*",coeff);
		printf("%s",a); aPower == 1 ? printf("") : printf("^%d",aPower);
		printf("*");
		printf("%s",b); bPower == 1 ? printf("+"): printf("^%d+",bPower);
	}
	power == 1 ? printf("%s\n",b ) : printf("%s^%d\n",b, power);
}

void ncrDp () {
	fr(n, 1, 51) {
		C[n][0] = C[n][n] = 1;
		fr (k, 1, n) {
			C[n][k] = C[n - 1][k] + C[n - 1][k - 1];
		}
	}
}

int main () {
	int t;
	ipInt(t);
	char waste, a[110], b[110];
	memset (C, 0, sizeof C);
	ncrDp();
	int caseNo = 0;
	while (t--) {
		caseNo++;
		scanf("\n");
		getchar();
		char *ptr = a, c;
		while(scanf("%c", &c) && c != '+') *ptr++ = c;
		*ptr = '\0';
		ptr = b;
		while(scanf("%c", &c) && c != ')') *ptr++ = c;
		*ptr = '\0'; getchar();
		int power;
		scanf("%d", &power);
		printf("Case %d: ", caseNo);
		binomialPrint(a, b, power);
	}
	return 0;
}