# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define PI 3.14159265

double findX (int number, int level) {
	double midOffset = ( (level + 1) * (level + 1)  - level * level ) / 2;
	double mid = (level * level) + midOffset;
	return (number - mid) / 2.0;
}

double findY(int number, int level) {
	int yOffset = (3 * (level - 1)) + 1;
	int numberOffset = number - (level * level);
	if (numberOffset & 1) {
		yOffset += 1;
	} else {
		yOffset += 2;
	}
	return (sqrt(3) / 6) * yOffset;
}

int main () {
	int n, m;
	while (scanf("%d %d", &n, &m) != EOF) {
		double levelStart = sqrt(n), levelEnd = sqrt(m);
		double startX = findX(n, levelStart), endX = findX(m, levelEnd), startY = findY(n, levelStart), endY = findY(m, levelEnd);
		double ans = sqrt(pow(startX - endX, 2) + pow(startY - endY, 2));
		printf("%0.3lf\n",ans);
	}
	return 0;
}