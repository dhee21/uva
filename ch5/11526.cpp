# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
# include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i <= ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
typedef long long int lli;
typedef pair<lli, int> pii;

int main () {
	int t;
	ipInt(t);
	while(t--) {
		int n;
		ipInt(n);
		int x = (int) sqrt(n);
		lli sum = 0;
		if (n <= 0) {
			printf("0\n"); continue;
		}
		sum += n;
		fr(i, 2, x) {
			sum += n / i;
			// cout << sum << endl;
			sum += ( (n / (i - 1)) - (n / i) ) * (i - 1);
			// cout << sum <<endl;
		}
		fr (i, x + 1, (n / x) ) {
			sum += n  / i;
		}
		printf("%lld\n", sum );
	}
	return 0;
}