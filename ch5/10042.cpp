# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define maxVal 33000

typedef vector<int> vi;
typedef long long int lli;
//sieve vars
int sieveWorker[maxVal], primeList[maxVal], numPrimes;

void primeSieve () {
	int primesTill = (int)sqrt(1000000000);
	int goTillHere = (int)sqrt(primesTill);
	for (int i = 2; i <= goTillHere; ++i) {
		if (sieveWorker[i]) {
			primeList[numPrimes++] = i;
			for(int j = i + i; j <= primesTill; j += i) {
				sieveWorker[j] = 0;
			}
		}
	}
	for (int i = goTillHere + 1; i <= primesTill; ++i) {
		if (sieveWorker[i]) {
			primeList[numPrimes++] = i;
		}
	}
}

int sumDigits(int number) {
	int sum = 0;
	while (number) {
		sum += number % 10; number /= 10;
	}
	return sum;
}

int primeFactorSum (int number) {
	int sum = 0, primeFactors = 0;
	for (int i = 0; i < numPrimes; ++i) {
		int primeVal = primeList[i];
		if (number % primeVal == 0) {
			sum += sumDigits(primeVal); number /= primeVal;
			primeFactors++;
			if (number == 1) break;
			--i;
		}
	}
	if ((number == 1 && primeFactors == 1) || (number != 1 && primeFactors == 0) ) return -1;
	sum += (number == 1) ? 0 : sumDigits(number);
	return sum;
}

int giveNext (int number) {
	int ans;
	for (int num = number + 1; ; ++num) {
		if (primeFactorSum(num) == sumDigits(num)) {
			ans = num; break;
		}
	}
	return ans;
}

int main () {
	numPrimes = 0;
	fr (i, 0, maxVal + 1) sieveWorker[i] = 1;
	primeSieve();
	int t;
	ipInt(t);
	while(t --) {
		int number;
		ipInt(number);
		int nextNum = giveNext(number);
		printf("%d\n",nextNum);
	}
	return 0;
}