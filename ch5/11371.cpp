# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

bool func(char a, char b) {
	return a > b;
}

int main () {
	char input[20] , inputCopy[20];
	while (fgets(input, 20, stdin) != NULL) {
		int leng = strlen(input);
		if (input[leng - 1] == '\n') input[strlen(input) - 1] = '\0';
		int len = strlen(input);

		sort(input, input + len, func);
		char minChar;
		int minIndex;
		for (int i = len - 1; i >=0 ; --i) {
			if (input[i] != '0') {
				minChar = input[i]; minIndex = i; break;
			}
		}
		inputCopy[0] = minChar;
		int index = 1;
		fr(i, 0, len) {
			if (i != minIndex) inputCopy[index++] = input[i];
		}
		inputCopy[index] = '\0';
		sort(inputCopy + 1, inputCopy + len);
		lli num1 = atoll(input), num2 = atoll(inputCopy);
		lli diff = num1 - num2;
		printf("%lld - %lld = %lld = 9 * %lld\n",num1, num2, diff,diff / 9 );
	}
	return 0;
}