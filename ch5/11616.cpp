# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
# include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
map <char, int> value;

int romanToInt (char *inp) {
	int len = strlen(inp);
	int ans;
	fr (i, 0, len) {
		if (i != len - 1 && value[inp[i + 1]] > value[inp[i]]) {
			ans += -value[inp[i]];
		} else {
			ans += value[inp[i]];
		}
	}
	return ans;
}

string intToRoman (int num) {
	string uni[] = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X" };
	string dec[] = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC", "C" };
	string hundred[] = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM", "M"};
	string thousand[] = {"", "M", "MM", "MMM"};
	string ans;
	ans = uni[num % 10] + ans; num /= 10;
	ans = dec[num % 10] + ans; num /= 10;
	ans = hundred[num % 10] + ans; num /= 10;
	ans = thousand[num % 10] + ans; num /= 10;
	return ans;
}

int main () {
	char inp[100];
	value['I'] = 1;
	value['V'] = 5;
	value['X'] = 10;
	value['L'] = 50;
	value['C'] = 100;
	value['D'] = 500;
	value['M'] = 1000;
	while(fgets(inp, 100, stdin) != NULL) {
		if (atoi(inp) != 0) {
			printf("%s\n", intToRoman(atoi(inp)).data());
		} else {
			printf("%d\n", romanToInt(inp));
		}
	}
	return 0;
}