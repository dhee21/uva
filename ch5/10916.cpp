# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
#include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

int ans[25];

void calculate() {
	int decade = 0;
	int bitsAvailable = 1 << (2 + decade);
	double bitsForPrevNumber = log2(2) + 1;
	for (int factorial = 3; ; ++factorial ) {
		double bitsRequiredNow = bitsForPrevNumber + log2(factorial);
		if ((int)bitsRequiredNow > bitsAvailable) {
			while (bitsAvailable < bitsRequiredNow) {
				ans[decade ++] = factorial - 1;
				bitsAvailable = 1 << (2 + decade);
			}
			if (decade == 22) break;
		}
		bitsForPrevNumber = bitsRequiredNow;
	}
}

int main () {
	int year;
	calculate();

	while(ipInt(year) * year) {
		int decade = (year - 1960) / 10;
		printf("%d\n",ans[decade]);
	}
	return 0;
}