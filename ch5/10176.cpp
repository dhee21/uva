# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define ModVal 131071
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

lli findPow (lli base, lli power, lli m) {
	if (power == 0) return 1 % m;
	if (power == 1) return base % m;
	lli halfPower = findPow(base, power / 2, m);
	lli value = (halfPower * halfPower) % m;
	if (power & 1) {
		value = (value * (base % m)) % m;
	}
	if (value < 0) return m - value;
	return value;
}

bool findAns (char* input) {
	int len = strlen(input);
	lli ans = 0;
	for (int i = len - 1; i >= 0; --i) {
		if (input[i] == '1') {
			ans = (ans +  findPow(2, (len - 1) - i, ModVal)) % ModVal;
		}
	}
	return ans == 0;
}

int main () {
	char str[10010], ch;
	int i = 0;
	while (scanf("%c", &ch) != EOF) {
			if (ch !='\n' && ch != '#') {
				str[i++] = ch;
			}
			if (ch =='#') {
				str[i] = '\0';
				bool divisible = findAns(str);
				if (divisible) printf("YES\n");
				else printf("NO\n");
				i = 0;
			}
	}
	return 0;
}