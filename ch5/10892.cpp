# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

lli gcd (lli a, lli b) { return b == 0 ? a : gcd (b, a % b); }

lli lcm (lli a, lli b) { return a * b / gcd (a, b); }

int main () {
	lli n;
	while (scanf("%lld", &n) && n) {
		vlli divisors;
		int m = (int) sqrt(n);
		fr (i, 1, m + 1) {
			if (n % i == 0) {
				lli x = n / i;
				divisors.push_back(i);
				if (x != i) divisors.push_back(x);
			}
		}
		lli ans = 0;
		fr (i, 0, divisors.size()) fr (j, i, divisors.size()) {
			if (lcm(divisors[i], divisors[j]) == n) ans++;
		}
		printf("%lld %lld\n",n, ans );
	}
	return 0;
}