# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define PI 3.14159265

int main () {
	double a, b, v, theta, t;
	while (scanf("%lf %lf %lf %lf %lf", &a, &b, &v, &theta, &t) && (a || b || v || theta || t)) {
		theta = (theta * PI) / 180;

		double vX = v * cos(theta), vY = v * sin(theta) ;
		int hitY = 0, hitX = 0;
		double sX = (vX * t ) / 2, sY = (vY * t) / 2;
		if (sX >= (a / 2)) {
			sX -= a / 2;
			hitY++;
		}
		hitY += sX / a;
		if (sY >= b / 2) {
			sY -= b / 2;
			hitX ++;
		}
		hitX += sY / b;
		printf("%d %d\n", hitY, hitX );
	}
	return 0;
}