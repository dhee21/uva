# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
# include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
typedef long long int lli;
typedef pair<lli, int> pii;

int expense[1010];

int main () {
	int n;
	while (ipInt(n) && n) {
		int sum = 0;
		fr (i, 0, n) {
			int dollar, cent;
            char temp;
            cin >> dollar >> temp >> cent;
			expense[i] = dollar * 100 + cent;
			sum += expense[i];
		}
		bool isAvgInteger = (sum % n) == 0;
		double average = (double)sum / (double)n;
		int underPaidAverage = floor(average);
		int overPaidAverage = ceil(average);
		int moneyTranferred = 0, overPairs = 0, overPaidSum = 0;
		fr (i, 0, n) {
			if (expense[i] < underPaidAverage) {
				moneyTranferred += underPaidAverage - expense[i];
			} else if (expense[i] > underPaidAverage) {
				overPairs++;
				overPaidSum += expense[i];
			}
		}
		int overPaidSumToGet = overPaidAverage * overPairs;
		int overPairsSumReducedTo = overPaidSum - moneyTranferred;
		int ans;
		if (overPairsSumReducedTo <= overPaidSumToGet) {
			ans = moneyTranferred;
		} else {
			ans = moneyTranferred + (overPairsSumReducedTo - overPaidSumToGet);
		}
		double dollars = (double)ans / 100.0;
		printf("$%0.2lf\n", dollars);
	}
	return 0;
}