# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i <= ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define forAllEmptyPositions fr(i, 0, 3) fr(j, 0, 3) if (board[i][j] == empty)
#define empty '.'

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition
char board[5][5], opponent[30], globalRow, globalCol;

bool doesWin (char player) {
	// fr (i, 0, 3){
	// 	fr (j, 0, 3) printf("%c ",board[i][j] );
	// 	printf("\n");
	// }
	// printf("\n %c see \n", player);
	bool allVAluesXinDiag = true, allVAluesXinDiagRev = true;
	fr(row, 0, 3) {
		bool allVAluesX = true, allVAluesXinCol = true;
		fr (col, 0, 3){
			allVAluesX = allVAluesX && board[row][col] == player;
			allVAluesXinCol = allVAluesXinCol && board[col][row] == player;
		}
		if (allVAluesX || allVAluesXinCol) {return true;}
		allVAluesXinDiag = allVAluesXinDiag && board[row][row] == player;
		allVAluesXinDiagRev = allVAluesXinDiagRev && board[row][3 - row] == player;
	}
	if (allVAluesXinDiag || allVAluesXinDiagRev) { return true;}

	return false;
}

void printGrid() {
	fr (i, 0, 3){
		fr (j, 0, 3) printf("%c ",board[i][j] );
		printf("\n");
	}
	printf("\n");
}

bool noMatterOCanXWin (char player) {
	if (doesWin('x')) return true; if (doesWin('o')) return false;
	if (player == 'x') {
		bool hasOneForcedWinMove = false;
		forAllEmptyPositions {
				board[i][j] = 'x';
				hasOneForcedWinMove = noMatterOCanXWin('o');
				board[i][j] = empty;
				if (hasOneForcedWinMove) return true;
		}
		return false;
	} else {
		bool full = true, doesXWinAll = true;
		forAllEmptyPositions {
			full = false;
			board[i][j] = 'o';
			doesXWinAll = doesXWinAll && noMatterOCanXWin('x');
			board[i][j] = empty;
			if (!doesXWinAll) return false;	
		}
		return full ? false : true;
	}
	return true;
}

int main () {
	char st;
	opponent['x' - 'a'] = 'o';
	opponent['o' - 'a'] = 'x';
	while (cin >> st && st == '?') {
		fr (i, 0, 3){
		 	fr(j, 0, 3) {
				cin >> board[i][j];
			}
		}
		bool full = true, ansFound = false;
		pii ans;
		fr (i, 0, 3) { 
			fr (j, 0, 3) {
				if (board[i][j] == empty) {
					full = false;
					board[i][j] = 'x';
					if ( noMatterOCanXWin('o') ) {
						ans = pii(i, j); ansFound = true; break;
					}
					board[i][j] = empty;
				}
			}
			if (ansFound) {break;}
		}
		if (full) {
			ansFound = noMatterOCanXWin('x');
		}
		if (ansFound) {
			printf("(%d,%d)\n",ans.first, ans.second );
		} else {
			printf("#####\n");
		}
		
	}
	return 0;
}