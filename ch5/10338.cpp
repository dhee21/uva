# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

lli fact[21];

void calculate () {
	fact[0] = fact[1] = 1;
	fr (i, 2, 22) {
		fact[i] = fact[i - 1] * (lli) i;
	}
}

int main () {
	int t, caseNo = 0;
	ipInt(t); getchar();
	char input [20];
	lli freq[26];
	calculate();
	while (t--) {
		caseNo ++;
		memset(freq, 0, sizeof freq);
		scanf("%s", input);
		int len = strlen(input);
		fr(i, 0, len) {
			freq[input[i] - 'A']++;;
		}
		lli ans = fact[len]; 
		fr(i, 0, 26) {
			ans /= fact[freq[i]];
		}
		printf("Data set %d: %lld\n",caseNo, ans);
	}
	return 0;
}