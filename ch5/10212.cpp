# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i <= ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

lli removeZeros(lli N) {
	while (N && (N % 10 == 0)) {
		N /= 10;
	}
	return N;
}

int main () {
	lli n, m;
	lli billion = 1000000000;
	while (scanf("%lld %lld", &n,&m) != EOF) {
		if (m == 0) {
			printf("1\n");continue;
		}
		lli lastval = n - m + 1;
		lli lastDigits = 1;
		for (lli i = n; i >= lastval; --i) {
			// lli iNonZero = removeZeros(i);
			lastDigits = lastDigits * i;
			lastDigits = removeZeros(lastDigits);
			lastDigits %= billion;
		}
		printf("%lld\n",lastDigits % 10);
	}
	return 0;
}