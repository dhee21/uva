#include <stdio.h>
#include <iostream>
#include <stack>
using namespace std;

void getTrains(int &blockNum, stack<int> & st , int numberToGet) {
	while(blockNum <= numberToGet) {
		st.push(blockNum++);
	}
}

bool getAnswer(int blocks, int* perm) {
	int i = 0, blockNum=1;
	stack<int> st;
	while(i < blocks) {
		if (st.empty() || st.top() < perm[i]) {
			getTrains(blockNum, st, perm[i]);
		}
		while(!st.empty() && st.top()==perm[i]) {
			st.pop();
			++i;
			if(i==blocks) {
				return true;
			}
		}
		if(!st.empty() && st.top() > perm[i]) {
			return false;
		}
	}
	return true;
}

int main() {
	int n, p[1010],ans, first=1, x;
	while(scanf("%d",&n) && n) {
		while(scanf("%d", &x) && x) {
			p[0] = x;
			for(int i = 1 ; i<n ; ++i) {
				scanf("%d", &p[i]);
			}
			ans = getAnswer(n,p);
				if(ans) {
					printf("Yes\n");
				}else {
					printf("No\n");
				}
		}
			printf("\n");
	}
}