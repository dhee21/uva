#include <stdio.h>
#include <iostream>
#include <cmath>
#include <limits>
#include <vector>
#include <stdlib.h>
#include <utility>
#include <cstring>
#include <bitset>
using namespace std;
#define maxSize 1024009
bitset<maxSize> pirates;
int infinity = numeric_limits<int>::max();

struct treeNode {
	int ones,zeroes,toUpdate;
	char lazyVal;
}useless={-infinity,-infinity,0,'E'};

int treeSize = int (2*pow(2.0, floor(log(maxSize)/log(2.0) + 1 )));
vector<treeNode> tree(treeSize);

class lazySegmentTree{
	public :
	int size;

	void putVal(int treeId, int value) {
		if(value) {
			tree[treeId].ones = 1;
			tree[treeId].zeroes = 0;
		} else {
			tree[treeId].ones = 0;
			tree[treeId].zeroes = 1;
		}
		tree[treeId].lazyVal = 'N' ;
		tree[treeId].toUpdate = 0;
	}
	treeNode calculate(treeNode left , treeNode right) {
		treeNode node = {0,0,0,'N'};
		node.zeroes = left.zeroes + right.zeroes;
		node.ones = left.ones + right.ones;
		return node;
	}
	void updateLazyValue(int treeId, char newLazyVal) {
		if(newLazyVal == 'I') {
			if(tree[treeId].toUpdate == 0)
			{
				tree[treeId].lazyVal = 'I';
				return;
			}
			if(tree[treeId].lazyVal == 'F') {
				tree[treeId].lazyVal = 'E';
			} else if (tree[treeId].lazyVal == 'E') {
				tree[treeId].lazyVal = 'F';
			} else if (tree[treeId].lazyVal == 'I') {
				tree[treeId].lazyVal = 'N';
			} else {
				tree[treeId].lazyVal = 'I';
			}
			return ;
		} 
		if(newLazyVal != 'N') {
			tree[treeId].lazyVal = newLazyVal;
		}

	}
	void propogate (int treeId, char val) {
		updateLazyValue(treeId<<1 , val);
		tree[treeId << 1].toUpdate = 1 ;
		updateLazyValue((treeId << 1) + 1, val);
		tree[(treeId << 1) + 1].toUpdate = 1;
	}
	void updateNode (int treeId, int st, int ed, char val) {
		if (val == 'I') {
			int x = tree[treeId].zeroes ;
			tree[treeId].zeroes = tree[treeId].ones;
			tree[treeId].ones = x;
		} else if (val == 'E') {
			tree[treeId].zeroes = ed-st+1;
			tree[treeId].ones = 0;
		} else if ( val == 'F') {
			tree[treeId].ones = ed-st+1;
			tree[treeId].zeroes = 0; 
		} else {
			tree[treeId].toUpdate = 0;
			return;
		}
		if(st != ed) {
			propogate(treeId, val);
		}
		tree[treeId].toUpdate = 0;
	}

	void createTree(bitset<maxSize> &array, double size) {
		this->size = size ;
		buildTree(array, 1,0,this->size - 1);
	}
	
	void buildTree( bitset<maxSize> &array, int id , int st, int ed) {
		if(st > ed) {
			return ;
		}
		if( st == ed ) {
			putVal(id, array[st]==true);
			return ;
		}
		int mid = (st + ed)>>1;
		buildTree(array,(id<<1) ,st, mid);
		buildTree(array, (id<<1) + 1 , mid+1, ed);
		tree[id] = calculate(tree[id<<1], tree[(id<<1) +1]);
		return ;
	}
	treeNode query(int id, int segSt, int segEd, int ourSt, int ourEd) {
			if(segSt > segEd || ourSt>ourEd || ourSt > segEd || ourEd <segSt) {
				return useless;
			}
			if(tree[id].toUpdate) {
				updateNode(id, segSt, segEd, tree[id].lazyVal);
			}
			if(segSt>=ourSt && segEd <=ourEd) {
				return tree[id];
			}
			int mid = (segSt + segEd ) >> 1;
			treeNode left = query((id <<1), segSt, mid, ourSt, ourEd);
			treeNode right = query( (id << 1) + 1 , mid+1, segEd, ourSt, ourEd );
			if(left.ones == useless.ones) {
				return right;
			}
			if(right.ones == useless.ones) {
				return left;
			}
			return calculate(left, right);
	}
	void update (int id, int segSt, int segEd , int ourSt, int ourEd, char changeVal) {
				if(tree[id].toUpdate) {
					updateNode(id, segSt, segEd, tree[id].lazyVal);
				}
				if(segSt > segEd || ourSt> ourEd || segSt > ourEd || segEd < ourSt) {
					return;
				}

				
				if(segSt >= ourSt && segEd <= ourEd) {
					updateNode(id, segSt, segEd, changeVal);
					return ;
				}
				int mid = (segSt + segEd) >> 1;
				int child = id << 1;
				update(id << 1, segSt, mid , ourSt, ourEd, changeVal);
				update((id << 1)+1, mid +1, segEd, ourSt, ourEd, changeVal);
				tree[id] = calculate(tree[child], tree[child+1]);
				return ;
	}
	 void doUpdate(int stIndex, int endIndex , char changeVal) {
      update( 1 , 0 ,this->size -1 , stIndex, endIndex , changeVal);
    }

    int doQuery(int start , int end ) {
      return query( 1 , 0 ,this->size - 1 , start , end ).ones ;
    }
};

int main () {
	int t,m;
	scanf("%d", &t);
	for(int i = 0 ; i < t ; ++i) {
		scanf("%d", &m );
		int bitsetLength = 0;
		while(m--) {
			int times;
			char string[60];
			scanf("%d\n", &times);
			scanf("%s",string);
			// printf("%s %d\n",string, strlen(string) );
			while(times--) {
				for(int j =0 ; j< strlen(string) ; ++j) {
					pirates[bitsetLength++] = (string[j]=='1');
				}
			}

		}
		lazySegmentTree  segTree;
		int queries,st,ed, queryNo = 1;
		char type;
		segTree.createTree(pirates, bitsetLength);
		printf("Case %d:\n", i+1 );
		scanf("%d\n", &queries);
		for (int q = 1 ; q <= queries ; ++q) {
			scanf("%c %d %d\n", &type, &st, &ed);
			if(type == 'S') {
				printf("Q%d: %d\n",queryNo++, segTree.doQuery(st, ed) );
			} else
				segTree.doUpdate(st, ed , type);
			}
		}
		
		pirates.reset();
		return 0 ;
	}

