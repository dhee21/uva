# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
# include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
typedef long long int lli;
typedef pair<lli, int> pii;
typedef pair<char, char> pcc;
map<char, int> value;
map<int, char> charValue;

pcc add(char number1, char number2) {
	int valueNum1 = value[number1], valueNum2 = value[number2];
	pcc ans;
	int addVal = valueNum2 + valueNum1;
	return pcc(charValue[(addVal) / 4], charValue[addVal % 4]);
}

string add(string num1, string num2) {
	char carry = 'V', number = 'V';
	string ans;
	while (num1.size() < num2.size()) num1.push_back('V');
	fr(i, 0, num1.size()) {
		number = carry;
		carry = 'V';
		pcc addition = add(number, num2[i]);
		carry = addition.first; number = addition.second;
		addition = add(number, num1[i]);
		if (carry == 'V') {carry = addition.first;}
		number = addition.second;
		ans.push_back(number);
	}
	if (carry != 'V') ans.push_back(carry);
	return ans;
}

string operate (string num2, char operation, string num1) {
	switch (operation) {
		case 'N':
			return num2;
		case 'L':
			return "V" + num2;
		case 'R':
			return num2.substr(1, num2.size() - 1) + "V";
		case 'A':
			return add(num1, num2);
		default : 
			break;
		}
}

int main () {
	int t;
	charValue[0] = 'V'; value['V'] = 0;
	charValue[1] = 'U';	value['U'] = 1;
	charValue[2] = 'C';	value['C'] = 2;
	charValue[3] = 'D'; value['D'] = 3 ;
	printf("COWCULATIONS OUTPUT\n");
	ipInt(t);
	while (t--) {
		string num1, num2, result;
		char op1, op2, op3;
		cin >> num1; cin >> num2; cin >> op1; cin >> op2; cin >> op3; cin >> result;
		reverse(num1.begin(), num1.end());
		reverse(num2.begin(), num2.end());
		num2 = operate(num2, op1, num1); num2 = operate(num2, op2, num1); num2 = operate(num2, op3, num1);
		if (num2.size() != 8) {
			while (num2.size() < 8) num2.push_back('V');
		}
		reverse(num2.begin(), num2.end());
		(num2 == result) ? printf("YES\n") : printf("NO\n");;
	}
	printf("END OF OUTPUT\n");
	return 0;
}