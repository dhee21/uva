# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 600

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// primefactorization and primeSieve variables
bitset<primesTill + 1> probablePrime; 
vi primeList, factors, exponents;
// primefactorization and primeSieve variables


void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	int goTill = (int)sqrt(primesTill);
	for (int i = 2; i <= goTill ; ++i) {
		if (probablePrime.test(i) == 1) {
			primeList.push_back(i);
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime.reset(j);
			}
		}
	}
	for (int i = goTill + 1; i <= primesTill; ++i) {
		if (probablePrime.test(i) == 1) primeList.push_back(i);
	}
}

void primeFactorization (lli N) {
	int PFIndex = 0;
	lli PF = primeList[0];
	factors.clear(); exponents.clear();
	while (PF * PF <= N && N != 1 && PFIndex < primeList.size()) {
		if (N % PF == 0) {
			int expVal = 0;
			while (N % PF == 0) {
				N /= PF;  expVal ++;
			}
			factors.push_back(PF);
			exponents.push_back(expVal);
		}
		PF = primeList[++PFIndex];
	}
	if (N != 1) {
		factors.push_back(N); exponents.push_back(1);
	}
}

int main () {
	string str;
	primeSieve();
	while (true) {
		int base, num = 1, exponent;

		getline (cin, str);
		stringstream ss(str);
		ss >> base;
		if (!base) break;
		ss >> exponent;
		num *= (int)pow(base, exponent);
		while (ss >> base && ss >> exponent) {
			num *= (int)pow(base, exponent);
		}
		num --;
		primeFactorization(num);
		reverse(factors.begin(), factors.end());
		reverse(exponents.begin(), exponents.end());
		fr (i, 0, factors.size()) {
			printf("%d %d",factors[i], exponents[i]);
			i != factors.size() - 1 ? printf(" ") : printf("\n");
		}
	}
	return 0;
}