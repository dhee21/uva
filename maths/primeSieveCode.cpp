# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 1000000

//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinitions

bitset<primesTill + 1> probablePrime; 
vi primeList;

//calculate primes till primesTIll
void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	int goTill = (int)sqrt(primesTill);
	for (int i = 2; i <= goTill ; ++i) {
		if (probablePrime.test(i) == 1) {
			primeList.push_back(i);
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime.reset(j);
			}
		}
	}
	for (int i = goTill + 1; i <= primesTill; ++i) {
		if (probablePrime.test(i) == 1) primeList.push_back(i);
	}
}

bool isPrime(int num) {
 	return probablePrime.test(num);	
}

int main () {
	primeSieve();
	int n;
	while (ipInt(n) && n) {
		bool solutionThere = false;
		for (int i = 0; primeList[i] <= (n / 2) && i < primeList.size(); ++i) {
			int primeNum = primeList[i];
			if (isPrime(n - primeNum)) {
				printf("%d = %d + %d\n",n, primeNum, n - primeNum);
				solutionThere = true;
				break;
			}
		}
		if (solutionThere == false) printf("Goldbach's conjecture is wrong.\n");
	}
	return 0;
}