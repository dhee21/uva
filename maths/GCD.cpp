# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// a and b +ve integers and a >= b
int gcd (int a, int b) { return b == 0 ? a : gcd (b, a % b); }

int main () {
	vi input;
	int a;
	while (ipInt(a) && a) {
		input.push_back(a);
		while (ipInt(a) && a) input.push_back(a);
		sort(input.begin(), input.end());
		int toSubtract = input[0];
		fr (i, 0, input.size()) input[i] -= toSubtract;
		int gcdVal = input[1];
		fr (i, 2, input.size()) gcdVal = gcd (input[i], gcdVal);
		printf("%d\n",gcdVal );
		input.clear();
	}
	return 0;
}