# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// linear Diophantine variables
lli x, y, d, X0, Y0, XAns, YAns;
bool solutionExists;
// linear Diophantine variables

void extendedEuclid (lli a, lli b) {
	if (b == 0) {x = 1; y = 0; d = a; return;}
	extendedEuclid(b, a % b);
	lli x1, y1;
	x1 = y;
	y1 = x - (a / b) * y;
	x = x1;
	y = y1;
}

void calculateSpecificSolution (lli c) {
	X0 = x * (c / d);
	Y0 = y * (c / d);
}

lli cost(lli nVal, lli a, lli b) {
	lli XVal = X0 + (b / d) * nVal, YVal = Y0 - (a / d) * nVal;
	return abs(XVal) + abs(YVal);
}

lli slope (lli cX, lli cY, lli a, lli b) {
	return cX * (b / d) - cY * (a / d);
}
// x = x0 + (b / d) * n
// y = y0 - (a / d) * n
// a and b can be equal to zero
// changes according to question
void calculateGeneralSolution (lli a, lli b) {
	lli bByD = b / d, aByD = a / d;
	lli numPoints;
	lli pointsToCheckOn[4];
	if (a == 0) {
		numPoints = 2;
		double nVal = -(double)X0 / bByD;
		pointsToCheckOn[0] = floor(nVal);
		pointsToCheckOn[1] = ceil(nVal);
	} else if (b == 0) {
		numPoints = 2;
		double nVal = (double)Y0 / aByD;
		pointsToCheckOn[0] = floor(nVal);
		pointsToCheckOn[1] = ceil(nVal);
	} else {
		double nVal1 = -(double)X0 / bByD, nVal2 =(double)Y0 / aByD;
		double less = min (nVal1, nVal2), more = (less == nVal1) ? nVal2 : nVal1;
		lli nMin = ceil(less), nMax = floor(more);
		numPoints = 4;
		pointsToCheckOn[0] = nMin; pointsToCheckOn[1] =  nMin - 1; pointsToCheckOn[2]=  nMax; pointsToCheckOn[3] = nMax + 1 ;
	}
	
	vlli pointsWithMinCost;
	lli minCost = cost(pointsToCheckOn[0], a, b); 
	fr (i, 0, numPoints - 1) {
		lli costVal = cost(pointsToCheckOn[i], a, b); 
		if (costVal < minCost) {
			minCost = costVal;
			pointsWithMinCost.clear();
			pointsWithMinCost.push_back(pointsToCheckOn[i]);
		} else if (minCost == costVal) {
			pointsWithMinCost.push_back(pointsToCheckOn[i]);
		}
	}
	if (pointsWithMinCost.size() == 1) {
		lli nValueForAns = pointsWithMinCost[0];
		XAns = X0 + (bByD * nValueForAns);
		YAns = Y0 - (aByD * nValueForAns);
		return;
	}
	fr(i, 0, pointsWithMinCost.size()) {
		lli nValueForAns = pointsWithMinCost[i];
		XAns = X0 + (bByD * nValueForAns);
		YAns = Y0 - (aByD * nValueForAns);
		if (XAns <= YAns) return;
	}
}



void doesSolutionExists (lli c) {
	solutionExists = (c % d) == 0;
}

void initialize () {
	solutionExists = false;
}

// n1X + n2Y = N OR n1Y + n2X = N
int main () {
	lli a, b, c;
	while (scanf("%lld %lld", &a, &b) != EOF) {
		extendedEuclid(a, b);
		calculateSpecificSolution(d);
		calculateGeneralSolution(a, b);
		printf("%lld %lld %lld\n",XAns, YAns, d);
	}
	return 0;
}