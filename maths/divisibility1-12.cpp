# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int length;
char number[1010];

bool divTwo () {
	int num = number[length - 1] - '0';
	return (num & 1) == 0;
}
bool divThree () {
	int sum = 0;
	fr (i, 0, length) {
		sum += number[i] - '0';
	}
	return (sum % 3) == 0;
}
bool divFour () {
	int num = number[length - 1] - '0';
	if (length - 2 >= 0) {
		int val = number[length - 2] - '0';
		num = val * 10 + num;
	}
	return (num % 4) == 0;
}
bool divFive () {
	return number[length - 1] == '5' || number[length - 1] == '0';
}
bool divSix () {
	return divTwo() && divThree();
}
bool divSeven () {
	int mutiplyWith[6] = {1, 3, 2, 6, 4, 5}, sum = 0;
	for (int i = length - 1; i >= 0; --i) {
		sum += (number[i] - '0') * mutiplyWith[(length - 1 - i) % 6];
	}
	return (sum % 7) == 0;
}
bool divEight () {
	int num = number[length - 1] - '0';
	if (length - 2 >= 0) {
		int val = number[length - 2] - '0';
		num = val * 10 + num;
	}
	if (length - 3 >= 0) {
		int val = number[length - 3] - '0';
		num = val * 100 + num;
	}
	return (num % 8) == 0;
}
bool divNine () {
	int sum = 0;
	fr (i, 0, length) {
		sum += number[i] - '0';
	}
	return (sum % 9) == 0;
}
bool divTen () {
	return number[length - 1] == '0';
}
bool divEleven () {
	int sum = 0;
	fr (i, 0, length) {
		int toMultiply = (i & 1) ? -1 : 1;
		sum += (number[i] - '0') * toMultiply;
	}
	return sum % 11 == 0;
}
bool divTwelve () {
	return divFour() && divThree();
}
 
bool checkDivisibility(int with) {
	switch (with) {
		case 2:
			return divTwo();
		case 3 :
			return divThree();
		case 4 :
			return divFour();
		case 5 : 
			return divFive ();
		case 6 : 
			return divSix();
		case 7: 
			return divSeven();
		case 8 : 
			return divEight();
		case 9 :
		 	return divNine();
		 case 10 : 
		 	return divTen();
		 case 11 :
		 	return divEleven();
		 case 12 : 
		 	return divTwelve();
		 default : 
		 	return true;
	}
}

int main () {
	int t, setLen, num;
	ipInt(t); 
	while (t--) {
		cin >> number;
		length = strlen(number);
		ipInt(setLen);
		bool isWonderful = true;
		while (setLen --) {
			ipInt(num);
			isWonderful = isWonderful & checkDivisibility(num);
		}
		if (isWonderful) {
			printf("%s - Wonderful.\n", number);
		} else {
			printf("%s - Simple.\n", number);
		}
	}
	return 0;
}