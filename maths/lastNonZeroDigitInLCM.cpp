# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <sstream>
# include <bitset>
# include <math.h>
# include <sstream>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 1000
#define lcmTill 1000000

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// primefactorization and primeSieve variables
bitset<(primesTill) + 1> probablePrime; 
vi primeList, factors, exponents;
// primefactorization and primeSieve variables

// LCM variables
// exponentOf stores exponents of primes in prime factorization
// lastDigit[i] stores the last digit od LCM of firt i natural numbers
int exponentOf[lcmTill + 1], lastDigit[lcmTill + 1] ;
int lastDigitExceptTwoAndFive;
// LCM variables


void primeSieve () {
	probablePrime.set();
	probablePrime.reset(0); probablePrime.reset(1);
	int goTill = (int)sqrt(primesTill);
	for (int i = 2; i <= goTill ; ++i) {
		if (probablePrime.test(i) == 1) {
			primeList.push_back(i);
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime.reset(j);
			}
		}
	}
	for (int i = goTill + 1; i <= primesTill; ++i) {
		if (probablePrime.test(i) == 1) primeList.push_back(i);
	}
}

void primeFactorization (lli N) {
	int PFIndex = 0;
	lli PF = primeList[0];
	factors.clear(); exponents.clear();
	while (PF * PF <= N && N != 1 && PFIndex < primeList.size()) {
		if (N % PF == 0) {
			int expVal = 0;
			while (N % PF == 0) {
				N /= PF;  expVal ++;
			}
			factors.push_back(PF);
			exponents.push_back(expVal);
		}
		PF = primeList[++PFIndex];
	}
	if (N != 1) {
		factors.push_back(N); exponents.push_back(1);
	}
}

void changeLastDigit (int base, int expVal) {
	int lastDigit = 1;
	fr (i, 0, expVal) {
		lastDigit = (lastDigit * base) % 10;
	}
	lastDigitExceptTwoAndFive = (lastDigitExceptTwoAndFive * lastDigit) % 10;
}

int giveLastDigit (int n) {
	int exponentOfTwo = exponentOf[2], exponentOfFive = exponentOf[5];
	int diffInPowers = exponentOfTwo - exponentOfFive;
	int toMultiplyWith = (diffInPowers > 0) ? (int)pow(2, diffInPowers) % 10 : (int)pow(5, diffInPowers) % 10;
	return (lastDigitExceptTwoAndFive * toMultiplyWith ) % 10;
}

void findLCM () {
	memset(exponentOf, 0, sizeof exponentOf);
	lastDigitExceptTwoAndFive = 1;
	lastDigit[1] = 1;
	fr (n, 2, lcmTill + 1) {
		primeFactorization(n);
		fr (i, 0, factors.size()) {
			int factorVal = factors[i], expVal = exponents[i];
			if (expVal > exponentOf[factorVal]) {
				int diffInPowers = expVal - exponentOf[factorVal];
				if (factorVal != 2 && factorVal != 5) {
					changeLastDigit(factorVal, diffInPowers);
				}
				exponentOf[factorVal] += diffInPowers;
			}
		}
		lastDigit[n] = giveLastDigit(n);
	}
}

int main () {

	int n;
	primeSieve();
	findLCM();
	while (ipInt(n) && n) {
		printf("%d\n", lastDigit[n]);
	}
	return 0;
}