# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_N 102
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

struct Matrix { bool mat[MAX_N][MAX_N]; };

Matrix matMul (Matrix a,Matrix b) {
	int k;
	Matrix ans;
	fr(i, 0, MAX_N) fr(j, 0, MAX_N) for(ans.mat[i][j] = k = 0; k < MAX_N; ++k) {
		ans.mat[i][j] = (ans.mat[i][j] ||  (a.mat[i][k] && b.mat[k][j]);
	}
	return ans;
}

Matrix matPow (Matrix base, int p) {
	Matrix ans;
	fr (i, 0, MAX_N) fr(j, 0, MAX_N) ans.mat[i][j] = (i == j);
	while (p) {
		if (p & 1) ans = matMul(ans, base);
		base = matMul(base, base);
		p >>= 1;
	}
	return ans;
}

int main () {
	int c, l;
	Matrix adjMat;
	while (scanf("%d %d", &c, &l) && (c || l)) {
		memset (adjMat.mat, 0, sizeof adjMat.mat);
		while (l --) {
			int from, to;
			scanf("%d %d", &from, &to); from--; to--;
			adjMat.mat[from][to] = adjMat.mat[to][from] = 1;
		}
		int st, ed, d;
		scanf("%d %d %d", &st, &ed, &d); st--; ed--;
		Matrix ans = matPow(adjMat, d);
		if (ans.mat[st][ed]) {
			printf("Yes, Teobaldo can travel.\n");
		} else {
			printf("No, Teobaldo can not travel.\n");
		}
	}

	return 0;
}