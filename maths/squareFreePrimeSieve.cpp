# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define primesTill 1000000

//typedefinitions
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

int probablePrime[primesTill + 10], M[primesTill + 10], numFactors[primesTill + 10];

void primeSieve () {
	fr (i, 0, primesTill + 1) probablePrime[i] = 1;
	memset (numFactors, 0, sizeof numFactors);
	probablePrime[0] = probablePrime[1] = 0;
	M[1] = 1; M[0] = 0;
	for (int i = 2; i <= primesTill ; ++i) {
		if (probablePrime[i] == 1) {
			int mI = -1;
			M[i] = M[i - 1] + mI;
			for (int j = i + i; j <= primesTill; j += i) {
				probablePrime[j] *= i;
				numFactors[j] ++;
			}
		} else if (probablePrime[i] == i) {
			int mI = (numFactors[i] & 1) ? -1 : 1;
			M[i] = M[i - 1] + mI;
		} else {
			int mI = 0;
			M[i] = M[i - 1] + mI;
		}
	}
}

string getString (string str) {
	while (str.size() < 8) {
		str = " " + str;
	}
	return str;
}

int main () {
	primeSieve();
	int num;
	while (ipInt(num) && num) {
		string number = to_string(num), mu = to_string(M[num] - M[num - 1]), mVal = to_string(M[num]);
		printf("%s%s%s\n", getString(number).data(), getString(mu).data(), getString(mVal).data());
	}
	return 0;
}