# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <limits>
# include <float.h>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
typedef long long int lli;
lli constants[1000000];

lli findPolVal(lli x, lli order) {
    lli prevB = constants[0];
    for (lli i = 1; i <= order; ++i) {
      prevB = constants[i] + (prevB * x);
    }
    return prevB;
}

void getDerivativeConstants(int order) {
    for (lli power = 0; power <= order; ++power) {
      lli constantIndex = order - power;
      constants[constantIndex] *= power;
    }
}

int main () {
    char inp[1000000];
    lli x;
     while (scanf("%lld", &x) != EOF) {
        scanf("\n");
        fgets(inp, 1000000, stdin);
        char *ptr = strtok(inp, " \n");
        lli order = 0;
        while (ptr != NULL) {
            lli c = atoi(ptr);
            constants[order ++] = (lli) c;
            ptr = strtok(NULL, " \n");
         }
      order--;
      // taking derivtive
      getDerivativeConstants(order);
      order--;
      printf("%lld\n", findPolVal(x, order) );
    }
    return 0;
}