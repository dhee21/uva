# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
//typedefinition

// linear Diophantine variables
lli x, y, d, X0, Y0, XAns, YAns;
bool solutionExists;
// linear Diophantine variables

void extendedEuclid (lli a, lli b) {
	if (b == 0) {x = 1; y = 0; d = a; return;}
	extendedEuclid(b, a % b);
	lli x1, y1;
	x1 = y;
	y1 = x - (a / b) * y;
	x = x1;
	y = y1;
}

void calculateSpecificSolution (lli c) {
	X0 = x * (c / d);
	Y0 = y * (c / d);
}

void doesSolutionExists (lli c) {
	solutionExists = (c % d) == 0;
}

void initialize () {
	solutionExists = false;
}

// n1X + n2Y = N OR n1Y + n2X = N
int main () {
	lli a, b, xInp, k, c;
	int t;
	ipInt(t);
	while ( t-- ) {
		scanf("%lld %lld", &xInp, &k);
		a = floor((double)xInp / k);
		b = ceil((double) xInp / k);
		extendedEuclid(a, b);
		calculateSpecificSolution(xInp);
		printf("%lld %lld\n",X0, Y0);
	}
	return 0;
}