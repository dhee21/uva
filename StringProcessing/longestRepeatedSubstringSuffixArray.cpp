# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_N 1010
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

// logest repeated substring
struct LRS
{
	int startIndex, len, timesRepeated;
};

mSI ans;

// suffixArray Vars
char T[MAX_N], str[MAX_N], ansStrings[MAX_N];
int n;
int SA[MAX_N], tempSA[MAX_N];
int RA[MAX_N], tempRA[MAX_N];
int freq[MAX_N];
// suffixArray Vars
// LCP vars
// previous suffix in Suffix array sorted order
 int Phi[MAX_N];
// permuted LCP
 int PLCP[MAX_N], LCP[MAX_N];
// LCP vars, 

void countingSort (int k) {
	int maxRank = max(300, n), sum = 0;
	memset(freq, 0, sizeof freq);
	// for each suffix SA[i] calculate its SA[i] + k th suffix rank and increment its freq .. i.e put it in the bucket
	fr (i, 0, n) {
		int bucketNum = SA[i] + k < n ? RA[SA[i] + k] : 0;
		freq[bucketNum]++;
	}
	fr (i, 0, maxRank + 1) {
		int t = freq[i]; freq[i] = sum; sum += t; 
	}
	// freq[bucketNum] now stores thre index at which the current suff should be in sorted order
	fr (i, 0, n) {
		int bucketNum = SA[i] + k < n ? RA[SA[i] + k] : 0;
		tempSA[freq[bucketNum]++] = SA[i];
	}
	fr (i, 0, n) SA[i] = tempSA[i];

}
// sort the suffix array
void radixSort (int k) {
	countingSort(k); countingSort(0);
}
void constructSA ( ) {
	fr (i, 0, n) SA[i] = i; 
	fr (i, 0, n) RA[i] = T[i] - '.' + 1;
	
	for (int k = 1; k < n; k <<= 1) {
		radixSort(k);
		int r;
		// calculate tempRA using previous ranks i.e RA
		tempRA[SA[0]] = r = 1;
		fr (i, 1, n) {
			bool isRankPairFirstSame = RA[SA[i]] == RA[SA[i - 1]];
			bool isRankPairSecondSame = (SA[i] + k >= n && SA[i - 1] + k >= n)
					|| ( SA[i] + k < n && SA[i - 1] + k < n &&  RA[SA[i] + k] == RA[SA[i - 1] + k] );
			tempRA[SA[i]] = ( isRankPairFirstSame && isRankPairSecondSame ) ? r : ++r;
		}
		// copy tempRA into RA to get the rank of new SA
		fr (i, 0, n) RA[i] = tempRA[i];		
	}
}
// computes longest common prefix length values
void computeLCP () {
	// setting previous suffix of SA[i] in sorted suffic array i.e SA[i - 1] in Phi[SA[i]]
	Phi[SA[0]] = -1;
	fr (i, 1, n) Phi[SA[i]] = SA[i - 1];
	// numbr of characters matched of a suffix with its previous suffix in SA
	int L = 0;
	// iterate on ith suffix
	fr (i, 0, n) {
		if (Phi[i] == -1) { PLCP[i] = 0; continue; }
		while (T[Phi[i] + L] == T[i + L]) L++;
		PLCP[i] = L;
		L = max (L - 1, 0);
	}
	fr (i, 0, n) LCP[i] = PLCP[SA[i]];
}
int  LCSubstring (int lenFirst) {
	int maxLCP = 0;
	fr (i, 0, n) {
		bool areInDiffStrings = (SA[i] < lenFirst && SA[i - 1] >= lenFirst) || (SA[i] > lenFirst && SA[i - 1] <= lenFirst) ;
		if (areInDiffStrings) maxLCP = max(maxLCP, LCP[i]);
		
	}
	if (maxLCP) fr (i, 0, n) {
		bool areInDiffStrings = (SA[i] < lenFirst && SA[i - 1] >= lenFirst) || (SA[i] > lenFirst && SA[i - 1] <= lenFirst) ;
		if (areInDiffStrings && LCP[i] == maxLCP) {
			int len = 0;
			fr (j, SA[i], SA[i] + maxLCP) ansStrings[len++] = T[j];
			ansStrings[len] = '\0';
			ans[ansStrings] = 1;
		}
	}
	return maxLCP;
}
LRS longestRepeatedSubstring () {
	int maxLCP = 0 , timesRepeated = 0, i = 0, startOfRepSubstr = 0;
	while (i < n) {
		if (LCP[i] > maxLCP) {
			int j = i + 1;
			while (LCP[j] == LCP[i] && j < n) j++;
			timesRepeated = j - i + 1;
			startOfRepSubstr = SA[i];
			maxLCP = LCP[i];
			i = j;
		} else i++;
	}
	LRS ans ;
	ans.len = maxLCP;
	ans.timesRepeated = timesRepeated;
	ans.startIndex = startOfRepSubstr;
	return ans;
}
int main () {
	int t;
	ipInt(t); 
	while (t--) {
		scanf("%s", T);
		n = strlen (T);
		constructSA();
		computeLCP();
		LRS ans = longestRepeatedSubstring();
		// fr (i, 0 ,n) {
		// 	printf("%2d \t %s (%d) \n",SA[i], T + SA[i] , LCP[i] );
		// }
		if (ans.len) {
			int st = ans.startIndex;
			fr (i, st, st + ans.len) printf("%c",T[i]);
			printf(" %d\n",ans.timesRepeated);
		} else {
			printf("No repetitions found!\n");
		}
	}
	return 0;
}