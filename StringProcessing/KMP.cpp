# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_N 3000000

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;


char T[MAX_N], P[MAX_N];
int b[MAX_N], n, m;
vi matchIndexes;

void kmpPreprocess () {
	b[0] = -1;
	int i = 0, j = -1;
	while (i < m) {
		while (j >= 0 && P[i] != P[j]) j = b[j];
		j++; i++;
		b[i] = j;
	}
}
// run kmpSearchbefore
void kmpSearch (int startSearchingFrom) {
	int i = startSearchingFrom, j = 0;
	while (i < n) {
		while (j >= 0 && T[i] != P[j]) j = b[j];
		j++; i++;
		if (j == m) {
			int matchedStartIndex = i - m;
			matchIndexes.push_back(matchedStartIndex);
			j = b[j];
		}
	}
}

void initialize () {
	matchIndexes.clear();
}
int main () {
	while (scanf("%s", P)) {
		initialize();
		if (P[0] == '.' && P[1] == '\0') break;
		m = strlen(P); n = m * 2;
		strcpy(T, P);
		strcat(T, P);
		kmpPreprocess();
		kmpSearch(1);
		int matchFirstIndex = matchIndexes[0];
		int ans = m / matchFirstIndex;
		printf("%d\n",ans);
	}
	return 0;
}