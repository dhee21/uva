# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <queue>
# include <map>
# include <bitset>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (lli i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define MAX_N 1010
//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
// typedefinitions

// suffixArray Vars
char T[MAX_N], str[MAX_N], P[MAX_N];
int n, m;
int SA[MAX_N], tempSA[MAX_N];
int RA[MAX_N], tempRA[MAX_N];
int freq[MAX_N];
// suffixArray Vars

void countingSort (int k) {
	int maxRank = max(300, n), sum = 0;
	memset(freq, 0, sizeof freq);
	// for each suffix SA[i] calculate its SA[i] + k th suffix rank and increment its freq .. i.e put it in the bucket
	fr (i, 0, n) {
		int bucketNum = SA[i] + k < n ? RA[SA[i] + k] : 0;
		freq[bucketNum]++;
	}
	fr (i, 0, maxRank + 1) {
		int t = freq[i]; freq[i] = sum; sum += t; 
	}
	// freq[bucketNum] now stores thre index at which the current suff should be in sorted order
	fr (i, 0, n) {
		int bucketNum = SA[i] + k < n ? RA[SA[i] + k] : 0;
		tempSA[freq[bucketNum]++] = SA[i];
	}
	fr (i, 0, n) SA[i] = tempSA[i];

}
// sort the suffix array
void radixSort (int k) {
	countingSort(k); countingSort(0);
}
void constructSA ( ) {
	fr (i, 0, n) SA[i] = i; 
	fr (i, 0, n) RA[i] = T[i] - '.' + 1;
	
	for (int k = 1; k < n; k <<= 1) {
		radixSort(k);
		int r;
		// calculate tempRA using previous ranks i.e RA
		tempRA[SA[0]] = r = 1;
		fr (i, 1, n) {
			bool isRankPairFirstSame = RA[SA[i]] == RA[SA[i - 1]];
			bool isRankPairSecondSame = (SA[i] + k >= n && SA[i - 1] + k >= n)
					|| ( SA[i] + k < n && SA[i - 1] + k < n &&  RA[SA[i] + k] == RA[SA[i - 1] + k] );
			tempRA[SA[i]] = ( isRankPairFirstSame && isRankPairSecondSame ) ? r : ++r;
		}
		// copy tempRA into RA to get the rank of new SA
		fr (i, 0, n) RA[i] = tempRA[i];		
	}
}
// returns starting and ending index of suffixes matching the input patter in m log n;
pii stringMatching () {
	int hi = n - 1, lo = 0;
	// findlower bound first
	while (lo < hi) {
		int mid = (hi + lo) / 2;
		if (strncmp(T + SA[mid], P,  m) >= 0) hi = mid;
		else lo = mid + 1;
	}
	if (strncmp(T + SA[lo], P, m) != 0) return pii(-1, -1);
	pii matchAns;
	matchAns.first = hi;
	// find upper bound
	lo = 0; hi = n - 1;
	while (lo < hi) {
		int mid = (hi + lo) / 2;
		if (strncmp(T + SA[mid], P, m) <= 0) lo = mid + 1;
		else hi = mid;
	}
	if (strncmp(T + SA[hi], P, m) != 0) hi--;
	matchAns.second = hi;
	return matchAns;
}

int main () {
	scanf("%s", T);
	n = strlen(T);
	constructSA();
	fr(i, 0, n) printf("%2d \t %s\n",SA[i], T + SA[i]);

	while (true) {
		scanf("%s", P);
		if (strlen(P) == 0) break;
		m = strlen(P);
		pii pos = stringMatching();
		if (pos.first != -1 && pos.second != -1) {
			fr (i, pos.first, pos.second + 1) {
				printf("index = %d , %s\n", SA[i], T + SA[i] );
			}
		} else {
			printf("%s not in %s\n",P, T);
		}
	}
	return 0;
}