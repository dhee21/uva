# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1
#define scoreDel -1
#define scoreIns -1
#define scoreSub -1
#define upOption 1
#define leftOption 2
#define diagOption 3

//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;

char A[90], B[90];
int V[90][90], arrowMatrix[90][90];

int score (char a, char b) {
	if (a == '_') return scoreIns;
	if (b == '_') return scoreDel;
	if (a == b) return 0;
	return scoreSub;
}
string alignA, alignB;

void getTheAlignment (int n, int m) {
	int i = n, j = m;
	int serialNum = 1;
	while (i && j) {
		if (arrowMatrix[i][j] == upOption) { alignA.push_back(A[i]); alignB.push_back('_');
			printf("%d Delete %d\n",serialNum++, i);
			i--;
		} else if (arrowMatrix[i][j] == leftOption) { alignA.push_back('_'); alignB.push_back(B[j]);
			printf("%d Insert %d,%c\n", serialNum++, i + 1, B[j]);
			j--;
		} else if (arrowMatrix[i][j] == diagOption) { alignA.push_back(A[i]); alignB.push_back(B[j]);
			if (A[i] != B[j]) printf("%d Replace %d,%c\n",serialNum++, i, B[j]);
			i--; j--; }
	}
	while (i) { alignA.push_back(A[i]); alignB.push_back('_');
		printf("%d Delete %d\n",serialNum++, i);
		i--;
	}
	while(j) {	
		printf("%d Insert %d,%c\n", serialNum++, 1, B[j]);
		alignA.push_back('_'); alignB.push_back(B[j]); j--;
	}
}

// global alignment
void maxAlignment (int n, int m) {
	// base cases
	V[0][0] = 0;
	fr (i, 1, n + 1) V[i][0] = i * scoreDel; fr (j, 0, m + 1) V[0][j] = j * scoreIns;
	// base cases

	fr (i, 1, n + 1) fr (j, 1, m + 1) {
		int option1 = V[i - 1][j] + score(A[i], '_');
		int option2 = V[i][j - 1] + score('_', B[j]);
		int option3 = V[i - 1][j - 1] + score(A[i], B[j]);
		int maxOption, maxOptionMove;
		if (option1 > option2) {
			maxOption = option1; maxOptionMove = upOption;
		} else {
			maxOption = option2; maxOptionMove = leftOption;
		}
		if (option3 > maxOption) {
			maxOption = option3; maxOptionMove = diagOption;
		}
		V[i][j] = maxOption; arrowMatrix[i][j] = maxOptionMove;
	}
}

void intitialize () {
	alignA.clear(); alignB.clear();
}
int main () {
	int caseNo = 0;
 	while (fgets(A + 1, 90, stdin) != NULL) {
 		if (caseNo++) printf("\n");
 		fgets(B + 1, 90, stdin);
 		A[strlen(A + 1)] = '\0';
 		if (B[strlen(B + 1)] == '\n') B[strlen(B + 1)] = '\0';
 		intitialize();
 		int n = strlen(A + 1), m = strlen(B + 1);
 		maxAlignment(n, m);
 		printf("%d\n",abs(V[n][m]));
 		getTheAlignment(n, m);
 	}
 	return 0;
 }