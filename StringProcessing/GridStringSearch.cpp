# include <iostream>
# include <stdio.h>
# include <string.h>
# include <string>
# include <utility>
# include <vector>
# include <algorithm>
# include <sstream>
# include <queue>
# include <map>
# include <bitset>
# include <set>
# include <math.h>
using namespace std;
#define fr(i, st, ed) for (int i = st ; i < ed; ++i )
#define ipInt(x) scanf("%d", &x)
#define inf (1<<30) - 1


//typedefinitions
typedef long long int lli;
typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<lli> vlli;
typedef vector<pii> vii;
typedef vector<vii> matrix;
typedef map<string, int> mSI;
typedef pair<int , pii> edge;
typedef vector<string> vs;
typedef pair<int , string> pis;
typedef vector<vi> vvi;


struct ansStruct {
	bool exists;
	pii coor;
};

char grid [110][110];
int m, n;

bool isOutside (int r, int c) {
	return r >= m || c >= n || r < 0 || c < 0 ;
}

bool areNotEqual(char a, char b) { return tolower(a) != tolower(b); }

ansStruct doesWordStartHere (int row, int col, char *word) {
	int len = strlen(word);
	ansStruct ans;
 	// 1
	bool existsRight = true;
	fr (i, 0, len) if (isOutside(row, col + i) ||  areNotEqual(grid[row][col + i] , word[i]) ) { existsRight = false; break; }
	if (existsRight) { ans.exists = true; ans.coor = pii (row, col + len - 1 ); return ans; }
 	// 2
	bool existsLeft = true;
	fr (i, 0, len) if (isOutside(row, col - i) ||  areNotEqual(grid[row][col - i] , word[i]) ) { existsLeft = false; break; }
	if (existsLeft) { ans.exists = true; ans.coor = pii (row, col - len + 1 ); return ans; }
 	// 3
	bool existsBelow = true;
	fr (i, 0, len) if (isOutside(row + i, col) ||  areNotEqual(grid[row + i][col] , word[i]) ) { existsBelow = false; break; }
	if (existsBelow) { ans.exists = true; ans.coor = pii (row + len - 1, col); return ans; }
 	// 4
	bool existsAbove = true;
	fr (i, 0, len) if (isOutside(row - i, col) ||  areNotEqual(grid[row - i][col] , word[i]) ) { existsAbove = false; break; }
	if (existsAbove) { ans.exists = true; ans.coor = pii (row - len + 1, col); return ans; }
 	// 5
	bool existsDiagRightDown = true;
	fr (i, 0, len) if (isOutside(row + i, col + i) ||  areNotEqual(grid[row + i][col + i] , word[i]) ) { existsDiagRightDown = false; break; }
	if (existsDiagRightDown) { ans.exists = true; ans.coor = pii (row + len - 1, col + len - 1); return ans; }
 	// 6
	bool existsDiagLeftDown = true;
	fr (i, 0, len) if (isOutside(row + i, col - i) ||  areNotEqual(grid[row + i][col - i] , word[i]) ) { existsDiagLeftDown = false; break; }
	if (existsDiagLeftDown) { ans.exists = true; ans.coor = pii (row + len - 1, col - len + 1); return ans; }
 	// 7
	bool existsDiagRightUp = true;
	fr (i, 0, len) if (isOutside(row - i, col + i) ||  areNotEqual( grid[row - i][col + i] , word[i]) ) { existsDiagRightUp = false; break; }
	if (existsDiagRightUp) { ans.exists = true; ans.coor = pii (row - len + 1, col + len - 1); return ans; }
 	// 8
	bool existsDiagLeftUp = true;
	fr (i, 0, len) if (isOutside(row - i, col - i) ||  areNotEqual( grid[row - i][col - i] , word[i]) ) { existsDiagLeftUp = false; break; }
	if (existsDiagLeftUp) { ans.exists = true; ans.coor = pii (row - len + 1, col - len + 1); return ans; }

	ans.exists = false;
	return ans;
}

int main () {
	int t, caseNo = 0;
	ipInt(t);
	while (t--) {
		if (caseNo++) printf("\n");
		ipInt(m); ipInt(n);
		fr (i, 0, m) scanf("%s", grid[i]);
		char word[110];
		int k;
		ipInt(k);
		while (k--) {
			scanf("%s", word);
			bool ansFound = false;
			fr (i, 0, m) {
				fr (j, 0, n) {
					ansStruct ans = doesWordStartHere(i, j, word);
					if (ans.exists) {
						printf("%d %d\n",i + 1, j + 1);
						ansFound = true; break;
					}
				}
				if (ansFound) break; 
			}
		}
	}
	
	return 0;
}